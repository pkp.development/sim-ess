<?php

namespace App\Console\Commands;

use App\Models\Accounts\Admin;
use Illuminate\Console\Command;
use App\Models\Config\Notification;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminNotification as AdminNotif;

class AdminNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Admin task notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admins = Admin::with('account', 'permissions', 'role_company', 'role_simbranch')->whereHas('account', function($q) {
            $q->where('role', 'admin');
        })->get();

        foreach($admins as $admin) {
            $permissions = [];

            foreach ($admin->permissions->where('permission_category_id', 3)->pluck('key') as $notifPermission) {
                $permissions[] = str_replace('notif-', '', $notifPermission);
            }

            $counts_notif  = Notification::with('account.profile', 'account.assignment')->whereHas('account.assignment', function($query) use ($admin, $permissions) {
                $query->whereIn('client_company_id', $admin->role_company->pluck('value'))->whereIn('sim_branch_id', $admin->role_simbranch->pluck('value'));
            })->where('status', 0)->whereIn('type', $permissions)->count();

           // Mail::to($admin->account->email)->send(new AdminNotif($admin->name, $counts_notif));
        }
    }
}
