<?php

namespace App\Console;

use App\Libraries\Facades\EssService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // $schedule->command('get:credentials')->everyMinute();
        if (env('SCHEDULER_ACTIVE', false) == true) {
            $schedule->command('get:credentials')->dailyAt('23:30');
            $schedule->command('get:credentials')->dailyAt('00:28');
            $schedule->command('admin:notification')->dailyAt('18:00');
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
