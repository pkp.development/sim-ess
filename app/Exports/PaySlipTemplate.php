<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use App\Models\SlipGaji\PaySlipAccount;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class PaySlipTemplate implements FromCollection, WithHeadings
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $payslip = PaySlipAccount::get();

        $heading[] = '11111';

        foreach($payslip as $account) {
            $heading[] = number_format(10000000);
        }

        return new Collection([$heading]);
    }

    public function headings(): array
    {
        $payslip = PaySlipAccount::get();

        $heading[] = 'simid';

        foreach($payslip as $account) {
            $heading[] = $account->code;
        }

        return $heading;
    }
}
