<?php

namespace App\Exports\Report;

use App\Models\Temporary\Contract;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ContractSignature implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $startDate, $endDate;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $contracts = Contract::with('account', 'verifiedBy.admin')
            ->whereDate('created_at', '>=', $this->startDate)
            ->whereDate('created_at', '<=', $this->endDate)
            ->get();
        $data = [];
        $no = 1;

        foreach ($contracts as $contract) {
            $data[] = [
                $no,
                $contract->account->sim_id,
                ($contract->verifiedBy) ? $contract->verifiedBy->admin->name : null,
                $contract->contract_number,
                $contract->status,
                $contract->description
            ];

            $no++;
            gc_collect_cycles();
        }

        return $data;
    }

    public function headings(): array
    {
        return [
            '#',
            'SIMID',
            'Verified By',
            'Contract Number',
            'Status',
            'Description',
        ];
    }
}
