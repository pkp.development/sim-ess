<?php

namespace App\Exports\Report;

use App\Models\Temporary\Profile;
use App\Repositories\SipasApi\GetRegion;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EmployeeData implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $startDate, $endDate;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $contracts = Profile::with('account', 'handledBy.admin')
            ->whereDate('created_at', '>=', $this->startDate)
            ->whereDate('created_at', '<=', $this->endDate)
            ->where('type', 'profile')
            ->get();

       // $repoRegion = new GetRegion;

        $data = [];
        $no = 1;

        foreach ($contracts as $contract) {
            $content = json_decode($contract->content);

            $historyStatus = 'Pending';
            if ($contract->status == 1) {
                $historyStatus = 'Verified';
            }
            elseif ($contract->status == 2) {
                $historyStatus = 'Refused';
            }

            $data[] = [
                $no,
                $contract->account->sim_id,
                ($contract->handledBy) ? $contract->handledBy->admin->name : null,
                $historyStatus,
                $contract->description,
                $content->name,
                $content->place_of_birth,
                $content->date_of_birth,
                $content->gender,
                $content->nik,
                $content->identity_image,
                $content->npwp_number,
                (@$content->npwp_periode) ? $content->npwp_periode : null,
                $content->npwp_image,
                $content->address_identity,
                //$repoRegion->getProvinceData($content->province_identity)['province_name'],
                //$repoRegion->getCityData($content->city_identity)['city_name'],
                0,
                0,
                $content->zip_code_identity,
                $content->marital_status,
                $content->religion,
                $content->recent_education,
                $content->phone_number,
                $content->secondary_phone,
                $content->mother_name,
                $content->email,
                $content->domicile_address,
                //$repoRegion->getProvinceData($content->domicile_province)['province_name'],
               // $repoRegion->getCityData($content->domicile_city)['city_name'],
                0,
                0,
                $content->domicile_zip_code,
            ];

            $no++;
            gc_collect_cycles();
        }

        return $data;
    }

    public function headings(): array
    {
        return [
            '#',
            'SIMID',
            'Verified By',
            'Status',
            'Description',
            "name",
            "place of birth",
            "date of birth",
            "gender",
            "nik",
            "identity image",
            "npwp number",
            "npwp periode",
            "npwp image",
            "address identity",
            "province identity",
            "city identity",
            "zip code identity",
            "marital status",
            "religion",
            "recent education",
            "phone number",
            "secondary phone",
            "mother name",
            "email",
            "domicile address",
            "domicile province",
            "domicile city",
            "domicile zip code",
        ];
    }
}
