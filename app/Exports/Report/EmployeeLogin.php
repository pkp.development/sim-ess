<?php

namespace App\Exports\Report;

use App\Models\Config\AuditTrail;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EmployeeLogin implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $startDate, $endDate;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $empLogin = AuditTrail::where('title', 'Employee Login')->whereDate('created_at', '>=', $this->startDate)->whereDate('created_at', '<=', $this->endDate)->get();
        $data = [];
        $no = 1;

        foreach($empLogin as $history) {
            $simid = explode(' : ', $history->description);
            $data[] = [
                $no,
                $simid[1],
                $history->created_at
            ];

            $no++;
            gc_collect_cycles();
        }

        return $data;
    }

    public function headings(): array
    {
        return [
            '#',
            'SIMID',
            'DateTime'
        ];
    }
}
