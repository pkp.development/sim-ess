<?php

namespace App\Exports\Report;

use App\Models\Claim\Outpatient;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EmployeeOutpatient implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $startDate, $endDate;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $outpatients = Outpatient::with('account', 'verifiedBy.admin')
            ->whereDate('created_at', '>=', $this->startDate)
            ->whereDate('created_at', '<=', $this->endDate)
            ->get();
        $data = [];
        $no = 1;

        foreach ($outpatients as $outpatient) {

            $historyStatus = 'Pending';
            if ($outpatient->status == 1) {
                $historyStatus = 'Verified';
            }
            elseif ($outpatient->status == 2) {
                $historyStatus = 'Refused';
            }

            $data[] = [
                $no,
                $outpatient->account->sim_id,
                ($outpatient->verifiedBy) ? $outpatient->verifiedBy->admin->name : null,
                $outpatient->doctor_name,
                $outpatient->patient_name,
                $outpatient->nominal,
                $outpatient->description,
                $outpatient->receipt_file,
                $outpatient->doctor_prescription_file,
                $outpatient->rejection_reason,
                $historyStatus,
                ($outpatient->receipt_date) ? $outpatient->receipt_date->format('d F Y') : null,
                ($outpatient->approved_date) ? $outpatient->approved_date->format('d F Y') : null,
                $outpatient->created_at,
            ];

            $no++;
            gc_collect_cycles();
        }

        return $data;
    }

    public function headings(): array
    {
        return [
            '#',
            'SIMID',
            'Handled By',
            'Doctor Name',
            'Patient Name',
            'Nominal',
            'Description',
            'Receipt File',
            'Doctor Prescription',
            'Rejection Reason',
            'Status',
            'Receipt Date',
            'Approved Date',
            'Created Date'
        ];
    }
}
