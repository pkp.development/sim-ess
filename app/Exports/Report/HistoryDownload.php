<?php

namespace App\Exports\Report;

use Carbon\Carbon;
use App\Models\Tasks\LasDownload;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class HistoryDownload implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $startDate, $endDate, $type;

    public function __construct($startDate, $endDate, $type)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
        $this->type      = $type;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $histories = LasDownload::with('account')->where('type', $this->type)
            ->whereDate('created_at', '>=', $this->startDate)
            ->whereDate('created_at', '<=', $this->endDate)
            ->get();
        $data = [];
        $no = 1;

        foreach ($histories as $history) {
            $data[] = [
                $no,
                $history->account->sim_id,
                ($history->month) ? Carbon::createFromFormat('n', $history->month)->format('F') : null,
                $history->year,
                $history->created_at
            ];

            $no++;
            gc_collect_cycles();
        }

        return $data;
    }

    public function headings(): array
    {
        return [
            '#',
            'SIMID',
            'Month',
            'Year',
            'Download Date'
        ];
    }
}
