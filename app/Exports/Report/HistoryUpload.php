<?php

namespace App\Exports\Report;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\SlipGaji\HistoryDetail as HistoryPayslip;
use App\Models\Spt\HistoryDetail as HistorySpt;
use App\Models\Tasks\Benefit;

class HistoryUpload implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $startDate, $endDate, $type;

    public function __construct($startDate, $endDate, $type)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
        $this->type      = $type;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $histories = [];
        $no        = 1;

        if ($this->type == 'payslip') {
            $datas = HistoryPayslip::with('history.uploadedby.admin')
                ->whereDate('created_at', '>=', $this->startDate)
                ->whereDate('created_at', '<=', $this->endDate)
                ->get();

            foreach ($datas as $data) {
                $histories[] = [
                    $no,
                    $data->history->uploadedby->admin->name,
                    $data->sim_id,
                    $data->status,
                    ($data->description) ? $data->description : "-",
                    $data->created_at
                ];

                $no++;
                gc_collect_cycles();
            }
        }
        if ($this->type == 'SPT') {
            $datas = HistorySpt::with('history.uploadedby.admin')
                ->whereDate('created_at', '>=', $this->startDate)
                ->whereDate('created_at', '<=', $this->endDate)
                ->get();

            foreach ($datas as $data) {
                $histories[] = [
                    $no,
                    $data->history->uploadedby->admin->name,
                    $data->sim_id,
                    $data->status,
                    ($data->description) ? $data->description : "-",
                    $data->created_at
                ];

                $no++;
                gc_collect_cycles();
            }
        }
        if ($this->type == 'id_card') {
            $datas = Benefit::with('account.admin', 'details')->where('type', 'id_card')
                ->whereDate('created_at', '>=', $this->startDate)
                ->whereDate('created_at', '<=', $this->endDate)
                ->get();

            foreach ($datas as $data) {
                foreach ($data->details as $detail) {
                    $histories[] = [
                        $no,
                        $data->account->admin->name,
                        $detail->simid,
                        $detail->status,
                        $detail->abbreviation,
                        $detail->description,
                        $detail->created_at
                    ];

                    $no++;
                    gc_collect_cycles();
                 }
            }
        }
        if ($this->type == 'bpjs_tkj') {
            $datas = Benefit::with('account.admin', 'details')->where('type', 'bpjs_ketenagakerjaan')
                ->whereDate('created_at', '>=', $this->startDate)
                ->whereDate('created_at', '<=', $this->endDate)
                ->get();

            foreach ($datas as $data) {
                foreach ($data->details as $detail) {
                    $histories[] = [
                        $no,
                        $data->account->admin->name,
                        $detail->simid,
                        $detail->status,
                        $detail->abbreviation,
                        $detail->description,
                        $detail->created_at
                    ];

                    $no++;
                    gc_collect_cycles();
                 }
            }
        }
        if ($this->type == 'bpjs_kes') {
            $datas = Benefit::with('account.admin', 'details')->where('type', 'bpjs_kesehatan')
                ->whereDate('created_at', '>=', $this->startDate)
                ->whereDate('created_at', '<=', $this->endDate)
                ->get();

            foreach ($datas as $data) {
                foreach ($data->details as $detail) {
                    $histories[] = [
                        $no,
                        $data->account->admin->name,
                        $detail->simid,
                        $detail->status,
                        $detail->abbreviation,
                        $detail->description,
                        $detail->created_at
                    ];

                    $no++;
                    gc_collect_cycles();
                 }
            }
        }

        return $histories;
    }

    public function headings(): array
    {
        if ($this->type == 'payslip' || $this->type == 'SPT') {
            return [
                '#',
                'Uploaded By',
                'SIMID',
                'Status',
                'Description',
                'Uploaded Date'
            ];
        }
        if ($this->type == 'id_card' || $this->type == 'bpjs_tkj' || $this->type == 'bpjs_kes') {
            return [
                '#',
                'Uploaded By',
                'SIMID',
                'Status',
                'Abbreviation',
                'Description',
                'Uploaded Date'
            ];
        }
    }
}
