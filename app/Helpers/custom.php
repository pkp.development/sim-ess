<?php

namespace App\Http\Controllers\Admin;

if (! function_exists('validation_response')) {
    function reconcile_data_master_bpr($mst_data) {
        $bpr_ref = array();
        foreach($mst_data as $new_data){
            foreach($new_data as $key2 => $new_data2){
                $bpr_ref[$new_data->custom_category][$key2] = $new_data2;
            }
        }
        return $bpr_ref;
    }
}
