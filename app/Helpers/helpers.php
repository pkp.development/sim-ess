<?php

if (! function_exists('menu')) {

    /**
     * Generate HTML Menu
     * @param  string $segment segment for active
     * @param  string $title   Title of Menu
     * @param  string $uri     URL of Menu
     * @param  string $icon    Icon of Menu default ti-line-double
     * @return string          HTML Menu
     */
    function menu(string $segment, string $title, string $uri, string $icon = 'fe-folder', $can = null)
    {
        $active = "";
        if (request()->segment(2) == 'admin') {
            if (request()->segment(3) == $segment) {
                $active = "active";
            }
        }
        elseif (request()->segment(2) == $segment) {
            $active = "active";
        }

        $html = '<li class="'. $active .'">';
        $html .= '<a href="'. $uri .'" class="'. $active .'">';
        $html .= '<i class="'. $icon .'"></i>';
        $html .= '<span> '. $title .' </span>';
        $html .= '</a>';
        $html .= '</li>';

        if ($can) {
            if (\Illuminate\Support\Facades\Gate::allows($can)) {
                return $html;
            }
        }
        else {
            return $html;
        }
    }
}

if (! function_exists('menu_with_sub')) {

    /**
     * Generate menu with submenu
     * @param  string     $title   Title of Menu
     * @param  string     $icon    Icon of Menu
     * @param  array|null $submenu Submenu for Menu Parent
     * @return string              HTML Menu with Submenu
     */
    function menu_with_sub(string $title, string $icon = 'fe-folder', array $submenus = null)
    {
        $sub_menu = null;
        $active   = null;

        if($submenus !== null)
        {
            foreach($submenus as $submenu)
            {
                $active_sub = null;
                if(request()->url() == $submenu['uri'])
                {
                    $active     = "active";
                    $active_sub = "active";
                }

                if (array_key_exists('can', $submenu)) {
                    if (\Illuminate\Support\Facades\Gate::allows($submenu['can'])) {
                        $sub_menu .= '<li class="'. $active_sub .'"><a href="'. $submenu['uri'] .'">'. $submenu['title'] .'</a></li>';
                    }
                }
                else {
                    $sub_menu .= '<li class="'. $active_sub .'"><a href="'. $submenu['uri'] .'">'. $submenu['title'] .'</a></li>';
                }
            }
        }

        $html = '<li>';
        $html .= '<a class="'. $active .'" href="javascript: void(0);">';
        $html .= '<i class="'. $icon .'"></i>';
        $html .= '<span> '. $title .' </span>';
        $html .= '<span class="menu-arrow"></span>';
        $html .= '</a>';
        $html .= '<ul class="nav-second-level" aria-expanded="false">';
        $html .= $sub_menu;
        $html .= '</ul>';
        $html .= '</li>';

        if ($sub_menu) {
            return $html;
        }
    }
}

if (! function_exists('page_title')) {

	/**
	 * Generate Page Title HTML
	 * @param  string   $title  This active page title
	 * @return string           Html for Page Title
	 */
    function page_title(string $title = 'Page Title')
    {
        return '<h3 class="page-title mt-3 ml-4">'. $title .'</h3>';
    }
}

if (! function_exists('action_btn')) {

    /**
     * Action Button Helpers
     *
     * @param String $url
     * @param Array $classes
     * @param String $icon
     * @return HTML
     */
    function action_btn($url, $classes, $icon, $title = null, $datas = [])
    {
        $html_class = null;
        foreach ($classes as $class) {
            $html_class .= $class . ' ';
        }

        $dataAttr = null;
        foreach ($datas as $data) {
            $dataAttr .= 'data-' . $data['key'] . "='" . $data['value'] . "' ";
        }

        $html = '<a href="'. $url .'" class="action-icon '. $html_class .'" '. $dataAttr .' title="'. $title .'" data-plugin="tippy" data-tippy-placement="top"><i class="'. $icon .'"></i> '. $title .'</a>';

        return $html;
    }
}

if (! function_exists('sipas_validation')) {

    /**
     * SIPAS VALIDATION
     *
     * @param String $data
     * @return String
     */
    function sipas_validation($data)
    {
        return strtoupper(str_replace("'", "", $data));
    }
}

if (! function_exists('validation_response')) {
    function validation_response($req, array $validation, array $messages = [], array $attributes = []) {
        $validator = \Illuminate\Support\Facades\Validator::make($req->all(), $validation, $messages, $attributes);

        if ($validator->fails()) {
            $errors = [];
            foreach($validator->getMessageBag()->toArray() as $key => $value)
            {
                $result = current($value);
            }

            return $result;
        }
        else {
            return false;
        }
    }
}


