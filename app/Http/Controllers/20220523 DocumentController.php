<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use App\Repositories\DocumentRepository;
use GuzzleHttp\Exception\ClientException;
use App\Repositories\AssignmentRepository;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Session;

class DocumentController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new DocumentRepository;
    }

    public function index()
    {
        try {
            if (auth()->user()->assignment->signed_at) {
                return redirect()->route('pe.dashboard');
            }

            $repo = new AssignmentRepository;
            $assignment = $repo->getActiveContract();

            if (Storage::disk('public')->exists('public/tmp-contract/' . auth()->user()->sim_id . '.pdf')) {
                Storage::disk('public')->delete('public/tmp-contract/' . auth()->user()->sim_id . '.pdf');
            }

            $token     = new GetToken;
            $result    = $token->getToken();
            $tokenAuth = $result['results']->access_token;

            // if (!file_exists(storage_path('app/public/tmp-contract/' . auth()->user()->sim_id . '.pdf'))) {
                $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/getDownloadContract', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $tokenAuth
                    ],
                    'query' => ['id' => $assignment->contract_id],
                    'sink' => storage_path('app/public/tmp-contract/' . auth()->user()->sim_id . '.pdf')
                ]);
            // }
        }
        catch (ClientException $e) {
            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return view('document');
    }

    public function signContract(Request $req)
    {
        $result = $this->repository->signContract($req);

        if (is_array($result)) {
            Session::flash('toast-error', 'Harap cek kembali kontrak anda yang harus ditanda tangani');
            return redirect()->route('pe.dashboard');
        }

        return redirect()->route('pe.dashboard');
    }
}
