<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Admin\BankRepository;
use App\Repositories\Admin\FamilyRepository;
use App\Repositories\Admin\EmployeeRepository;
use JsValidator;

class EmployeeController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new EmployeeRepository;
    }

    public function index(Request $req)
    {
        if($req->ajax()) {
            return $this->repository->datatables();
        }

        $validator = JsValidator::make([
            'password' => 'min:6|required|confirmed'
        ]);

        return view('pages.admin.employee.index', compact('validator'));
    }

    public function changePassword(Request $req, $uuid)
    {
        $this->validate($req, [
            'password' => 'min:6|required|confirmed'
        ]);

        $result = $this->repository->changePassword($req, $uuid);

        if (!$result) {
            Session::flash('toast-error', 'Perubahan password employee gagal');
            return redirect()->route('admin.employee.index');
        }

        Session::flash('toast-success', 'Perubahan password employee berhasil');
        return redirect()->route('admin.employee.index');
    }

    public function detail(Request $req, $uuid, $tabs = 'profile')
    {
        if ($tabs == 'profile') {
            $data = $this->repository->getData($uuid);
            if ($req->ajax()) {
                return $this->repository->datatablesTmpProfile($data->account->id);
            }
        }
        // dd($data);

        return view('pages.admin.employee.detail', compact('data', 'tabs'));
    }

    public function updateBiodataView($tmpProfileId)
    {
        $profile = $this->repository->getPendingProfile($tmpProfileId);

        if (!$profile) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }

        $dataOld = $this->repository->getProfile($profile->account_id, 'account_id');
        $content = json_decode($profile->content);

        return view('pages.admin.employee.update-profile', compact('content', 'profile', 'dataOld'));
    }

    public function getTmpData($uuid)
    {
        $profile = $this->repository->getPendingProfile($uuid, 'all');

        if (!$profile) {
            return response()->json(null, 404);
        }

        $content = $this->repository->extractRegionContent(json_decode($profile->content));
        $content->date_of_birth_format = Carbon::createFromFormat('Y-m-d', $content->date_of_birth)->format('d F Y');

        return response()->json($content);
    }

    public function getImage($type, $uuid, $temporary = true)
    {
        if($temporary === true) {
            if($type == 'identity' || $type == 'npwp' || $type == 'profile' || $type == 'buku-tabungan' || $type == 'selfie') {
                $tmpProfile = $this->repository->getPendingProfile($uuid);
            }
            if($type == 'family-card') {
                $familyRepo = new FamilyRepository;
                $tmpProfile = $familyRepo->getPendingFamily($uuid);
            }

            $profile = json_decode($tmpProfile->content);
        }
        else {
            if($type == 'identity' || $type == 'npwp' || $type == 'profile' || $type == 'family-card' || $type == 'selfie') {
                $profile = $this->repository->getProfile($uuid, 'uuid');
            }
            if($type == 'buku-tabungan') {
                $bankRepo   = new BankRepository;
                $profile    = $bankRepo->getBankAccount($uuid);
            }
        }

        if($type == 'identity') {
            $path = Storage::get($profile->identity_image);
        }
        if($type == 'npwp') {
            $path = Storage::get($profile->npwp_image);
        }
        if($type == 'profile') {
            $path = Storage::get($profile->profile_picture);
        }
        if($type == 'family-card') {
            $path = Storage::get($profile->family_card_image);
        }
        if($type == 'buku-tabungan') {
            $path = Storage::get($profile->bank_account_image);
        }
        if($type == 'selfie') {
            $path = Storage::get($profile->selfie_image);
        }


        return Image::make($path)->response();
    }

    public function updateBiodataPost(Request $req, $tmpProfileId)
    {
        $result = $this->repository->updateBiodataProfile($req, $tmpProfileId);

        return redirect()->route('admin.employee.index');
    }
}
