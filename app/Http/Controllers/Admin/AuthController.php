<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login()
    {
        $validator = JsValidator::make([
            'username'   => 'required|exists:accounts,username',
            'password' => 'required'
        ],[],[
            'username'   => 'Username',
            'password' => 'Kata Sandi'
        ]);

        return view('pages.admin.login', compact('validator'));
    }

    public function auth(Request $req)
    {
        $this->validate($req, [
            'username'   => 'required|exists:accounts,username',
            'password' => 'required'
        ]);

        $remember = false;
        if($req->has('remember-me')) {
            $remember = true;
        }

        if(Auth::attempt(['username' => $req->username, 'password' => $req->password], $remember)) {
            if(auth()->user()->role == 'super-admin' || auth()->user()->role == 'admin') {
                return redirect()->intended('admin/dashboard');
            }

            return redirect()->intended('dashboard');
        }

        Session::flash('toast-error', 'Password anda salah atau akun anda sedang tidak aktif');
        return redirect()->route('admin.login');
    }

    public function logout(Request $req)
    {
        auth()->logout();

        Session::flash('toast-success', 'Anda berhasil keluar dari aplikasi');

        return redirect()->route('admin.login');
    }
}
