<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\BprGoApi\RestBprGo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Repositories\Admin\BankRepository;

class BankController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new BankRepository;
    }

    public function updateRekeningView($uuid)
    {
        $profile = $this->repository->getPendingRekening($uuid);

        if (!$profile) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }

        $dataOld = $this->repository->getBankAccount($profile->account->id);
        $content = json_decode($profile->content);

        $masterApi = new RestBprGo();
        $endpoint = env('MASTER_MULTIPLE_DATA');
        $old_datas = $masterApi->bprApi(
            $endpoint,
            'GET',
            ['masters'=> [
                'bank' => $dataOld->name_of_bank]
            ]
        );

        if(!$old_datas->data){
            $bpr_ref_old['bank']['nama_data'] = '';
		}else{
            $bpr_ref_old = reconcile_data_master_bpr($old_datas->data);
        }
        $new_datas = $masterApi->bprApi(
            $endpoint,
            'GET',
            ['masters'=> [
                'bank' => $content->name_of_bank]
            ]
        );
        if(!$new_datas->data){
            $bpr_ref_new['bank']['nama_data'] = '';
		}else{
            $bpr_ref_new = reconcile_data_master_bpr($new_datas->data);
        }

        return view('pages.admin.bank.update-profile', compact('content', 'profile', 'dataOld', 'bpr_ref_new', 'bpr_ref_old'));
    }

    public function updateRekeningPost(Request $req, $uuid)
    {
        $result = $this->repository->updateRekening($req, $uuid);

        return redirect()->route('admin.employee.index');
    }
}
