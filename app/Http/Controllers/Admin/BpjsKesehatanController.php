<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Admin\ClaimBenefitRepository;
use Illuminate\Support\Facades\Session;

class BpjsKesehatanController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new ClaimBenefitRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatablesKesehatan();
        }

        $validator = JsValidator::make([
            'bpjs_kesehatan_csv' => 'required|mimes:csv,txt',
            'bpjs_kesehatan_zip' => 'required|mimes:zip'
        ], [], [
            'bpjs_kesehatan_csv' => 'No. BPJS Kesehatan',
            'bpjs_kesehatan_zip' => 'Kartu BPJS Kesehatan'
        ], '#form-bpjs-kes');

        return view('pages.admin.bpjs_kesehatan.index', compact('validator'));
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->repository->detailKesehatan($uuid);
        }

        return view('pages.admin.bpjs_kesehatan.detail', compact('uuid'));
    }

    public function upload(Request $req)
    {
        $this->validate($req, [
            'bpjs_kesehatan_csv' => 'required|mimes:csv,txt',
            'bpjs_kesehatan_zip' => 'required|mimes:zip'
        ]);

        $result = $this->repository->bpjsKesehatanUpload($req);
        Session::flash('toast-success', 'BPJS Kesehatan berhasil diupload');

        return redirect()->route('admin.bpjs-kes.index');
    }

    public function downloadTemplate()
    {
        return Storage::download('bpjs_kes_template.xlsx');
    }
}
