<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Admin\ClaimBenefitRepository;

class BpjsKetenagakerjaanController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new ClaimBenefitRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatablesKetenagakerjaan();
        }

        $validator = JsValidator::make([
            'bpjs_ketenagakerjaan_csv' => 'required|mimes:csv,txt',
            'bpjs_ketenagakerjaan_zip' => 'required|mimes:zip'
        ], [], [
            'bpjs_ketenagakerjaan_csv' => 'No. BPJS Ketenagakerjaan',
            'bpjs_ketenagakerjaan_zip' => 'Kartu BPJS Ketenagakerjaan'
        ], '#form-bpjs-tkj');

        return view('pages.admin.bpjs_ketenagakerjaan.index', compact('validator'));
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->repository->detailKetenagakerjaan($uuid);
        }

        return view('pages.admin.bpjs_ketenagakerjaan.detail', compact('uuid'));
    }

    public function upload(Request $req)
    {
        $this->validate($req, [
            'bpjs_ketenagakerjaan_csv' => 'required|mimes:csv,txt',
            'bpjs_ketenagakerjaan_zip' => 'required|mimes:zip'
        ]);

        $result = $this->repository->bpjsKetenagakerjaanUpload($req);
        Session::flash('toast-success', 'BPJS ketenagakerjaan berhasil diupload');

        return redirect()->route('admin.bpjs-tkj.index');
    }

    public function downloadTemplate()
    {
        return Storage::download('bpjs_tkj_template.xlsx');
    }
}
