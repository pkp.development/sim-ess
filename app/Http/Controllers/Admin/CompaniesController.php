<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Admin\CompaniesRepository;

class CompaniesController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new CompaniesRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatables();
        }

        $validator = JsValidator::make([
            'name'             => 'required|max:100',
            'abbreviation'     => 'required|max:5',
            'npwp'             => 'required|max:25',
            'coordinator'      => 'required|max:50',
            'coordinator_npwp' => 'required|max:25',
            'logo'             => 'image|max:4096',
            'coordinator_sign' => 'image|max:4096'
        ], [], [
            'name'             => 'Nama Perusahaan',
            'abbreviation'     => 'Abbreviation',
            'coordinator'      => 'Nama Direktur',
            'logo'             => 'Logo Perusahaan',
            'npwp'             => 'NPWP Perusahaan',
            'coordinator_npwp' => 'NPWP Direktur',
            'coordinator_sign' => 'Tanda tangan Direktur'

        ], '#form-edit-company');

        return view('pages.admin.company.index', compact('validator'));
    }

    public function sync()
    {
        $this->repository->sync();

        Session::flash('toast-success', 'Companies berhasil disinkronisasi');
        return redirect()->route('admin.company.index');
    }

    public function update(Request $req, $uuid)
    {
        $result = $this->repository->update($uuid, $req);

        if ($result) {
            Session::flash('toast-success', 'Perusahaan berhasil diubah');
        }
        else {
            Session::flash('toast-error', 'Perusahaan tidak ditemukan');
        }

        return redirect()->route('admin.company.index');
    }

    public function getData($uuid)
    {
        $result = $this->repository->getData($uuid);

        if($result) {
            $arr = [
                'uuid'             => $result->uuid,
                'name'             => $result->name,
                'abbreviation'     => $result->abbreviation,
                'logo'             => route('admin.company.getImage', [$result->uuid, 'logo']),
                'npwp'             => $result->npwp,
                'coordinator'      => $result->coordinator,
                'coordinator_npwp' => $result->coordinator_npwp,
                'coordinator_sign' => route('admin.company.getImage', [$result->uuid, 'sign']),
                'sync_at'          => $result->sync_at,
                'edit_url'         => route('admin.company.update', $result->uuid)

            ];

            return response()->json($arr);
        }

        return response()->json(null, 404);
    }

    public function getImage($uuid, $type)
    {
        $result = $this->repository->getData($uuid);

        if($type == 'logo') {
            $path = Storage::get($result->logo);
        }
        if($type == 'sign') {
            $path = Storage::get($result->coordinator_sign);
        }

        return Image::make($path)->response();
    }
}
