<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Config\Setting;
use App\Models\Config\SettingCredentials;
use App\Repositories\SipasApi\GetClientOrganization;
use App\Repositories\SipasApi\GetEmployeeCredentials;
use App\Repositories\SipasApi\GetSimOrganization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ConfigurationController extends Controller
{
    public function index(Request $req)
    {
        $settings = Setting::get();
        $branch   = null;
        $company  = null;

        return view('pages.admin.configuration', compact('settings', 'branch', 'company'));
    }

    public function store(Request $req)
    {
        DB::beginTransaction();
        try {
            if ($req->code == 'live_date') {
                $setting = Setting::where('code', 'live_date')->first();
                $date = Carbon::createFromFormat('d M Y', $req->live_date);
                $setting->value = $date->format('Y-m-d');
                $setting->save();
            }
            elseif ($req->code == 'cut_off') {
                $setting        = Setting::where('code', 'cut_off_periode')->first();
                $setting->value = $req->cut_off;
                $setting->save();
            }
            elseif ($req->code == 'user_limit') {
                $setting        = Setting::where('code', 'limit_credentials')->first();
                $setting->value = $req->user_limit;
                $setting->save();
            }
            elseif ($req->code == 'branch') {
                $setting        = Setting::where('code', 'credentials_branch')->first();
                $setting->value = $req->branch;
                $setting->save();
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Session::flash('toast-success', 'Configuration has saved');
        return redirect()->route('admin.configuration.index');
    }

    public function settingCredentials()
    {
        $settings = SettingCredentials::get();
        $include = [];
        $exclude = [];

        foreach ($settings as $setting) {
            if ($setting->type == 'include') {
                $include[$setting->organization][] = $setting->value;
            }
            elseif ($setting->type == 'exclude') {
                $exclude[$setting->organization][] = $setting->value;
            }
        }

        $apiCred     = new GetEmployeeCredentials;
        $filters     = [
            "limit" => 15,
            "exclude" => $exclude
        ];
        $apiCred     = $apiCred->getEmployeeCredentialsTest(array_merge($filters, $include));
        $credentials = $apiCred->data;
        $totalCreds  = $apiCred->total;

        return view('pages.admin.credentials', compact('settings', 'credentials', 'totalCreds'));
    }

    public function settingCredentialsStore(Request $req)
    {
        DB::beginTransaction();
        try {
            if ($req->type == 'include') {
                $organization = 'simbranch';
                $value        = $req->sim_branch;
            }
            elseif ($req->type == 'exclude') {
                $organization = 'company';
                $value        = $req->client_company;
            }

            $simApi   = new GetSimOrganization;
            $clienApi = new GetClientOrganization;
            $company  = null;
            $branch   = null;

            switch ($organization) {
                case 'simcompany':
                    $company = $simApi->getCompany(1, ["id" => $req->sim_company]);
                    $company = $company['results'][0]->companyname;
                    break;

                case 'simbranch':
                    $branch = $simApi->getBranch(1, ["id" => $req->sim_branch]);
                    $branch = $branch['results'][0]->companyname;

                    $company = $simApi->getCompanyFromBranch($req->sim_branch, 'companyname');
                    break;

                case 'company':
                    $company = $clienApi->getCompany(1, ["id" => $req->client_company]);
                    $company = $company['results']->result[0]->organization_name;
                    break;

                case 'branch':
                    $branch = $clienApi->getBranch(1, ["id" => $req->client_branch]);
                    $branch = $branch['results']->result[0]->organization_name;

                    $company = $simApi->getCompanyFromBranch($req->client_branch, 'organization_name');
                    break;

                default:
                    # code...
                    break;
            }

            SettingCredentials::create([
                'type'         => $req->type,
                'organization' => $organization,
                'value'        => $value,
                'company_name' => $company,
                'branch_name'  => $branch,
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        Session::flash('toast-success', 'Configuration has saved');
        return redirect()->route('admin.setting-credentials.index');
    }

    public function settingCredentialsDelete($id)
    {
        DB::beginTransaction();
        try {
            $setting = SettingCredentials::where('id', $id)->firstOrFail();
            $setting->delete();
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        Session::flash('toast-success', 'Configuration has deleted');
        return redirect()->route('admin.setting-credentials.index');
    }
}
