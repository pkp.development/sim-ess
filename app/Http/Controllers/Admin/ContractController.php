<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\Facades\GuzzleClient;
use App\Models\Config\AuditTrail;
use App\Repositories\DocumentRepository;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Exception;
use JsValidator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Models\Accounts\Accounts;
use App\Models\Temporary\Contract;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\SipasApi\PostContract;
use App\Models\Permissions\PermissionCompany;
use App\Models\Permissions\PermissionSimBranch;

class ContractController extends Controller
{
    public function index(Request $req)
    {
        if ($req->ajax()) {
            if (auth()->user()->role == 'admin') {
                $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();

                $model = Contract::with('account.profile', 'account.assignment', 'verifiedBy.admin')->select('tmp_contract.*')->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                    $allBranch = $simBrances->where('value', 0)->first();
                    $allClient = $companies->where('value', 0)->first();

                    if ($allBranch && $allClient) {
                        $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
                    }
                    elseif ($allBranch) {
                        $query->whereIn('client_company_id', $companies->pluck('value'));
                    }
                    elseif ($allClient) {
                        $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    else {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                });
            }
            else {
                $model = Contract::with('account.profile', 'account.assignment', 'verifiedBy.admin')->select('tmp_contract.*');
            }

            return DataTables::of($model)
                ->addColumn('pe_name', function($data){
                    return $data->account->profile->name;
                })
                ->addColumn('admin_name', function($data){
                    return $data->verifiedBy->admin->name;
                })
                ->make(true);
        }

        return view('pages.admin.contract.index');
    }

    public function verification($uuid)
    {

        $this->repository = new DocumentRepository;

        $validator = JsValidator::make([
            'description' => 'required|max:100'
        ], [], [
            'description' => 'Alasan Penolakan'
        ], '#form-refused');

        $account = Accounts::with('assignment')->whereUuid($uuid)->first();
        $assignment = $account->assignment;

        $contractBpr = $this->repository->checkEligibleContractInBprByNik($account->profile->nik);
        $contract_id = $contractBpr->data->contract_id;

        return view('pages.admin.contract.verification', compact('account', 'validator', 'contract_id'));
    }

    public function verificationPost(Request $req, $uuid)
    {

        $this->repository = new DocumentRepository;
        DB::beginTransaction();
        try {
            $status     = 0;
            $statusText = null;
            $reason     = null;

            $account = Accounts::with('assignment')->whereUuid($uuid)->first();

            if ($req->type == 'verified') {
                $status = 1;
                $statusText = 'Verified';
            }
            elseif ($req->type == 'refused') {
                $this->validate($req, [
                    'description' => 'required|max:100'
                ]);

                $status = 2;
                $reason = $req->description;
                $statusText = 'Rejected';
                $account->assignment->signed_at = null;
            }
            else {
                Session::flash('toast-error', 'Terjadi kesalahan');
                return redirect()->route('admin.contract.index');
            }

            $account->assignment->ess_status      = $status;
            $account->assignment->rejected_reason = $reason;
            $account->assignment->save();

            Contract::create([
                'uuid'            => Uuid::uuid4()->toString(),
                'verified_by'     => auth()->user()->id,
                'account_id'      => $account->id,
                'contract_number' => $account->assignment->contract_number,
                'status'          => $statusText,
                'description'     => $reason
            ]);
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            if ($req->type == 'verified') {
                $check = $this->repository->checkContractExists($req->contract_id, 'contract');

                if($check->status == true) {

                    $contractRes = $this->repository->sendContractFile($req->contract_id);

                    if($contractRes AND $contractRes->status AND $contractRes->code == '200') {

                        $contractRes = $this->repository->updateDataSignedContract($req->contract_id, $account->sim_id, $account->profile->nik);

                        if(!$contractRes->status OR $contractRes->code != '200') {

                            DB::rollBack();
                            throw new Exception('Update sent data failed!');
                        }

                    } else {
                        DB::rollBack();
                        throw new Exception('Send file failed!');
                    }
                }else{

                    DB::rollBack();
                    throw new Exception($check->msg);
                }
            }
        }
        catch (\Exception $e) {
            throw $e;
        }

        DB::commit();

        Session::flash('toast-success', 'Kontrak berhasil diverifikasi');
        return redirect()->route('admin.contract.index');
    }

    public function view($uuid) {

        $this->repository = new DocumentRepository;
        $account  = Accounts::whereUuid($uuid)->first();

        $contractBpr = $this->repository->checkEligibleContractInBprByNik($account->profile->nik);;

//        $filePath = 'contract/' . $account->sim_id . '.pdf';
        $filePath = 'contract/kontrak-' . $contractBpr->data->contract_id . '.pdf';

        if( ! Storage::exists($filePath) ) {
            abort(404);
        }

        $pdfContent = Storage::get($filePath);

        // for pdf, it will be 'application/pdf'
        $type       = Storage::mimeType($filePath);
        $fileName   = $account->sim_id . '.pdf';

        return Response::make($pdfContent, 200, [
            'Content-Type'        => $type,
            'Content-Disposition' => 'inline; filename="'.$fileName.'"'
        ]);
    }
}
