<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Config\AuditTrail;
use App\Models\Config\HistoryCredentials;
use App\Models\Config\HistoryCredentialsDetail;
use App\Models\Config\SmsCallback;
use App\Repositories\SipasApi\GetErrorLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ErrorController extends Controller
{
    public function auditTrail(Request $req)
    {
        if ($req->ajax()) {
            $model = AuditTrail::with('account.admin')->select('audit_trail.*');

            if ($req->filter_date) {
                $date = Carbon::createFromFormat('Y-m-d', $req->filter_date)->toDateString();
                $model = $model->whereDate('created_at', $date);
            }

            return DataTables::make($model)
                ->addColumn('date', function($data){
                    return $data->created_at->format('d F Y');
                })
                ->addColumn('admin', function($data){
                    return ($data->account_id) ? $data->account->admin->name : "-";
                })
                ->make(true);
        }

        return view('pages.admin.errors.audit-trail');
    }

    public function smsCallback(Request $req)
    {
        if ($req->ajax()) {
            $model = SmsCallback::select('sms_callback.*');

            if ($req->filter_date) {
                $date = Carbon::createFromFormat('Y-m-d', $req->filter_date)->toDateString();
                $model = $model->whereDate('created_at', $date);
            }

            return DataTables::make($model)
                ->addColumn('date', function($data){
                    return $data->created_at->format('d F Y');
                })
                ->make(true);
        }

        return view('pages.admin.errors.sms-callback');
    }

    public function sipasLog(Request $req)
    {
        $page = 1;
        if ($req->has('page')) {
            $page = $req->page;
        }

        $repo   = new GetErrorLog;
        $result = $repo->getLog($page);

        return view('pages.admin.errors.sipas-log', compact('result'));
    }

    public function credentials(Request $req)
    {
        if ($req->ajax()) {
            $model = HistoryCredentials::select('history_credentials.*');

            return DataTables::make($model)
                ->addColumn('total_failed', function($data){
                    $count = HistoryCredentialsDetail::where('status', 2)->where('credential_id', $data->id)->count();

                    return '<span class="badge bg-soft-danger text-danger">'. $count .'</span>';
                })
                ->editColumn('status', function($data){
                    if($data->status == 1) {
                        return '<span class="badge bg-soft-success text-success">Finished</span>';
                    }
                    else if($data->status == 0) {
                        return '<span class="badge bg-soft-danger text-primary">On Progress</span>';
                    }
                    else {
                        return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                    }
                })
                ->addColumn('action', function($data){
                    $html = action_btn(route('admin.credentials.detail', $data->uuid), [], 'mdi mdi-details');
                    return $html;
                })
                ->rawColumns(['status', 'total_failed', 'action'])
                ->make(true);
        }

        return view('pages.admin.errors.credentials');
    }

    public function credentialsDetails(Request $req, $uuid)
    {
        if ($req->ajax()) {
            $history = HistoryCredentials::whereUuid($uuid)->first();

            $model = HistoryCredentialsDetail::select('history_credentials_detail.*')->where('credential_id', $history->id);

            return DataTables::make($model)
                ->editColumn('status', function($data){
                    if($data->status == 1) {
                        return '<span class="badge bg-soft-success text-success">Success</span>';
                    }
                    else {
                        return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                    }
                })
                ->rawColumns(['status'])
                ->make(true);
        }

        return view('pages.admin.errors.credentials-detail', compact('uuid'));
    }
}
