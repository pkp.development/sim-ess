<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\BprGoApi\RestBprGo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Admin\EmployeeRepository;
use Illuminate\Support\Facades\Session;
use App\Repositories\Admin\FamilyRepository;

class FamilyController extends Controller
{
    protected $repository;
    protected $employeeRepository;

    public function __construct()
    {
        $this->repository = new FamilyRepository;
        $this->employeeRepository = new EmployeeRepository;
    }

    public function updateFamilyView($uuid)
    {
        $family = $this->repository->getPendingFamily($uuid);

        if (!$family) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }

        $dataOld = $this->employeeRepository->getProfile($family->account_id, 'account_id');
        $content = json_decode($family->content);

        $masterApi = new RestBprGo();
        $endpoint = env('MASTER_MULTIPLE_DATA');
        $old_datas = $masterApi->bprApi(
            $endpoint,
            'GET',
            ['masters'=> [
                'marital' => $dataOld->tax_marital_status]
            ]
        );

        if(!$old_datas->data){
            $bpr_ref_old['marital']['nama_data'] = '';
		}else{
	        $bpr_ref_old = reconcile_data_master_bpr($old_datas->data);
		}
	$new_datas = $masterApi->bprApi(
            $endpoint,
            'GET',
            ['masters'=> [
                'marital' => $content->tax_marital_status]
            ]
    );

        if(!$new_datas->data){
            $bpr_ref_new['marital']['nama_data'] = '';
		}else{
            $bpr_ref_new = reconcile_data_master_bpr($new_datas->data);
        }
        return view('pages.admin.family.update-profile', compact('content', 'family', 'dataOld', 'bpr_ref_new', 'bpr_ref_old'));
    }

    public function updateFamilyPost(Request $req, $tmpProfileId)
    {
        $result = $this->repository->updateFamily($req, $tmpProfileId);

        return redirect()->route('admin.employee.index');
    }

    public function daftarFamily($uuid)
    {
        $family = $this->repository->getFamily($uuid, 'uuid');

        if ($family->status != 0) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }
        $masterApi = new RestBprGo();
        $endpoint = env('MASTER_MULTIPLE_DATA');
        $new_datas = $masterApi->bprApi(
            $endpoint,
            'GET',
            ['masters'=> [
                'place_of_birth' => $family->place_of_birth,
                'relation' => $family->relation]
            ]
        );
        $bpr_ref_new = reconcile_data_master_bpr($new_datas->data);
        return view('pages.admin.family.update-family', compact('family', 'bpr_ref_new'));
    }

    public function daftarFamilyPost(Request $req, $uuid)
    {
        $family = $this->repository->daftarFamilyPost($req, $uuid);

        return redirect()->route('admin.employee.index');
    }
}
