<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Tasks\LasDownload;
use App\Models\Config\Notification;
use App\Http\Controllers\Controller;
use App\Models\Accounts\Accounts;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Permissions\PermissionCompany;
use App\Models\Permissions\PermissionSimBranch;

class HomeController extends Controller
{
    public function dashboard(Request $req)
    {
        if ($req->ajax()) {
            if ($req->periode) {
                $now = Carbon::createFromFormat('F Y', $req->periode);
            }
            else {
                $now = now();
            }

            if (auth()->user()->role == 'admin') {
                $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();

                $download = LasDownload::with('account.assignment')->whereYear('created_at', $now->format('Y'))->whereMonth('created_at', $now->format('m'))->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                    if (auth()->user()->role == 'admin') {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                })->get();

                // Active User
                $notActivePe = Accounts::with('assignment')->where('role', 'employee')->whereNull('actived_at')->whereHas('assignment', function($query) use ($companies, $simBrances) {
                    if (auth()->user()->role == 'admin') {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                })->count();
                $activePe = Accounts::with('assignment')->where('role', 'employee')->whereNotNull('actived_at')->whereHas('assignment', function($query) use ($companies, $simBrances) {
                    if (auth()->user()->role == 'admin') {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                })->count();
            }
            else {
                $download = LasDownload::whereYear('created_at', $now->format('Y'))->whereMonth('created_at', $now->format('m'))->get();

                // Active User
                $notActivePe = Accounts::with('assignment')->where('role', 'employee')->whereNull('actived_at')->count();
                $activePe = Accounts::with('assignment')->where('role', 'employee')->whereNotNull('actived_at')->count();
            }

            $result = [
                'label'       => $now->format('F'),
                'contract'    => $download->where('type', "contract")->count(),
                'skk'         => $download->where('type', "SKK")->count(),
                'id_card'     => $download->where('type', "id_card")->count(),
                'srk'         => $download->where('type', "SRK")->count(),
                'bpjstkj'     => $download->where('type', "bpjs_tkj")->count(),
                'bpjskes'     => $download->where('type', "bpjs_kes")->count(),
                'activePe'    => $activePe,
                'notActivePe' => $notActivePe,
            ];

            return response()->json($result);
        }

        return view('pages.admin.dashboard');
    }

    public function notification(Request $req)
    {
        if ($req->ajax()) {
            // $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
            // $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();
            // $permissions = [];

            // foreach (auth()->user()->admin->permissions->where('permission_category_id', 3)->pluck('key') as $notifPermission) {
            //     $permissions[] = str_replace('notif-', '', $notifPermission);
            // }

            // $notifications = Notification::with('account.profile', 'account.assignment')->select('notification.*')
            // ->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
            //     $allBranch = $simBrances->where('value', 0)->first();
            //     $allClient = $companies->where('value', 0)->first();

            //     if ($allBranch && $allClient) {
            //         $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
            //     }
            //     elseif ($allBranch) {
            //         $query->whereIn('client_company_id', $companies->pluck('value'));
            //     }
            //     elseif ($allClient) {
            //         $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
            //     }
            //     else {
            //         $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
            //     }
            //     // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
            // })->where('status', 0)->whereIn('type', $permissions)->latest();

            $notifications = null;
            if(auth()->user()->role == 'super-admin') {
                $notifications = Notification::with('account.profile', 'account.assignment')->where('status', 0)->latest();
            }
            elseif (auth()->user()->role == 'admin') {
                $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                $permissions = [];

                foreach (auth()->user()->admin->permissions->where('permission_category_id', 3)->pluck('key') as $notifPermission) {
                    $permissions[] = str_replace('notif-', '', $notifPermission);
                }

                $notifications = Notification::with('account.profile', 'account.assignment')->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                    $allBranch = $simBrances->where('value', 0)->first();
                    $allClient = $companies->where('value', 0)->first();

                    if ($allBranch && $allClient) {
                        $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
                    }
                    elseif ($allBranch) {
                        $query->whereIn('client_company_id', $companies->pluck('value'));
                    }
                    elseif ($allClient) {
                        $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    else {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                })->where('status', 0)->whereIn('type', $permissions)->latest();
            }
            else {
                $notifications = Notification::with('account.profile')->where(function($q){
                    $q->where('status', 1)->orWhere('status', 2);
                })->where('pe_read', null)->where('account_id', auth()->user()->id)->latest();
            }

            return DataTables::of($notifications)
                ->addColumn('date', function($data) {
                    return $data->created_at->format('d-M-Y H:i');
                })
                ->editColumn('type', function($data){
                    if($data->type == 'profile') {
                        return 'Telah melakukan perubahan biodata';
                    }
                    elseif($data->type == 'family') {
                        return 'Telah melakukan perubahan data keluarga';
                    }
                    elseif($data->type == 'bank') {
                        return 'Telah melakukan perubahan data rekening';
                    }
                    elseif($data->type == 'families') {
                        if($data->action == 'create') {
                            return 'Telah menambahkan daftar keluarga';
                        }
                        elseif($data->action == 'update') {
                            return 'Telah mengubah daftar keluarga';
                        }
                        elseif($data->action == 'delete') {
                            return 'Telah menghapus daftar keluarga';
                        }
                    }
                    elseif($data->type == 'outpatient') {
                        return 'Telah melakukan klaim rawat jalan';
                    }
                    elseif($data->type == 'contract') {
                        return 'Telah menandatangani kontrak';
                    }

                    return '-';
                })
                ->addColumn('action', function($data){
                    $url = null;
                    if($data->type == 'profile') {
                        $url = route('admin.employee.update.view', $data->uuid);
                    }
                    elseif($data->type == 'family') {
                        $url = route('admin.employee.update.family.view', $data->uuid);
                    }
                    elseif($data->type == 'bank') {
                        $url = route('admin.employee.update.rekening.view', $data->uuid);
                    }
                    elseif($data->type == 'families') {
                        $url = route('admin.employee.daftar.family', $data->uuid);
                    }
                    elseif($data->type == 'outpatient') {
                        $url = route('admin.outpatient.verification', $data->uuid);
                    }
                    elseif($data->type == 'contract') {
                        $url = route('admin.contract.verification', $data->account->uuid);
                    }

                    $html = '<a href="'. $url .'" class="action-icon" title="Detail" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-details"></i></a>';
                    return $html;
                })
                ->make(true);
        }

        return view('pages.admin.notification');
    }

}
