<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Repositories\Admin\ClaimBenefitRepository;

class IdCardController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new ClaimBenefitRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatablesIdCard();
        }

        $validator = JsValidator::make([
            'id_card_zip' => 'required|mimes:zip'
        ], [], [
            'id_card_zip' => 'ID Card'
        ], '#form-id-card');

        return view('pages.admin.idcard.index', compact('validator'));
    }

    public function upload(Request $req)
    {
        $this->validate($req, [
            'id_card_zip' => 'required|mimes:zip'
        ]);

        $result = $this->repository->employeeIdCardUpload($req);
        Session::flash('toast-success', 'Employee ID Card berhasil diupload');

        return redirect()->route('admin.employee.idcard.index');
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->repository->detailIdCard($uuid);
        }

        return view('pages.admin.idcard.detail', compact('uuid'));
    }
}
