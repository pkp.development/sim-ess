<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Repositories\Admin\InpatientRepository;

class InpatientController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new InpatientRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatable();
        }

        $validator = JsValidator::make([
            'simid'         => 'required|exists:accounts,sim_id',
            'patient_name'  => 'required|max:50',
            'diagnosis'     => 'required|max:50',
            'nominal'       => 'required|not_in:0',
            'hospital_name' => 'required|max:50',
            'status'        => 'required',
            'receipt_date'  => 'required',
            'received_date' => 'required',
            'disbursment'   => 'required_if:status,dana_cair_ke_karyawan',
            'transfer_date' => 'required_if:status,dana_cair_ke_karyawan',
            'description'   => 'max:150'
        ], [
            'nominal.not_in'     => 'Nominal tidak boleh 0',
            'simid.exists'       => 'SIMID tidak ditemukan'
        ], [
            'simid'         => 'SIMID',
            'patient_name'  => 'Nama pasien',
            'diagnosis'     => 'Diagnosis',
            'nominal'       => 'Nominal',
            'hospital_name' => 'Nama rumah sakit',
            'status'        => 'Status',
            'receipt_date'  => 'Tanggal kwitansi',
            'received_date' => 'Tanggal diterima',
            'disbursment'   => 'Nominal cair',
            'transfer_date' => 'Tanggal transfer',
            'description'   => 'Keterangan'
        ], '#form-inpatient');

        return view('pages.admin.inpatient.index', compact('validator'));
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'simid'         => 'required|exists:accounts,sim_id',
            'patient_name'  => 'required|max:50',
            'diagnosis'     => 'required|max:50',
            'nominal'       => 'required|not_in:0',
            'hospital_name' => 'required|max:50',
            'status'        => 'required',
            'receipt_date'  => 'required',
            'received_date' => 'required',
            'disbursment'   => 'required_if:status,dana_cair_ke_karyawan',
            'transfer_date' => 'required_if:status,dana_cair_ke_karyawan',
            'description'   => 'max:150'
        ], ['simid.exists' => 'SIMID tidak ditemukan']);

        $result = $this->repository->save($req);

        if($result === 'account') {
            Session::flash('toast-error', 'Akun dengan simid: ' . $req->simid . ' tidak ditemukan');
            return redirect()->route('admin.inpatient.index');
        }
        if($result === 'restricted') {
            Session::flash('toast-error', 'Akun dengan simid: ' . $req->simid . ' tidak dapat anda input');
            return redirect()->route('admin.inpatient.index');
        }

        Session::flash('toast-success', 'Klaim rawat inap berhasil ditambahkan');
        return redirect()->route('admin.inpatient.index');
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->repository->historyDatatable($uuid);
        }

        $validator = JsValidator::make([
            'status'        => 'required',
            'disbursment'   => 'required_if:status,dana_cair_ke_karyawan',
            'transfer_date' => 'required_if:status,dana_cair_ke_karyawan',
            'description'   => 'max:150'
        ], [], [
            'status'        => 'Status',
            'disbursment'   => 'Nominal cair',
            'transfer_date' => 'Tanggal transfer',
            'description'   => 'Keterangan'
        ], '#form-inpatient');

        $result = $this->repository->get($uuid);

        if (!$result) {
            Session::flash('toast-error', 'Klaim rawat inap tidak ditemukan');
            return redirect()->route('admin.inpatient.index');
        }

        return view('pages.admin.inpatient.detail', compact('result'));
    }

    public function update(Request $req, $uuid)
    {
        $this->validate($req, [
            'status'        => 'required',
            'disbursment'   => 'required_if:status,dana_cair_ke_karyawan',
            'transfer_date' => 'required_if:status,dana_cair_ke_karyawan',
            'description'   => 'max:150'
        ]);

        $result = $this->repository->update($req, $uuid);

        if (!$result) {
            Session::flash('toast-error', 'Klaim rawat inap tidak ditemukan');
            return redirect()->route('admin.inpatient.index');
        }

        return redirect()->route('admin.inpatient.detail', $uuid);
    }
}
