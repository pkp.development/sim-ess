<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Admin\OutpatientRepository;
use App\Repositories\BprApi\DataPlafond;
use Illuminate\Support\Facades\Session;
use App\Repositories\SipasApi\RawatJalan;

class OutpatientController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new OutpatientRepository;
    }

    public function verification($uuid)
    {
        $data = $this->repository->getOutpatient($uuid);

        if (!$data) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }
        else if($data->status != 0) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }

        $api          = new DataPlafond;
        $sisa_plafond = $api->getPlafondData($data->account->sim_id)->benefit;

        return view('pages.admin.outpatient.verification', compact('data', 'sisa_plafond'));
    }

    public function verificationPost(Request $req, $uuid)
    {
        $data = $this->repository->verificationOutpatient($req, $uuid);

        if (!$data) {
            Session::flash('toast-error', 'Tidak terdapat perubahan data');
            return redirect()->route('admin.employee.index');
        }
        if ($data === "insuficient_balance") {
            Session::flash('toast-error', 'Sisa plafond anda tidak mencukupi untuk melakukan klaim');
            return redirect()->route('admin.employee.index');
        }

        Session::flash('toast-success', 'Klaim rawat jalan berhasil diverifikasi');

        return redirect()->route('admin.employee.index');
    }
}
