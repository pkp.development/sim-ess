<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Config\PeraturanPerusahaan;

class PeraturanPerusahaanController extends Controller
{
    public function index(Request $req)
    {
        if ($req->ajax()) {
            $peraturan = PeraturanPerusahaan::select('peraturan_perusahaan.*');

            return DataTables::of($peraturan)
                ->editColumn('effective_date', function ($data) {
                    return $data->effective_date->format('d F Y');
                })
                ->addColumn('action', function ($data) {
                    $html = '<a href="'. route('admin.peraturan-perusahaan.getData', $data->uuid) .'" class="action-icon btn-edit" title="Edit" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-square-edit-outline"></i></a>';
                    $html .= '<form method="POST" action="'. route('admin.peraturan-perusahaan.delete', $data->uuid) .'" style="display: inline-block" accept-charset="UTF-8">';
                    $html .= method_field('DELETE');
                    $html .= csrf_field();
                    $html .= '<a href="#" class="action-icon btn-confirm" title="Delete" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-delete-outline"></i></a>';
                    $html .= '</form>';

                    return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        $validator = JsValidator::make([
            'title'          => 'required|max:100',
            'effective_date' => 'required|date_format:d F Y',
            'file_peraturan' => 'mimes:pdf'
        ], [], [
            'title'          => 'Judul',
            'effective_date' => 'Tanggal Berlaku',
            'file_peraturan' => 'File'
        ], '#form-peraturan-perusahaan');

        return view('pages.admin.peraturan-perusahaan', compact('validator'));
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'title'          => 'required|max:100',
            'effective_date' => 'required|date_format:Y-m-d',
            'file_peraturan' => 'mimes:pdf'
        ]);

        DB::beginTransaction();
        try {
            $result = PeraturanPerusahaan::create([
                'title'          => $req->title,
                'effective_date' => $req->effective_date,
                'type'           => $req->type
            ]);

            $imgLogo = null;
            if ($req->type == 'file') {
                if ($req->hasFile('file_peraturan')) {
                    $file     = $req->file('file_peraturan');
                    $filename = $result->uuid . '.' . $file->extension();
                    $path     = 'peraturan_perusahaan';
                    $imgLogo  = $path . '/' . $filename;

                    Storage::disk('public')->putFileAs($path, $file, $filename);
                }
            }
            elseif ($req->type == 'link') {
                $imgLogo = $req->link_peraturan;
            }

            $result->file = $imgLogo;
            $result->save();
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if(!$result) {
            Session::flash('toast-error', 'Peraturan perusahaan gagal disimpan');
            return redirect()->route('admin.peraturan-perusahaan.index');
        }

        Session::flash('toast-success', 'Peraturan perusahaan berhasil ditambahkan');
        return redirect()->route('admin.peraturan-perusahaan.index');
    }

    public function getData($uuid)
    {
        $result = PeraturanPerusahaan::whereUuid($uuid)->first();
        if (!$result) {
            return response()->json(null, 404);
        }

        return response()->json([
            'title'          => $result->title,
            'effective_date' => $result->effective_date,
            'type'           => $result->type,
            'file'           => $result->file,
        ]);
    }

    public function update($uuid, Request $req)
    {
        $this->validate($req, [
            'title'          => 'required|max:100',
            'effective_date' => 'required|date_format:Y-m-d',
            'file_peraturan' => 'mimes:pdf'
        ]);

        DB::beginTransaction();
        try {
            $result = PeraturanPerusahaan::whereUuid($uuid)->first();

            $imgLogo = $result->file;
            if ($req->type == 'file' && $req->hasFile('file_peraturan')) {
                $file     = $req->file('file_peraturan');
                $filename = $result->uuid . '.' . $file->extension();
                $path     = 'peraturan_perusahaan';
                $imgLogo  = $path . '/' . $filename;

                Storage::disk('public')->putFileAs($path, $file, $filename);
            }
            else {
                $imgLogo = $req->link_peraturan;
            }

            $result->update([
                'title'          => $req->title,
                'effective_date' => $req->effective_date,
                'file'           => $imgLogo,
                'type'           => $req->type
            ]);
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if(!$result) {
            Session::flash('toast-error', 'Peraturan perusahaan gagal disimpan');
            return redirect()->route('admin.peraturan-perusahaan.index');
        }

        Session::flash('toast-success', 'Peraturan perusahaan berhasil disimpan');
        return redirect()->route('admin.peraturan-perusahaan.index');
    }

    public function destroy($uuid)
    {
        DB::beginTransaction();
        try {
            $result = PeraturanPerusahaan::whereUuid($uuid)->first();

            if (Storage::disk('public')->exists($result->file)) {
                Storage::disk('public')->delete($result->file);
            }

            $result->delete();
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if(!$result) {
            Session::flash('toast-error', 'Peraturan perusahaan gagal dihapus');
            return redirect()->route('admin.peraturan-perusahaan.index');
        }

        Session::flash('toast-success', 'Peraturan perusahaan berhasil dihapus');
        return redirect()->route('admin.peraturan-perusahaan.index');
    }
}
