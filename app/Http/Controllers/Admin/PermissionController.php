<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Session;
use App\Repositories\Admin\AdminPermissionRepository;

class PermissionController extends Controller
{
    protected $repository;
    protected $userRepository;

    public function __construct(AdminPermissionRepository $adminPermissionRepository, UserRepository $userRepository)
    {
        $this->repository     = $adminPermissionRepository;
        $this->userRepository = $userRepository;
    }

    public function permissionStore(Request $req, $uuid)
    {
        $user   = $this->userRepository->get($uuid);
        $result = $this->repository->storePermission($user->admin, $req);

        if ($result) {
            Session::flash('toast-success', 'Admin berhasil disimpan');
                return redirect()->route('admin.user.index');
        }

        Session::flash('toast-error', 'Admin gagal disimpan');
        return redirect()->route('admin.user.index');
    }

    public function clientDatatable(Request $req, $uuid)
    {
        $user = $this->userRepository->get($uuid);
        return $this->repository->datatableClient($user->admin->id);
    }

    public function clientStore(Request $req, $uuid)
    {
        $this->validate($req, [
            'client_company' => 'required_without:client_company_for_all'
        ]);

        $user   = $this->userRepository->get($uuid);
        $result = $this->repository->storeClient($req, $user->admin->id);

        if ($result) {
            Session::flash('toast-success', 'Client Permission berhasil disimpan');
            return redirect()->route('admin.user.permission', $uuid);
        }

        Session::flash('toast-error', 'Client Permission gagal disimpan');
        return redirect()->route('admin.user.permission', $uuid);
    }

    public function clientDelete($id)
    {
        $result = $this->repository->deleteClient($id);

        if ($result) {
            Session::flash('toast-success', 'Client Permission berhasil dihapus');
            return redirect()->route('admin.user.permission', $result);
        }

        Session::flash('toast-error', 'Client Permission gagal dihapus');
        return redirect()->route('admin.user.permission', $result);
    }

    public function simBranchDatatable(Request $req, $uuid)
    {
        $user = $this->userRepository->get($uuid);
        return $this->repository->datatableSimBranch($user->admin->id);
    }

    public function simBranchStore(Request $req, $uuid)
    {
        $this->validate($req, [
            'sim_branch' => 'required_without:sim_branch_for_all'
        ]);

        $user   = $this->userRepository->get($uuid);
        $result = $this->repository->storeSimBranch($req, $user->admin->id);

        if ($result) {
            Session::flash('toast-success', 'SIM Branch Permission berhasil disimpan');
            return redirect()->route('admin.user.permission', $uuid);
        }

        Session::flash('toast-error', 'SIM Branch Permission gagal disimpan');
        return redirect()->route('admin.user.permission', $uuid);
    }

    public function simBranchDelete($id)
    {
        $result = $this->repository->deleteSimBranch($id);

        if ($result) {
            Session::flash('toast-success', 'SIM Branch Permission berhasil dihapus');
            return redirect()->route('admin.user.permission', $result);
        }

        Session::flash('toast-error', 'SIM Branch Permission gagal dihapus');
        return redirect()->route('admin.user.permission', $result);
    }
}
