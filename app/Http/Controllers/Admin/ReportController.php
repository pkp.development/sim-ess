<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Report\ContractSignature;
use App\Exports\Report\EmployeeData;
use App\Exports\Report\EmployeeLogin;
use App\Exports\Report\EmployeeOutpatient;
use App\Exports\Report\HistoryDownload;
use App\Exports\Report\HistoryUpload;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JsValidator;

class ReportController extends Controller
{
    public function index()
    {
        $validator = JsValidator::make([
            'type'            => 'required',
            'periode_laporan' => 'required'
        ], [], [
            'type'            => 'Tipe Laporan',
            'periode_laporan' => 'Periode Laporan'
        ], '#form-report');

        return view('pages.admin.report.index', compact('validator'));
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'type'            => 'required',
            'periode_laporan' => 'required'
        ]);

        $periode = explode(' to ', $req->periode_laporan);

        switch ($req->type) {
            case "employee_login":
                return (new EmployeeLogin($periode[0], $periode[1]))->download('employee-login ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "tanda_tangan_kontrak":
                return (new ContractSignature($periode[0], $periode[1]))->download('employee-contract ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "perubahan_biodata":
                return (new EmployeeData($periode[0], $periode[1]))->download('employee-biodata ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "input_klaim_rawat_jalan":
                return (new EmployeeOutpatient($periode[0], $periode[1]))->download('employee-outpatient ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_history_kontrak":
                return (new HistoryDownload($periode[0], $periode[1], 'contract'))->download('history-Contract ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_payslip":
                return (new HistoryDownload($periode[0], $periode[1], 'payslip'))->download('history-Payslip ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_spt":
                return (new HistoryDownload($periode[0], $periode[1], 'spt'))->download('history-SPT ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "downlad_skk":
                return (new HistoryDownload($periode[0], $periode[1], 'SKK'))->download('history-SKK ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_srk":
                return (new HistoryDownload($periode[0], $periode[1], 'SRK'))->download('history-SRK ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_idcard":
                return (new HistoryDownload($periode[0], $periode[1], 'id_card'))->download('history-IDCard ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_bpjstk":
                return (new HistoryDownload($periode[0], $periode[1], 'bpjs_tkj'))->download('history-BPJSTK ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "download_bpjs_kesehatan":
                return (new HistoryDownload($periode[0], $periode[1], 'bpjs_kes'))->download('history-BPJSKes ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "upload_payslip":
                return (new HistoryUpload($periode[0], $periode[1], 'payslip'))->download('history-upload-payslip ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "upload_spt":
                return (new HistoryUpload($periode[0], $periode[1], 'SPT'))->download('history-upload-SPT ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "upload_idcard":
                return (new HistoryUpload($periode[0], $periode[1], 'id_card'))->download('history-upload-IDCard ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "upload_bpjstk":
                return (new HistoryUpload($periode[0], $periode[1], 'bpjs_tkj'))->download('history-upload-BPJSTK ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            case "upload_bpjs_kesehatan":
                return (new HistoryUpload($periode[0], $periode[1], 'bpjs_kes'))->download('history-upload-BPJSKes ('. $periode['0'] . '-'. $periode['1'] .').xlsx');
                break;
            default:
                break;
        }
    }
}
