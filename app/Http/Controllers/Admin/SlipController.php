<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Carbon\Carbon;
use App\Imports\PaySlip;
use Illuminate\Http\Request;
use App\Exports\PaySlipTemplate;
use App\Http\Controllers\Controller;
use App\Models\SlipGaji\History;
use App\Repositories\SlipRepository;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;

class SlipController extends Controller
{
    protected $view = 'pages.admin.slip_gaji.';
    protected $slipsRepository;

    public function __construct(SlipRepository $slipsRepository)
    {
        $this->slipsRepository = $slipsRepository;
    }

    public function index(Request $req)
    {
        $validator = JsValidator::make(['name' => 'required', 'type'  => 'required'], [], [
            'name'    => 'Nama Akun',
            'type'    => 'Tipe Akun',
        ], '#form_slip');

        if($req->ajax()) {
            return $this->slipsRepository->datatables();
        }

        return view('pages.admin.slip_gaji.index', compact('validator'));
    }

    public function store(Request $req)
    {
        // dd($req->all());

        $this->validate($req, [
            'name' => 'required',
            'type' => 'required',
        ]);

        $this->slipsRepository->save($req);

        return redirect()->route('admin.slip_gaji.index');
    }

    public function update(Request $req, $code)
    {
        // dd($req->all());

        $this->validate($req, [
            'name' => 'required',
            'type' => 'required',
        ]);

        $this->slipsRepository->update($req, $code);

        return redirect()->route('admin.slip_gaji.index');
    }

    public function downloadTemplate()
    {
        return (new PaySlipTemplate)->download('payslip.xlsx');
    }

    public function indexUpload(Request $req)
    {
        if ($req->ajax()) {
            return $this->slipsRepository->datatablesHistory();
        }

        $validator = JsValidator::make([
            'payslip_date'   => 'required',
            'periode'        => 'required',
            'payslip_upload' => 'required|mimes:csv,txt'
        ], [], [
            'payslip_date'   => 'Payment Date',
            'periode'        => 'Periode Payslip',
            'payslip_upload' => 'File Upload'
        ], '#form-payslip');

        return view('pages.admin.slip_gaji.create', compact('validator'));
    }

    public function storeUpload(Request $req)
    {
        $this->validate($req, [
            'payslip_date'   => 'required',
            'periode'        => 'required',
            'payslip_upload' => 'required|mimes:csv,txt'
        ]);

        $carbon = Carbon::createFromFormat('F Y', $req->periode);
        $history = null;
        try {
            $history = History::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'uploaded_by' => auth()->user()->id,
                'status'      => 0
            ]);

            Excel::import(new PaySlip($carbon->format('Y'), $carbon->format('m'), $req->payslip_date, $history->id), $req->file('payslip_upload'), null, \Maatwebsite\Excel\Excel::CSV);

            $history->status = 1;
            $history->save();
        }
        catch (\Throwable $e) {
            $history->status = 2;
            $history->save();
            throw $e;
        }

        Session::flash('toast-success', 'Data Payslip berhasil ditambahkan');
        return redirect()->route('admin.slip_gaji.index-upload');
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->slipsRepository->detailHistory($uuid);
        }

        return view('pages.admin.slip_gaji.detail', compact('uuid'));
    }

    public function getAccount($code)
    {
        $result = $this->slipsRepository->first('code', $code);

        if($result) {
            return response()->json($result);
        }

        return response()->json(null, 404);
    }
}
