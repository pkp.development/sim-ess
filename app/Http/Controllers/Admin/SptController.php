<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Ramsey\Uuid\Uuid;
use App\Imports\SptImport;
use App\Models\Spt\History;
use Illuminate\Http\Request;
use App\Models\Spt\HistoryDetail;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SptController extends Controller
{
    public function index(Request $req)
    {
        if ($req->ajax()) {
            $model = History::with('uploadedby.admin')->select('history_spt.*');

            return DataTables::of($model)
                ->editColumn('status', function($data){
                    if($data->status == 1) {
                        return '<span class="badge bg-soft-success text-success">Finished</span>';
                    }

                    else {
                        return '<span class="badge bg-soft-danger text-danger">On Progress</span>';
                    }
                })
                ->addColumn('total_failed', function($data){
                    $totalFailed = HistoryDetail::where('history_spt_id', $data->id)->where('status', 'Failed')->count();
                    return '<span class="badge bg-soft-danger text-danger">'. $totalFailed .'</span>';
                })
                ->addColumn('tanggal', function($data){
                    return $data->created_at->format('d F Y H:i:s');
                })
                ->addColumn('action', function($data){
                    $html = action_btn(route('admin.spt.detail', $data->uuid), [], 'mdi mdi-details');
                    return $html;
                })
                ->rawColumns(['status', 'total_failed', 'action'])
                ->make(true);
        }

        $validator = JsValidator::make([
            'year'       => 'required|date_format:Y',
            'spt_upload' => 'required|mimes:csv,txt'
        ], [], [
            'year'       => 'Tahun SPT',
            'spt_upload' => 'File Data',
        ], '#form-spt');

        return view('pages.admin.spt.create', compact('validator'));
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            $history = History::select('id')->where('uuid', $uuid)->first();
            $model   = HistoryDetail::select('history_spt_detail.*')->where('history_spt_id', $history->id);

            return DataTables::of($model)
                ->editColumn('status', function($data){
                    if($data->status == 'Passed') {
                        return '<span class="badge bg-soft-success text-success">Passed</span>';
                    }

                    else {
                        return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                    }
                })
                ->rawColumns(['status'])
                ->make(true);
        }

        return view('pages.admin.spt.detail', compact('uuid'));
    }

    public function downloadTemplate()
    {
        return Storage::download('spt_template.xlsx');
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'year'       => 'required|date_format:Y',
            'spt_upload' => 'required|mimes:csv,txt'
        ]);

        $history = History::create([
            'uuid'        => Uuid::uuid4()->toString(),
            'uploaded_by' => auth()->user()->id,
            'status'      => 0
        ]);

        Excel::import(new SptImport($req->year, $history->id), $req->file('spt_upload'), null, \Maatwebsite\Excel\Excel::CSV);

        $history->status = 1;
        $history->save();

        Session::flash('toast-success', 'Data SPT berhasil ditambahkan');
        return redirect()->route('admin.spt.index');
    }
}
