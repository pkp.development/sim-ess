<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permissions\PermissionCategory;
use App\Models\Permissions\Permissions;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use JsValidator;

class UserController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new UserRepository;
    }

    /**
     * Get Datatable
     *
     * @param Request $req
     * @return Datatable
     */
    public function datatable(Request $req)
    {
        return $this->repository->datatable();
    }

    /**
     * Index Page of Admin User
     *
     * @param Request $req
     * @return view
     */
    public function index(Request $req)
    {
        return view('pages.admin.user.index');
    }

    /**
     * Create Page of Admin User
     *
     * @param Request $req
     * @return view
     */
    public function create(Request $req)
    {
        $validator = JsValidator::make([
            'username'     => 'required|unique:accounts,username|max:25',
            'email'        => 'required|email|unique:accounts,email|max:50',
            'name'         => 'required',
            'phone_number' => 'nullable|numeric|digits_between:10,15',
            'password'     => 'min:6|required|confirmed'
        ]);

        return view('pages.admin.user.create', compact('validator'));
    }

    public function permission(Request $req, $uuid)
    {
        $permissionCategories   = PermissionCategory::with('permissions')->get();
        $permissionUncategories = Permissions::whereNull('permission_category_id')->get();
        $user                   = $this->repository->get($uuid);

        return view('pages.admin.user.permission', compact('permissionCategories', 'permissionUncategories', 'uuid', 'user'));
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'username'     => 'required|unique:accounts,username|max:25',
            'email'        => 'required|email|unique:accounts,email|max:50',
            'name'         => 'required',
            'phone_number' => 'numeric|digits_between:10,15',
            'password'     => 'min:6|required|confirmed'
        ]);

        $result = $this->repository->save($req);

        if ($result) {
            if ($result->role == 'super-admin') {
                Session::flash('toast-success', 'Admin berhasil disimpan');
                return redirect()->route('admin.user.index');
            }
            else if ($result->role == 'admin') {
                return redirect()->route('admin.user.permission', $result->uuid);
            }
            else {
                Session::flash('toast-error', 'Admin gagal disimpan');
                return redirect()->route('admin.user.index');
            }
        }

        Session::flash('toast-error', 'Admin gagal disimpan');
        return redirect()->route('admin.user.index');
    }

    public function edit($uuid)
    {
        $data = $this->repository->get($uuid);

        if (!$data) {
            Session::flash('toast-error', 'Admin tidak ditemukan');
            return redirect()->route('admin.user.index');
        }

        $validator = JsValidator::make([
            'username'     => 'required|max:25|unique:accounts,username,' . $data->id,
            'email'        => 'required|max:50|email|unique:accounts,email,' . $data->id,
            'name'         => 'required',
            'phone_number' => 'numeric|digits_between:10,15',
            'password'     => 'nullable|min:6|confirmed'
        ]);

        return view('pages.admin.user.edit', compact('data', 'validator'));
    }

    public function update(Request $req, $uuid)
    {
        $data = $this->repository->get($uuid);

        if (!$data) {
            Session::flash('toast-error', 'Admin tidak ditemukan');
            return redirect()->route('admin.user.index');
        }

        $this->validate($req, [
            'username'     => 'required|max:25|unique:accounts,username,' . $data->id,
            'email'        => 'required|max:50|email|unique:accounts,email,' . $data->id,
            'name'         => 'required',
            'phone_number' => 'numeric|digits_between:10,15',
            'password'     => 'nullable|min:6|confirmed'
        ]);

        $result = $this->repository->update($req, $data);

        if ($result) {
            Session::flash('toast-success', 'Admin berhasil diupdate');
            return redirect()->route('admin.user.index');
        }

        Session::flash('toast-error', 'Admin gagal diupdate');
        return redirect()->route('admin.user.index');
    }

    public function destroy($uuid)
    {
        $data = $this->repository->get($uuid);

        if (!$data) {
            Session::flash('toast-error', 'Admin tidak ditemukan');
            return redirect()->route('admin.user.index');
        }

        $result = $this->repository->destroy($data);
        Session::flash('toast-success', $result);
        return redirect()->route('admin.user.index');
    }
}
