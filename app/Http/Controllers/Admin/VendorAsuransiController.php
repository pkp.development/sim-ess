<?php

namespace App\Http\Controllers\Admin;

use JsValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Admin\ClaimBenefitRepository;

class VendorAsuransiController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new ClaimBenefitRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatablesAsuransi();
        }

        $validator = JsValidator::make([
            'insurance_csv' => 'required|mimes:csv,txt'
        ], [], [
            'insurance_csv' => 'Vendor Asuransi'
        ], '#form-vendor-asuransi');

        return view('pages.admin.insurance.index', compact('validator'));
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->repository->detailAsuransi($uuid);
        }

        return view('pages.admin.insurance.detail', compact('uuid'));
    }

    public function upload(Request $req)
    {
        $this->validate($req, [
            'insurance_csv' => 'required|mimes:csv,txt'
        ]);

        $result = $this->repository->insuranceUpload($req);
        Session::flash('toast-success', 'Vendor Asuransi berhasil diupload');

        return redirect()->route('admin.vendor-asuransi.index');
    }

    public function downloadTemplate()
    {
        return Storage::download('insurance_template.xlsx');
    }
}
