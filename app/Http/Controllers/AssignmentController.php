<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use ZanySoft\Zip\Zip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Claim\HistoryIdCard;
use App\Libraries\Facades\GuzzleClient;
use App\Libraries\Facades\ImageHandler;
use App\Repositories\SipasApi\GetToken;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Exception\ClientException;
use App\Repositories\AssignmentRepository;
use GuzzleHttp\Exception\RequestException;
use App\Repositories\SipasApi\GetSuratKerja;
use Symfony\Component\HttpFoundation\StreamedResponse;

class AssignmentController extends Controller
{
    protected $respository;

    public function __construct()
    {
        $this->respository = new AssignmentRepository;
    }

    public function index()
    {
        $simID  = auth()->user()->sim_id ; 
        $url    = "http://147.139.199.220:3400/api/datasrk/" . $simID ;
        $curl   = curl_init();
        curl_setopt_array($curl, array(
            //CURLOPT_URL => "http://147.139.199.220:3400/api/datasrk/PEG21100615",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
           // print_r($response);
        }
        $response  = json_decode($response);
        $total     = $response->jumlah ;
        $total-- ;

        $tabs       = 'penempatan';
        $histories  = []; // $this->respository->getHistoryContract();
        $assignment = $this->respository->getActiveContract();
        $status     = [];// $this->respository->getStatusEmployee();

        return view('pages.employee.informasi-karyawan', compact('tabs', 'histories', 'assignment', 'status','response'));
    }

    public function downloadContract($url)
    {
        try {
            $token     = new GetToken;
            $result    = $token->getToken();
            $tokenAuth = $result['results']->access_token;

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/getDDSContract', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $tokenAuth
                ],
                'query' => ['id' => $url],
                'stream' => true
            ]);

            $body = $response->getBody();

            $response = new StreamedResponse(function() use ($body) {
                while (!$body->eof()) {
                    echo $body->read(1024);
                }
            });

            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', 'attachment; filename="kontrak-'. $url .'.pdf"');

            $this->respository->saveLastDownload('contract');
        }
        catch (ClientException $e) {
            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response;
    }

    public function downloadIdCard()
    {
        if(Storage::exists('images/id_card/' . auth()->user()->sim_id . '.pdf')) {
            $headers = [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="id-card_'. auth()->user()->sim_id .'.pdf"',
            ];

            $this->respository->saveLastDownload('id_card');

            return response()->download(
                storage_path('app/images/id_card/' . auth()->user()->sim_id . '.pdf'), 'id-card_' . auth()->user()->sim_id .'.pdf', $headers
            );
        }

        Session::flash('toast-error', 'ID Card tidak ditemukan');
        return redirect()->route('pe.info.penempatan');
    }

    public function uploadIdCard(Request $req)
    {
        // Validation
        $validation = [
            'idcard_image' => 'image|max:4096'
        ];
        $result = validation_response($req, $validation, [], ['idcard_image' => 'Foto ID Card']);
        if ($result) {
            Session::flash('toast-error', $result);
                return redirect()->route('pe.info.penempatan');
        }

        DB::beginTransaction();
        try {
            if($req->hasFile('idcard_image')) {
                $image    = $req->file('idcard_image');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/self_idcard/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/self_idcard/' . $filename, $image);

                $imagePath = $path . $filename;

                $history = HistoryIdCard::where('account_id', auth()->user()->id)->first();

                if ($history) {
                    $history->idcard_image = $imagePath;
                    $history->save();
                }
                else {
                    HistoryIdCard::create([
                        'account_id'   => auth()->user()->id,
                        'idcard_image' => $imagePath
                    ]);
                }
            }
            else {
                Session::flash('toast-error', 'ID Card gagal diupload');
                return redirect()->route('pe.info.penempatan');
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Session::flash('toast-success', 'ID Card berhasil diupload');
        return redirect()->route('pe.info.penempatan');
    }

    public function downloadSKK()
    {
        try {
            $status       = $this->respository->getStatusEmployee();
            $now          = Carbon::now();
            $lastDownload = $this->respository->getLastDownload('SKK');

            if ($status->is_active != 'Y') {
                Session::flash('toast-error', 'Anda belum diperbolehkan mendownload Surat Keterangan Kerja');
                return redirect()->route('pe.info.penempatan');
            }

            if ($lastDownload) {
                if ($lastDownload->created_at->diff($now)->days < 1) {
                    Session::flash('toast-error', 'Anda hanya diperbolehkan satu kali download dalam sehari');
                    return redirect()->route('pe.info.penempatan');
                }
            }

            $token     = new GetToken;
            $result    = $token->getToken();
            $tokenAuth = $result['results']->access_token;

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/getDownloadSKK', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $tokenAuth
                ],
                'query' => ['id' => auth()->user()->sim_id],
                'json' => ['simid' => auth()->user()->sim_id],
                'stream' => true
            ]);

            $body = $response->getBody();

            $response = new StreamedResponse(function() use ($body) {
                while (!$body->eof()) {
                    echo $body->read(1024);
                }
            });

            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', 'attachment; filename="skk-'. auth()->user()->sim_id .'.pdf"');

            $this->respository->saveLastDownload('SKK');
        }
        catch (ClientException $e) {
            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response;
    }

    public function downloadSRK()
    {
        try {
            $status       = $this->respository->getStatusEmployee();
            $now          = Carbon::now();
            $lastDownload = $this->respository->getLastDownload('SRK');

            if ($status->is_active != 'N') {
                Session::flash('toast-error', 'Anda belum diperbolehkan mendownload Surat Riwayat Kerja');
                return redirect()->route('pe.info.penempatan');
            }

            if ($lastDownload) {
                if ($lastDownload->created_at->diff($now)->days < 1) {
                    Session::flash('toast-error', 'Anda hanya diperbolehkan satu kali download dalam sehari');
                    return redirect()->route('pe.info.penempatan');
                }
            }

            $api = new GetSuratKerja;
            $srk = $api->getSRK(['simid' => auth()->user()->sim_id]);

            $path = 'srk/' . auth()->user()->sim_id;

            Storage::deleteDirectory($path);

            Storage::makeDirectory($path);
            $zip = Zip::create(storage_path('app/' . $path . '/srk-'. auth()->user()->sim_id .'.zip'));

            if (!$srk) {
                Session::flash('toast-error', 'SRK gagal dibuat');
                return redirect()->route('pe.info.penempatan');
            }

            foreach ($srk as $srk) {
                $token     = new GetToken;
                $result    = $token->getToken();
                $tokenAuth = $result['results']->access_token;

                $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/getDownloadSRK', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $tokenAuth
                    ],
                    'query' => ['id' => $this->respository->getContractId($srk->url)],
                    'sink' => storage_path('app/' . $path . '/' . str_replace('/', '-', $srk->nosrk . '.pdf'))
                ]);

                $zip->add(storage_path('app/' . $path . '/' . str_replace('/', '-', $srk->nosrk . '.pdf')));

                $body = $response->getBody();
            }

            $zip->close();

            $this->respository->saveLastDownload('SRK');
        }
        catch (ClientException $e) {
            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        if (Storage::exists($path . '/srk-'. auth()->user()->sim_id .'.zip')) {
            $headers = [
                'Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment; filename="'. 'srk-'. auth()->user()->sim_id .'.zip"',
            ];

            return response()->download(
                storage_path('app/' . $path . '/srk-'. auth()->user()->sim_id .'.zip')
            );
        }
        Session::flash('toast-error', 'SRK gagal dibuat');

        return redirect()->route('pe.info.penempatan');
    }
}
