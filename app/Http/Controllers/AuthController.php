<?php

namespace App\Http\Controllers;

use JsValidator;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;
use App\Mail\ForgotPassword;
use Illuminate\Http\Request;
use App\Models\Config\Setting;
use App\Models\Accounts\Accounts;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Repositories\EmployeeRepository;
use App\Repositories\AssignmentRepository;
use App\Repositories\SipasApi\GetEmployeeStatus;

class AuthController extends Controller
{
    public function login()
    {
        $validator = JsValidator::make([
            'sim_id'   => 'required|exists:accounts,sim_id',
            'password' => 'required'
        ],[],[
            'sim_id'   => 'SIM ID',
            'password' => 'Kata Sandi'
        ]);

        return view('login', compact('validator'));
    }

    public function auth(Request $req)
    {
        $this->validate($req, [
            'sim_id'   => 'required|exists:accounts,sim_id',
            'password' => 'required'
        ]);

        $remember = false;
        if($req->has('remember-me')) {
            $remember = true;
        }

        $repoEmployee    = new EmployeeRepository;
        // $repoTerminate   = new GetEmployeeStatus;
        // $resultTerminate = $repoTerminate->getEmployeeStatus(['simid' => $req->sim_id]);

        // $setting = Setting::where('code', 'cut_off_periode')->first();
        // $cutOff  = 0;
        // if($setting) {
        //     $cutOff = $setting->value;
        // }
        // else {
        //     $cutOff = 3;
        // }

        // if ($resultTerminate->termin_date) {
        //     $terminateDate = Carbon::createFromFormat('Y-m-d H:i:s', $resultTerminate->termin_date);

        //     $diff = $terminateDate->diff(now());

        //     if($diff->m >= $cutOff) {
        //         Session::flash('toast-error', 'Anda tidak bisa lagi mengakses sistem ini');
        //         return redirect()->route('login');
        //     }
        // }

        $employee = null;

        if(Auth::attempt(['sim_id' => $req->sim_id, 'password' => $req->password], $remember)) {
            if(!auth()->user()->actived_at) {
                $employee = $repoEmployee->updateEmployeeBpr(auth()->user()->sim_id);
            }
            else {
                $employee = $repoEmployee->updateEmployeeBpr(auth()->user()->sim_id, true);
            }

            $repoEmployee->updateSignature();

            $repoAssignment = new AssignmentRepository;
            $repoAssignment->updateAssignmentBpr($employee);

            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'Employee Login',
                'description' => 'Employee with SIMID : ' . auth()->user()->sim_id,
            ]);

            // Session::flash('employee-data', $employee);
            return redirect()->intended('dashboard');
        }

        Session::flash('toast-error', 'Password anda salah atau akun anda sedang tidak aktif');
        return redirect()->route('login');
    }

    public function logout()
    {
        $role = auth()->user()->role;

        auth()->logout();

        if($role == 'super-admin' || $role == 'admin') {
            return redirect()->route('admin.login');
        }

        return redirect()->route('login');
    }

    public function forgotPassword()
    {
        $validator = JsValidator::make([
            'sim_id' => 'required|exists:accounts,sim_id'
        ],[],[
            'sim_id' => 'SIM ID'
        ]);

        return view('forgot-password', compact('validator'));
    }

    public function forgotPasswordPost(Request $req)
    {
        $this->validate($req, [
            'sim_id' => 'required|exists:accounts,sim_id'
        ]);

        $account = Accounts::where('sim_id', $req->sim_id)->first();
        if (!$account || !$account->email) {
            Session::flash('toast-error', 'Akun anda tidak ditemukan');
            return redirect()->route('login');
        }
        else if (!$account->email) {
            Session::flash('toast-error', 'Akun anda tidak memiliki email, silahkan hubungi admin');
            return redirect()->route('login');
        }

        $token = Str::random(60);
        $data = [
            'email'      => $account->email,
            'token'      => $token,
            'expired_at' => now()->addHours(1)
        ];
        $crypted = Crypt::encrypt($data);

        $account->forgot_token = $token;
        $account->save();

        Mail::to($account->email)->send(new ForgotPassword($crypted));

        Session::flash('toast-success', 'Silahkan cek email anda untuk mengubah kata sandi anda');
        return redirect()->route('login');
    }

    public function recoveryPassword(Request $req)
    {
        try {
            $token   = $req->token;
            $decrypt = Crypt::decrypt($token);
            $account = Accounts::where('email', $decrypt['email'])->where('forgot_token', $decrypt['token'])->where('role', 'employee')->first();

            if (!$account) {
                Session::flash('toast-error', 'Akun anda tidak ditemukan');
                return redirect()->route('login');
            }

            $validator = JsValidator::make([
                'password' => 'required|confirmed'
            ],[],[
                'password' => 'Kata Sandi'
            ]);
        }
        catch (\Throwable $e) {
            Session::flash('toast-error', 'Token tidak valid');
            return redirect()->route('login');
        }

        return view('reset-password', compact('token', 'validator'));
    }

    public function recoveryPasswordPost(Request $req)
    {
        try {
            $this->validate($req, [
                'password' => 'required|confirmed'
            ]);

            $token   = $req->token;
            $decrypt = Crypt::decrypt($token);
            $account = Accounts::where('email', $decrypt['email'])->where('forgot_token', $decrypt['token'])->where('role', 'employee')->first();

            if (!$account) {
                Session::flash('toast-error', 'Akun anda tidak ditemukan');
                return redirect()->route('login');
            }

            $account->password = bcrypt($req->password);
            $account->save();
        }
        catch (\Throwable $e) {
            Session::flash('toast-error', 'Token tidak valid');
            return redirect()->route('login');
        }

        Session::flash('toast-success', 'Kata sandi berhasil diubah, silahkan login kembali');
        return redirect()->route('login');
    }
}
