<?php

namespace App\Http\Controllers;

use App\Repositories\BprGoApi\RestBprGo;
use JsValidator;
use Illuminate\Http\Request;
use App\Repositories\BankRepository;
use Illuminate\Support\Facades\Session;

class BankController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new BankRepository;
    }

    public function index()
    {
        $validator = JsValidator::make($this->repository->validator(),[],[
            'name_of_bank'       => 'Nama bank',
            'account_number'     => 'Nomor rekening',
            'account_name'       => 'Nama akun',
            'bank_account_image' => 'Foto buku tabungan'
        ],'#form-rekening');

        $tabs = 'rekening';
        $bank = $this->repository->getDataBank();
        $rest = new RestBprGo;
        $endpoint = env('MASTER_BPR');
        $banks = $rest->bprApi($endpoint.'/EBA', 'GET')->data;

        return view('pages.employee.informasi-karyawan', compact('tabs', 'bank', 'validator', 'banks'));
    }

    public function store(Request $req)
    {
        $this->validate($req, $this->repository->validator());

        $result = $this->repository->updateData($req);
        if(!$result) {
            Session::flash('toast-error', 'Profile bank gagal diupdate');
        }
        elseif ($result === 'restricted') {
            Session::flash('toast-error', 'Anda belum bisa melakukan perubahan data, perubahan data sebelumnya masih menunggu verifikasi admin');
        }
        elseif ($result === 'no-updated') {
            Session::flash('toast-error', 'Tidak ada perubahan pada data profile bank kamu');
        }
        else {
            Session::flash('toast-success', 'Profile bank anda berhasil diupdate');
        }

        return redirect()->route('pe.info.rekening');
    }
}
