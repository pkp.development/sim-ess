<?php

namespace App\Http\Controllers;

use App\Repositories\AssignmentRepository;
use App\Repositories\BenefitRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BenefitController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new BenefitRepository;
    }

    public function index()
    {
        $tabs    = 'benefit';
        $benefit = $this->repository->getBenefit();
        $bpjstk_ = $this->repository->getBpjsKetenagakerjaan();

        return  view('pages.employee.informasi-karyawan', compact('tabs', 'benefit', 'bpjstk_'));
    }

    public function bpjsKesehatanDownload()
    {
        $benefit = $this->repository->getBenefit();
        if($benefit) {
            if($benefit->image_bpjs_kesehatan) {
                $headers = [
                    'Content-Type'        => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="'. $benefit->no_bpjs_kesehatan .'.pdf"',
                ];

                $repoAssign = new AssignmentRepository;
                $repoAssign->saveLastDownload('bpjs_kes');

                return response()->download(
                    storage_path('app/'.$benefit->image_bpjs_kesehatan), $benefit->no_bpjs_kesehatan . '.pdf', $headers
                );
            }
        }

        Session::flash('toast-error', 'BPJS Kesehatan tidak ditemukan');
        return redirect()->route('pe.info.benefit');
    }

    public function bpjsKetenagakerjaanDownload($notkj)
    {
        $result = $this->repository->getBpjsKetenagakerjaan($notkj);

        if($result) {
            if($result->image) {
                $headers = [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="'. $result->card_number .'.pdf"',
                ];

                $repoAssign = new AssignmentRepository;
                $repoAssign->saveLastDownload('bpjs_tkj');

                return response()->download(
                    storage_path('app/'.$result->image), $result->card_number .'.pdf', $headers
                );
            }
        }

        Session::flash('toast-error', 'BPJS Kesehatan tidak ditemukan');
        return redirect()->route('pe.info.benefit');
    }
}
