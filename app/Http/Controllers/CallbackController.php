<?php

namespace App\Http\Controllers;

use App\Models\Config\SmsCallback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CallbackController extends Controller
{
    public function sms(Request $req)
    {
        DB::beginTransaction();
        try {
            $decode = json_decode(file_get_contents("php://input"));
            $callback = SmsCallback::where('trxid', $decode->trxid)->first();
            if ($callback) {
                $callback->update([
                    'code'  => $decode->code,
                    'phone' => $decode->phone
                ]);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        $data = json_decode(file_get_contents("php://input"));
        Print_r($data);
    }
}
