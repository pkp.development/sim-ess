<?php

namespace App\Http\Controllers;

use App\Models\Config\AuditTrail;
use App\Repositories\BprGoApi\RestBprPhp;
use Ramsey\Uuid\Uuid;
use Storage;
use Illuminate\Http\Request;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use App\Repositories\DocumentRepository;
use GuzzleHttp\Exception\ClientException;
use App\Repositories\AssignmentRepository;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Session;

class DocumentController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new DocumentRepository;
    }

    public function index()
    {
        try {
            if (auth()->user()->assignment->signed_at) {
                return redirect()->route('pe.dashboard');
            }

            $repo = new AssignmentRepository;
            if(!auth()->user()->profile->nik){
                die('This user doesn\'t has NIK/KTP in profile');
            }

            $contractBpr = $this->repository->checkEligibleContractInBprByNik(auth()->user()->profile->nik);
//            print_r($contractBpr);die;

            if($contractBpr->status != 'OK' or $contractBpr->code != '200'){
                return view('document')->with('error', $contractBpr->data);
            }

            if (Storage::disk('public')->exists('public/tmp-contract/kontrak-' . $contractBpr->data->contract_id . '.pdf')) {
                Storage::disk('public')->delete('public/tmp-contract/kontrak-' . $contractBpr->data->contract_id . '.pdf');
            }

            $response_file = $this->repository->checkContractExists($contractBpr->data->contract_id, 'generate');

            if(!$response_file->status or $response_file->code != '200'){
                return view('document')->with('error', $response_file->msg);
            }

             if (!file_exists(
                 storage_path('app/public/tmp-contract/kontrak-' . $contractBpr->data->contract_id . '.pdf'))
             ) {

                 $this->repository->getStreamContractFile($contractBpr->data->contract_id);
             }
        }
        catch (ClientException $e) {
            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        $contractId = $contractBpr->data->contract_id;

        return view('document', compact('contractId'));
    }

    public function signContract(Request $req)
    {
        $result = $this->repository->signContract($req);

        if (is_array($result)) {
            Session::flash('toast-error', 'Harap cek kembali kontrak anda yang harus ditanda tangani');
            return redirect()->route('pe.dashboard');
        }

        return redirect()->route('pe.dashboard');
    }

    public function storeCustomTtd(Request $req){

        $outputPath    = storage_path('app/public/signature/' . $req->simid.'.png');
        $data = $req->data;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        if (file_exists($outputPath)) {
            rename($outputPath, storage_path('app/public/signature/'.$req->simid . '_'.date('YmdHis').'.png'));
        }

        file_put_contents($outputPath, $data);
        return ['status' => 200];
//        return view('test');
    }

    public function test(){

        return view('test');
    }

}
