<?php

namespace App\Http\Controllers;

use App\Repositories\BprGoApi\RestBprGo;
use JsValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Claim\Outpatient;
use App\Models\Accounts\Families;
use App\Models\Accounts\Profiles;
use App\Models\Config\Notification;
use Intervention\Image\Facades\Image;
use App\Repositories\FamilyRepository;
use App\Repositories\SipasApi\GetOther;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\EmployeeRepository;

class EmployeeController extends Controller
{
    protected $repository, $familyRepository;

    public function __construct(EmployeeRepository $repository)
    {
        $this->repository       = $repository;
        $this->familyRepository = new FamilyRepository;
    }

    public function index(Request $req)
    {
        $validator = JsValidator::make($this->repository->validator(),[
            'npwp_image.required_with' => 'Foto NPWP wajib diisi'
        ],[
            'profile_picture'       => 'Foto Profile',
            'selfie_identity_image' => 'Foto Selfie dengan KTP',
            'name'                  => 'Nama',
            'date_of_birth'         => 'Tanggal lahir',
            'place_of_birth'        => 'Tempat lahir',
            'gender'                => 'Jenis kelamin',
            'nik'                   => 'NIK',
            'identity_image'        => 'Foto identitas',
            'inp_identity_image'    => 'Foto identitas',
            'address_identity'      => 'Alamat identitas',
            'zip_code_identity'     => 'Kodepos identitas',
            'city_identity'         => 'Kota identitas',
            'province_identity'     => 'Provinsi identitas',
            'religion'              => 'Agama',
            'phone_number'          => 'No. Telepon',
            'email'                 => 'Email',
            'npwp_number'           => 'Nomor Npwp',
            'npwp_periode'          => 'Tahun Terbit Npwp',
            'npwp_image'            => 'Foto Npwp',
            'domicile_address'      => 'Alamat domisili',
            'domicile_zip_code'     => 'Kodepos domisili',
            'domicile_city'         => 'Kota domisili',
            'domicile_province'     => 'Provinsi domisili',
        ],'#form-profile');

//        $otherApi = new GetOther;

        $profile    = $this->repository->getProfile();
        $tmpCheck   = $this->repository->tmpProfile();
        $tabs       = 'profile';

        $rest = new RestBprGo;
        $endpoint = env('MASTER_REGION_GROUPED_BY_CITY');
        $region = $rest->bprApi($endpoint, 'GET')->data;

        return view('pages.employee.informasi-karyawan', compact('profile','tmpCheck','tabs','validator', 'region'));
    }

    public function updateProfile(Request $req)
    {
        $this->validate($req, $this->repository->validator());

        $updated = $this->repository->updateProfile($req);

        if(!$updated) {
            Session::flash('toast-error', 'Perubahan data gagal');
        }
        elseif ($updated === 'restricted') {
            Session::flash('toast-error', 'Anda belum bisa melakukan perubahan data, perubahan data sebelumnya masih menunggu verifikasi admin');
        }
        elseif ($updated === 'no-change') {
            Session::flash('toast-error', 'Tidak ada perubahan pada data profile kamu');
        }
        else {
            Session::flash('toast-success', 'Perubahan data anda akan kami verifikasi terlebih dahulu');
        }

        return redirect()->route('pe.info.karyawan');
    }

    public function getImage($type)
    {
        $profile = Profiles::with('account.assignment', 'account.bank_account', 'account.history_idcard')->where('account_id', auth()->user()->id)->first();

        if($type == 'identity') {
            $path = Storage::get($profile->identity_image);
        }
        if($type == 'npwp') {
            $path = Storage::get($profile->npwp_image);
        }
        if($type == 'profile') {
            $path = Storage::get($profile->profile_picture);
        }
        if($type == 'id_card') {
            $path = Storage::get($profile->account->assignment->id_card_image);
        }
        if($type == 'idcard_selfie') {
            $path = Storage::get($profile->account->history_idcard->idcard_image);
        }
        if($type == 'kartu_keluarga') {
            $path = Storage::get($profile->family_card_image);
        }
        if($type == 'rekening') {
            $path = Storage::get($profile->account->bank_account->bank_account_image);
        }
        if($type == 'selfie_image') {
            $path = Storage::get($profile->selfie_image);
        }

        return Image::make($path)->response();
    }

    public function compareProfile($type, $tmpUuid)
    {
        if ($type == 'profile') {
            $profile = $this->repository->getTemporaryProfile($tmpUuid);

            if (!$profile) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return redirect()->route('pe.info.karyawan');
            }

            if ($profile->status == 1 || $profile->status == 2) {
                $profile->pe_read = Carbon::now();
                $profile->save();
            }

            return redirect()->route('pe.info.karyawan');
        }
        if ($type == 'family') {
            $profile = $this->familyRepository->getTmpFamily($tmpUuid);

            if (!$profile) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return redirect()->route('pe.info.keluarga');
            }

            if ($profile->status == 1 || $profile->status == 2) {
                $profile->pe_read = Carbon::now();
                $profile->save();
            }

            return redirect()->route('pe.info.keluarga');
        }
        if ($type == 'families') {
            $profile = $this->familyRepository->getFamily('uuid', $tmpUuid);

            if (!$profile) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return redirect()->route('pe.info.keluarga');
            }

            if ($profile->status == 1 || $profile->status == 2) {
                $profile->pe_read = Carbon::now();
                $profile->save();
            }

            return redirect()->route('pe.info.keluarga');
        }
        if ($type == 'bank') {
            $profile = $this->repository->getTemporaryProfile($tmpUuid);

            if (!$profile) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return redirect()->route('pe.info.rekening');
            }

            if ($profile->status == 1 || $profile->status == 2) {
                $profile->pe_read = Carbon::now();
                $profile->save();
            }

            return redirect()->route('pe.info.rekening');
        }

        if ($type == 'outpatient') {
            $profile = Outpatient::where('uuid', $tmpUuid)->first();

            if (!$profile) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return redirect()->route('pe.rawat-jalan.index');
            }

            if ($profile->status == 1 || $profile->status == 2) {
                $profile->pe_read = Carbon::now();
                $profile->save();
            }

            return redirect()->route('pe.rawat-jalan.index');
        }
    }

    public function readAll()
    {
        $notifications = Notification::with('account.profile')->where(function($q){
            $q->where('status', 1)->orWhere('status', 2);
        })->where('pe_read', null)->where('account_id', auth()->user()->id)->latest()->get();

        foreach ($notifications as $notification) {
            if ($notification->type == 'profile') {
                $profile = $this->repository->getTemporaryProfile($notification->uuid);

                if ($profile) {
                    if ($profile->status == 1 || $profile->status == 2) {
                        $profile->pe_read = Carbon::now();
                        $profile->save();
                    }
                }
            }
            if ($notification->type == 'family') {
                $profile = $this->familyRepository->getTmpFamily($notification->uuid);

                if ($profile) {
                    if ($profile->status == 1 || $profile->status == 2) {
                        $profile->pe_read = Carbon::now();
                        $profile->save();
                    }
                }
            }
            if ($notification->type == 'families') {
                $profile = $this->familyRepository->getFamily('uuid', $notification->uuid);

                if ($profile) {
                    if ($profile->status == 1 || $profile->status == 2) {
                        $profile->pe_read = Carbon::now();
                        $profile->save();
                    }
                }
            }
            if ($notification->type == 'bank') {
                $profile = $this->repository->getTemporaryProfile($notification->uuid);

                if ($profile) {
                    if ($profile->status == 1 || $profile->status == 2) {
                        $profile->pe_read = Carbon::now();
                        $profile->save();
                    }
                }
            }

            if ($notification->type == 'outpatient') {
                $profile = Outpatient::where('uuid', $notification->uuid)->first();

                if ($profile) {
                    if ($profile->status == 1 || $profile->status == 2) {
                        $profile->pe_read = Carbon::now();
                        $profile->save();
                    }
                }
            }
        }

        return redirect()->back();
    }
}
