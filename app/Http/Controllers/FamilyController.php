<?php

namespace App\Http\Controllers;

use App\Repositories\BprGoApi\RestBprGo;
use JsValidator;
use Illuminate\Http\Request;
use App\Repositories\FamilyRepository;
use App\Repositories\SipasApi\GetOther;
use Illuminate\Support\Facades\Session;

class FamilyController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new FamilyRepository;
    }

    public function index()
    {
        $validator = JsValidator::make([
            'family_card_number' => 'numeric|digits:16',
            'family_card_image'    => 'image|max:4096'
        ],[],[
            'family_card_number' => 'Nomor Kartu Keluarga',
            'family_card_image'    => 'Foto Kartu Keluarga'
        ],'#form-data');

        $validator2 = JsValidator::make([
            'name'           => 'required|max:50',
            'nik'            => 'required|digits:16|numeric',
            'place_of_birth' => 'required|max:50',
            'date_of_birth'  => 'required',
            'phone_number'   => 'required|numeric|digits_between:10,15',
            'jenis_kelamin'  => 'required',
            'relation'       => 'required',
        ],[],[
            'name'           => 'Nama',
            'nik'            => 'NIK',
            'place_of_birth' => 'Tempat lahir',
            'date_of_birth'  => 'Tanggal lahir',
            'phone_number'   => 'No. Telepon',
            'jenis_kelamin'  => 'Jenis kelamin',
            'relation'       => 'Hubungan Keluarga',
        ],'#form-family');
        $tabs     = 'keluarga';
        $profile  = auth()->user()->profile;
        $families = $this->repository->getFamilies();

        $rest = new RestBprGo;
        $endpoint = env('MASTER_REGION_GROUPED_BY_CITY');
        $region = $rest->bprApi($endpoint, 'GET')->data;

        $endpoint = env('MASTER_BPR');
        $marital = $rest->bprApi($endpoint.'/ESPE', 'GET')->data;

        return view('pages.employee.informasi-karyawan', compact('tabs', 'profile', 'families', 'validator', 'validator2', 'region', 'marital'));
    }

    public function updateData(Request $req)
    {
        $this->validate($req, [
            'nomor_kartu_keluarga' => 'numeric|digits:16',
            'image_kartu_keluarga' => 'image|max:4096'
        ]);

        $result = $this->repository->updateData($req);

        if(!$result) {
            Session::flash('toast-error', 'Profile keluarga gagal diupdate');
        }
        elseif ($result === 'restricted') {
            Session::flash('toast-error', 'Anda belum bisa melakukan perubahan data, perubahan data sebelumnya masih menunggu verifikasi admin');
        }
        elseif ($result === 'no-updated') {
            Session::flash('toast-error', 'Tidak ada perubahan pada data profile keluarga kamu');
        }
        else {
            Session::flash('toast-success', 'Profile keluarga anda berhasil diupdate');
        }

        return redirect()->route('pe.info.keluarga');
    }

    public function getFamily(Request $req, $uuid)
    {
        $result = $this->repository->getFamily('uuid', $uuid);

        if ($req->ajax()) {
            if ($result->status == 0) {
                return response()->json("restricted");
            }

            return response()->json($result);
        }
    }

    public function updateFamily(Request $req)
    {
        $this->validate($req, [
            'name'           => 'required|max:50',
            'nik'            => 'required|digits:16|numeric',
            'place_of_birth' => 'required|max:50',
            'date_of_birth'  => 'required',
            'phone_number'   => 'nullable|numeric|digits_between:10,15',
            'jenis_kelamin'  => 'required',
            'relation'       => 'required',
        ]);

        $result = $this->repository->updateFamily($req);

        if(!$result) {
            Session::flash('toast-error', 'Profile keluarga gagal diupdate');
        }
        elseif ($result === 'restricted') {
            Session::flash('toast-error', 'Anda harus melengkapi data kartu keluarga terlebih dahulu.');
        }
        else {
            Session::flash('toast-success', 'Data keluarga anda berhasil diupdate');
        }

        return redirect()->route('pe.info.keluarga');
    }

    public function patchFamily(Request $req, $uuid)
    {
        $this->validate($req, [
            'name'           => 'required|max:50',
            'nik'            => 'required|digits:16|numeric',
            'place_of_birth' => 'required|max:50',
            'date_of_birth'  => 'required',
            'phone_number'   => 'nullable|numeric|digits_between:10,15',
            'jenis_kelamin'  => 'required',
            'relation'       => 'required',
        ]);

        $result = $this->repository->updateFamily($req, $uuid);

        if(!$result) {
            Session::flash('toast-error', 'Profile keluarga gagal diupdate');
        }
        elseif ($result === 'restricted') {
            Session::flash('toast-error', 'Anda harus melengkapi data kartu keluarga terlebih dahulu.');
        }
        else {
            Session::flash('toast-success', 'Data keluarga anda berhasil diupdate');
        }

        return redirect()->route('pe.info.keluarga');
    }

    public function deleteFamily(Request $req, $uuid)
    {
        $result = $this->repository->updateFamily($req, $uuid, 'delete');

        if(!$result) {
            $message = 'Profile keluarga gagal diupdate';
            $code = 500;
        }
        elseif ($result === 'restricted') {
            $message = 'Anda harus melengkapi data kartu keluarga terlebih dahulu.';
            $code = 500;
        }
        else {
            $message = 'Data keluarga anda berhasil diupdate';
            $code = 200;
        }

        return response()->json($message, $code);
    }
}
