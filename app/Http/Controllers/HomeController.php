<?php

namespace App\Http\Controllers;

use JsValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Config\PeraturanPerusahaan;
use App\Repositories\AssignmentRepository;
use Carbon\Carbon;
use Requests;

class HomeController extends Controller
{
    protected $respository;
    public function __construct()
    {
        $this->respository = new AssignmentRepository;
    }
    
    public function dashboard()
    {
        $repoAssignment = new AssignmentRepository;
        $activeContract = $repoAssignment->getActiveContract();
        $isBpr = 1;

        if (auth()->user()->is_bpr == 0) {
            $isBpr                   = 0;
            auth()->user()->is_bpr   = 1;
            auth()->user()->bpr_sync = Carbon::now();
            auth()->user()->save();
        }

        return view('pages.dashboard', compact('activeContract', 'isBpr'));
    }

    public function changePassword()
    {
        $validator = JsValidator::make([
            'password'     => 'required',
            'new_password' => 'required|min:6|confirmed'
        ], [
            'new_password.confirmed' => 'Konfirmasi password baru salah'
        ], [
            'password'     => 'Password',
            'new_password' => 'Password Baru'
        ], '#form-change-password');

        return view('pages.change-password', compact('validator'));
    }

    public function changePasswordPost(Request $req)
    {
        $this->validate($req, [
            'password'     => 'required',
            'new_password' => 'required|min:6|confirmed'
        ]);

        DB::beginTransaction();
        try {
            if (Hash::check($req->password, auth()->user()->password)) {
                auth()->user()->password = bcrypt($req->new_password);
                auth()->user()->save();
            }
            else {
                Session::flash('toast-error', 'Password anda salah');
                return redirect()->route('change-password');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Session::flash('toast-success', 'Perubahan password anda berhasil');
        return redirect()->route('change-password');
    }

    public function peraturanPerusahaan(Request $req)
    {
        if ($req->ajax()) {
            $peraturan = PeraturanPerusahaan::select('peraturan_perusahaan.*');

            return DataTables::of($peraturan)
                ->editColumn('effective_date', function ($data) {
                    return $data->effective_date->locale('id_ID')->format('d F Y');
                })
                ->addColumn('download', function ($data) {
                    if ($data->type == 'file') {
                        $html = '<a href="'. asset('storage/' . $data->file) .'" target="_blank" class="action-icon btn-edit" title="Unduh" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-file-download-outline"></i></a>';
                    }
                    elseif ($data->type == 'link') {
                        $html = '<a href="//'. $data->file .'" target="_blank" class="action-icon btn-edit" title="Unduh" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-file-download-outline"></i></a>';
                    }

                    return $html;
                })
                ->rawColumns(['download'])
                ->make(true);
        }

        return view('pages.peraturan-perusahaan');
    }
   
    public function surat()
    { 
        $simID  = auth()->user()->sim_id ; 
        $url    = "http://147.139.199.220:3400/api/datasrk/" . $simID ;
        $curl   = curl_init();
        curl_setopt_array($curl, array(
            //CURLOPT_URL => "http://147.139.199.220:3400/api/datasrk/PEG21100615",
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
           // print_r($response);
        }
        $response  = json_decode($response);
        $total     = $response->jumlah ;
        $total-- ;
       
        for ($x = 0; $x <= $total; $x++) {
           

        }
       
        $repoAssignment = new AssignmentRepository;
        $activeContract = $repoAssignment->getActiveContract();
        $isBpr = 1;

        if (auth()->user()->is_bpr == 0) {
            $isBpr                   = 0;
            auth()->user()->is_bpr   = 1;
            auth()->user()->bpr_sync = Carbon::now();
            auth()->user()->save();
        }

        //return view('pages.dwnsrk', compact('activeContract', 'isBpr','response'));
        // coba dari sini
        $tabs       = 'penempatan';
        $histories  = []; // $this->respository->getHistoryContract();
        $assignment = $this->respository->getActiveContract();
        $status     = [];// $this->respository->getStatusEmployee();
        // echo "<pre>";
        // print_r(auth()->user()->sim_id);
        // die();
        return view('pages.employee.informasi-karyawan2', compact('tabs', 'histories', 'assignment', 'status','response'));

    }


        public function php1nf0(){
		        phpinfo();
			    }
}
