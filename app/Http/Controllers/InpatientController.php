<?php

namespace App\Http\Controllers;

use App\Repositories\InpatientRepository;
use Illuminate\Http\Request;

class InpatientController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new InpatientRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatable();
        }

        return view('pages.inpatient.index');
    }

    public function detail(Request $req, $uuid)
    {
        if ($req->ajax()) {
            return $this->repository->historyDatatable($uuid);
        }

        $result = $this->repository->get($uuid);

        if (!$result) {
            Session::flash('toast-error', 'Klaim rawat inap tidak ditemukan');
            return redirect()->route('pe.inpatient.index');
        }

        return view('pages.inpatient.detail', compact('result'));
    }
}
