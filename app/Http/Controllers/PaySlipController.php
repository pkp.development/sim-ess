<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\Repositories\PaySlipRepository;
use Illuminate\Support\Facades\Session;
use App\Repositories\AssignmentRepository;

class PaySlipController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new PaySlipRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatables($req);
        }

        return view('pages.payslip.index');
    }

    public function downloadPayslip($uuid)
    {
        $data = $this->repository->downloadPaySlipData($uuid);
        if (!$data) {
            Session::flash('toast-error', 'Perusahaan tidak dtemukan');

            return redirect()->route('pe.info.pay-slip');
        }
        // elseif ($data == 'restrict') {
        //     Session::flash('toast-error', 'Silahkan lengkapi data NPWP anda terlebih dahulu');

        //     return redirect()->route('pe.info.pay-slip');
        // }
        // elseif ($data == 'restrict_selfie') {
        //     Session::flash('toast-error', 'Silahkan lengkapi data Selfie KTP anda terlebih dahulu');

        //     return redirect()->route('pe.info.pay-slip');
        // }

        $data['localIp'] = (env('IP_LOCAL')) ? env('IP_LOCAL') : asset("");

        $pdf = PDF::loadView('payslip2', $data)->setPaper('a4');

        $monthName = $data['payslip']->month_name;

        $repoAssign = new AssignmentRepository;
        $repoAssign->saveLastDownload('payslip', $data['payslip']->month_periode, $data['payslip']->year_periode);

        return $pdf->download('payslip-'. $monthName .'.pdf');
    }
}
