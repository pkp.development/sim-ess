<?php

namespace App\Http\Controllers;

use App\Repositories\BprApi\DataPlafond;
use App\Repositories\RawatJalanRepository;
use App\Repositories\SipasApi\RawatJalan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use JsValidator;

class RawatJalanController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new RawatJalanRepository;
    }

    public function index(Request $req)
    {
        if ($req->ajax()) {
            return $this->repository->datatables();
        }

        $validator = JsValidator::make([
            'doctor_name'              => 'required|max:50',
            'patient_name'             => 'required|max:50',
            'nominal'                  => 'required|not_in:0',
            'receipt_date'             => 'required',
            'description'              => 'required|max:150',
            'receipt_file'             => 'required|max:4096|image',
            'doctor_prescription_file' => 'required|max:4096|image',
        ], [
            'nominal.not_in' => 'Nominal tidak boleh 0'
        ], [
            'doctor_name'              => 'Nama Dokter',
            'patient_name'             => 'Nama Pasien',
            'nominal'                  => 'Nominal Klaim',
            'receipt_date'             => 'Tanggal Kwitansi',
            'description'              => 'Keterangan',
            'receipt_file'             => 'File Kwitansi',
            'doctor_prescription_file' => 'File Resep Dokter',
        ], '#form-rawat-jalan');

        $api          = new DataPlafond;
        $sisa_plafond = $api->getPlafondData(auth()->user()->sim_id)->benefit; // $api->getPlafond(auth()->user()->sim_id)->sisa_plafon;
        $rawat_jalan  = []; // $api->getRawatJalan(auth()->user()->sim_id);

        return view('pages.rawat_jalan.index', compact('sisa_plafond', 'validator', 'rawat_jalan'));
    }

    public function detail($uuid)
    {
        $data = $this->repository->getData($uuid);

        if (!$data) {
            return response()->json(null, 404);
        }

        $data->date    = $data->receipt_date->format('d F Y');
        $data->nominal = number_format($data->nominal);

        return response()->json($data);
    }

    public function store(Request $req)
    {
        $this->validate($req, [
            'doctor_name'              => 'required|max:50',
            'patient_name'             => 'required|max:50',
            'nominal'                  => 'required|not_in:0',
            'receipt_date'             => 'required',
            'description'              => 'required|max:150',
            'receipt_file'             => 'required|max:4096|image',
            'doctor_prescription_file' => 'required|max:4096|image',
        ]);

        if (str_replace(',', '', $req->nominal) == 0) {
            Session::flash('toast-error', 'Nominal tidak boleh 0');
            return redirect()->route('pe.rawat-jalan.index');
        }

        if (!$this->repository->checkBalance(str_replace(',', '', $req->nominal))) {
            Session::flash('toast-error', 'Sisa plafond anda tidak mencukupi untuk melakukan klaim');
            return redirect()->route('pe.rawat-jalan.index');
        }

        $receipt_date = Carbon::createFromFormat('Y-m-d', $req->receipt_date);
        $diff = $receipt_date->diff(now());

        if ($diff->m >= 3) {
            Session::flash('toast-error', 'Batas waktu kwitansi maksimal 3 bulan');
            return redirect()->route('pe.rawat-jalan.index');
        }

        if (!$this->repository->checkBalance(str_replace(',', '', $req->nominal))) {
            Session::flash('toast-error', 'Sisa plafond anda tidak mencukupi untuk melakukan klaim');
            return redirect()->route('pe.rawat-jalan.index');
        }

        $this->repository->save($req);

        Session::flash('toast-success', 'Klaim rawat jalan berhasil ditambahkan');
        return redirect()->route('pe.rawat-jalan.index');
    }
}
