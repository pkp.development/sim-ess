<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Repositories\SipasApi\GetBank;
use Illuminate\Http\Request;

class ServiceBankController extends Controller
{
    protected $sipasAPI;

    public function __construct()
    {
        $this->sipasAPI = new GetBank;
    }

    public function getBank(Request $req)
    {
        $filter = [
            "bank_name" => [
                "like" => strtoupper($req->nama)
            ]
        ];

        if($req->has('bank_code')) {
            $filter['bank_code'] = $req->bank_code;
        }

        $result = $this->sipasAPI->getBank($req->page, $filter);
        $arr_result = null;

        foreach($result->result as $res) {
            $arr_result[] = [
                'id'        => $res->bank_code,
                'text'      => $res->bank_name,
                'min_digit' => $res->min_digit_account,
                'max_digit' => $res->max_digit_account
            ];
        }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => ($result->last_page > $req->page) ? true : false
            ]
        ];

        return response()->json($results);
    }
}
