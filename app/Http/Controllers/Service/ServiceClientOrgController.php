<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Repositories\BprApi\MasterClientCompany;
use App\Repositories\SipasApi\GetClientOrganization;
use Illuminate\Http\Request;

class ServiceClientOrgController extends Controller
{
    protected $sipasAPI;

    public function __construct()
    {
        $this->sipasAPI = new GetClientOrganization;
    }

    public function getCompanies(Request $req)
    {
        // $filter = [
        //     "organization_name" => [
        //         "like" => strtoupper($req->nama)
        //     ]
        // ];

        // $result = $this->sipasAPI->getCompany($req->page, $filter);

        $api        = new MasterClientCompany;
        $pagination = false;
        $arr_result = null;

        if ($req->has('nama') && $req->nama) {
            $result = $api->getClientCompany(1, $req->nama);
            foreach($result as $res) {
                $arr_result[] = [
                    'id'   => $res->id,
                    'text' => $res->nama
                ];
            }
            $pagination = false;
        }
        else {
            $result = $api->getClientCompany($req->page);
            foreach($result->data as $res) {
                $arr_result[] = [
                    'id'   => $res->id,
                    'text' => $res->nama
                ];
            }
            $pagination = ($result->current_page < $result->last_page) ? true : false;
        }

        // foreach($result['results']->result as $res) {
        //     $arr_result[] = [
        //         'id'   => $res->id,
        //         'text' => $res->organization_name
        //     ];
        // }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => $pagination
            ]
        ];

        return response()->json($results);
    }

    public function getBranch(Request $req)
    {
        $filter = [
            "id_org_parent" => $req->company_id,
            "organization_name" => [
                "like" => strtoupper($req->nama)
            ]
        ];

        $result = $this->sipasAPI->getBranch($req->page, $filter);
        $arr_result = null;

        foreach($result['results']->result as $res) {
            $arr_result[] = [
                'id'   => $res->id,
                'text' => $res->organization_name
            ];
        }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => ($result['results']->last_page > $req->page) ? true : false
            ]
        ];

        return response()->json($results);
    }
}
