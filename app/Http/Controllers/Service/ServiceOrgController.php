<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Models\Organization\SimCompanies;
use App\Repositories\BprApi\MasterSimBranch;
use App\Repositories\SipasApi\GetSimOrganization;
use Illuminate\Http\Request;

class ServiceOrgController extends Controller
{
    protected $sipasAPI;

    public function __construct()
    {
        $this->sipasAPI = new GetSimOrganization;
    }

    public function getCompanies(Request $req)
    {
        $filter = [
            "companyname" => [
                "like" => strtoupper($req->nama)
            ]
        ];

        $result = $this->sipasAPI->getCompanies($req->page, $filter);
        $arr_result = null;

        foreach($result->result as $res) {
            $arr_result[] = [
                'id'   => $res->id,
                'text' => $res->companyname
            ];
        }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => ($result->last_page > $req->page) ? true : false
            ]
        ];

        return response()->json($results);
    }

    public function getBranch(Request $req)
    {
        $api          = new MasterSimBranch;
        $pagination   = false;
        $simCompanies = SimCompanies::get();
        $arr_result   = null;

        if ($req->has('nama') && $req->nama) {
            $result = $api->getSimBranch(1, $req->nama);
            foreach ($result  as $res) {
                $simCompany = $simCompanies->where('sipas_id', $res->id_perusahaan)->first();
                $abbreviation = null;
                if ($simCompany) {
                    $abbreviation = " (" . $simCompany->abbreviation . ")";
                }

                $arr_result[] = [
                    'id'   => $res->id,
                    'text' => $res->cabang . $abbreviation,
                ];
            }
            $pagination = false;
        }
        else {
            $result = $api->getSimBranch($req->page);
            foreach ($result->data  as $res) {
                $simCompany = $simCompanies->where('sipas_id', $res->id_perusahaan)->first();
                $abbreviation = null;
                if ($simCompany) {
                    $abbreviation = " (" . $simCompany->abbreviation . ")";
                }

                $arr_result[] = [
                    'id'   => $res->id,
                    'text' => $res->cabang . $abbreviation,
                ];
            }
            $pagination = ($result->current_page < $result->last_page) ? true : false;
        }

        // $filter = [
        //     "companyname" => [
        //         "like" => strtoupper($req->nama)
        //     ]
        // ];

        // if ($req->has('company_id')) {
        //     $filter["id_org_parent"] = $req->company_id;
        // }

        // $result = $this->sipasAPI->getBranches($req->page, $filter);
        // $arr_result = null;

        // $simCompanies = SimCompanies::get();

        // foreach($result->result as $res) {
        //     $simCompany = $simCompanies->where('sipas_id', $res->sim_company_id)->first();
        //     $abbreviation = null;
        //     if ($simCompany) {
        //         $abbreviation = " (" . $simCompany->abbreviation . ")";
        //     }

        //     $arr_result[] = [
        //         'id'   => $res->id,
        //         'text' => $res->companyname . $abbreviation
        //     ];
        // }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => $pagination
            ]
        ];

        return response()->json($results);
    }
}
