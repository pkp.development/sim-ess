<?php

namespace App\Http\Controllers\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\BprApi\MasterCity;
use App\Repositories\BprApi\MasterProvince;
use App\Repositories\SipasApi\GetRegion;

class ServiceRegionController extends Controller
{
    protected $sipasAPI;

    public function __construct()
    {
//        $this->sipasAPI = new GetRegion;
    }

    public function getProvince(Request $req)
    {
        $api        = new MasterProvince;
        $pagination = false;

        if ($req->has('nama') && $req->nama) {
            $result = $api->getMasterProvince(1, $req->nama);
            foreach ($result  as $res) {
                $arr_result[] = [
                    'id' => $res->kodeData,
                    'text' => $res->namaData,
                ];
            }
            $pagination = false;
        }
        else {
            $result = $api->getMasterProvince($req->page);
            foreach ($result->data  as $res) {
                $arr_result[] = [
                    'id' => $res->kodeData,
                    'text' => $res->namaData,
                ];
            }
            $pagination = ($result->current_page < $result->last_page) ? true : false;
        }

        // $filter = [
        //     "province_name" => [
        //         "like" => strtoupper($req->nama)
        //     ]
        // ];

        // $result = $this->sipasAPI->getProvince($req->page, $filter);
        // $arr_result = null;

        // foreach($result['results']->result as $res) {
        //     $arr_result[] = [
        //         'id'   => $res->province_code,
        //         'text' => $res->province_name
        //     ];
        // }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => $pagination
            ]
        ];

        return response()->json($results);
    }

    public function getCity(Request $req)
    {
        $api        = new MasterCity;
        $pagination = false;

        if ($req->has('nama') && $req->nama) {
            $result = $api->getMasterCity(1, $req->nama, $req->province_code);
            foreach ($result  as $res) {
                $arr_result[] = [
                    'id' => $res->kodeData,
                    'text' => $res->namaData,
                ];
            }
            $pagination = false;
        }
        else {
            $result = $api->getMasterCity($req->page, null, $req->province_code);
            foreach ($result  as $res) {
                $arr_result[] = [
                    'id' => $res->kodeData,
                    'text' => $res->namaData,
                ];
            }
            $pagination = false;
        }

        // $filter = [
        //     "city_name" => [
        //         "like" => strtoupper($req->nama)
        //     ],
        //     "province_code" => $req->province_code
        // ];

        // $result = $this->sipasAPI->getCity($req->page, $filter);
        // $arr_result = null;

        // foreach($result['results']->result as $res) {
        //     $arr_result[] = [
        //         'id'   => $res->city_code,
        //         'text' => $res->city_name
        //     ];
        // }

        $results = [
            "results" => $arr_result,
            "pagination" => [
                "more" => $pagination
            ]
        ];

        return response()->json($results);
    }
}
