<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Spt\Spt;
use Illuminate\Http\Request;
use App\Repositories\SipasApi\GetJobs;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Organization\SimCompanies;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Repositories\AssignmentRepository;
use App\Repositories\SipasApi\GetHistoryContract;
use App\Repositories\SipasApi\GetSimOrganization;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SptController extends Controller
{
    public function index(Request $req)
    {
        if ($req->ajax()) {
            $model = Spt::with('account')->where('account_id', auth()->user()->id)->select('employee_spt.*');

            return DataTables::of($model)
                ->addColumn('action', function($data){
                    $html = action_btn(route('pe.download.spt', $data->uuid), [], 'fas fa-file-download');
                    return $html;
                })
                ->rawColumns(['account.status', 'action'])
                ->make(true);
        }

        return view('pages.spt.index');
    }

    public function downloadSpt($id)
    {
        try {
            $api = new GetSimOrganization;
            $jobsApi = new GetJobs;
            $jobs = null;

            $spt = Spt::with('content')->where('uuid', $id)->first();

            $profile = auth()->user()->profile;
            $assignment = auth()->user()->assignment;

            //if(($profile->nik == null || $profile->name == null || $profile->address_identity == null) && $assignment->terminated_date == NULL) {
            //    Session::flash('toast-error', 'Silahkan engkapi biodata profile anda terlebih dahulu');
            //    return redirect()->route('pe.info.karyawan');
            //}

            if(strlen($profile->npwp_number) == '15' or strlen($profile->npwp_number) == '16'){
                $profile->npwp_number = substr_replace($profile->npwp_number,".",12,0);
                $profile->npwp_number = substr_replace($profile->npwp_number,"-",9,0);
                $profile->npwp_number = substr_replace($profile->npwp_number,".",8,0);
                $profile->npwp_number = substr_replace($profile->npwp_number,".",5,0);
                $profile->npwp_number = substr_replace($profile->npwp_number,".",2,0);
            }

            if ($profile->npwp_number) {
                $npwpExp = explode('-', $profile->npwp_number);
                if(isset($npwpExp[1])) {
                    $npwpBck = explode('.', $npwpExp[1]);
                }else{
                    $npwpExp = null;
                    $npwpBck = null;
                }
            } else {
                $npwpExp = null;
                $npwpBck = null;
            }

            $company = null;

            if ($spt->content->{"company_abbr"}) {
                $company = SimCompanies::where('abbreviation', $spt->content->{"company_abbr"})->latest()->first();
            }

            if (!$company) {
                $companyApi = $api->getCompanyFromBranch(auth()->user()->assignment->sim_branch_id, 'id');
                $company = SimCompanies::where('sipas_id', $companyApi)->first();
            }

            if (!$company || (!$company->coordinator || !$company->name || !$company->npwp || !$company->coordinator_npwp || !$company->coordinator_sign)) {
                Session::flash('toast-error', 'Perusahaan tidak dtemukan');

                return redirect()->route('pe.info.spt');
            }

            // $jobs = $jobsApi->getJobs(['job_code' => auth()->user()->assignment->job_id]);
            // $jobs = $jobs->result[0];

            $data = [
                'spt'     => $spt,
                'npwpExp' => $npwpExp,
                'npwpBck' => $npwpBck,
                'profile' => $profile,
                'company' => $company,
                'localIp' => (env('IP_LOCAL')) ? env('IP_LOCAL') : asset("")
            ];

            $pdf = PDF::loadView('pages.spt-report2', $data)
                ->setOption('page-width', '210')
                ->setOption('page-height', '330');

            $filename = auth()->user()->sim_id . '_laporan-SPT' . '_' . $spt->year . '.pdf';

            $repoAssign = new AssignmentRepository;
            $repoAssign->saveLastDownload('spt', null, $spt->year);

            gc_collect_cycles();

            return $pdf->download($filename);
        }
        catch(\Exception $e) {
            throw $e;
        }
    }

    public function downloadSptTemplate()
    {
        $data = [
            'localIp' => (env('IP_LOCAL')) ? env('IP_LOCAL') : asset("")
        ];

        $pdf = PDF::loadView('spt-report', $data)
            ->setOption('page-width', '210')
            ->setOption('page-height', '330');

        return $pdf->stream('spt-template.pdf');
        // return Storage::download('spt.xlsx');
    }
}
