<?php

namespace App\Http\Middleware;

use Closure;

class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $super = false)
    {
        if (auth()->user()->role == 'super-admin' || auth()->user()->role == 'admin') {
            if ($super && auth()->user()->role == 'admin') {
                abort(403);
            }
            return $next($request);
        }

        abort(403);
    }
}
