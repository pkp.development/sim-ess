<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (Auth::check()) {
            $role = Auth::user()->role;
            if (! $request->expectsJson()) {
                if($role == 'super-admin' || $role == 'admin') {
                    return route('admin.login');
                }
                else {
                    return route('login');
                }
            }
        }
        else {
            if($request->route()->getPrefix() == 'admin') {
                return redirect()->route('admin.login');
            }
            else {
                return redirect()->route('login');
            }
        }
    }
}
