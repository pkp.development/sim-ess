<?php

namespace App\Http\Middleware;

use App\Models\Config\Setting;
use Closure;
use Illuminate\Support\Facades\Session;

class ContractValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $assignment = auth()->user()->assignment;

        if ($assignment->ess_status != 2 && $assignment->signed_at) {
            $setting = Setting::where('code', 'cut_off_periode')->first();
            $cutOff  = 0;
            if($setting) {
                $cutOff = $setting->value;
            }
            else {
                $cutOff = 3;
            }

            if (auth()->user()->assignment->terminated_date) {
                $terminatedDate = auth()->user()->assignment->terminated_date;
                $now = now();

                $diff = $terminatedDate->diff($now);

                if($diff->m > $cutOff) {
                    auth()->logout();

                    Session::flash('toast-error', 'Anda tidak bisa mengakses sistem ini');
                    return redirect()->route('pe.dashboard');
                }
            }

            return $next($request);
        }

        Session::flash('toast-error', 'Anda belum bisa akses menu sebelum tanda tangan kontrak');
        return redirect()->route('pe.dashboard');
    }
}
