<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class MiddlewareStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()) {
            if(auth()->user()->status != 1) {
                $role = auth()->user()->role;
                Session::flash('toast-error', 'Akun anda sedang tidak aktif');
                auth()->logout();
                if($role == 'super-admin' || $role == 'admin') {
                    return redirect()->route('admin.login');
                }
                else {
                    return redirect()->route('login');
                }
            }

            return $next($request);
        }
        else {
            if($request->route()->getPrefix() == 'admin') {
                return redirect()->route('admin.login');
            }
            else {
                return redirect()->route('login');
            }
        }
    }
}
