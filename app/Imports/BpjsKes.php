<?php

namespace App\Imports;

use App\Mail\BPJSNotification;
use App\Models\Accounts\Accounts;
use App\Models\Business\Benefits;
use Illuminate\Support\Facades\DB;
use App\Models\Tasks\BenefitDetail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class BpjsKes implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithCustomCsvSettings
{
    protected $path, $benefit_id;

    public function __construct($path, $benefit_id)
    {
        $this->path       = $path;
        $this->benefit_id = $benefit_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        DB::beginTransaction();
        try {
            $account     = Accounts::select('id', 'email')->with('profile')->where('sim_id', $row['simid'])->first();
            $fileBpjs    = null;
            $description = null;

            if($account) {
                // if (!ctype_digit(trim($row['no_bpjs_kesehatan']))) {
                //     throw new \Exception('No. BPJS Kesehatan harus angka.');
                // }

                if (strlen(trim($row['no_bpjs_kesehatan'])) > 20) {
                    throw new \Exception('No. BPJS Kesehatan tidak boleh lebih dari 20 digit.');
                }

                if (Storage::exists($this->path . '/' . $row['simid'] . '.pdf')) {

                    if(Storage::exists('images/bpjs_kesehatan/' . $row['simid'] . '.pdf')) {
                        Storage::delete('images/bpjs_kesehatan/' . $row['simid'] . '.pdf');
                    }

                    Storage::copy($this->path . '/' . $row['simid'] . '.pdf', 'images/bpjs_kesehatan/' . $row['simid'] . '.pdf');
                    $fileBpjs = 'images/bpjs_kesehatan/' . $row['simid'] . '.pdf';

                    $benefit = Benefits::where('account_id', $account->id)->first();
                    if ($benefit) {
                        $benefit->no_bpjs_kesehatan    = $row['no_bpjs_kesehatan'];
                        $benefit->image_bpjs_kesehatan = $fileBpjs;
                        $benefit->save();
                    }
                    else {
                        Benefits::create([
                            'account_id'           => $account->id,
                            'no_bpjs_kesehatan'    => $row['no_bpjs_kesehatan'],
                            'image_bpjs_kesehatan' => $fileBpjs
                        ]);
                    }
                }
                else {
                    throw new \Exception('Kartu BPJS Kesehatan tidak ditemukan dalam zip');
                }

                if ($account->email && (env('MAIL_FEATURE', false))) {
                   // Mail::to($account->email)->send(new BPJSNotification('kesehatan', $account->profile->name));
                }

                BenefitDetail::create([
                    'task_benefit_id' => $this->benefit_id,
                    'simid'           => $row['simid'],
                    'status'          => 'Passed',
                    'description'     => $description
                ]);
            }
            else {
                throw new \Exception('SIMID Not Found');
            }
        }
        catch (\Exception $e) {
            BenefitDetail::create([
                'task_benefit_id' => $this->benefit_id,
                'simid'           => $row['simid'],
                'status'          => 'Failed',
                'description'     => $e->getMessage()
            ]);
            // throw $e;
        }
        DB::commit();
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => '|'
        ];
    }
}
