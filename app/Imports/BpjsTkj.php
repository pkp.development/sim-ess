<?php

namespace App\Imports;

use App\Mail\BPJSNotification;
use App\Models\Accounts\Accounts;
use Illuminate\Support\Facades\DB;
use App\Models\Tasks\BenefitDetail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Organization\SimCompanies;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Business\BpjsKetenagakerjaan;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class BpjsTkj implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithCustomCsvSettings
{
    protected $path, $benefit_id;

    public function __construct($path, $benefit_id)
    {
        $this->path       = $path;
        $this->benefit_id = $benefit_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        DB::beginTransaction();
        try {
            $account     = Accounts::select('id', 'email')->with('profile')->where('sim_id', trim($row['simid']))->first();
            $fileBpjs    = null;
            $description = null;

            if($account) {
                $company = SimCompanies::where('abbreviation', $row['company_abbr'])->first();
                if (!$company) {
                    throw new \Exception('Abbreviation ' . $row['company_abbr'] . ' tidak ditemukan dalam database.');
                }

                // if (!ctype_digit(trim($row['no_bpjstkj']))) {
                //     throw new \Exception('No. BPJS Ketenagakerjaan harus angka.');
                // }

                if (strlen(trim($row['no_bpjstkj'])) > 20) {
                    throw new \Exception('No. BPJS Ketenagakerjaan tidak boleh lebih dari 20 digit.');
                }

                if (Storage::exists($this->path . '/' . trim($row['no_bpjstkj']) . '.pdf')) {
                    if(Storage::exists('images/bpjs_ketenagakerjaan/' . trim($row['no_bpjstkj']) . '.pdf')) {
                        Storage::delete('images/bpjs_ketenagakerjaan/' . trim($row['no_bpjstkj']) . '.pdf');
                    }

                    Storage::copy($this->path . '/' . trim($row['no_bpjstkj']) . '.pdf', 'images/bpjs_ketenagakerjaan/' . trim($row['no_bpjstkj']) . '.pdf');
                    $fileBpjs = 'images/bpjs_ketenagakerjaan/' . trim($row['no_bpjstkj']) . '.pdf';

                    $benefit = BpjsKetenagakerjaan::where('account_id', $account->id)->where('card_number', trim($row['no_bpjstkj']))->first();
                    if ($benefit) {
                        $benefit->card_number          = trim($row['no_bpjstkj']);
                        $benefit->image                = $fileBpjs;
                        $benefit->company_abbreviation = $row['company_abbr'];
                        $benefit->save();
                    }
                    else {
                        BpjsKetenagakerjaan::create([
                            'account_id'           => $account->id,
                            'card_number'          => trim($row['no_bpjstkj']),
                            'image'                => $fileBpjs,
                            'company_abbreviation' => $row['company_abbr']
                        ]);
                    }
                }
                else {
                    throw new \Exception('Kartu BPJS Ketenagakerjaan tidak ditemukan dalam zip');
                }

                if ($account->email && (env('MAIL_FEATURE', false))) {
                   // Mail::to($account->email)->send(new BPJSNotification('ketenagakerjaan', $account->profile->name));
                }

                BenefitDetail::create([
                    'task_benefit_id' => $this->benefit_id,
                    'simid'           => trim($row['simid']),
                    'status'          => 'Passed',
                    'description'     => $description,
                    'abbreviation'    => $row['company_abbr']
                ]);
            }
            else {
                throw new \Exception('SIMID Not Found');
            }
        }
        catch (\Exception $e) {
            BenefitDetail::create([
                'task_benefit_id' => $this->benefit_id,
                'simid'           => trim($row['simid']),
                'status'          => 'Failed',
                'description'     => $e->getMessage(),
                'abbreviation'    => $row['company_abbr']
            ]);
            // throw $e;
        }
        DB::commit();
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => '|'
        ];
    }
}
