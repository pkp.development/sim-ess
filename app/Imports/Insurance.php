<?php

namespace App\Imports;

use App\Models\Accounts\Accounts;
use App\Models\Business\Benefits;
use Illuminate\Support\Facades\DB;
use App\Models\Tasks\BenefitDetail;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class Insurance implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithCustomCsvSettings
{
    protected $benefit_id;

    public function __construct($benefit_id)
    {
        $this->benefit_id = $benefit_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        DB::beginTransaction();
        try {
            $account     = Accounts::select('id', 'email')->where('sim_id', $row['simid'])->first();
            $description = null;

            if($account) {

                $benefit = Benefits::where('account_id', $account->id)->first();
                if ($benefit) {
                    $benefit->insurance_vendor = $row['vendor_asuransi'];
                    $benefit->insurance_number = $row['no_asuransi'];
                    $benefit->save();
                }
                else {
                    Benefits::create([
                        'account_id'       => $account->id,
                        'insurance_vendor' => $row['vendor_asuransi'],
                        'insurance_number' => $row['no_asuransi']
                    ]);
                }

                BenefitDetail::create([
                    'task_benefit_id' => $this->benefit_id,
                    'simid'           => $row['simid'],
                    'status'          => 'Passed',
                    'description'     => $description
                ]);
            }
            else {
                throw new \Exception('SIMID Not Found');
            }
        }
        catch (\Exception $e) {
            BenefitDetail::create([
                'task_benefit_id' => $this->benefit_id,
                'simid'           => $row['simid'],
                'status'          => 'Failed',
                'description'     => $e->getMessage()
            ]);
            // throw $e;
        }
        DB::commit();
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => '|'
        ];
    }
}
