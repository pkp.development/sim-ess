<?php

namespace App\Imports;

use App\Mail\PayslipNotification;
use Ramsey\Uuid\Uuid;
use App\Models\Accounts\Accounts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\SlipGaji\HistoryDetail;
use App\Models\SlipGaji\PaySlipAccount;
use App\Models\SlipGaji\PaySlipContent;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\SlipGaji\PaySlip as PaySlipModel;
use App\Repositories\BprApi\DataBank;
use App\Repositories\BprApi\DataLastContract;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class PaySlip implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithCustomCsvSettings
{
    protected $year;
    protected $month;
    protected $payment_date;
    protected $historyId;

    public function __construct($year, $month, $payment_date, $historyId)
    {
        $this->year         = $year;
        $this->month        = $month;
        $this->payment_date = $payment_date;
        $this->historyId    = $historyId;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $slipContent = null;
        DB::beginTransaction();
        try {
            $account = Accounts::select('id', 'sim_id', 'email')->with('profile')->where('sim_id', $row['simid'])->first();

            if($account) {
                $apiLastContract = new DataLastContract;
                $apiBank         = new DataBank;
                $paySlipAccount  = PaySlipAccount::get();
                $checkPaySlip    = PaySlipModel::where('account_id', $account->id)->where('month_periode', $this->month)->where('year_periode', $this->year)->first();

                if($checkPaySlip) {

                    foreach($paySlipAccount as $paySlip) {
                        ini_set('memory_limit','4096M');
                        $slipContent = PaySlipContent::where('payslip_id', $checkPaySlip->id)->where('payslip_account_id', $paySlip->id)->first();
                        $nominal     = null;

                        if($row[$paySlip->code] === '-' || strtolower($row[$paySlip->code]) === 'null') {
                            $nominal = null;
                        }
                        elseif($row[$paySlip->code] === 0) {
                            $nominal = 0;
                        }
                        else {
                            $nominal = str_replace(',', '', $row[$paySlip->code]);
                        }

                        if ($slipContent) {
                            $slipContent->update([
                                'nominal' => $nominal
                            ]);
                        }
                        else {
                            $slipContent = PaySlipContent::create([
                                'payslip_id'         => $checkPaySlip->id,
                                'payslip_account_id' => $paySlip->id,
                                'nominal'            => $nominal
                            ]);
                        }

                        gc_collect_cycles();
                    }
                }
                else {
                    $resultBank         = $apiBank->getDataBank($account->sim_id);
                    $resultLastContract = $apiLastContract->getDataLastContract($account->sim_id);

                    $payslipModel = PaySlipModel::create([
                        'uuid'                => Uuid::uuid4()->toString(),
                        'created_by'          => auth()->user()->id,
                        'account_id'          => $account->id,
                        'sim_company_id'      => $resultLastContract->sim_company_id,
                        'payment_date'        => $this->payment_date,
                        'month_periode'       => $this->month,
                        'year_periode'        => $this->year,
                        'client_company_name' => $resultLastContract->client_company_name,
                        'job_name'            => $resultLastContract->job_name,
                        'join_date'           => $resultLastContract->join_date,
                        'account_number'      => $resultBank->no_rek,
                        'bank_name'           => $resultBank->nama_bank
                    ]);

                    foreach($paySlipAccount as $paySlip) {
                        $nominal = null;

                        if($row[$paySlip->code] === '-' || strtolower($row[$paySlip->code]) === 'null') {
                            $nominal = null;
                        }
                        elseif($row[$paySlip->code] === 0) {
                            $nominal = 0;
                        }
                        else {
                            $nominal = str_replace(',', '', $row[$paySlip->code]);
                        }

                        $slipContent = PaySlipContent::create([
                            'payslip_id'         => $payslipModel->id,
                            'payslip_account_id' => $paySlip->id,
                            'nominal'            => $nominal
                        ]);
                    }
                }

                HistoryDetail::create([
                    'history_payslip_id' => $this->historyId,
                    'sim_id'             => $account->sim_id,
                    'status'             => 'Passed',
                    'description'        => "{$this->month}/{$this->year}"
                ]);

                if ($account->email && (env('MAIL_FEATURE', false))) {
                    $dateName = Carbon::createFromFormat('Y-m-d', $this->payment_date)->format('F');
                   // Mail::to($account->email)->send(new PayslipNotification($account->profile->name, $dateName, $this->year));
                }
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            if(isset($row['simid'])) {
                $updby = $account->sim_id;

            }else{
                $updby = 'wrong_csv';
            }
                HistoryDetail::create([
                    'history_payslip_id' => $this->historyId,
                    'sim_id'             => $updby,
                    'status'             => 'Failed',
                    'description'        => $e->getMessage()
                ]);
            // throw $e;
        }
        DB::commit();
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => '|'
        ];
    }
}
