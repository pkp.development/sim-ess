<?php

namespace App\Imports;

use Ramsey\Uuid\Uuid;
use App\Models\Spt\Spt;
use App\Models\Spt\SptContent;
use App\Mail\PayslipNotification;
use App\Models\Accounts\Accounts;
use App\Models\Spt\HistoryDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class SptImport implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithCustomCsvSettings
{
    protected $year, $historyId;

    public function __construct($year, $historyId)
    {
        $this->year      = $year;
        $this->historyId = $historyId;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        DB::beginTransaction();
        try {
            $account = Accounts::with('assignment')->where('sim_id', $row['simid'])->first();
            $sptContent = null;

            if($account) {
                $spt = Spt::where('account_id', $account->id)->where('year', $this->year)->first();
                if($spt) {
                    $sptContent = SptContent::where('employee_spt_id', $spt->id)->first();
                    $sptContent->update([
                        'employee_spt_id'          => $spt->id,
                        'no'                       => $row['no'],
                        'company_abbr'             => ($row['company_abbr']) ? $row['company_abbr'] : null,
                        'tax_marital_tk'           => (is_numeric($row['tax_marital_tk'])) ? $row['tax_marital_tk'] : null,
                        'tax_marital_k'            => (is_numeric($row['tax_marital_k'])) ? $row['tax_marital_k'] : null,
                        'perolehan_1'              => (is_numeric($row['perolehan_1'])) ? $row['perolehan_1'] : null,
                        'perolehan_2'              => (is_numeric($row['perolehan_2'])) ? $row['perolehan_2'] : null,
                        '1_gaji_pokok'             => (is_numeric($row['1_gaji_pokok'])) ? $row['1_gaji_pokok'] : null,
                        '2_tunjangan_pajak'        => (is_numeric($row['2_tunjangan_pajak'])) ? $row['2_tunjangan_pajak'] : null,
                        '3_tunjangan_lainnya'      => (is_numeric($row['3_tunjangan_lainnya'])) ? $row['3_tunjangan_lainnya'] : null,
                        '4_honorarium'             => (is_numeric($row['4_honorarium'])) ? $row['4_honorarium'] : null,
                        '5_asuransi'               => (is_numeric($row['5_asuransi'])) ? $row['5_asuransi'] : null,
                        '6_natura'                 => (is_numeric($row['6_natura'])) ? $row['6_natura'] : null,
                        '7_tantiem'                => (is_numeric($row['7_tantiem'])) ? $row['7_tantiem'] : null,
                        '8_jmlh_penghasilan_bruto' => (is_numeric($row['8_jmlh_penghasilan_bruto'])) ? $row['8_jmlh_penghasilan_bruto'] : null,
                        '9_biaya_jabatan'          => (is_numeric($row['9_biaya_jabatan'])) ? $row['9_biaya_jabatan'] : null,
                        '10_iuran_pensiun'         => (is_numeric($row['10_iuran_pensiun'])) ? $row['10_iuran_pensiun'] : null,
                        '11_jmlh_pengurangan'      => (is_numeric($row['11_jmlh_pengurangan'])) ? $row['11_jmlh_pengurangan'] : null,
                        '12_jmlh_netto'            => (is_numeric($row['12_jmlh_netto'])) ? $row['12_jmlh_netto'] : null,
                        '13_jmlh_netto_sblmnya'    => (is_numeric($row['13_jmlh_netto_sblmnya'])) ? $row['13_jmlh_netto_sblmnya'] : null,
                        '14_jmlh_netto_pph'        => (is_numeric($row['14_jmlh_netto_pph'])) ? $row['14_jmlh_netto_pph'] : null,
                        '15_ptkp'                  => (is_numeric($row['15_ptkp'])) ? $row['15_ptkp'] : null,
                        '16_pkp'                   => (is_numeric($row['16_pkp'])) ? $row['16_pkp'] : null,
                        '17_pkp_pph'               => (is_numeric($row['17_pkp_pph'])) ? $row['17_pkp_pph'] : null,
                        '18_pph_sblmnya'           => (is_numeric($row['18_pph_sblmnya'])) ? $row['18_pph_sblmnya'] : null,
                        '19_pph_terutang'          => (is_numeric($row['19_pph_terutang'])) ? $row['19_pph_terutang'] : null,
                        '20_pph_dilunasi'          => (is_numeric($row['20_pph_dilunasi'])) ? $row['20_pph_dilunasi'] : null
                    ]);
                }
                else {
                    $spt = Spt::create([
                        'uuid'       => Uuid::uuid4()->toString(),
                        'created_by' => auth()->user()->id,
                        'account_id' => $account->id,
                        'year'       => $this->year,
                        'file'       => $row['simid'] . '-' . $this->year . '.pdf'
                    ]);

                    $sptContent = new SptContent([
                        'employee_spt_id'          => $spt->id,
                        'no'                       => $row['no'],
                        'company_abbr'             => ($row['company_abbr']) ? $row['company_abbr'] : null,
                        'job_name'                 => ($account->assignment) ? $account->assignment->job_name : null,
                        'tax_marital_tk'           => (is_numeric($row['tax_marital_tk'])) ? $row['tax_marital_tk'] : null,
                        'tax_marital_k'            => (is_numeric($row['tax_marital_k'])) ? $row['tax_marital_k'] : null,
                        'perolehan_1'              => (is_numeric($row['perolehan_1'])) ? $row['perolehan_1'] : null,
                        'perolehan_2'              => (is_numeric($row['perolehan_2'])) ? $row['perolehan_2'] : null,
                        '1_gaji_pokok'             => (is_numeric($row['1_gaji_pokok'])) ? $row['1_gaji_pokok'] : null,
                        '2_tunjangan_pajak'        => (is_numeric($row['2_tunjangan_pajak'])) ? $row['2_tunjangan_pajak'] : null,
                        '3_tunjangan_lainnya'      => (is_numeric($row['3_tunjangan_lainnya'])) ? $row['3_tunjangan_lainnya'] : null,
                        '4_honorarium'             => (is_numeric($row['4_honorarium'])) ? $row['4_honorarium'] : null,
                        '5_asuransi'               => (is_numeric($row['5_asuransi'])) ? $row['5_asuransi'] : null,
                        '6_natura'                 => (is_numeric($row['6_natura'])) ? $row['6_natura'] : null,
                        '7_tantiem'                => (is_numeric($row['7_tantiem'])) ? $row['7_tantiem'] : null,
                        '8_jmlh_penghasilan_bruto' => (is_numeric($row['8_jmlh_penghasilan_bruto'])) ? $row['8_jmlh_penghasilan_bruto'] : null,
                        '9_biaya_jabatan'          => (is_numeric($row['9_biaya_jabatan'])) ? $row['9_biaya_jabatan'] : null,
                        '10_iuran_pensiun'         => (is_numeric($row['10_iuran_pensiun'])) ? $row['10_iuran_pensiun'] : null,
                        '11_jmlh_pengurangan'      => (is_numeric($row['11_jmlh_pengurangan'])) ? $row['11_jmlh_pengurangan'] : null,
                        '12_jmlh_netto'            => (is_numeric($row['12_jmlh_netto'])) ? $row['12_jmlh_netto'] : null,
                        '13_jmlh_netto_sblmnya'    => (is_numeric($row['13_jmlh_netto_sblmnya'])) ? $row['13_jmlh_netto_sblmnya'] : null,
                        '14_jmlh_netto_pph'        => (is_numeric($row['14_jmlh_netto_pph'])) ? $row['14_jmlh_netto_pph'] : null,
                        '15_ptkp'                  => (is_numeric($row['15_ptkp'])) ? $row['15_ptkp'] : null,
                        '16_pkp'                   => (is_numeric($row['16_pkp'])) ? $row['16_pkp'] : null,
                        '17_pkp_pph'               => (is_numeric($row['17_pkp_pph'])) ? $row['17_pkp_pph'] : null,
                        '18_pph_sblmnya'           => (is_numeric($row['18_pph_sblmnya'])) ? $row['18_pph_sblmnya'] : null,
                        '19_pph_terutang'          => (is_numeric($row['19_pph_terutang'])) ? $row['19_pph_terutang'] : null,
                        '20_pph_dilunasi'          => (is_numeric($row['20_pph_dilunasi'])) ? $row['20_pph_dilunasi'] : null
                    ]);
                }

                HistoryDetail::create([
                    'history_spt_id' => $this->historyId,
                    'sim_id'         => $account->sim_id,
                    'status'         => 'Passed',
                    'description'    => $this->year
                ]);

                if ($account->email && (env('MAIL_FEATURE', false))) {
                    // Mail::to($account->email)->send(new PayslipNotification($account->profile->name, 12, $this->year, 'spt'));
                }
            }
            else {
                HistoryDetail::create([
                    'history_spt_id' => $this->historyId,
                    'sim_id'         => $row['simid'],
                    'status'         => 'Failed',
                    'description'    => 'SIMID not found'
                ]);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            HistoryDetail::create([
                'history_spt_id' => $this->historyId,
                'sim_id'         => $row['simid'],
                'status'         => 'Failed',
                'description'    => $e->getMessage()
            ]);
        }
        DB::commit();

        return $sptContent;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => '|'
        ];
    }
}
