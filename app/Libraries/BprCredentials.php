<?php

namespace App\Libraries;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use App\Models\Config\Setting;
use App\Mail\EmployeeCredential;
use App\Models\Accounts\Accounts;
use App\Models\Accounts\Profiles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Repositories\SipasApi\SendSMS;
use App\Models\Config\HistoryCredentials;
use App\Repositories\BprApi\GetCredentials;
use Propaganistas\LaravelPhone\PhoneNumber;
use App\Models\Config\HistoryCredentialsDetail;
use App\Repositories\BprApi\PostFlagCredential;
use Propaganistas\LaravelPhone\Exceptions\NumberParseException;

class BprCredentials
{
    public function scheduleCredentials()
    {
        $smsApi  = new SendSMS;
        $empCred = new GetCredentials();
        $apiFlag = new PostFlagCredential;
        $limit   = Setting::where('code', 'limit_credentials')->first();
        $result  = null;

        if (!$limit->value) {
            $limit = 10;
        }
        else {
            $limit = $limit->value;
        }

        $history = HistoryCredentials::create([
            'uuid'   => Uuid::uuid4()->toString(),
            'status' => 0
        ]);
        $results = DB::select('select max(created_at) from accounts');

        for ($i = 1; $i <= $limit; $i++) {
            DB::beginTransaction();
            try {
                $result = $empCred->getCredentials();

                if ($result->status == '404') {
                    $i = $limit;
                }
                else {
                    if(!Accounts::select('id')->where('sim_id', $result->sim_id)->first()) {
                        $password = Carbon::createFromFormat('Y-m-d', $result->birth_date)->format('dmy');

                        $account = Accounts::create([
                            'uuid'     => Uuid::uuid4()->toString(),
                            'sim_id'   => $result->sim_id,
                            'status'   => 1,
                            'email'    => $result->email,
                            'password' => bcrypt($password),
                            'role'     => 'employee',
                            'is_bpr'   => 1
                        ]);

                        Profiles::create([
                            'account_id'      => $account->id,
                            'uuid'            => Uuid::uuid4()->toString(),
                            'name'            => $result->nama,
                            'date_of_birth'   => $result->birth_date,
                            'phone_number'    => ($result->phone1 && $result->phone1 != '-') ? str_replace('-', '', $result->phone1) : null,
                            'secondary_phone' => ($result->phone2 && $result->phone2 != '-') ? str_replace('-', '', $result->phone2) : null,
                        ]);

                        HistoryCredentialsDetail::create([
                            'credential_id' => $history->id,
                            'simid'         => $result->sim_id,
                            'status'        => 1,
                            'description'   => 'System created new employee credential.'
                        ]);

                        $expName = explode(' ', $result->nama);

                        if (count($expName) > 1) {
                            $nickname = $expName[0] . ' ' . $expName[1];
                        }
                        else {
                            $nickname = $expName[0];
                        }

                        $smsNotification  = false;
                        $mailNotification = false;
                        try {
                            $phone = null;

                            if ($result->phone1 && $result->phone1 != '-') {
                                $phone = PhoneNumber::make(str_replace('-', '', $result->phone1), 'ID')->formatForMobileDialingInCountry('ID');
                            }
                            else if($result->phone2 && $result->phone2 != '-'){
                                $phone = PhoneNumber::make(str_replace('-', '', $result->phone2), 'ID')->formatForMobileDialingInCountry('ID');
                            }

                            if ($phone) {
                                $contentSms = "Selamat bergabung di SIMGROUP Sdr ". $nickname .", untuk informasi data karyawan silahkan buka link\n\ness.simgroup.co.id\nSIMID : " . $result->sim_id . "\nPass : " . $password . "\n\nSalam Sukses";

                                if ((env('SMS_FEATURE', false))) {
                                    $smsApi->sendSMS($result->sim_id, $phone, $contentSms);
                                }
                            }

                            $smsNotification  = true;
                        }
                        catch (NumberParseException $e) {
                            $smsNotification  = false;
                        }

                        if ($result && isset($result->email)) {
                            if ((env('MAIL_FEATURE', false)) && $result->email) {
                                if (env('MAIL_PRODUCTION', false)) {
                                    $email = $result->email;
                                }
                                else {
                                    $email = 'spam.yoma@gmail.com';
                                }

                                //Mail::to($email)->send(new EmployeeCredential($nickname, $result->sim_id, $password));
                            }

                            $mailNotification = true;
                        }
                        else {
                            $mailNotification = false;
                        }

                        if (!$smsNotification && !$mailNotification) {
                            throw new \Exception('Email and Phone Number not Valid');
                        }

                        $apiFlag->postFlagCredential($result->count);
                    }
                    else {
                        HistoryCredentialsDetail::create([
                            'credential_id' => $history->id,
                            'simid'         => $result->sim_id,
                            'status'        => 2,
                            'description'   => 'SIMID already existed.'
                        ]);

                        $apiFlag->postFlagCredential($result->count);
                    }
                }
            }
            catch (\Throwable $e) {
                DB::commit();
                $apiFlag->postFlagCredential($result->count);
                HistoryCredentialsDetail::create([
                    'credential_id' => $history->id,
                    'simid'         => $result->sim_id,
                    'status'        => 2,
                    'description'   => $e->getMessage()
                ]);
                Log::error($e);
            }
            DB::commit();
        }

        $history->status = 1;
        $history->save();
    }
}
