<?php
namespace App\Libraries;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use App\Models\Config\Setting;
use App\Mail\EmployeeCredential;
use App\Models\Accounts\Accounts;
use App\Models\Accounts\Profiles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Repositories\SipasApi\SendSMS;
use libphonenumber\NumberParseException;
use App\Models\Config\HistoryCredentials;
use App\Models\Config\SettingCredentials;
use Propaganistas\LaravelPhone\PhoneNumber;
use App\Repositories\SipasApi\GetEmployeeData;
use App\Models\Config\HistoryCredentialsDetail;
use App\Repositories\SipasApi\PostUnflagCredentials;
use App\Repositories\SipasApi\GetEmployeeCredentials;

class EssService
{
    /**
     * Schedule get employee credentials
     *
     * @return void
     */
    public function scheduleCredentials()
    {
        $smsApi     = new SendSMS;
        $empCred    = new GetEmployeeCredentials();
        $limit      = Setting::where('code', 'limit_credentials')->first();
        $unflag     = false;
        $dataUnflag = [];

        if (!$limit->value) {
            $limit = 10;
        }
        else {
            $limit = $limit->value;
        }

        $settings = SettingCredentials::get();
        $include  = [];
        $exclude  = [];

        foreach ($settings as $setting) {
            if ($setting->type == 'include') {
                $include[$setting->organization][] = $setting->value;
            }
            elseif ($setting->type == 'exclude') {
                $exclude[$setting->organization][] = $setting->value;
            }
        }

        if (env('APP_ENV', 'local') == 'local') {
            $filters     = [
                "limit"   => $limit,
                "exclude" => $exclude,
                "company" => [6897]
            ];
        }
        else {
            $filters     = [
                "limit"   => $limit,
                "exclude" => $exclude
            ];
        }

        try {
            $results = $empCred->getEmployeeCredentials(array_merge($filters, $include));

            $history = HistoryCredentials::create([
                'uuid'   => Uuid::uuid4()->toString(),
                'status' => 0
            ]);

            foreach($results->data as $result) {
                DB::beginTransaction();
                try {
                    if(!Accounts::select('id')->where('sim_id', $result->id)->first()) {
                        $password = Carbon::createFromFormat('Y-m-d H:i:s', $result->date_birth)->format('dmy');

                        $apiEmp       = new GetEmployeeData;
                        $dataEmployee = $apiEmp->getEmployeeData(['id' => $result->id]);

                        $account = Accounts::create([
                            'uuid'     => Uuid::uuid4()->toString(),
                            'sim_id'   => $result->id,
                            'status'   => 1,
                            'email'    => $dataEmployee[0]->email,
                            'password' => bcrypt($password),
                            'role'     => 'employee'
                        ]);

                        Profiles::create([
                            'account_id'      => $account->id,
                            'uuid'            => Uuid::uuid4()->toString(),
                            'name'            => $result->emp_name,
                            'date_of_birth'   => $result->date_birth,
                            'phone_number'    => ($result->phone1 && $result->phone1 != '-') ? str_replace('-', '', $result->phone1) : null,
                            'secondary_phone' => ($result->phone2 && $result->phone2 != '-') ? str_replace('-', '', $result->phone2) : null,
                        ]);

                        HistoryCredentialsDetail::create([
                            'credential_id' => $history->id,
                            'simid'         => $result->id,
                            'status'        => 1,
                            'description'   => 'System created new employee credential.'
                        ]);

                        $expName = explode(' ', $result->emp_name);

                        if (count($expName) > 1) {
                            $nickname = $expName[0] . ' ' . $expName[1];
                        }
                        else {
                            $nickname = $expName[0];
                        }

                        $smsNotification  = false;
                        $mailNotification = false;
                        try {
                            $phone = null;

                            if ($result->phone1 && $result->phone1 != '-') {
                                $phone = PhoneNumber::make(str_replace('-', '', $result->phone1), 'ID')->formatForMobileDialingInCountry('ID');
                            }
                            else if($result->phone2 && $result->phone2 != '-'){
                                $phone = PhoneNumber::make(str_replace('-', '', $result->phone2), 'ID')->formatForMobileDialingInCountry('ID');
                            }

                            if ($phone) {
                                $contentSms = "Selamat bergabung di SIMGROUP Sdr ". $nickname .", untuk informasi data karyawan silahkan buka link\n\ness.simgroup.co.id\nSIMID : " . $result->id . "\nPass : " . $password . "\n\nSalam Sukses";
    
                                if ((env('SMS_FEATURE', false))) {
                                    $smsApi->sendSMS($result->id, $phone, $contentSms);
                                }
                            }

                            $smsNotification  = true;
                        }
                        catch (NumberParseException $e) {
                            $smsNotification  = false;
                        }

                        if ($dataEmployee && isset($dataEmployee[0]->email)) {
                            if ((env('MAIL_FEATURE', false)) && $dataEmployee[0]->email) {
                                Mail::to($dataEmployee[0]->email)->send(new EmployeeCredential($nickname, $result->id, $password));
                            }

                            $mailNotification = true;
                        }
                        else {
                            $mailNotification = false;
                        }

                        if (!$smsNotification && !$mailNotification) {
                            throw new \Exception('Email and Phone Number not Valid');
                        }
                    }
                    else {
                        HistoryCredentialsDetail::create([
                            'credential_id' => $history->id,
                            'simid'         => $result->id,
                            'status'        => 2,
                            'description'   => 'SIMID already existed.'
                        ]);
                    }
                }
                catch (\Throwable $e) {
                    DB::rollback();
                    HistoryCredentialsDetail::create([
                        'credential_id' => $history->id,
                        'simid'         => $result->id,
                        'status'        => 2,
                        'description'   => $e->getMessage()
                    ]);

                    // $history->status = 2;
                    // $history->save();

                    $unflag = true;
                    $dataUnflag[] = $result->id;
                    Log::error($e);
                }
                DB::commit();
            }
        }
        catch (\Throwable $e) {
            $history->status = 2;
            $history->save();

            if ($unflag) {
                $apiUnflag = new PostUnflagCredentials;
                $apiUnflag->postUnflagCredentials($dataUnflag);
            }

            Log::error($e);
        }

        if ($unflag) {
            $apiUnflag = new PostUnflagCredentials;
            $apiUnflag->postUnflagCredentials($dataUnflag);
        }

        $history->status = 1;
        $history->save();
    }
}
