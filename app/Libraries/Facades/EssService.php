<?php
namespace App\Libraries\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \App\Libraries\EssService scheduleCredentials()
 *
 * @see \App\Libraries\EssService
 */
class EssService  extends Facade
{
    protected static function getFacadeAccessor() { return 'ess'; }
}
