<?php

namespace App\Libraries\Facades;

use Illuminate\Support\Facades\Facade;

class GuzzleClient extends Facade {
	protected static function getFacadeAccessor() { return 'guzzle'; }
}