<?php
namespace App\Libraries\Facades;

use Illuminate\Support\Facades\Facade;

class ImageHandler extends Facade
{
    protected static function getFacadeAccessor() { return 'image-handler'; }
}
