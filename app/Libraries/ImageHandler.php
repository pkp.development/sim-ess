<?php
namespace App\Libraries;

use Intervention\Image\Facades\Image;

class ImageHandler
{
    public function resizing($image, $width = 800, $height = 800, $square = false, $name = '')
    {
        // $this->checkImage($image);

        $thumb = Image::make($image);
        $exp   = explode('/', $thumb->mime());
        $mime  = $exp[1];

        // Get size image
        $image_width  = $thumb->width();
        $image_height = $thumb->height();

        // Resized image
        if ($image_width >= $width || $image_height >= $height) {
            $resize_width  = ($image_width >= $image_height) ? $width : null;
            $resize_height = ($image_width <= $image_height) ? $height : null;
        } else {
            $resize_width  = ($image_width >= $image_height) ? $image_width : null;
            $resize_height = ($image_width <= $image_height) ? $image_height : null;
        }

        $thumb->resize($resize_width, $resize_height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // Canvas image
        if($square) {
            $canvas = Image::canvas($width, $height);
        }
        else {
            $canvas = Image::canvas($thumb->width(), $thumb->height());
        }
        $canvas->insert($thumb, 'center');

        if (!empty($name) && $name == 'thumbnail') {
            return (string) $canvas->blur()->encode($mime, 30);
        } else {
            return (string) $canvas->encode($mime, 70);
        }
    }
}
