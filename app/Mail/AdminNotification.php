<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $name, $count;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $count)
    {
        $this->name  = $name;
        $this->count = $count;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SIMGROUP, Admin tasks notification')
            ->with([
                'name'       => $this->name,
                'countNotif' => $this->count
            ])
            ->view('mails.admin-notification');
    }
}
