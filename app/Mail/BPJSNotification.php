<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BPJSNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $type, $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($type, $name)
    {
        $this->type = $type;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->type == 'kesehatan') {
            return $this->subject('SIMGROUP, Informasi BPJS Kesehatan telah tersedia')
                ->with([
                    'name' => $this->name
                ])
                ->view('mails.bpjskes-notification');
        }
        elseif ($this->type == 'ketenagakerjaan') {
            return $this->subject('SIMGROUP, Informasi BPJS Ketenagakerjaan telah tersedia')
                ->with([
                    'name' => $this->name
                ])
                ->view('mails.bpjstk-notification');
        }
    }
}
