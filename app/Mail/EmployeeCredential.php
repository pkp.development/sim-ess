<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmployeeCredential extends Mailable
{
    use Queueable, SerializesModels;

    protected $name, $simid, $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $simid, $password)
    {
        $this->name     = $name;
        $this->simid    = $simid;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SIMGROUP, Informasi Akun Employee Self Service')
            ->with([
                'name'     => $this->name,
                'simid'    => $this->simid,
                'password' => $this->password
            ])
            ->view('mails.employee-credentials');
    }
}
