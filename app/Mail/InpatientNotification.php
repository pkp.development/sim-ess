<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InpatientNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $name, $patient, $diagnosis, $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $patient, $diagnosis, $status)
    {
        $this->name      = $name;
        $this->patient   = $patient;
        $this->diagnosis = $diagnosis;
        $this->status    = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SIMGROUP, Informasi status klaim rawat jalan')
            ->with([
                'name'      => $this->name,
                'patient'   => $this->patient,
                'diagnosis' => $this->diagnosis,
                'status'    => ucwords(str_replace('_', ' ', $this->status))
            ])
            ->view('mails.inpatient-notification');
    }
}
