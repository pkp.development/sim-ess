<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PayslipNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $name, $bulan, $tahun, $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $bulan, $tahun, $type = 'payslip')
    {
        $this->name  = $name;
        $this->bulan = $bulan;
        $this->tahun = $tahun;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->type == 'payslip') {
            return $this->subject('SIMGROUP, Informasi Payslip telah tersedia')
            ->with([
                'name'  => $this->name,
                'bulan' => $this->bulan,
                'tahun' => $this->tahun
            ])
            ->view('mails.payslip-notification');
        }
        else if ($this->type == 'spt') {
            return $this->subject('SIMGROUP, Informasi SPT telah tersedia')
            ->with([
                'name'  => $this->name,
                'bulan' => $this->bulan,
                'tahun' => $this->tahun
            ])
            ->view('mails.spt-notification');
        }
    }
}
