<?php

namespace App\Models\Accounts;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Accounts extends Authenticatable
{
    use SoftDeletes, Notifiable;


    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'username',
        'sim_id',
        'email',
        'status',
        'password',
        'remember_token',
        'forgot_token',
        'role',
        'signature',
        'actived_at',
        'bpr_sync',
        'is_bpr'
    ];

    protected $dates = ['actived_at'];

    protected $hidden = [
        'password', 'remember_token', 'forgot_token'
    ];

    public function admin()
    {
        return $this->hasOne('App\Models\Accounts\Admin', 'account_id');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Accounts\Profiles', 'account_id');
    }

    public function assignment()
    {
        return $this->hasOne('App\Models\Business\Assignments', 'account_id');
    }

    public function bank_account()
    {
        return $this->hasOne('App\Models\Business\BankAccounts', 'account_id');
    }

    public function history_idcard()
    {
        return $this->hasOne('App\Models\Claim\HistoryIdCard', 'account_id');
    }
}
