<?php

namespace App\Models\Accounts;

use App\Models\Permissions\PermissionCompany;
use App\Models\Permissions\Permissions;
use App\Models\Permissions\PermissionSimBranch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'name',
        'phone_number'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permissions::class, 'permissions_has_admin', 'admin_id', 'permission_id');
    }

    public function role_company()
    {
        return $this->hasMany(PermissionCompany::class, 'admin_id');
    }

    public function role_simbranch()
    {
        return $this->hasMany(PermissionSimBranch::class, 'admin_id');
    }
}
