<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profiles extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'account_id',
        'client_id',
        'profile_picture',
        'name',
        'place_of_birth',
        'date_of_birth',
        'gender',
        'nik',
        'identity_image',
        'address_identity',
        'zip_code_identity',
        'province_identity',
        'province_identity_name',
        'city_identity',
        'city_identity_name',
        'marital_status',
        'religion',
        'recent_education',
        'phone_number',
        'secondary_phone',
        'mother_name',
        'npwp_number',
        'npwp_periode',
        'npwp_image',
        'domicile_address',
        'domicile_province',
        'domicile_province_name',
        'domicile_city',
        'domicile_city_name',
        'domicile_zip_code',
        'tax_marital_status',
        'family_card_number',
        'family_card_image',
        'selfie_image',
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }
}
