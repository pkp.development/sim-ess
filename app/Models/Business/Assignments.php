<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignments extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assignments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'sim_company_id',
        'sim_company',
        'sim_branch_id',
        'sim_branch',
        'client_branch_id',
        'client_branch',
        'client_company_id',
        'client_company',
        'job_id',
        'job_name',
        'npo',
        'contract_number',
        'contract_file',
        'id_card_image',
        'join_date',
        'hired_date',
        'terminated_date',
        'contract_start',
        'contract_end',
        'contract_type',
        'contract_status',
        'ess_status',
        'signed_at',
        'rejected_reason'
    ];

    protected $dates = ['join_date', 'hired_date', 'terminated_date', 'contract_start', 'contract_end', 'signed_at'];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }
}
