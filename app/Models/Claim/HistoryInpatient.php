<?php

namespace App\Models\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoryInpatient extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'history_inpatient';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inpatient_id',
        'updated_by',
        'status'
    ];

    public function inpatient()
    {
        return $this->belongsTo('App\Models\Claim\Inpatient', 'inpatient_id');
    }

    public function updatedby()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'updated_by');
    }
}
