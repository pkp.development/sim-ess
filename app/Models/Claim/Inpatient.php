<?php

namespace App\Models\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inpatient extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inpatient';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'created_by',
        'uuid',
        'patient_name',
        'hospital_name',
        'diagnosis',
        'nominal',
        'status',
        'receipt_date',
        'received_date',
        'disbursment',
        'transfer_date',
        'description'
    ];

    protected $dates = ['receipt_date', 'received_date', 'transfer_date'];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'created_by');
    }

    public function histories()
    {
        return $this->hasMany('App\Models\Claim\HistoryInpatient', 'inpatient_id');
    }
}
