<?php

namespace App\Models\Claim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outpatient extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'outpatient';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'verified_by',
        'uuid',
        'doctor_name',
        'patient_name',
        'nominal',
        'description',
        'receipt_file',
        'doctor_prescription_file',
        'receipt_date',
        'approved_date',
        'status',
        'rejection_reason'
    ];

    protected $dates = ['receipt_date', 'approved_date'];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }

    public function verified_by()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'verified_by');
    }

    public function verifiedBy()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'verified_by');
    }
}
