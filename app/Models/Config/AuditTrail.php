<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditTrail extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'audit_trail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'account_id',
        'type',
        'title',
        'description',
        'old_json',
        'new_json'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }
}
