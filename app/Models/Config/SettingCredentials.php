<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;

class SettingCredentials extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'setting_credentials';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'organization',
        'value',
        'company_name',
        'branch_name'
    ];
}
