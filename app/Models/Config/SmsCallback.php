<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;

class SmsCallback extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sms_callback';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trxid',
        'code',
        'phone',
        'desc',
        'response_code',
        'created_at'
    ];

    protected $dates = ['created_at'];
}
