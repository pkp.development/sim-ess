<?php

namespace App\Models\Organization;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SimCompanies extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sim_companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'sipas_id',
        'name',
        'abbreviation',
        'logo',
        'npwp',
        'coordinator',
        'coordinator_npwp',
        'coordinator_sign',
        'sync_at'
    ];

    protected $dates = ['sync_at'];
}
