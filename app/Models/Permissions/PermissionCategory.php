<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Model;

class PermissionCategory extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permission_category';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function permissions()
    {
        return $this->hasMany('App\Models\Permissions\Permissions', 'permission_category_id');
    }
}
