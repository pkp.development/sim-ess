<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Model;

class PermissionSimBranch extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permission_admin_simbranch';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_id',
        'value',
        'name',
        'company'
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Accounts\Admin', 'admin_id');
    }
}
