<?php

namespace App\Models\Permissions;

use App\Models\Accounts\Admin;
use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Models\Permissions\PermissionCategory', 'permission_category_id');
    }

    public function admin()
    {
        return $this->belongsToMany(Admin::class);
    }
}
