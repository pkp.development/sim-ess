<?php

namespace App\Models\SlipGaji;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PaySlip extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payslip';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'created_by',
        'account_id',
        'sim_company_id',
        'payment_date',
        'month_periode',
        'year_periode',
        'client_company_name',
        'job_name',
        'join_date',
        'account_number',
        'bank_name'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }

    public function content()
    {
        return $this->hasMany('App\Models\SlipGaji\PaySlipContent', 'payslip_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Organization\SimCompanies', 'sim_company_id');
    }
}
