<?php

namespace App\Models\SlipGaji;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaySlipContent extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payslip_content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payslip_id', 'payslip_account_id', 'nominal'
    ];

    public function payslip()
    {
        return $this->belongsTo('App\Models\SlipGaji\PaySlip', 'payslip_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\SlipGaji\PaySlipAccount', 'payslip_account_id');
    }
}
