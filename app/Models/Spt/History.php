<?php

namespace App\Models\Spt;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'history_spt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'uploaded_by', 'status'
    ];

    public function uploadedby()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'uploaded_by');
    }

    public function details()
    {
        return $this->hasMany('App\Models\Spt\HistoryDetail', 'history_spt_id');
    }
}
