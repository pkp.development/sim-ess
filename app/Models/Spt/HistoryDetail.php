<?php

namespace App\Models\Spt;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoryDetail extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'history_spt_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'history_spt_id', 'sim_id', 'status', 'description'
    ];

    public function history()
    {
        return $this->belongsTo('App\Models\Spt\History', 'history_spt_id');
    }
}
