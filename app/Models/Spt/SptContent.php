<?php

namespace App\Models\Spt;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SptContent extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'spt_content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_spt_id',
        'no',
        'company_abbr',
        'job_name',
        'tax_marital_tk',
        'tax_marital_k',
        'perolehan_1',
        'perolehan_2',
        '1_gaji_pokok',
        '2_tunjangan_pajak',
        '3_tunjangan_lainnya',
        '4_honorarium',
        '5_asuransi',
        '6_natura',
        '7_tantiem',
        '8_jmlh_penghasilan_bruto',
        '9_biaya_jabatan',
        '10_iuran_pensiun',
        '11_jmlh_pengurangan',
        '12_jmlh_netto',
        '13_jmlh_netto_sblmnya',
        '14_jmlh_netto_pph',
        '15_ptkp',
        '16_pkp',
        '17_pkp_pph',
        '18_pph_sblmnya',
        '19_pph_terutang',
        '20_pph_dilunasi'
    ];

    public function spt()
    {
        return $this->belongsTo('App\Models\Spt\Spt', 'employee_spt_id');
    }
}
