<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Benefit extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'task_benefit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'uploaded_by',
        'status',
        'type',
        'excel_name',
        'zip_name'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'uploaded_by');
    }

    public function details()
    {
        return $this->hasMany('App\Models\Tasks\BenefitDetail', 'task_benefit_id');
    }
}
