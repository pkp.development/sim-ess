<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BenefitDetail extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'task_benefit_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_benefit_id',
        'simid',
        'status',
        'description',
        'abbreviation'
    ];

    public function benefit()
    {
        return $this->belongsTo('App\Models\Tasks\BenefitDetail', 'task_benefit_id');
    }
}
