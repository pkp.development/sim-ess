<?php

namespace App\Models\Temporary;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tmp_contract';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'verified_by',
        'account_id',
        'contract_number',
        'status',
        'description'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }

    public function verifiedBy()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'verified_by');
    }
}
