<?php

namespace App\Models\Temporary;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var array
     */
    protected $keyType = 'bigint';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tmp_profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'updated_by',
        'account_id',
        'status',
        'description',
        'content',
        'type'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'account_id');
    }

    public function handledBy()
    {
        return $this->belongsTo('App\Models\Accounts\Accounts', 'updated_by');
    }
}
