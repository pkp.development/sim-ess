<?php

namespace App\Providers;

use App\Models\Accounts\Admin;
use App\Models\Permissions\Permissions;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach ($this->getPermission() as $permission) {

            Gate::define($permission->key, function($user) use ($permission) {
                if ($user->role == 'super-admin') {
                    return true;
                }

                $admin = Admin::with('permissions')->where('account_id', $user->id)->first();
                if ($admin->permissions->where('id', $permission->id)->first()) {
                    return true;
                }

                return false;
            });
        }

        Gate::define('pe-terminated', function ($user) {

            if ($user->assignment->terminated_date) {
                return false;
            }

            return true;
        });
    }

    protected function getPermission()
    {
        return Permissions::with('category')->get();
    }
}
