<?php

namespace App\Providers;

use App\Libraries\EssService;
use Illuminate\Support\ServiceProvider;

class EssServiceProvider extends ServiceProvider
{
    protected $ess;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ess', function() {
            return $this->ess;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->ess = new EssService();
    }
}
