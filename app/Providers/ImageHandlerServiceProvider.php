<?php

namespace App\Providers;

use App\Libraries\ImageHandler;
use Illuminate\Support\ServiceProvider;

class ImageHandlerServiceProvider extends ServiceProvider
{
    protected $image_handler;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('image-handler', function() {
            return $this->image_handler;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->image_handler = new ImageHandler();
    }
}
