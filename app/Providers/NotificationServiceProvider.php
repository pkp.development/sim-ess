<?php

namespace App\Providers;

use App\Models\Config\Notification;
use App\Models\Permissions\PermissionCompany;
use App\Models\Permissions\PermissionSimBranch;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.topbar', function($view){

            if(auth()->user()->role == 'super-admin') {
                $notifications = Notification::with('account.profile', 'account.assignment')->where('status', 0)->latest()->take(10)->get();
                $counts_notif  = Notification::with('account.profile', 'account.assignment')->where('status', 0)->count();
            }
            elseif (auth()->user()->role == 'admin') {
                $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                $permissions = [];

                foreach (auth()->user()->admin->permissions->where('permission_category_id', 3)->pluck('key') as $notifPermission) {
                    $permissions[] = str_replace('notif-', '', $notifPermission);
                }

                $notifications = Notification::with('account.profile', 'account.assignment')->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                    $allBranch = $simBrances->where('value', 0)->first();
                    $allClient = $companies->where('value', 0)->first();

                    if ($allBranch && $allClient) {
                        $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
                    }
                    elseif ($allBranch) {
                        $query->whereIn('client_company_id', $companies->pluck('value'));
                    }
                    elseif ($allClient) {
                        $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    else {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                })->where('status', 0)->whereIn('type', $permissions)->latest()->take(10)->get();

                $counts_notif  = Notification::with('account.profile', 'account.assignment')->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                    $allBranch = $simBrances->where('value', 0)->first();
                    $allClient = $companies->where('value', 0)->first();

                    if ($allBranch && $allClient) {
                        $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
                    }
                    elseif ($allBranch) {
                        $query->whereIn('client_company_id', $companies->pluck('value'));
                    }
                    elseif ($allClient) {
                        $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    else {
                        $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                    }
                    // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                })->where('status', 0)->whereIn('type', $permissions)->count();
            }
            else {
                $notifications = Notification::with('account.profile')->where(function($q){
                    $q->where('status', 1)->orWhere('status', 2);
                })->where('pe_read', null)->where('account_id', auth()->user()->id)->latest()->take(10)->get();

                $counts_notif = Notification::where(function($q){
                    $q->where('status', 1)->orWhere('status', 2);
                })->where('pe_read', null)->where('account_id', auth()->user()->id)->count();
            }

            $view->with(compact('notifications', 'counts_notif'));
        });
    }
}
