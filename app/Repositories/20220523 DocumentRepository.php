<?php

namespace App\Repositories;

use App\Libraries\AetherSign\AetherSign;
use App\Models\Business\Assignments;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Storage;

class DocumentRepository
{
    public function signContract($req)
    {
        $totalPage = $req->totalPage - 1;
        $arrPage   = [];
        $actions   = json_decode(base64_decode($req->actions), true);

        for ($i = 1; $i <= $totalPage; $i++) {
            $arrPage[$i] = $i;
        }

        foreach ($actions as $action) {
            if (array_key_exists($action['page'], $arrPage)) {
                unset($arrPage[$action['page']]);
            }
        }

        if (count($arrPage) > 0) {
            return $arrPage;
        }

        $public = false;
        $pdf = new AetherSign(null, 'px');

        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

        $inputPath     = storage_path('app/public/tmp-contract/' . auth()->user()->sim_id . '.pdf');
        $outputName    = auth()->user()->sim_id . ".pdf";
        $outputPath    = storage_path('app/contract/' . $outputName);
        $pdf->numPages = $pdf->setSourceFile($inputPath);
        $actions       = json_decode(base64_decode($req->actions), true);

        $signature = storage_path('app/public/signature/' . auth()->user()->sim_id . '.png');

        foreach(range(1, $pdf->numPages, 1) as $page) {
            $rotate = false;
            $degree = 0;
            try {
                $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
                throw $e;
            }

            $size  = $pdf->getTemplateSize($pdf->_tplIdx);
            $scale = round($size['w'] / $req->docWidth, 3);

            $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h'], 'Rotate'=>$degree), true);
            $pdf->useTemplate($pdf->_tplIdx);

            foreach($actions as $action) {
                if(((int) $action['page']) === $page) {
                    if ($action['type'] == "signature") {
                        if (!$public) {
                            $pdf->Image($signature, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }
                        else{
                            $imageArray = explode( ',', $action['image'] );
                            $imgdata = base64_decode($imageArray[1]);
                            $pdf->Image('@'.$imgdata, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }
                    }
                }
            }
        }

        if (file_exists($outputPath)) {
            unlink($outputPath);
        }

        $pdf->Output($outputPath, 'F');

        DB::beginTransaction();
        try {
            $assignment = Assignments::where('account_id', auth()->user()->id)->first();
            $assignment->update([
                'ess_status' => 0,
                'signed_at'  => now()
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    /**
     * Check file orientation
     *
     * @param   float $width
     * @param   float $height
     * @return  string
     */
    public static function orientation($width, $height) {
        if ($width > $height) {
            return "L";
        }else{
            return "P";
        }
    }

    /**
     * Scale element dimension
     *
     * @param   int $dimension
     * @return  int
     */
    public static function scale($dimension, $scale) {
        return round($dimension * $scale);
    }
}
