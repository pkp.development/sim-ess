<?php

namespace App\Repositories\Admin;

use App\Models\Accounts\Accounts;
use App\Models\Accounts\Profiles;
use App\Models\Temporary\Families;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Accounts\Families as Family;
use App\Repositories\SipasApi\FamilyHandle;
use App\Repositories\SipasApi\GetEmployeeData;

class FamilyRepository
{
    public function getPendingFamily($uuid)
    {
        return Families::with('account')->where('uuid', $uuid)->where('status', 0)->first();
    }

    public function getFamily($id, $columnSearch)
    {
        return Family::with('account.profile')->where($columnSearch, $id)->first();
    }

    public function updateFamily($req, $tmpProfileId)
    {
        DB::beginTransaction();
        try {
            $family  = Families::with('account')->where('uuid', $tmpProfileId)->where('status', 0)->first();
            $account = Accounts::select('sim_id')->where('id', $family->account_id)->first();

            if ($req->type == 'verified') {
                $content = json_decode($family->content);

                $profile = Profiles::where('account_id', $family->account_id)->first();

                $familyCardImage = null;
                if ($content->family_card_image_change && Storage::exists($content->family_card_image)) {
                    $explode = explode('/', $content->family_card_image);

                    if(Storage::exists('images/kartu_keluarga/' . end($explode))) {
                        Storage::delete('images/kartu_keluarga/' . end($explode));
                    }

                    Storage::copy($content->family_card_image, 'images/kartu_keluarga/' . end($explode));

                    $familyCardImage = 'images/kartu_keluarga/' . end($explode);
                }

                $profile->update([
                    'family_card_number' => $content->family_card_number,
                    'family_card_image' => ($content->family_card_image_change) ? $familyCardImage : $profile->family_card_image,
                    'tax_marital_status' => $content->tax_marital_status
                ]);

                $family->status     = 1;
                $family->updated_by = auth()->user()->id;
                $family->save();

                // $repoEmployee = new GetEmployeeData;

                $content->familyCardImage = $familyCardImage;

                // $result = $repoEmployee->postEmployeeData($content, $account->sim_id, 'family');

                Session::flash('toast-success', 'Data Keluarga Employee berhasil diverifikasi');
            }
            elseif ($req->type == 'refused') {
                $content = json_decode($family->content);

                $family->status      = 2;
                $family->description = $req->description;
                $family->updated_by  = auth()->user()->id;
                $family->save();

                Session::flash('toast-success', 'Data Keluarga Employee berhasil ditolak');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if ($content->family_card_image_change && Storage::exists($content->family_card_image)) {
            Storage::delete($content->family_card_image);
        }

        return true;
    }

    public function daftarFamilyPost($req, $uuid)
    {
        DB::beginTransaction();
        try {
            $family = $this->getFamily($uuid, 'uuid');

            if ($family->status != 0) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return 'restricted';
            }

            if ($req->type == 'verified') {
                $family->status      = 1;
                $family->verified_by = auth()->user()->id;
                $family->save();

                // $api = new FamilyHandle;

                if ($family->action == 'create') {
                    // $api->postFamily($family, 'create');
                }
                elseif ($family->action == 'update') {
                    // $api->postFamily($family, 'update');
                }
                elseif ($family->action == 'delete') {
                    // $api->postFamily($family, 'delete');
                    $family->delete();
                }

                Session::flash('toast-success', 'Daftar keluarga berhasil diverifikasi');
            }
            elseif ($req->type == 'refused') {
                $family->status      = 2;
                $family->description = $req->description;
                $family->verified_by = auth()->user()->id;
                $family->save();

                Session::flash('toast-success', 'Daftar keluarga berhasil ditolak');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }
}
