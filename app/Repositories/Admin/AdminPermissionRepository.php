<?php

namespace App\Repositories\Admin;

use App\Models\Accounts\Admin;
use App\Models\Organization\SimCompanies;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Permissions\PermissionCompany;
use App\Models\Permissions\PermissionSimBranch;
use App\Repositories\BprApi\MasterClientCompany;
use App\Repositories\BprApi\MasterSimBranch;
use App\Repositories\SipasApi\GetSimOrganization;
use App\Repositories\SipasApi\GetClientOrganization;

class AdminPermissionRepository
{
    public function storePermission(Admin $admin, $req)
    {
        DB::beginTransaction();
        try {
            $admin->permissions()->sync($req->permissions);
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function datatableClient($adminId)
    {
        $model = PermissionCompany::where('admin_id', $adminId)->select('permission_admin_company.*');

        return DataTables::of($model)
            ->addColumn('action', function ($data) {
                $html = '<form method="POST" action="'. route('admin.user.permission-client.delete', $data->id) .'" style="display: inline-block" accept-charset="UTF-8">';
                $html .= method_field('DELETE');
                $html .= csrf_field();
                $html .= '<a href="#" class="action-icon btn-confirm" title="Delete" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-delete-outline"></i></a>';
                $html .= '</form>';

                return $html;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function storeClient($req, $adminId)
    {
        DB::beginTransaction();
        try {
            if ($req->has('client_company_for_all')) {
                PermissionCompany::where('admin_id', $adminId)->delete();

                PermissionCompany::create([
                    'admin_id' => $adminId,
                    'value'    => 0,
                    'name'     => 'Semua Client Company'
                ]);
            }
            else {
                $clients = PermissionCompany::where('admin_id', $adminId)->get();
                PermissionCompany::where('admin_id', $adminId)->where('value', 0)->delete();
                foreach ($req->client_company as $client) {
                    $filter = [
                        "id" => $client
                    ];

                    // $sipasAPI = new GetClientOrganization;
                    $api = new MasterClientCompany;

                    // $result      = $sipasAPI->getCompany($req->page, $filter);
                    $result  = $api->getClientCompanyParams($filter);
                    // $value       = null;
                    // $companyName = null;

                    // foreach($result['results']->result as $res) {
                        $value       = $result->id;
                        $companyName = $result->nama;
                    // }

                    if (isset($clients)) {
                        if (!$clients->where('value', $value)->first()) {
                            PermissionCompany::create([
                                'admin_id' => $adminId,
                                'value'    => $value,
                                'name'     => $companyName
                            ]);
                        }
                    }
                }
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function deleteClient($id)
    {
        DB::beginTransaction();
        try {
            $clientPermission = PermissionCompany::with('admin.account')->where('id', $id)->first();
            $uuid = null;

            if ($clientPermission) {
                $uuid = $clientPermission->admin->account->uuid;
                $clientPermission->delete();
            }
            else {
                return false;
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return $uuid;
    }

    public function datatableSimBranch($adminId)
    {
        $model = PermissionSimBranch::where('admin_id', $adminId)->select('permission_admin_simbranch.*');

        return DataTables::of($model)
            ->editColumn('name', function ($data) {
                if ($data->company) {
                    $company = " " . "({$data->company})";
                }
                else {
                    $company = null;
                }

                return $data->name . $company;
            })
            ->addColumn('action', function ($data) {
                $html = '<form method="POST" action="'. route('admin.user.permission-sim-branch.delete', $data->id) .'" style="display: inline-block" accept-charset="UTF-8">';
                $html .= method_field('DELETE');
                $html .= csrf_field();
                $html .= '<a href="#" class="action-icon btn-confirm" title="Delete" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-delete-outline"></i></a>';
                $html .= '</form>';

                return $html;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function storeSimBranch($req, $adminId)
    {
        DB::beginTransaction();
        try {
            if ($req->has('sim_branch_for_all')) {
                $simBranches = PermissionSimBranch::where('admin_id', $adminId)->delete();

                PermissionSimBranch::create([
                    'admin_id' => $adminId,
                    'value'    => 0,
                    'name'     => 'Semua Cabang',
                    'company'  => 'All'
                ]);
            }
            else {
                $simBranches = PermissionSimBranch::where('admin_id', $adminId)->get();
                PermissionSimBranch::where('admin_id', $adminId)->where('value', 0)->delete();
                foreach ($req->sim_branch as $simBranch) {
                    $filter = [
                        "id" => $simBranch
                    ];

                    $api          = new MasterSimBranch;
                    $simCompanies = SimCompanies::get();

                    $result  = $api->getSimBranchParams($filter);
                    // $value   = null;
                    // $name    = null;
                    // $company = null;

                    // foreach($result['results'] as $res) {
                        $value      = $result->id;
                        $name       = $result->cabang;
                        $simCompany = $simCompanies->where('sipas_id', $result->id_perusahaan)->first();
                        $company    = ($simCompany) ? $simCompany->abbreviation : null;
                    // }

                    if (isset($simBranches)) {
                        if (!$simBranches->where('value', $value)->first()) {
                            PermissionSimBranch::create([
                                'admin_id' => $adminId,
                                'value'    => $value,
                                'name'     => $name,
                                'company'  => $company
                            ]);
                        }
                    }
                }
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function deleteSimBranch($id)
    {
        DB::beginTransaction();
        try {
            $simBranchPermission = PermissionSimBranch::with('admin.account')->where('id', $id)->first();
            $uuid = null;

            if ($simBranchPermission) {
                $uuid = $simBranchPermission->admin->account->uuid;
                $simBranchPermission->delete();
            }
            else {
                return false;
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return $uuid;
    }
}
