<?php

namespace App\Repositories\Admin;

use App\Models\Temporary\Profile;
use App\Repositories\BprGoApi\RestBprGo;
use App\Repositories\BprPhpApi\RestBprPhp;
use Illuminate\Support\Facades\DB;
use App\Models\Business\BankAccounts;
use App\Repositories\SipasApi\GetBank;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\SipasApi\GetEmployeeData;

class BankRepository
{
    public function getPendingRekening($id)
    {
        // $api = new GetBank;
        $profile = Profile::with('account')->where('uuid', $id)->where('type', 'bank')->where('status', 0)->first();
        $content = json_decode($profile->content);

        // $getBank = $api->getBank(1, ['bank_code' => $content->name_of_bank]);

        // $content->bank_name = $getBank->result[0];
        $profile->content   = json_encode($content);

        return $profile;
    }

    public function getBankAccount($account_id)
    {
        $bankAccount = BankAccounts::with('account')->where('account_id', $account_id)->first();

        // $api     = new GetBank;
        // $getBank = $api->getBank(1, ['bank_code' => $bankAccount->name_of_bank]);
        // $bankAccount->bank_name = ($getBank->result) ? $getBank->result[0] : null;

        return $bankAccount;
    }

    public function updateRekening($req, $uuid)
    {
        DB::beginTransaction();
        try {
            $rekening = $this->getPendingRekening($uuid);

            if(!$rekening) {
                Session::flash('toast-error', 'Biodata Employee gagal diupdate');

                return false;
            }

            if($req->type == 'verified') {
                $content = json_decode($rekening->content);

                $bankAccount = BankAccounts::with('account')->where('account_id', $rekening->account_id)->first();

                $bankImage = null;
                if ($content->bank_account_image_change && Storage::exists($content->bank_account_image)) {
                    $explode = explode('/', $content->bank_account_image);

                    if(Storage::exists('images/buku_tabungan/' . end($explode))) {
                        Storage::delete('images/buku_tabungan/' . end($explode));
                    }

                    Storage::copy($content->bank_account_image, 'images/buku_tabungan/' . end($explode));
                    $explode_bank_end = end($explode);
                    $bankImage = 'images/buku_tabungan/' . end($explode);
                }

                $bankAccount->update([
                    'name_of_bank'       => $content->name_of_bank,
                    'account_number'     => $content->account_number,
                    'account_name'       => $content->account_name,
                    'bank_account_image' => ($content->bank_account_image) ? $bankImage : $bankAccount->bank_account_image
                ]);

                $rekening->status     = 1;
                $rekening->updated_by = auth()->user()->id;
                $saved_rekening = $rekening->save();

                if($saved_rekening){

                    $apiEmp = new RestBprGo;

                    $_pathEmpBprByRegNik = env('EMP_BPR_BY_REG_NIK').'/'.$rekening->account->sim_id.'/'.$rekening->account->profile->nik;
                    $empBpr = $apiEmp->bprApi($_pathEmpBprByRegNik, 'GET');

                    if ($empBpr->code == '200' and $empBpr->status == 'OK') {
                        if ($empBpr->data) {

                            $_pathEmpBprByParent = env('EMP_BPR_BANK_ACC_BY_PARENT').'/'.$empBpr->data->id;
                            $checkAcc = new RestBprGo;
                            $checkBankAcc = $checkAcc->bprApi($_pathEmpBprByParent, 'GET');

                            if ($content->bank_account_image_change && Storage::exists($content->bank_account_image)) {
                                $format_file_image = explode('.', $explode_bank_end);
                                $bankBprFilename = 'bank-' . $empBpr->data->id . '.' . end($format_file_image);
                                $bankApi = new RestBprPHP();
                                $backupOldFile = $bankApi->sendFile($bankBprFilename, 'bank', 'app/' . $bankImage, 'check');
                                if ($backupOldFile->status) {
                                    $send_rekening = $bankApi->sendFile($bankBprFilename, 'bank', 'app/' . $bankImage, 'send');

                                    if (!$send_rekening->status) {

                                        DB::rollback();
                                        Session::flash('toast-success', 'Gagal menyimpan file: acc bank di BPR. Err: ' . $send_rekening->message);
                                        return false;
                                    }else{
                                        $fileTypeBpr = 195590;
                                        $rekFile = new RestBprGo;
                                        $pathRekCheck = env('EMP_FILE_BPR_BY_EMP_ID').'/'.$empBpr->data->id.'/'.$fileTypeBpr;
                                        $pathRek = env('EMP_FILE_BPR');
                                        $RekBprExists = $rekFile->bprApi($pathRekCheck, 'GET');

                                        $body = [
                                            'file_no' => "",
                                            'file_name' => "RECBANK-ESS-".$bankAccount->account->sim_id,
                                            'file_type' => $fileTypeBpr,
                                            'file_start_date' => "0000-00-00",
                                            'file_end_date' => "0000-00-00",
                                            'filename' => $bankBprFilename,
                                            'remark' => "",
                                            'status' => "t",
                                        ];

                                        if($RekBprExists->code == '200' and $RekBprExists->status == 'OK' and $RekBprExists->data){
                                            //update
                                            $body['update_by'] = "8";
                                            $rekBprUpgrade = $rekFile->bprApi($pathRek."/".$RekBprExists->data[0]->id, 'PUT', $body);
                                        }else if($RekBprExists->code == '200' and $RekBprExists->status == 'OK'){
                                            $body['parent_id'] = $empBpr->data->id;
                                            $body['create_by'] = "8";

                                            $rekBprUpgrade = $rekFile->bprApi($pathRek, 'POST', $body);
                                        }else{

                                            DB::rollback();
                                            Session::flash('toast-success', 'Gagal insert data rekening di emp_file (1). Err:'.$RekBprExists->data);
                                            return false;
                                        }

                                        if($rekBprUpgrade->code != '200'){

                                            DB::rollback();
                                            Session::flash('toast-success', 'Gagal insert data rekening di emp_file (2). Err:'.$rekBprUpgrade->data);
                                            return false;
                                        }

                                    }
                                } else {

                                    DB::rollback();
                                    Session::flash('toast-success', 'Gagal menyimpan file: profile di BPR. Err: backup file pic');
                                    return false;
                                }
                            }

                            if ($checkBankAcc->code == 200) {
                                if ($checkBankAcc->data) {
                                    foreach($checkBankAcc->data as $keyBank => $bankAccdata){
                                        $bodies = [
                                            'bank_id' => $bankAccdata->bank_id,
                                            'parent_id' => $bankAccdata->parent_id,
                                            'branch' => $bankAccdata->branch,
                                            'account_no' => $bankAccdata->account_no,
                                            'account_name' =>  $bankAccdata->account_name,
                                            'remark' =>  $bankAccdata->remark,
                                            'filename' =>  $bankAccdata->filename,
                                            'status' => 'f',
                                            'create_by' =>  $bankAccdata->create_by,
                                            'create_date' =>  $bankAccdata->create_date,
                                            'update_by' => '8'
                                        ];
                                        $_pathUpdateEmpBankAccBpr = env('EMP_BPR_BANK_ACC') . '/' . $bankAccdata->id;
                                        $apiCreateBankAcc = new RestBprGo;
                                        $upgradeEmpBankAcc = $apiCreateBankAcc->bprApi($_pathUpdateEmpBankAccBpr, 'PUT', $bodies);
                                        if ($upgradeEmpBankAcc->code != 200) {

                                            DB::rollback();
                                            Session::flash('toast-success', json_encode($upgradeEmpBankAcc->data, true));
                                            return false;
                                        }
                                    }
                                }

                                $bodies = [
                                    'bank_id' => (int)$content->name_of_bank,
                                    'branch' => "",
                                    'account_no' => $content->account_number,
                                    'account_name' => $content->account_name,
                                    'remark' => '',
                                    'filename' => $bankBprFilename,
                                    'status' => 't',
                                    'parent_id' => $empBpr->data->id,
                                    'create_by' => '8'
                                ];
                                $_pathCreateEmpBankAccBpr = env('EMP_BPR_BANK_ACC');
                                $apiCreateBankAcc = new RestBprGo;
                                $createEmpBankAcc = $apiCreateBankAcc->bprApi($_pathCreateEmpBankAccBpr, 'POST', $bodies);

                                if ($createEmpBankAcc->code != 200) {

                                    DB::rollback();
                                    Session::flash('toast-success', json_encode($upgradeEmpBankAcc->data, true));
                                    return false;
                                }
                            }else{

                                DB::rollback();
                                Session::flash('toast-success', 'Check bank account di BPR error');
                                return false;
                            }
                        } else {
                            DB::rollback();
                            Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                            return false;
                        }
                    } else {
                        DB::rollback();
                        Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                        return false;
                    }

                }

                // $repoEmployee = new GetEmployeeData;

                $content->bankImage = $bankImage;

                // $result = $repoEmployee->postEmployeeData($content, $rekening->account->sim_id, 'bank');

                Session::flash('toast-success', 'Data Rekening Employee berhasil diverifikasi');
            }
            elseif($req->type == 'refused') {
                $content = json_decode($rekening->content);

                $rekening->status      = 2;
                $rekening->description = $req->description;
                $rekening->updated_by  = auth()->user()->id;
                $rekening->save();

                Session::flash('toast-success', 'Data Rekening Employee berhasil ditolak');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if ($content->bank_account_image_change && Storage::exists($content->bank_account_image)) {
            Storage::delete($content->bank_account_image);
        }

        return true;
    }
}
