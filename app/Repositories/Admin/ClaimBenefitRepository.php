<?php

namespace App\Repositories\Admin;

use Storage;
use Ramsey\Uuid\Uuid;
use ZanySoft\Zip\Zip;
use App\Imports\BpjsKes;
use App\Imports\BpjsTkj;
use App\Imports\Insurance;
use Spatie\PdfToImage\Pdf;
use Illuminate\Support\Str;
use App\Models\Tasks\Benefit;
use App\Mail\IdCardNotification;
use App\Models\Accounts\Accounts;
use Illuminate\Support\Facades\DB;
use App\Models\Tasks\BenefitDetail;
use App\Models\Business\Assignments;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Org_Heigl\Ghostscript\Ghostscript;
use Yajra\DataTables\Facades\DataTables;

class ClaimBenefitRepository
{
    public function datatablesKesehatan()
    {
        $model = Benefit::with('account.admin', 'details')->select('task_benefit.*')->where('type', 'bpjs_kesehatan');

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Finished</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">On Progress</span>';
                }
            })
            ->addColumn('total_failed', function($data){
                return '<span class="badge bg-soft-danger text-danger">'. count($data->details->where('status', 'Failed')) .'</span>';
            })
            ->addColumn('tanggal', function($data){
                return $data->created_at->format('d F Y H:i:s');
            })
            ->addColumn('action', function($data){
                $html = action_btn(route('admin.bpjs-kes.detail', $data->uuid), [], 'mdi mdi-details');
                return $html;
            })
            ->rawColumns(['status', 'total_failed', 'action'])
            ->make(true);
    }

    public function datatablesKetenagakerjaan()
    {
        $model = Benefit::with('account.admin', 'details')->select('task_benefit.*')->where('type', 'bpjs_ketenagakerjaan');

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Finished</span>';
                }
                else if($data->status == 2) {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
                else {
                    return '<span class="badge bg-soft-danger text-danger">On Progress</span>';
                }
            })
            ->addColumn('tanggal', function($data){
                return $data->created_at->format('d F Y H:i:s');
            })
            ->addColumn('total_failed', function($data){
                return '<span class="badge bg-soft-danger text-danger">'. count($data->details->where('status', 'Failed')) .'</span>';
            })
            ->addColumn('action', function($data){
                $html = action_btn(route('admin.bpjs-tkj.detail', $data->uuid), [], 'mdi mdi-details');
                return $html;
            })
            ->rawColumns(['status', 'total_failed', 'action'])
            ->make(true);
    }

    public function datatablesAsuransi()
    {
        $model = Benefit::with('account.admin')->select('task_benefit.*')->where('type', 'insurance');

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Finished</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">On Progress</span>';
                }
            })
            ->addColumn('tanggal', function($data){
                return $data->created_at->format('d F Y');
            })
            ->addColumn('action', function($data){
                $html = action_btn(route('admin.vendor-asuransi.detail', $data->uuid), [], 'mdi mdi-details');
                return $html;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function datatablesIdCard()
    {
        $model = Benefit::with('account.admin')->select('task_benefit.*')->where('type', 'id_card');

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Finished</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">On Progress</span>';
                }
            })
            ->addColumn('tanggal', function($data){
                return $data->created_at->format('d F Y');
            })
            ->addColumn('action', function($data){
                $html = action_btn(route('admin.employee.idcard.detail', $data->uuid), [], 'mdi mdi-details');
                return $html;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function detailKesehatan($uuid)
    {
        $benefit = Benefit::select('id')->where('uuid', $uuid)->where('type', 'bpjs_kesehatan')->first();
        $model   = BenefitDetail::select('task_benefit_detail.*')->where('task_benefit_id', $benefit->id);

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 'Passed') {
                    return '<span class="badge bg-soft-success text-success">Passed</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function detailKetenagakerjaan($uuid)
    {
        $benefit = Benefit::select('id')->where('uuid', $uuid)->where('type', 'bpjs_ketenagakerjaan')->first();
        $model   = BenefitDetail::select('task_benefit_detail.*')->where('task_benefit_id', $benefit->id);

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 'Passed') {
                    return '<span class="badge bg-soft-success text-success">Passed</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function detailAsuransi($uuid)
    {
        $benefit = Benefit::select('id')->where('uuid', $uuid)->where('type', 'insurance')->first();
        $model   = BenefitDetail::select('task_benefit_detail.*')->where('task_benefit_id', $benefit->id);

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 'Passed') {
                    return '<span class="badge bg-soft-success text-success">Passed</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function detailIdCard($uuid)
    {
        $benefit = Benefit::select('id')->where('uuid', $uuid)->where('type', 'id_card')->first();
        $model   = BenefitDetail::select('task_benefit_detail.*')->where('task_benefit_id', $benefit->id);

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 'Passed') {
                    return '<span class="badge bg-soft-success text-success">Passed</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function bpjsKesehatanUpload($req)
    {
        DB::beginTransaction();
        try {
            $uuid     = Uuid::uuid4()->toString();
            $path     = 'bpjs-kesehatan/' . $uuid;
            $filename = null;

            Storage::makeDirectory($path);

            $benefit = Benefit::create([
                'uuid'        => $uuid,
                'uploaded_by' => auth()->user()->id,
                'status'      => 0,
                'type'        => 'bpjs_kesehatan',
                'excel_name'  => $uuid . '.csv',
                'zip_name'    => $uuid . '.zip'
            ]);

            if($req->hasFile('bpjs_kesehatan_zip')) {
                $file     = $req->file('bpjs_kesehatan_zip');
                $filename = $uuid . '.' . $file->extension();

                Storage::putFileAs($path, $file, $filename);
            }

            $zip = Zip::open(storage_path('app/' . $path . '/' . $filename));
            $zip->extract(storage_path('app/' . $path));
            $zip->close();
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Excel::import(new BpjsKes($path, $benefit->id), $req->file('bpjs_kesehatan_csv'), null, \Maatwebsite\Excel\Excel::CSV);

        if (Storage::exists($path . '/' . $filename)) {
            Storage::delete($path . '/' . $filename);
            Storage::deleteDirectory($path);
        }

        $benefit->status = 1;
        $benefit->save();

        return true;
    }

    public function bpjsKetenagakerjaanUpload($req)
    {
        DB::beginTransaction();
        try {
            $uuid     = Uuid::uuid4()->toString();
            $path     = 'bpjs-ketenagakerjaan/' . $uuid;
            $filename = null;

            Storage::makeDirectory($path);

            $benefit = Benefit::create([
                'uuid'        => $uuid,
                'uploaded_by' => auth()->user()->id,
                'status'      => 0,
                'type'        => 'bpjs_ketenagakerjaan',
                'excel_name'  => $uuid . '.csv',
                'zip_name'    => $uuid . '.zip'
            ]);

            if($req->hasFile('bpjs_ketenagakerjaan_zip')) {
                $file     = $req->file('bpjs_ketenagakerjaan_zip');
                $filename = $uuid . '.' . $file->extension();

                Storage::putFileAs($path, $file, $filename);
            }

            $zip = Zip::open(storage_path('app/' . $path . '/' . $filename));
            $zip->extract(storage_path('app/' . $path));
            $zip->close();
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Excel::import(new BpjsTkj($path, $benefit->id), $req->file('bpjs_ketenagakerjaan_csv'), null, \Maatwebsite\Excel\Excel::CSV);

        if (Storage::exists($path . '/' . $filename)) {
            Storage::delete($path . '/' . $filename);
            Storage::deleteDirectory($path);
        }

        $benefit->status = 1;
        $benefit->save();

        return true;
    }

    public function insuranceUpload($req)
    {
        DB::beginTransaction();
        try {
            $uuid     = Uuid::uuid4()->toString();

            $benefit = Benefit::create([
                'uuid'        => $uuid,
                'uploaded_by' => auth()->user()->id,
                'status'      => 0,
                'type'        => 'insurance',
                'excel_name'  => $uuid . '.csv'
            ]);

            Excel::import(new Insurance($benefit->id), $req->file('insurance_csv'), null, \Maatwebsite\Excel\Excel::CSV);

            $benefit->status = 1;
            $benefit->save();
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function employeeIdCardUpload($req)
    {
        DB::beginTransaction();
        try {
            $uuid     = Uuid::uuid4()->toString();
            $path     = 'id-card/' . $uuid;
            $filename = null;

            Storage::makeDirectory($path);

            $benefit = Benefit::create([
                'uuid'        => $uuid,
                'uploaded_by' => auth()->user()->id,
                'status'      => 0,
                'type'        => 'id_card',
                'zip_name'    => $uuid . '.zip'
            ]);

            if($req->hasFile('id_card_zip')) {
                $file     = $req->file('id_card_zip');
                $filename = $uuid . '.' . $file->extension();

                Storage::putFileAs($path, $file, $filename);
            }

            $zip = Zip::open(storage_path('app/' . $path . '/' . $filename));
            $zip->extract(storage_path('app/' . $path));
            $zip->close();

            Storage::delete($path . '/' . $filename);

            $files = Storage::files($path);
            foreach ($files as $file) {
                $fileName = explode('/', $file);
                $simid = explode('.', end($fileName))[0];

                $account = Accounts::with('assignment', 'profile')->where('sim_id', trim($simid))->first();

                if ($account) {
                    if (env('GS_PATH_ENABLED', false)) {
                        Ghostscript::setGsPath(env('GS_PATH'));
                    }
                    $pdf = new Pdf(storage_path('app/' . $file));

                    if (Storage::exists('images/id_card/' . trim($simid) . '.jpg')) {
                        Storage::delete('images/id_card/' . trim($simid) . '.jpg');
                    }

                    $pdf->setPage(1)->saveImage(storage_path('app/images/id_card/' . trim($simid) . '.jpg'));

                    if (Storage::exists('images/id_card/' . trim($simid) . '.pdf')) {
                        Storage::delete('images/id_card/' . trim($simid) . '.pdf');
                    }

                    Storage::copy($file, 'images/id_card/' . trim($simid) . '.pdf');

                    if ($account->assignment) {
                        $account->assignment->id_card_image = 'images/id_card/' . trim($simid) . '.jpg';
                        $account->assignment->save();
                    }
                    else {
                        Assignments::create([
                            'account_id'    => $account->id,
                            'id_card_image' => 'images/id_card/' . trim($simid) . '.jpg'
                        ]);
                    }

                    BenefitDetail::create([
                        'task_benefit_id' => $benefit->id,
                        'simid'           => trim($simid),
                        'status'          => 'Passed'
                    ]);

                    //Mail::to($account->email)->send(new IdCardNotification($account->profile->name));
                }
                else {
                    BenefitDetail::create([
                        'task_benefit_id' => $benefit->id,
                        'simid'           => trim($simid),
                        'status'          => 'Failed',
                        'description'     => 'SIMID Not Found',
                    ]);
                }
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Storage::deleteDirectory($path);

        $benefit->status = 1;
        $benefit->save();

        return true;
    }
}
