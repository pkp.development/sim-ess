<?php

namespace App\Repositories\Admin;

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use App\Libraries\Facades\ImageHandler;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Organization\SimCompanies;
use App\Repositories\BprApi\MasterSimCompanies;
use App\Repositories\SipasApi\GetSimOrganization;

class CompaniesRepository
{
    public function datatables()
    {
        $model = SimCompanies::select('sim_companies.*');

        return DataTables::of($model)
            ->editColumn('logo', function ($data){
                if ($data->logo) {
                    $html = '<a href="'. route('admin.company.getImage', [$data->uuid, 'logo']) .'" class="image-popup" title="'. $data->name .'"><img class="avatar-sm img-responsive" src="'. route('admin.company.getImage', [$data->uuid, 'logo']) .'"></img></a>';
                }
                else {
                    $html = '<a href="'. asset('assets/images/logo-placeholder.png') .'" class="image-popup" title="'. $data->name .'"><img class="avatar-sm img-responsive" src="'. asset('assets/images/logo-placeholder.png') .'"></img></a>';
                }

                return $html;
            })
            ->addColumn('action', function ($data) {
                $html = action_btn('javascript:void(0)', ['btn-detail'], 'mdi mdi-details', null, [['key' => 'url', 'value' => route('admin.company.getData', $data->uuid)]]);
                $html .= action_btn('javascript:void(0)', ['btn-edit'], 'mdi mdi-square-edit-outline', null, [['key' => 'url', 'value' => route('admin.company.getData', $data->uuid)]]);
                return $html;
            })
            ->rawColumns(['logo', 'action'])
            ->make(true);
    }

    public function sync()
    {
        DB::beginTransaction();
        try {
            $api = new MasterSimCompanies;

            $result = $api->getSimCompanies();

            if ($result->current_page < $result->last_page) {
                for ($i = 1; $i <= $result->last_page; $i++) {
                    if ($i != 1) {
                        $result = $api->getSimCompanies($i);
                    }

                    foreach ($result->data as $company) {
                        $check = SimCompanies::where('sipas_id', $company->id)->first();
                        if ($check) {
                            $check->update([
                                'name'         => $company->perusahaan,
                                'abbreviation' => $company->kode,
                                'sync_at'      => now()
                            ]);
                        }
                        else {
                            SimCompanies::create([
                                'uuid'         => Uuid::uuid4()->toString(),
                                'sipas_id'     => $company->id,
                                'name'         => $company->perusahaan,
                                'abbreviation' => $company->kode,
                                'sync_at'      => now()
                            ]);
                        }
                    }
                }
            }
            else {
                foreach ($result->data as $company) {
                    $check = SimCompanies::where('sipas_id', $company->id)->first();
                    if ($check) {
                        $check->update([
                            'name'         => $company->perusahaan,
                            'abbreviation' => $company->kode,
                            'sync_at'      => now()
                        ]);
                    }
                    else {
                        SimCompanies::create([
                            'uuid'         => Uuid::uuid4()->toString(),
                            'sipas_id'     => $company->id,
                            'name'         => $company->perusahaan,
                            'abbreviation' => $company->kode,
                            'sync_at'      => now()
                        ]);
                    }
                }
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function update($uuid, $req)
    {
        DB::beginTransaction();
        try {
            $data = $this->getData($uuid);

            if (!$data) {
                return false;
            }

            $imgLogo = $data->logo;
            if ($req->hasFile('logo')) {
                $image    = $req->file('logo');
                $filename = $req->abbreviation . '.' . $image->extension();
                $path     = 'images/company_logo';
                $image    = ImageHandler::resizing($image);
                $imgLogo  = $path . '/' . $filename;

                Storage::put($imgLogo, $image);
                Storage::disk('public')->put($imgLogo, $image);
            }

            $imgSign = $data->coordinator_sign;
            if ($req->hasFile('coordinator_sign')) {
                $image    = $req->file('coordinator_sign');
                $filename = $req->abbreviation . '.' . $image->extension();
                $path     = 'images/coordinator_sign';
                $image    = ImageHandler::resizing($image);
                $imgSign  = $path . '/' . $filename;

                Storage::put($imgSign, $image);
                Storage::disk('public')->put($imgSign, $image);
            }

            $data->update([
                'name'             => $req->name,
                'abbreviation'     => $req->abbreviation,
                'npwp'             => $req->npwp,
                'coordinator'      => $req->coordinator,
                'logo'             => $imgLogo,
                'coordinator_npwp' => $req->coordinator_npwp,
                'coordinator_sign' => $imgSign
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function getData($uuid)
    {
        $result = SimCompanies::where('uuid', $uuid)->first();

        if($result) {
            return $result;
        }

        return false;
    }
}
