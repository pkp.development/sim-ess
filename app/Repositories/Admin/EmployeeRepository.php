<?php

namespace App\Repositories\Admin;

use App\Repositories\BprPhpApi\RestBprPhp;
use Ramsey\Uuid\Uuid;
use App\Models\Accounts\Accounts;
use App\Models\Accounts\Profiles;
use App\Models\Config\AuditTrail;
use App\Models\Temporary\Profile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Repositories\SipasApi\GetRegion;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Permissions\PermissionCompany;
use App\Repositories\SipasApi\GetEmployeeData;
use App\Models\Permissions\PermissionSimBranch;
use App\Repositories\SipasApi\GetSimOrganization;
use App\Repositories\BprGoApi\RestBprGo;
use function App\Http\Controllers\Admin\reconcile_data_master_bpr;

class EmployeeRepository
{
    public function datatables()
    {
        if (auth()->user()->role == 'admin') {
            $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
            $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();

            $model = Profiles::with('account.assignment')->select('profiles.*')->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                $allBranch = $simBrances->where('value', 0)->first();
                $allClient = $companies->where('value', 0)->first();

                if ($allBranch && $allClient) {
                    $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
                }
                elseif ($allBranch) {
                    $query->whereIn('client_company_id', $companies->pluck('value'));
                }
                elseif ($allClient) {
                    $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
                }
                else {
                    $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                }
                // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
            });
        }
        else {
            $model = Profiles::with('account.assignment')->select('profiles.*');
        }

        return DataTables::of($model)
            ->editColumn('account.status', function($data){
                if($data->account->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Aktif</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">Tidak Aktif</span>';
                }
            })
            ->addColumn('action', function($data){
                $html = '<a href="'. route('admin.employee.detail', $data->uuid) .'" class="action-icon" title="Detail" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-details"></i></a>';

                if (Gate::allows('update-password')) {
                    $html .= '<a href="javascript:void(0)" class="action-icon btn-edit" title="Edit" data-route="'. route('admin.employee.change-password', $data->account->uuid) .'" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-square-edit-outline"></i></a>';
                }

                return $html;
            })
            ->rawColumns(['account.status', 'action'])
            ->make(true);
    }

    public function getData($uuid)
    {
        // $simOrg    = new GetSimOrganization;
        // $simRegion = new GetRegion;
        $dbProfile = Profiles::with('account.assignment')->where('uuid', $uuid)->first();

        if($dbProfile->assignment) {
           // $simBranch              = $simOrg->getCompanyFromBranch($dbProfile->assignment->sim_branch_id, 'companyname');
           // $dbProfile->sim_company = $simBranch;
            $dbProfile->sim_company = "";
        }

        if($dbProfile->province_identity) {
            // $province                      = $simRegion->getProvinceData($dbProfile->province_identity);
            // $dbProfile->provinces_identity = $province;
            $dbProfile->provinces_identity = "";
        }

        if($dbProfile->city_identity) {
            // $city                       = $simRegion->getCityData($dbProfile->city_identity);
            // $dbProfile->cities_identity = $city;
            $dbProfile->cities_identity = "";
        }

        if($dbProfile->domicile_province) {
            // $province                      = $simRegion->getProvinceData($dbProfile->domicile_province);
            // $dbProfile->domicile_provinces = $province;
            $dbProfile->domicile_provinces = "";

        }

        if($dbProfile->domicile_city) {
            // $city                       = $simRegion->getCityData($dbProfile->domicile_city);
            // $dbProfile->domicile_cities = $city;
            $dbProfile->domicile_cities = "";
        }

        return $dbProfile;
    }

    public function getPendingProfile($id, $status = 'pending')
    {
        if ($status == 'pending') {
            return Profile::with('account')->where('uuid', $id)->where('status', 0)->first();
        }

        return Profile::with('account')->where('uuid', $id)->first();
    }

    public function extractRegionContent($content)
    {
        $simRegion = new GetRegion;

        if($content->province_identity) {
            $province                    = $simRegion->getProvinceData($content->province_identity);
            $content->provinces_identity = $province;
        }

        if($content->city_identity) {
            $city                       = $simRegion->getCityData($content->city_identity);
            $content->cities_identity = $city;
        }

        if($content->domicile_province) {
            $province                    = $simRegion->getProvinceData($content->domicile_province);
            $content->domicile_provinces = $province;
        }

        if($content->domicile_city) {
            $city                     = $simRegion->getCityData($content->domicile_city);
            $content->domicile_cities = $city;
        }

        return $content;
    }

    public function getProfile($id, $columnSearch)
    {
        $profile   = Profiles::where($columnSearch, $id)->first();

        return $profile;
    }

    public function updateBiodataProfile($req, $tmpProfileId)
    {
        DB::beginTransaction();
        try {
            $profile = Profile::where('uuid', $tmpProfileId)->where('status', 0)->first();

            if(!$profile) {
                Session::flash('toast-error', 'Biodata Employee gagal diupdate');
                return false;
            }

            if($req->type == 'verified') {
                $content = json_decode($profile->content);
                $account = Accounts::with('profile')->where('id', $profile->account_id)->first();

                if(!$account) {
                    Session::flash('toast-error', 'Biodata Employee gagal diupdate');
                    return false;
                }

                $profilePicture = null;
                $identityImage  = null;
                $selfieImage    = null;
                $npwpImage      = null;

                $account->update([
                    'email' => $content->email,
                ]);

                if ($content->profile_picture_change && Storage::exists($content->profile_picture)) {
                    $explode_pic = explode('/', $content->profile_picture);

                    if(Storage::exists('images/profile_picture/' . end($explode_pic))) {
                        Storage::delete('images/profile_picture/' . end($explode_pic));
                    }

                    Storage::copy($content->profile_picture, 'images/profile_picture/' . end($explode_pic));
                    $explode_pic_end = end($explode_pic);
                    $profilePicture = 'images/profile_picture/' . $explode_pic_end;
                }
                if ($content->identity_image_change && Storage::exists($content->identity_image)) {
                    $explode_ktp = explode('/', $content->identity_image);

                    if(Storage::exists('images/identity_image/' . end($explode_ktp))) {
                        Storage::delete('images/identity_image/' . end($explode_ktp));
                    }

                    Storage::copy($content->identity_image, 'images/identity_image/' . end($explode_ktp));

                    $explode_ktp_end = end($explode_ktp);
                    $identityImage = 'images/identity_image/' . end($explode_ktp);
                }
                if ($content->npwp_image_change && Storage::exists($content->npwp_image)) {
                    $explode_npwp = explode('/', $content->npwp_image);

                    if(Storage::exists('images/npwp_image/' . end($explode_npwp))) {
                        Storage::delete('images/npwp_image/' . end($explode_npwp));
                    }

                    Storage::copy($content->npwp_image, 'images/npwp_image/' . end($explode_npwp));

                    $explode_npwp_end = end($explode_npwp);
                    $npwpImage = 'images/npwp_image/' . end($explode_npwp);
                }
                if ($content->selfie_image_change && Storage::exists($content->selfie_image)) {
                    $explode_selfie = explode('/', $content->selfie_image);

                    if(Storage::exists('images/selfie/' . end($explode_selfie))) {
                        Storage::delete('images/selfie/' . end($explode_selfie));
                    }

                    Storage::copy($content->selfie_image, 'images/selfie/' . end($explode_selfie));

                    $selfieImage = 'images/selfie/' . end($explode_selfie);
                }

                $old_nik = $account->profile->nik;
                $account->profile->update([
                    'profile_picture'        => ($content->profile_picture_change) ? $profilePicture : $account->profile->profile_picture,
                    'selfie_image'           => ($content->selfie_image_change) ? $selfieImage : $account->profile->selfie_image,
                    'name'                   => $content->name,
                    'place_of_birth'         => $content->place_of_birth,
                    'date_of_birth'          => $content->date_of_birth,
                    'gender'                 => $content->gender,
                    'nik'                    => $content->nik,
                    'identity_image'         => ($content->identity_image_change) ? $identityImage : $account->profile->identity_image,
                    'address_identity'       => $content->address_identity,
                    'zip_code_identity'      => $content->zip_code_identity,
                    'province_identity'      => $content->province_identity,
                    'province_identity_name' => @$content->province_identity_name,
                    'city_identity'          => $content->city_identity,
                    'city_identity_name'     => @$content->city_identity_name,
                    'marital_status'         => $content->marital_status,
                    'religion'               => $content->religion,
                    'recent_education'       => $content->recent_education,
                    'phone_number'           => $content->phone_number,
                    'secondary_phone'        => $content->secondary_phone,
                    'mother_name'            => $content->mother_name,
                    'npwp_number'            => $content->npwp_number,
                    'npwp_periode'           => (@$content->npwp_periode) ? $content->npwp_periode : null,
                    'npwp_image'             => ($content->npwp_image_change) ? $npwpImage : $account->profile->npwp_image,
                    'domicile_address'       => $content->domicile_address,
                    'domicile_province'      => $content->domicile_province,
                    'domicile_province_name' => $content->domicile_province_name,
                    'domicile_city'          => $content->domicile_city,
                    'domicile_city_name'     => $content->domicile_city_name,
                    'domicile_zip_code'      => $content->domicile_zip_code
                ]);

                $profile->status     = 1;
                $profile->updated_by = auth()->user()->id;
                $saved_profile = $profile->save();


                if($saved_profile) {
                    $employee = new RestBprGo;
                    $pathEmpBprByRegNik = env('EMP_BPR_BY_REG_NIK').'/'.$account->sim_id.'/'.$old_nik;
                    $emp = $employee->bprApi($pathEmpBprByRegNik, 'GET');

                    if ($emp->code == '200' and $emp->status == 'OK') {
                        if ($emp->data) {
                            $empId = $emp->data->id;

                            if($content->profile_picture_change && Storage::exists($content->profile_picture)) {

                                $format_file_pic = explode('.', $explode_pic_end);
                                $picBprFilename = 'pic-' . $empId . '.' . end($format_file_pic);
                                $picApi = new RestBprPHP();
                                $backupOldFile = $picApi->sendFile($picBprFilename, 'pic', 'app/' . $profilePicture, 'check');
                                if($backupOldFile->status) {
                                    $send_profile = $picApi->sendFile($picBprFilename, 'pic', 'app/' . $profilePicture, 'send');

                                    if (!$send_profile->status) {

                                        DB::rollback();
                                        Session::flash('toast-success', 'Gagal menyimpan file: profile di BPR. Err: ' . $profile->message);
                                        return false;
                                    }
                                }else{

                                    DB::rollback();
                                    Session::flash('toast-success', 'Gagal menyimpan file: profile di BPR. Err: backup file pic' );
                                    return false;
                                }
                            }else{
                                $picBprFilename = $emp->data->pic_filename;
                            }

                            if($content->identity_image_change && Storage::exists($content->identity_image)) {

                                $format_file_ktp = explode('.', $explode_ktp_end);
                                $ktpBprFilename = 'ktp-' . $empId . '.' . end($format_file_ktp);
                                $ktpApi = new RestBprPHP();
                                $backupOldFile = $ktpApi->sendFile($ktpBprFilename, 'ktp', 'app/' . $identityImage, 'check');
                                if($backupOldFile->status) {
                                    $ktp = $ktpApi->sendFile($ktpBprFilename, 'ktp', 'app/' . $identityImage, 'send');
                                    if (!$ktp->status) {
                                        DB::rollback();
                                        Session::flash('toast-success', 'Gagal menyimpan file: ktp di BPR. Err: ' . $ktp->message);
                                        return false;
                                    }
                                }else{
print_r($backupOldFile);die;
                                    DB::rollback();
                                    Session::flash('toast-success', 'Gagal menyimpan file: ktp di BPR. Err: Backup old file ktp. Err:'.$backupOldFile->msg);
                                    return false;
                                }
                            }else{
                                $ktpBprFilename = $emp->data->ktp_filename;
                            }

                            if($content->npwp_image_change && Storage::exists($content->npwp_image)) {

                                $format_file_npwp = explode('.', $explode_npwp_end);
                                $npwpBprFilename = 'npwp-' . $empId . '.' . end($format_file_npwp);

                                $npwpApi = new RestBprPHP();
                                $backupOldFile = $npwpApi->sendFile($npwpBprFilename, 'file', 'app/' . $npwpImage, 'check');
                                if($backupOldFile->status) {
                                    $npwp = $npwpApi->sendFile($npwpBprFilename, 'file', 'app/' . $npwpImage, 'send');
                                    if (!$npwp->status) {
                                        DB::rollback();
                                        Session::flash('toast-success', 'Gagal menyimpan file: npwp di BPR. Err: ' . $profile->message);
                                        return false;
                                    }else{
                                        $fileTypeBpr = 195591;
                                        $npwpFile = new RestBprGo;
                                        $pathNpwpCheck = env('EMP_FILE_BPR_BY_EMP_ID').'/'.$empId.'/'.$fileTypeBpr;
                                        $pathNpwp = env('EMP_FILE_BPR');
                                        $npwpBprExists = $npwpFile->bprApi($pathNpwpCheck, 'GET');


                                        $body = [
                                            'file_no' => "",
                                            'file_name' => "NPWP-ESS-".$account->sim_id,
                                            'file_type' => $fileTypeBpr,
                                            'file_start_date' => "0000-00-00",
                                            'file_end_date' => "0000-00-00",
                                            'filename' => $npwpBprFilename,
                                            'remark' => "",
                                            'status' => "t",
                                            ];

                                        if($npwpBprExists->code == '200' and $npwpBprExists->status == 'OK' and $npwpBprExists->data){
                                            //update
                                            $body['update_by'] = "8";
                                            $npwpBprUpgrade = $npwpFile->bprApi($pathNpwp."/".$npwpBprExists->data[0]->id, 'PUT', $body);
                                        }else if($npwpBprExists->code == '200' and $npwpBprExists->status == 'OK'){
                                            $body['parent_id'] = $empId;
                                            $body['create_by'] = "8";

                                            $npwpBprUpgrade = $npwpFile->bprApi($pathNpwp, 'POST', $body);
                                        }else{

                                            DB::rollback();
                                            Session::flash('toast-success', 'Gagal insert data npwp di emp_file (1). Err:'.$npwpBprExists->data);
                                            return false;
                                        }

                                        if($npwpBprUpgrade->code != '200'){

                                            DB::rollback();
                                            Session::flash('toast-success', 'Gagal insert data npwp di emp_file (2). Err:'.$npwpBprUpgrade->data);
                                            return false;
                                        }

                                    }
                                }else{

                                    DB::rollback();
                                    Session::flash('toast-success', 'Gagal menyimpan file: npwp di BPR. Err: backup file NPWP');
                                    return false;
                                }
                            }else{
                                $npwpBprFilename = $account->profile->npwp_image;
                            }
			    $kk_no = ($emp->data->kk_no)? $emp->data->kk_no : '0000000000000000';
                            $body = [
                                'name' => $content->name,
                                'birth_date' => $content->date_of_birth,
                                'birth_place' => (int)  $content->place_of_birth,
                                'ktp_no' => $content->nik,
                                'kk_no' => $kk_no,
                                'email' => $content->email,
                                'gender' => $content->gender == 'LAKI-LAKI'? 'M' : ($content->gender == 'PEREMPUAN'? 'F' : ''),
                                'ktp_address' => $content->address_identity,
                                'ktp_prov' => (int) $content->province_identity,
                                'ktp_city' => (int) $content->city_identity,
                                'ktp_postal_code' => (int) $content->zip_code_identity,
                                'dom_address' => $content->domicile_address,
                                'dom_prov' => (int) $content->domicile_province,
                                'dom_city' => (int) $content->domicile_city,
                                'dom_postal_code' => (int) $content->domicile_zip_code,
                                'marital' => $emp->data->marital,
                                'ptkp' => $emp->data->ptkp,
                                'religion' => $content->religion,
                                'cell_no' => $content->phone_number,
                                'phone_no' => $content->secondary_phone,
                                'npwp_no' => $content->npwp_number,
                                'npwp_date' => $content->npwp_periode.'-01-01',
                                'npwp_filename' => $npwpBprFilename,
                                'pic_filename' => $picBprFilename,
                                'ktp_filename' => $ktpBprFilename,
                                'kk_filename' => $emp->data->kk_filename
                            ];
                            $newEmployee = new RestBprGo;
                            $pathUpdateEmpBpr = env('EMP_BPR').'/'.$empId;
                            $newEmp = $newEmployee->bprApi($pathUpdateEmpBpr, 'PUT', $body);

                            if ($newEmp->code == '200' and $newEmp->status == 'OK') {
//                                return ['code' => '200', 'msg' => 'OK'];
                            }else{

                                DB::rollback();
                                Session::flash('toast-success', $newEmp->status?  $emp->data->kk_no.'--'.$newEmp->data : 'Gagal merubah data di sistem BPR');
                                return false;
                            }
                        } else {

                            DB::rollback();
                            Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                            return false;
                        }
                    } else {
                        DB::rollback();
                        Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                        return false;
                    }
                }else{

                    DB::rollback();
                    Session::flash('toast-success', 'Gagal menyimpan data di ESS');
                    return false;
                }

                // $repoEmployee = new GetEmployeeData;

                $content->profilePicture = $profilePicture;
                $content->identityImage  = $identityImage;
                $content->npwpImage      = $npwpImage;

                // $result = $repoEmployee->postEmployeeData($content, $account->sim_id);

                Session::flash('toast-success', 'Biodata Employee berhasil diverifikasi');
            }
            elseif ($req->type == 'refused') {
                $content = json_decode($profile->content);

                $profile->status      = 2;
                $profile->description = $req->description;
                $profile->updated_by  = auth()->user()->id;
                $profile->save();

                Session::flash('toast-success', 'Biodata Employee berhasil ditolak');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if ($content->profile_picture_change && Storage::exists($content->profile_picture)) {
            Storage::delete($content->profile_picture);
        }
        if ($content->identity_image_change && Storage::exists($content->identity_image)) {
            Storage::delete($content->identity_image);
        }
        if ($content->npwp_image_change && Storage::exists($content->npwp_image)) {
            Storage::delete($content->npwp_image);
        }

        return true;
    }

    public function datatablesTmpProfile($accountId)
    {
        $model = Profile::with('handledBy.admin')->select('tmp_profile.*')->where('type', 'profile')->where('account_id', $accountId);

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Verified</span>';
                }
                elseif($data->status == 0) {
                    return '<span class="badge bg-soft-warning text-success">Pending</span>';
                }
                else {
                    return '<span class="badge bg-soft-danger text-danger">Rejected</span>';
                }
            })
            ->addColumn('tanggal', function($data){
                return $data->created_at->format('d-M-Y H:i');
            })
            ->addColumn('admin', function($data){
                if ($data->handledBy) {
                    return $data->handledBy->admin->name;
                }

                return '-';
            })
            ->addColumn('reason', function($data){
                if ($data->description) {
                    return '<button class="btn btn-xs btn-primary btn-reason" data-reason="'. $data->description .'">Deskripsi</button>';
                }

                return "-";
            })
            ->addColumn('action', function($data){
                $html = '<a href="'. route('admin.employee.tmp-data', $data->uuid) .'" class="action-icon btn-detail" title="Detail" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-details"></i></a>';

                return $html;
            })
            ->rawColumns(['status', 'reason', 'action'])
            ->make(true);
    }

    public function changePassword($req, $uuid)
    {
        DB::beginTransaction();
        try {
            $account = Accounts::whereUuid($uuid)->first();

            if (!$account) {
                return false;
            }

            $account->password = bcrypt($req->password);
            $account->save();

            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'account_id'  => auth()->user()->id,
                'type'        => 'admin',
                'title'       => 'Admin Change Password PE',
                'description' => 'Admin change password PE with SIMID : ' . $account->sim_id
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }


}
