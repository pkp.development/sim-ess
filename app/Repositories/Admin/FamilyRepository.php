<?php

namespace App\Repositories\Admin;

use App\Models\Accounts\Accounts;
use App\Models\Accounts\Profiles;
use App\Models\Temporary\Families;
use App\Repositories\BprGoApi\RestBprGo;
use App\Repositories\BprPhpApi\RestBprPhp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Accounts\Families as Family;
use App\Repositories\SipasApi\FamilyHandle;
use App\Repositories\SipasApi\GetEmployeeData;

class FamilyRepository
{
    public function getPendingFamily($uuid)
    {
        return Families::with('account')->where('uuid', $uuid)->where('status', 0)->first();
    }

    public function getFamily($id, $columnSearch)
    {
        return Family::with('account.profile')->where($columnSearch, $id)->first();
    }

    public function updateFamily($req, $tmpProfileId)
    {
        DB::beginTransaction();
        try {
            $family  = Families::with('account')->where('uuid', $tmpProfileId)->where('status', 0)->first();
//            $account = Accounts::select('sim_id')->where('id', $family->account_id)->first();
            $account = Accounts::with('profile')->where('id', $family->account_id)->first();

            if ($req->type == 'verified') {
                $content = json_decode($family->content);

                $profile = Profiles::where('account_id', $family->account_id)->first();

                $familyCardImage = null;
                if ($content->family_card_image_change && Storage::exists($content->family_card_image)) {
                    $explode = explode('/', $content->family_card_image);

                    if(Storage::exists('images/kartu_keluarga/' . end($explode))) {
                        Storage::delete('images/kartu_keluarga/' . end($explode));
                    }

                    Storage::copy($content->family_card_image, 'images/kartu_keluarga/' . end($explode));
                    $explode_kk_end = end($explode);
                    $familyCardImage = 'images/kartu_keluarga/' . $explode_kk_end;
                }

                $profile->update([
                    'family_card_number' => $content->family_card_number,
                    'family_card_image' => ($content->family_card_image_change) ? $familyCardImage : $profile->family_card_image,
                    'tax_marital_status' => $content->tax_marital_status
                ]);

                $family->status     = 1;
                $family->updated_by = auth()->user()->id;
                $saved_family = $family->save();

                if($saved_family){
                    $employee = new RestBprGo;
                    $pathEmpBprByRegNik = env('EMP_BPR_BY_REG_NIK').'/'.$account->sim_id.'/'.$account->profile->nik;
                    $emp = $employee->bprApi($pathEmpBprByRegNik, 'GET');
                    if($emp->code == '200' and $emp->status == 'OK'){
                        if ($emp->data) {
                            $empId = $emp->data->id;
                            //ex: 21693

                            if ($content->family_card_image_change && Storage::exists($content->family_card_image)) {
                                $format_file_pic = explode('.', $explode_kk_end);
                                $bprFilename = 'kk-' . $empId . '.' . end($format_file_pic);
                                $famApi = new RestBprPHP();
                                $backupOldFile = $famApi->sendFile($bprFilename, 'file', 'app/' . $familyCardImage, 'check');
                                if($backupOldFile->status) {
                                    $fileImage = $famApi->sendFile($bprFilename, 'file', 'app/' . $familyCardImage, 'send');

                                    if (!$fileImage->status) {
//                                    $err =
                                        DB::rollback();
                                        Session::flash('toast-success', 'Gagal menyimpan file: kk di BPR. Err: ' . $profile->message);
                                        return false;
                                    }else{
                                        $fileTypeBpr = 195589;
                                        $kkFile = new RestBprGo;
                                        $pathKkCheck = env('EMP_FILE_BPR_BY_EMP_ID').'/'.$empId.'/'.$fileTypeBpr;
                                        $pathKk = env('EMP_FILE_BPR');
                                        $KkBprExists = $kkFile->bprApi($pathKkCheck, 'GET');

                                        $body = [
                                            'file_no' => "",
                                            'file_name' => "KK-ESS-".$account->sim_id,
                                            'file_type' => $fileTypeBpr,
                                            'file_start_date' => "0000-00-00",
                                            'file_end_date' => "0000-00-00",
                                            'filename' => $bprFilename,
                                            'remark' => "",
                                            'status' => "t",
                                        ];

                                        if($KkBprExists->code == '200' and $KkBprExists->status == 'OK' and $KkBprExists->data){
                                            //update
                                            $body['update_by'] = "8";
                                            $kkBprUpgrade = $kkFile->bprApi($pathKk."/".$KkBprExists->data[0]->id, 'PUT', $body);
                                        }else if($KkBprExists->code == '200' and $KkBprExists->status == 'OK'){
                                            $body['parent_id'] = $empId;
                                            $body['create_by'] = "8";

                                            $kkBprUpgrade = $kkFile->bprApi($pathKk, 'POST', $body);
                                        }else{

                                            DB::rollback();
                                            Session::flash('toast-success', 'Gagal insert data kk di emp_file (1). Err:'.$KkBprExists->data);
                                            return false;
                                        }

                                        if($kkBprUpgrade->code != '200'){

                                            DB::rollback();
                                            Session::flash('toast-success', 'Gagal insert data kk di emp_file (2). Err:'.$kkBprUpgrade->data);
                                            return false;
                                        }

                                    }
                                }else{

                                    DB::rollback();
                                    Session::flash('toast-success', 'Gagal merubah data di sistem BPR');
                                    return false;
                                }
                            }

                            if($content->tax_marital_status){
                                $masterApi = new RestBprGo();
                                $endpoint = env('MASTER_SIMILAR_DATA');
                                $new_datas = $masterApi->bprApi(
                                    $endpoint.'/ESPTKP/'.$content->tax_marital_status,
                                    'GET'
                                );
                                $ptkp_based_on_tax = (string) $new_datas->data[0]->kode_data;
                            }else{
                                $ptkp_based_on_tax = '';
                            }

                            $body = [
                                'name' => $emp->data->name,
                                'birth_date' => $emp->data->birth_date,
                                'birth_place' => $emp->data->birth_place,
                                'ktp_no' => $emp->data->ktp_no,
                                'kk_no' => $content->family_card_number,
                                'email' => $emp->data->email,
                                'gender' => $emp->data->gender,
                                'ktp_address' => $emp->data->ktp_address,
                                'ktp_prov' => $emp->data->ktp_prov,
                                'ktp_city' => $emp->data->ktp_city,
                                'ktp_postal_code' => $emp->data->ktp_postal_code,
                                'dom_address' => $emp->data->dom_address,
                                'dom_prov' => $emp->data->dom_prov,
                                'dom_city' => $emp->data->dom_city,
                                'dom_postal_code' => $emp->data->dom_postal_code,
                                'marital' => $content->tax_marital_status,
                                'ptkp' => $ptkp_based_on_tax,
                                'religion' => $emp->data->religion,
                                'cell_no' => $emp->data->cell_no,
                                'phone_no' => $emp->data->phone_no,
                                'npwp_no' => $emp->data->npwp_no,
                                'npwp_date' => $emp->data->npwp_date,
                                'npwp_filename' => $emp->data->npwp_filename,
                                'pic_filename' => $emp->data->pic_filename,
                                'ktp_filename' => $emp->data->ktp_filename,
                                'kk_filename' =>  ($content->family_card_image_change) ? $bprFilename : $emp->data->kk_filename
                            ];

                            $newEmployee = new RestBprGo;
                            $pathUpdateEmpBpr = env('EMP_BPR').'/'.$empId;

                            $newEmp = $newEmployee->bprApi($pathUpdateEmpBpr, 'PUT', $body);

                            if ($newEmp->code == '200' and $newEmp->status == 'OK') {
//                                return ['code' => '200', 'msg' => 'OK'];
                            }else{

                                DB::rollback();
                                Session::flash('toast-success', $newEmp->data? $newEmp->data : 'Gagal merubah data di sistem BPR');
                                return false;
                            }
                        } else {

                            DB::rollback();
                            Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                            return false;
                        }

                    } else {
                        DB::rollback();
                        Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                        return false;
                    }
                }else{

                    DB::rollback();
                    Session::flash('toast-success', 'Gagal menyimpan data di ESS');
                    return false;
                }

                // $repoEmployee = new GetEmployeeData;

                $content->familyCardImage = $familyCardImage;

                // $result = $repoEmployee->postEmployeeData($content, $account->sim_id, 'family');

                Session::flash('toast-success', 'Data Keluarga Employee berhasil diverifikasi');
            }
            elseif ($req->type == 'refused') {
                $content = json_decode($family->content);

                $family->status      = 2;
                $family->description = $req->description;
                $family->updated_by  = auth()->user()->id;
                $family->save();

                Session::flash('toast-success', 'Data Keluarga Employee berhasil ditolak');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        if ($content->family_card_image_change && Storage::exists($content->family_card_image)) {
            Storage::delete($content->family_card_image);
        }

        return true;
    }

    public function daftarFamilyPost($req, $uuid)
    {
        DB::beginTransaction();
        try {
            $family = $this->getFamily($uuid, 'uuid');

            if ($family->status != 0) {
                Session::flash('toast-error', 'Tidak terdapat perubahan data');
                return 'restricted';
            }

            if ($req->type == 'verified') {
                $family->status      = 1;
                $family->verified_by = auth()->user()->id;
                $family->save();


                $apiEmp = new RestBprGo;

                $_pathEmpBprByRegNik = env('EMP_BPR_BY_REG_NIK').'/'.$family->account->sim_id.'/'.$family->account->profile->nik;
                $empBpr = $apiEmp->bprApi($_pathEmpBprByRegNik, 'GET');

                if ($empBpr->code == '200' and $empBpr->status == 'OK') {
                    if ($empBpr->data) {
                        if ($family->action == 'create' or $family->action == 'update') {

                            $_pathEmpFamBprByEmpId = env('EMP_FAM_BPR_BY_EMP_ID').'/'.$empBpr->data->id;
                            $apiFam = new RestBprGo;
                            $empFamBpr = $apiFam->bprApi($_pathEmpFamBprByEmpId, 'GET');

                            $nik_edit = 'create';
                            if($empFamBpr->data){
                                foreach($empFamBpr->data as $familyBpr){
                                    if($familyBpr->ktp_no == $family->nik){
                                        $nik_edit = $familyBpr->id;
                                        break;
                                    }
                                }
                            }

                            $body = [
                                'name' => $family->name,
                                'rel' => (int) $family->relation,
                                'birth_place' => (int) $family->place_of_birth,
                                'birth_date' => substr($family->date_of_birth,0,10),
                                'gender' => $family->gender == 'LAKI-LAKI'? 'M' : ($family->gender == 'PEREMPUAN'? 'F' : $family->gender),
                                'ktp_no' => $family->nik,
                                'bpjs_date' => '0000-00-00',
                                'bpjs_no' => '0',
                                'bpjs_faskes' => 0,
                                'kelas_rawat' => 0,
                                'marital' => 0,
                                'insurance_no' => '0',
                                'insurance_date' => '0000-00-00',
                                'rel_filename' => '',
                                'remark' => $family->description,
                                'no_hp' => $family->phone_number
                            ];

                            if($nik_edit == 'create'){
                                $bodies = array_merge($body, [
                                        'emp_id' => $empBpr->data->id,
                                        'create_by' => '8'
                                    ]
                                );
                                $_pathCreateEmpFamBpr= env('EMP_FAM_BPR');
                                $apiCreateFam = new RestBprGo;
//                                print_r(json_encode($bodies));die;
                                $upgradeEmpFam = $apiCreateFam->bprApi($_pathCreateEmpFamBpr, 'POST', $bodies);
                            }else{
                                $bodies = array_merge($body, [
                                        'update_by' => '8'
                                    ]
                                );
                                $_pathUpdateEmpFamBprByEmpId = env('EMP_FAM_BPR').'/'.$nik_edit;
                                $apiCreateFam = new RestBprGo;
                                $upgradeEmpFam = $apiCreateFam->bprApi($_pathUpdateEmpFamBprByEmpId, 'PUT', $bodies);
                            }
                        } elseif ($family->action == 'delete') {
                            // $api->postFamily($family, 'delete');
                            $family->delete();
                        }
                        if($upgradeEmpFam->code != 200){
//                            print_r($upgradeEmpFam);die;
                            DB::rollback();
                            Session::flash('toast-success', json_encode($upgradeEmpFam->data, true));
                            return false;
                        }
                    } else {
                        DB::rollback();
                        Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                        return false;
                    }
                } else {
                    DB::rollback();
                    Session::flash('toast-success', 'Employee tidak ditemukan di BPR');
                    return false;
                }


                Session::flash('toast-success', 'Daftar keluarga berhasil diverifikasi');
            }
            elseif ($req->type == 'refused') {
                $family->status      = 2;
                $family->description = $req->description;
                $family->verified_by = auth()->user()->id;
                $family->save();

                Session::flash('toast-success', 'Daftar keluarga berhasil ditolak');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }
}
