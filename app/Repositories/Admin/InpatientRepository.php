<?php

namespace App\Repositories\Admin;

use Ramsey\Uuid\Uuid;
use App\Models\Claim\Inpatient;
use App\Models\Accounts\Accounts;
use Illuminate\Support\Facades\DB;
use App\Mail\InpatientNotification;
use Illuminate\Support\Facades\Mail;
use App\Models\Claim\HistoryInpatient;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Permissions\PermissionCompany;
use App\Models\Permissions\PermissionSimBranch;

class InpatientRepository
{
    public function datatable()
    {
        if (auth()->user()->role == 'admin') {
            $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
            $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();

            $model = Inpatient::with('account.assignment')->select('inpatient.*')->whereHas('account.assignment', function($query) use ($companies, $simBrances) {
                // $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                $allBranch = $simBrances->where('value', 0)->first();
                $allClient = $companies->where('value', 0)->first();

                if ($allBranch && $allClient) {
                    $query->whereNotNull('client_company_id')->whereNotNull('sim_branch_id');
                }
                elseif ($allBranch) {
                    $query->whereIn('client_company_id', $companies->pluck('value'));
                }
                elseif ($allClient) {
                    $query->whereIn('sim_branch_id', $simBrances->pluck('value'));
                }
                else {
                    $query->whereIn('client_company_id', $companies->pluck('value'))->whereIn('sim_branch_id', $simBrances->pluck('value'));
                }
            });
        }
        else {
            $model = Inpatient::with('account.assignment')->select('inpatient.*');
        }

        return DataTables::of($model)
            ->editColumn('nominal', function ($data) {
                return number_format($data->nominal);
            })
            ->editColumn('status', function ($data) {
                return ucwords(str_replace('_', ' ', $data->status));
            })
            ->addColumn('action', function ($data) {
                $html = '<a href="'. route('admin.inpatient.detail', $data->uuid) .'" class="action-icon" title="Detail" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-details"></i></a>';

                return $html;
            })
            ->make(true);
    }

    public function save($req)
    {
        DB::beginTransaction();
        try {
            $uuid = Uuid::uuid4()->toString();

            $account = Accounts::with('profile','assignment')->where('sim_id', $req->simid)->first();
            if (auth()->user()->role == 'admin') {
                if ($account->assignment) {
                    $companies   = PermissionCompany::select('value')->where('admin_id', auth()->user()->admin->id)->get();
                    $simBrances  = PermissionSimBranch::select('value')->where('admin_id', auth()->user()->admin->id)->get();

                    $allBranch = $simBrances->where('value', 0)->first();
                    $oneBranch = $simBrances->where('value', $account->assignment->sim_branch_id)->first();
                    $allClient = $companies->where('value', 0)->first();
                    $oneClient = $companies->where('value', $account->assignment->client_company_id)->first();
                    if (!($allBranch || $oneBranch) || !($allClient || $oneClient)) {
                        return 'restricted';
                    }
                }
            }

            if (!$account) {
                return 'account';
            }

            $inpatient = Inpatient::create([
                'account_id'    => $account->id,
                'created_by'    => auth()->user()->id,
                'uuid'          => $uuid,
                'patient_name'  => sipas_validation($req->patient_name),
                'hospital_name' => sipas_validation($req->hospital_name),
                'nominal'       => str_replace(',', '', $req->nominal),
                'diagnosis'     => sipas_validation($req->diagnosis),
                'status'        => $req->status,
                'receipt_date'  => $req->receipt_date,
                'received_date' => $req->received_date,
                'disbursment'   => $req->disbursment,
                'transfer_date' => $req->transfer_date,
                'description'   => sipas_validation($req->description)
            ]);

            HistoryInpatient::create([
                'inpatient_id' => $inpatient->id,
                'updated_by'   => auth()->user()->id,
                'status'       => $req->status,
            ]);

            //Mail::to($account->email)->send(new InpatientNotification($account->profile->name, $req->patient_name, $req->diagnosis, $req->status));
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function get($uuid)
    {
        $inpatient = Inpatient::with('createdby.admin', 'account.profile')->where('uuid', $uuid)->first();

        if (!$inpatient) {
            return false;
        }

        return $inpatient;
    }

    public function historyDatatable($uuid)
    {
        $inpatient = Inpatient::select('id')->where('uuid', $uuid)->first();
        $histories = HistoryInpatient::with('updatedby.admin')->where('inpatient_id', $inpatient->id)->select('history_inpatient.*');

        return DataTables::of($histories)
            ->addColumn('tanggal', function($data) {
                return $data->created_at->format('d F Y H:i');
            })
            ->editColumn('status', function ($data) {
                return ucwords(str_replace('_', ' ', $data->status));
            })
            ->addColumn('action', function ($data) {
                $html = '<a href="'. route('admin.inpatient.detail', $data->uuid) .'" class="action-icon" title="Detail" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-details"></i></a>';

                return $html;
            })
            ->make(true);
    }

    public function update($req, $uuid)
    {
        DB::beginTransaction();
        try {
            $inpatient = Inpatient::with('account.profile')->where('uuid', $uuid)->first();

            if (!$inpatient) {
                return false;
            }

            $inpatient->status        = $req->status;
            $inpatient->transfer_date = $req->transfer_date;
            $inpatient->description   = $req->description;
            $inpatient->disbursment   = str_replace(',', '', $req->disbursment);
            $inpatient->save();

            HistoryInpatient::create([
                'inpatient_id' => $inpatient->id,
                'updated_by'   => auth()->user()->id,
                'status'       => $req->status,
            ]);

            Mail::to($inpatient->account->email)->send(new InpatientNotification($inpatient->account->profile->name, $inpatient->patient_name, $inpatient->diagnosis, $req->status));
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }
}
