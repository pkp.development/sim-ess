<?php

namespace App\Repositories\Admin;

use App\Models\Claim\Outpatient;
use App\Repositories\BprApi\DataPlafond;
use App\Repositories\SipasApi\RawatJalan;
use Illuminate\Support\Facades\DB;

class OutpatientRepository
{
    public function getOutpatient($uuid)
    {
        return Outpatient::with('account.profile')->where('uuid', $uuid)->firstOrFail();
    }

    public function verificationOutpatient($req, $uuid)
    {
        DB::beginTransaction();
        try {
            $status = 0;
            $reason = null;
            if ($req->type == 'verified') {
                $status = 1;
            }
            elseif ($req->type == 'refused') {
                $status = 2;
                $reason = $req->description;
            }

            $data = $this->getOutpatient($uuid);

            if (!$this->checkBalance(str_replace(',', '', $data->nominal), $data->account->sim_id)) {
                $data->verified_by      = auth()->user()->id;
                $data->approved_date    = now();
                $data->status           = 2;
                $data->rejection_reason = "Sisa plafond anda tidak mencukupi untuk melakukan klaim";
                $data->save();

                DB::commit();

                return "insuficient_balance";
            }

            $data->verified_by      = auth()->user()->id;
            $data->approved_date    = now();
            $data->status           = $status;
            $data->rejection_reason = $reason;
            $data->save();

            if ($status == 1) {
                // $api = new RawatJalan;
                // $api->postOutpatient($data);
            }

        }
        catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }
        DB::commit();

        return true;
    }

    public function checkBalance($nominal, $simid)
    {
        $api          = new DataPlafond;
        $sisa_plafond = $api->getPlafondData($simid)->benefit;

        if ($sisa_plafond < $nominal) {
            return false;
        }

        return true;
    }
}
