<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Business\Assignments;
use App\Models\Config\Setting;
use App\Models\Tasks\LasDownload;
use App\Repositories\BprApi\DataLastContract;
use App\Repositories\BprApi\DataPenempatan;
use App\Repositories\BprApi\MasterSimCompanies;
use App\Repositories\SipasApi\GetJobs;
use App\Repositories\SipasApi\GetEmployeeData;
use App\Repositories\SipasApi\GetHistoryContract;
use App\Repositories\SipasApi\GetClientOrganization;
use App\Repositories\SipasApi\GetEmployeeStatus;
use Carbon\Carbon;

class AssignmentRepository
{
    public function getActiveContract()
    {
        $active = Assignments::with('account.history_idcard')->where('account_id', auth()->user()->id)->first();
        // if ($active) {
        //     $clientApi = new GetClientOrganization;
        //     $jobsApi   = new GetJobs;

        //     if ($active->client_branch_id) {
        //         $branch = $clientApi->getBranchWithoutCache(['id' => $active->client_branch_id])->result[0];
        //         $company = $clientApi->getCompanyWithoutCache(['id' => $branch->company_id])->result[0];

        //         $active->branch  = $branch;
        //         $active->company = $company;
        //     }
        //     else {
        //         $active->branch  = null;
        //         $active->company = null;
        //     }

        //     if ($active->job_id) {
        //         $jobs        = $jobsApi->getJobs(['job_code' => $active->job_id]);
        //         $active->job = $jobs->result[0];
        //     }
        //     else {
        //         $active->job = null;
        //     }

        //     $active->contract_id = $this->getContractId($active->contract_file);
        // }

        return $active;
    }

    public function getHistoryContract()
    {
        $api = new GetHistoryContract;

        $histories = $api->getHistoryContract(['simid' => auth()->user()->sim_id]);

        return $histories;
    }

    public function getStatusEmployee()
    {
        $api = new GetEmployeeStatus;

        $status = $api->getEmployeeStatus(['simid' => auth()->user()->sim_id]);

        return $status;
    }

    public function updateAssignmentBpr($employee = null)
    {
        DB::beginTransaction();
        try {
            $apiAssignment = new DataLastContract;
            $result        = $apiAssignment->getDataLastContract($employee->sim_id);

            $assignment    = Assignments::where('account_id', auth()->user()->id)->first();

            if ($assignment) {
                auth()->user()->assignment->update([
                    'sim_company_id'    => $result->sim_company_id,
                    'sim_company'       => $result->sim_company_name,
                    'sim_branch_id'     => $result->sim_branch_id,
                    'sim_branch'        => $result->sim_branch_name,
                    'client_branch_id'  => $result->client_branch_id,
                    'client_branch'     => $result->client_branch_name,
                    'client_company_id' => $result->client_company_id,
                    'client_company'    => $result->client_company_name,
                    'job_id'            => $result->job_id,
                    'job_name'          => $result->job_name,
                    'npo'               => $result->npo,
                    'contract_number'   => $result->contract_number,
                    'contract_file'     => @$result->contract_file,
                    'join_date'         => $result->join_date,
                    'hired_date'        => $result->hired_date,
                    'terminated_date'   => null,
                    'contract_start'    => @$result->contract_start_date,
                    'contract_end'      => @$result->contract_end_date,
                    'contract_type'     => @$result->contract_flow,
                    'contract_status'   => @$result->contract_status,
                    // 'ess_status'        => 1, //$essStatus,
                    // 'signed_at'         => now()
                ]);
            }
            else {
                $assignment = Assignments::create([
                    'account_id'        => auth()->user()->id,
                    'sim_company_id'    => $result->sim_company_id,
                    'sim_company'       => $result->sim_company_name,
                    'sim_branch_id'     => $result->sim_branch_id,
                    'sim_branch'        => $result->sim_branch_name,
                    'client_branch_id'  => $result->client_branch_id,
                    'client_branch'     => $result->client_branch_name,
                    'client_company_id' => $result->client_company_id,
                    'client_company'    => $result->client_company_name,
                    'job_id'            => $result->job_id,
                    'job_name'          => $result->job_name,
                    'npo'               => $result->npo,
                    'contract_number'   => $result->contract_number,
                    'contract_file'     => @$result->contract_file,
                    'join_date'         => $result->join_date,
                    'hired_date'        => $result->hired_date,
                    'terminated_date'   => null,
                    'contract_start'    => @$result->contract_start_date,
                    'contract_end'      => @$result->contract_end_date,
                    'contract_type'     => @$result->contract_flow,
                    'contract_status'   => @$result->contract_status,
                    // 'ess_status'        => 1, //$essStatus,
                    // 'signed_at'         => now()
                ]);
            }
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    }

    public function updateAssignment($employee = null)
    {
        DB::commit();
        try {
            $new        = false;
            $signedAt   = null;
            $essStatus  = null;
            $api        = new GetHistoryContract;
            $assignment = Assignments::where('account_id', auth()->user()->id)->first();
            $active     = $api->getActiveContract(['simid' => auth()->user()->sim_id]);

            if (!$employee) {
                $apiEmp     = new GetEmployeeData;
                $employee = $apiEmp->getEmployeeData(['id' => auth()->user()->sim_id])[0];
            }

            if (!$assignment) {
                $assignment = Assignments::create([
                    'account_id' => auth()->user()->id,
                    'ess_status' => 1
                ]);

                $new = true;
            }

            if($assignment->contract_number != $active->contractno) {

                $contract_status    = null;
                $contractStatusDate = null;

                foreach($active->contract_status as $status) {
                    if ($contract_status !== "ON HAND CORRECT") {

                        if (!$contractStatusDate) {
                            $contractStatusDate = Carbon::createFromFormat('Y-m-d H:i:s', $status->created_date);
                            $contract_status    = $status->doc_del_status;
                        }
                        else {
                            $secondDate = Carbon::createFromFormat('Y-m-d H:i:s', $status->created_date);
                            if ($secondDate->gte($contractStatusDate)) {
                                $contractStatusDate = $secondDate;
                                $contract_status    = $status->doc_del_status;
                            }
                        }
                    }
                }

                if ($new) {
                    $liveDate = Setting::where('code', 'live_date')->first();

                    if ($liveDate->value) {
                        $joinDate = Carbon::createFromFormat('Y-m-d H:i:s', $employee->join_date);
                        $liveDate = Carbon::createFromFormat('Y-m-d', $liveDate->value);
                        if($joinDate->lt($liveDate)) {
                            if ($contract_status == "ON HAND CORRECT") {
                                $signedAt  = now();
                                $essStatus = 1;
                            }
                        }
                    }
                }

                auth()->user()->assignment->update([
                    'sim_company_id'    => $employee->id_sim_company,
                    'sim_branch_id'     => $employee->id_sim_branch,
                    'client_branch_id'  => $active->branchid,
                    'client_company_id' => $employee->company,
                    'job_id'            => $active->jobid,
                    'npo'               => $employee->npo,
                    'contract_number'   => $active->contractno,
                    'contract_file'     => $active->url,
                    'join_date'         => $employee->join_date,
                    'hired_date'        => $employee->hired_date,
                    'terminated_date'   => $employee->termin_date,
                    'contract_start'    => @$active->contractstart,
                    'contract_end'      => @$active->contractfinish,
                    'contract_type'     => $active->contract_type->ctype_name,
                    'contract_status'   => $contract_status,
                    'ess_status'        => 1, //$essStatus,
                    'signed_at'         => $signedAt
                ]);
            }

            auth()->user()->assignment->update([
                'terminated_date' => $employee->termin_date,
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    }

    public function saveLastDownload($type, $month = null, $year = null)
    {
        DB::beginTransaction();
        try {
            LasDownload::create([
                'account_id' => auth()->user()->id,
                'type'       => $type,
                'month'      => $month,
                'year'       => $year
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function getLastDownload($type)
    {
        return LasDownload::where('account_id', auth()->user()->id)->where('type', $type)->latest()->first();
    }

    public function getContractId($uri)
    {
        $parts = parse_url($uri);
        parse_str($parts['query'], $query);

        return $query['id'];
    }
}
