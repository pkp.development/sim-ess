<?php

namespace App\Repositories;

use Ramsey\Uuid\Uuid;
use App\Models\Temporary\Profile;
use Illuminate\Support\Facades\DB;
use App\Repositories\SipasApi\GetBank;
use App\Libraries\Facades\ImageHandler;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class BankRepository
{
    public function validator()
    {
        return [
            'name_of_bank'       => 'required',
            'account_number'     => 'required|numeric',
            'account_name'       => 'required|max:50',
            'bank_account_image' => 'required|image|max:4096'
        ];
    }

    public function getDataBank()
    {
        $bank = auth()->user()->bank_account;

        return $bank;
    }

    public function updateData($req)
    {
        DB::beginTransaction();
        try {
            // $api = new GetBank;

            $tmpBank = $this->getTmpBank();

            if($tmpBank) {
                return 'restricted';
            }

            $request = $req->all();

            // $apiBank = $api->getBank(1, ['bank_code' => $req->name_of_bank]);
            // $result  = $apiBank->result[0];

            // if(strlen($req->account_number) < $result->min_digit_account) {
            //     Session::flash('toast-error', 'Nomor rekening tidak boleh kurang dari ' . $result->min_digit_account . ' digit');

            //     return false;
            // }
            // if(strlen($req->account_number) > $result->max_digit_account) {
            //     Session::flash('toast-error', 'Nomor rekening tidak boleh lebih dari ' . $result->max_digit_account . ' digit');

            //     return false;
            // }

            $request['bank_account_image']        = auth()->user()->bank_account->bank_account_image;
            $request['bank_account_image_change'] = false;
            if($req->hasFile('bank_account_image')) {
                $image    = $req->file('bank_account_image');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/temporary/buku_tabungan/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/temporary/buku_tabungan/' . $filename, $image);

                $request['bank_account_image']        = $path . $filename;
                $request['bank_account_image_change'] = true;
            }

            $change = 'no-updated';
            $bank = auth()->user()->bank_account;
            if ($request['name_of_bank'] != $bank->name_of_bank) {
                $change = 'updated';
            } if ($request['account_number'] != $bank->account_number) {
                $change = 'updated';
            } if ($request['account_name'] != $bank->account_name) {
                $change = 'updated';
            } if ($request['bank_account_image_change']) {
                $change = 'updated';
            }

            if ($change == 'no-updated') {
                return $change;
            }

            Profile::create([
                'uuid'       => Uuid::uuid4(),
                'account_id' => auth()->user()->id,
                'status'     => 0,
                'content'    => json_encode($request),
                'type'       => 'bank'
            ]);

            Session::flash('toast-success', 'Perubahan data anda akan kami verifikasi terlebih dahulu');
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function getTmpBank($uuid = null)
    {
        if ($uuid) {
            return Profile::where('uuid', $uuid)->where('type', 'bank')->first();
        }

        return Profile::where('account_id', auth()->user()->id)->where('status', 0)->where('type', 'bank')->first();
    }
}
