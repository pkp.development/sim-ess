<?php

namespace App\Repositories;

use App\Models\Business\Benefits;
use App\Models\Business\BpjsKetenagakerjaan;

class BenefitRepository
{
    public function getBenefit()
    {
        return Benefits::where('account_id', auth()->user()->id)->first();
    }

    public function getBpjsKetenagakerjaan($card_number = null)
    {
        $result = null;

        if ($card_number) {
            $result = BpjsKetenagakerjaan::where('account_id', auth()->user()->id)->where('card_number', $card_number)->first();
        }
        else {
            $result = BpjsKetenagakerjaan::where('account_id', auth()->user()->id)->get();
        }

        return $result;
    }
}
