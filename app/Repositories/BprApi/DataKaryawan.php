<?php

namespace App\Repositories\BprApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Log;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class DataKaryawan
{
    public function __construct()
    {

    }

    protected function callApi($filters = [])
    {
        try {
            $body = null;

            $response = GuzzleClient::request('GET', env('BPR_ENDPOINT', 'http://147.139.199.220:3400').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    // 'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API Exception',
                'description' => 'BPR API Employee return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getEmployeeData($sim_id)
    {
        $this->uri = '/api/dataKaryawan/'.$sim_id;

        $result = $this->callApi();
echo env('BPR_ENDPOINT', 'http://147.139.199.220:3400').$this->uri;
        $result = json_decode($result);

        return $result;
    }
}
