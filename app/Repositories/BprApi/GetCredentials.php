<?php

namespace App\Repositories\BprApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GetCredentials
{
    public function __construct()
    {

    }

    protected function callApi()
    {
        try {
            $response = GuzzleClient::request('GET', env('BPR_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                ],
                'json' => null
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API Exception',
                'description' => 'BPR API Employee Credentials return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
            // throw $e;
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Throwable $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getCredentials()
    {
        $this->uri = '/api/credensialAmbil';
        $result    = $this->callApi();
        $result    = json_decode($result);

        return $result;
    }
}
