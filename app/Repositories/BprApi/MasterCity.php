<?php

namespace App\Repositories\BprApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Log;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class MasterCity
{
    public function __construct()
    {

    }

    protected function callApi($filters = [])
    {
        try {
            $body = $filters;

            $response = GuzzleClient::request('GET', env('BPR_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    // 'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'query' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API Exception',
                'description' => 'BPR API City return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getMasterCity($page = 1, $name = null, $provinceId = null)
    {
        $this->uri = '/api/kota';

        $result = $this->callApi(['page' => $page, 'nama' => $name, 'idPro' => $provinceId]);
        $result = json_decode($result);

        return $result;
    }

    public function getMasterCityParams($filters)
    {
        $this->uri = '/api/kota';

        $result = $this->callApi($filters);
        $result = json_decode($result);
        if ($result) {
            return $result[0];
        }

        return NULL;
    }
}
