<?php

namespace App\Repositories\BprApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class PostFlagCredential
{
    public function __construct()
    {

    }

    protected function callApi($simid)
    {
        try {
            $body = [
                'id' => $simid
            ];

            $response = GuzzleClient::request('POST', env('BPR_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API Exception',
                'description' => 'BPR API Employee Unflag Credentials return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function postFlagCredential($simid)
    {
        $this->uri ='/api/credensialStatus';

        $result = $this->callApi($simid);
        $result = json_decode($result);

        return $result;
    }
}
