<?php

namespace App\Repositories\BprGoApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Log;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class RestBprGo
{
    public function __construct()
    {

    }

    protected function callApi($method, $filters)
    {
        try {
            $body = $filters;

            $response = GuzzleClient::request($method, env('BPR_RESTGO_ENDPOINT').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'X-API-Key'     => env('RESTFUL_BPR_API_KEY')
                ],
                'body' => json_encode($body)
            ]);

        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API Go Exception',
                'description' => 'BPR API Go return code : ' . $e->getCode(),
            ]);

            return $e->getResponse()->getBody()->getContents();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function bprApi($path, $method, $filters = [])
    {
        $this->uri = $path;

        $result = $this->callApi($method, $filters);
        $result = json_decode($result, false);

        return $result;
    }

}
