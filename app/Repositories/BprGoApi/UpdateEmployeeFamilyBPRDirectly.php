<?php

namespace App\Repositories\BprApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Log;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class UpdateEmployeeFamilyBPRDirectly
{
    public function __construct()
    {

    }

    protected function callApi($method, $filters = [])
    {
        try {
            $body = $filters;

            $response = GuzzleClient::request($method, env('BPR_RESTGO_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'X-API-Key'     => env('RESTFUL_BPR_API_KEY', 'localhost')
                    // 'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'body' => json_encode($body)
            ]);

        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API Exception',
                'description' => 'BPR API City return code : ' . $e->getCode(),
            ]);

            return $e->getResponse()->getBody()->getContents();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }


    public function getEmpFamBprById($empFamId)
    {
        $this->uri = '/bpr/api/employeeFamily/'.$empFamId;

        $result = $this->callApi('GET');
        $result = json_decode($result, false);

        return $result;
    }

    public function getEmpFamBorByParentId($bprEmpId)
    {
        $this->uri = '/bpr/api/employeeFamilyByParentId/'.$bprEmpId;

        $result = $this->callApi('GET');
        $result = json_decode($result, false);

        return $result;
    }

    public function employeeFamilyByParentId($empFamId)
    {
        $this->uri = '/bpr/api/employeeFamily/'.$empFamId;

        $result = $this->callApi('PUT');
        $result = json_decode($result, false);

        return $result;
    }

    public function updateEmpBprById($empId, $filters)
    {
        $this->uri = '/bpr/api/employee/'.$empId;

        $result = $this->callApi('PUT', $filters);
        $result = json_decode($result, false);

        return $result;
    }
}
