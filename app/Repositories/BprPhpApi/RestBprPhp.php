<?php

namespace App\Repositories\BprPhpApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Log;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class RestBprPhp
{

    protected function callApi($method, $filters, $multipart)
    {
        try {
            $body = $filters;
            $content_type = $multipart? 'multipart/form-data' : 'application/json';
            $response = GuzzleClient::request($method, env('BPR_RESTPHP_ENDPOINT').$this->uri, [
                'headers' => [
//                    'Content-Type'  => 'multipart/form-data',
                    'Accept'        => 'application/json',
                ],
                'multipart' => $multipart
            ]);

        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API PHP Exception',
                'description' => 'BPR API PHP return code : ' . $e->getCode(),
            ]);

            return $e->getResponse()->getBody()->getContents();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function bprApi($path, $method, $filters = [], $multipart = [])
    {
        $this->uri = $path;

        $result = $this->callApi($method, $filters, $multipart);
        $result = json_decode($result, false);

        return $result;
    }

    public function bprApiSink($path, $method, $filters = [], $multipart = [])
    {
        $this->uri = $path;

        $result = $this->callApiSink($method, $filters, $multipart);
        $result = json_decode($result, false);

        return $result;
    }


    protected function callApiSink($method, $filters, $multipart)
    {

        try {
            $body = (object) $filters;

            $response = GuzzleClient::request($method, env('BPR_RESTPHP_ENDPOINT').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                ],
                'body' => json_encode($body),
                'query' => ['id' => 1],
                'sink' => storage_path('app/public/tmp-contract/kontrak-' . $body->contract_id . '.pdf')
            ]);

        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'BPR API PHP Exception',
                'description' => 'BPR API PHP return code : ' . $e->getCode(),
            ]);

            return $e->getResponse()->getBody()->getContents();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function sendFile($filename, $type, $file, $action){

        $multipart = [[
            'name'     => 'filename',
            'contents' => $filename
        ],[
            'name'     => 'file',
            'contents' => fopen(storage_path($file), 'r')
        ],[
            'name'     => 'type',
            'contents' => $type
        ],[
            'name'     => 'token',
            'contents' => env('TOKEN_GET_CONTRACT')
        ]
        ];
        if($action != 'send' and $action != 'check'){
            throw new Exception("Invalid Action");
        }
        $path = '/file_'.$action.'.php';
        $result = $this->bprApi($path, "POST", [], $multipart);
        return $result;

    }

}
