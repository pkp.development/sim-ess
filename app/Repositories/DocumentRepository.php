<?php

namespace App\Repositories;

use App\Libraries\AetherSign\AetherSign;
use App\Models\Business\Assignments;
use App\Repositories\BprGoApi\RestBprGo;
use App\Repositories\BprPhpApi\RestBprPhp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Storage;

class DocumentRepository
{
    public function signContract($req)
    {

        $totalPage = $req->totalPage - 1;
        $arrPage   = [];
        $actions   = json_decode(base64_decode($req->actions), true);

        for ($i = 1; $i <= $totalPage; $i++) {
            $arrPage[$i] = $i;
        }

        foreach ($actions as $action) {
            if (array_key_exists($action['page'], $arrPage)) {
                unset($arrPage[$action['page']]);
            }
        }

        if (count($arrPage) > 0) {
            return $arrPage;
        }

        $public = false;
        $pdf = new AetherSign(null, 'px');

        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);

        $inputPath     = storage_path('app/public/tmp-contract/kontrak-' . $req->contract_id . '.pdf');
        $outputName    = 'kontrak-'.$req->contract_id . ".pdf";
        $outputPath    = storage_path('app/contract/' . $outputName);

        if (file_exists($outputPath)) {
            rename($outputPath, storage_path('app/contract/' . 'kontrak-'.$req->contract_id . '_'.date('YmdHis').'.pdf'));
//            gc_collect_cycles();
//            unlink($outputPath);
        }

        $pdf->numPages = $pdf->setSourceFile($inputPath);
        $actions       = json_decode(base64_decode($req->actions), true);

        $signature = storage_path('app/public/signature/' . auth()->user()->sim_id . '.png');

        foreach(range(1, $pdf->numPages, 1) as $page) {
            $rotate = false;
            $degree = 0;
            try {
                $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
                throw $e;
            }

            $size  = $pdf->getTemplateSize($pdf->_tplIdx);
            $scale = round($size['w'] / $req->docWidth, 3);

            $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h'], 'Rotate'=>$degree), true);
            $pdf->useTemplate($pdf->_tplIdx);

            foreach($actions as $action) {
                if(((int) $action['page']) === $page) {
                    if ($action['type'] == "signature") {
                        if (!$public) {
                            $pdf->Image($signature, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }
                        else{
                            $imageArray = explode( ',', $action['image'] );
                            $imgdata = base64_decode($imageArray[1]);
                            $pdf->Image('@'.$imgdata, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }
                    }
                }
            }
        }

        if (file_exists($outputPath)) {
            gc_collect_cycles();
            unlink($outputPath);
        }

        $pdf->Output($outputPath, 'F');

        DB::beginTransaction();
        try {
            $assignment = Assignments::where('account_id', auth()->user()->id)->first();
            $assignment->update([
                'ess_status' => 0,
                'signed_at'  => now()
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    /**
     * Check file orientation
     *
     * @param   float $width
     * @param   float $height
     * @return  string
     */
    public static function orientation($width, $height) {
        if ($width > $height) {
            return "L";
        }else{
            return "P";
        }
    }

    /**
     * Scale element dimension
     *
     * @param   int $dimension
     * @return  int
     */
    public static function scale($dimension, $scale) {
        return round($dimension * $scale);
    }

    public static function checkEligibleContractInBprByNik($nik){

        $BprGo = new RestBprGo();
        $path = "/contract/signatureEligible/" . $nik;
        $contractBpr = $BprGo->bprApi($path, "GET");
        return $contractBpr;

    }
    public static function updateDataSignedContract($contract_id, $sim_id, $ktp){

        $BprGo = new RestBprGo();
        $path = "/contract/contractVerification/" . $contract_id;
        $body = ['sim_id' => $sim_id,
    'ktp' => $ktp];
        $contractBpr = $BprGo->bprApi($path, "PUT", $body);
        return $contractBpr;

    }
    public static function checkContractExists($contract_id, $type){

        $BprPHP = new RestBprPHP();
        $path = '/contract_check.php/'.$contract_id.'/'.env('TOKEN_GET_CONTRACT').'/'.$type;
        $contractBpr = $BprPHP->bprApi($path, "GET");
        return $contractBpr;

    }
    public static function getStreamContractFile($contract_id){

        $BprPHP = new RestBprPHP();
        $path = '/contract_stream.php/'.$contract_id.'/'.env('TOKEN_GET_CONTRACT').'/generate';
        $body = ['contract_id' => $contract_id];
        $contractBpr = $BprPHP->bprApiSink($path, "GET", $body);
        return $contractBpr;

    }
    public static function sendContractFile($contract_id){

        $multipart = [[
            'name'     => 'contract_id',
            'contents' => $contract_id
        ],[
            'name'     => 'file_contract',
            'contents' => fopen(storage_path('app/contract/kontrak-' . $contract_id . '.pdf'), 'r')
        ],[
            'name'     => 'token',
            'contents' => env('TOKEN_GET_CONTRACT')
        ]
        ];

        $BprPHP = new RestBprPHP();
        $path = '/contract_send.php';
        $contractBpr = $BprPHP->bprApi($path, "POST", [], $multipart);
        return $contractBpr;

    }
}
