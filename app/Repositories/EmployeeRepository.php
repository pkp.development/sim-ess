<?php

namespace App\Repositories;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Intervention\Image\Gd\Font;
use App\Models\Accounts\Profiles;
use App\Models\Temporary\Profile;
use Illuminate\Support\Facades\DB;
use App\Models\Business\BankAccounts;
use App\Repositories\BprApi\DataBank;
use Intervention\Image\Facades\Image;
use App\Libraries\Facades\ImageHandler;
use App\Models\Accounts\Families;
use App\Repositories\BprApi\MasterCity;
use Illuminate\Support\Facades\Storage;
use App\Repositories\SipasApi\GetRegion;
use App\Repositories\BprApi\DataKaryawan;
use App\Repositories\BprApi\DataKeluarga;
use App\Repositories\BprApi\DataPenempatan;
use App\Repositories\BprApi\MasterProvince;
use App\Repositories\SipasApi\GetEmployeeData;
use App\Repositories\SipasApi\GetHistoryContract;
use App\Repositories\SipasApi\GetSimOrganization;

class EmployeeRepository
{
    public function validator()
    {
        return [
            'profile_picture'       => 'image|max:4096',
            'selfie_identity_image' => 'image|max:4096',
            'name'                  => 'required|max:50',
            'date_of_birth'         => 'required',
            'place_of_birth'        => 'required|max:50',
            'gender'                => 'required',
            'nik'                   => 'required|numeric|digits:16',
            'identity_image'        => 'required',
            'inp_identity_image'    => 'image|max:4096',
            'address_identity'      => 'required|max:150',
            'zip_code_identity'     => 'required|numeric|digits:5',
            'city_identity'         => 'required',
            'province_identity'     => 'required',
            'religion'              => 'required',
            'phone_number'          => 'required|numeric|digits_between:10,15',
            'secondary_phone'       => 'nullable|numeric|digits_between:10,15',
            'email'                 => 'required|email|unique:accounts,email,' . auth()->user()->id,
            'npwp_image'            => 'required_with:npwp_image_change|image|max:4096',
            'npwp_number'           => 'required',
            'npwp_periode'          => 'required',
            'domicile_address'      => 'required|max:150',
            'domicile_zip_code'     => 'required|numeric|digits:5',
            'domicile_city'         => 'required',
            'domicile_province'     => 'required',
            'mother_name'           => 'nullable|max:50'
        ];
    }

    public function getProfile()
    {
        $dbProfile = Profiles::with('account.assignment')->where('account_id', auth()->user()->id)->first();

        return $dbProfile;
    }

    public function tmpProfile($status = 0)
    {
        return Profile::where('account_id', auth()->user()->id)->where('status', $status)->first();
    }

    public function getTemporaryProfile($uuid)
    {
        return Profile::where('uuid', $uuid)->where('account_id', auth()->user()->id)->first();
    }

    public function updateProfile($req)
    {
        DB::beginTransaction();
        try {
            $request = $req->all();
            if (!$req->has('marital_status')) {
                $request['marital_status'] = null;
            }

            $tmpProfile = $this->tmpProfile();
            $dbProfile  = $this->getProfile();

            if($tmpProfile) {
                return 'restricted';
            }

            $request['profile_picture']        = $dbProfile->profile_picture;
            $request['profile_picture_change'] = false;
            if($req->hasFile('profile_picture')) {
                $image    = $req->file('profile_picture');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/temporary/profile_picture/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/temporary/profile_picture/' . $filename, $image);

                $request['profile_picture'] = $path . $filename;
                $request['profile_picture_change'] = true;
            }

            $request['selfie_image']        = $dbProfile->selfie_image;
            $request['selfie_image_change'] = false;
            if($req->hasFile('selfie_identity_image')) {
                $image    = $req->file('selfie_identity_image');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/temporary/selfie/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/temporary/selfie/' . $filename, $image);

                $request['selfie_image'] = $path . $filename;
                $request['selfie_image_change'] = true;
            }

            $request['identity_image']        = $dbProfile->identity_image;
            $request['identity_image_change'] = false;
            if($req->hasFile('inp_identity_image')) {
                $image    = $req->file('inp_identity_image');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/temporary/identity/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/temporary/identity/' . $filename, $image);

                $request['identity_image'] = $path . $filename;
                $request['identity_image_change'] = true;
            }

            $request['npwp_image']        = $dbProfile->npwp_image;
            $request['npwp_image_change'] = false;
            if($req->hasFile('npwp_image')) {
                $image    = $req->file('npwp_image');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/temporary/npwp/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/temporary/npwp/' . $filename, $image);

                $request['npwp_image'] = $path . $filename;
                $request['npwp_image_change'] = true;
            }

            $provinceApi = new MasterProvince;
            $cityApi     = new MasterCity;

            $request['province_identity_name'] = null;
            if($req->has('province_identity')) {
                $provinceResult = $provinceApi->getMasterProvinceParams(['id' => $req->province_identity]);
                $request['province_identity_name'] = $provinceResult->namaData;
            }

            $request['city_identity_name'] = null;
            if($req->has('city_identity')) {
                $cityResult = $cityApi->getMasterCityParams(['id' => $req->city_identity]);
                $request['city_identity_name'] = $cityResult->namaData;
            }

            $request['domicile_province_name'] = null;
            if($req->has('domicile_province')) {
                $provinceResult = $provinceApi->getMasterProvinceParams(['id' => $req->domicile_province]);
                $request['domicile_province_name'] = $provinceResult->namaData;
            }

            $request['domicile_city_name'] = null;
            if($req->has('domicile_city')) {
                $cityResult = $cityApi->getMasterCityParams(['id' => $req->domicile_city]);
                $request['domicile_city_name'] = $cityResult->namaData;
            }

            if (!$this->checkDataChange($request)) {
                return 'no-change';
            }

            if($tmpProfile) {
                $tmpProfile->update([
                    'content' => json_encode($request)
                ]);
            }
            else {
                Profile::create([
                    'uuid'       => Uuid::uuid4(),
                    'account_id' => auth()->user()->id,
                    'status'     => 0,
                    'content'    => json_encode($request),
                    'type'       => 'profile'
                ]);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    private function checkDataChange($request)
    {
        $profile = auth()->user()->profile;
        if ($request['profile_picture_change']) {
            return true;
        } if ($request['selfie_image_change']) {
            return true;
        } if ($request['name'] != $profile->name) {
            return true;
        } if ($request['place_of_birth'] != $profile->place_of_birth) {
            return true;
        } if ($request['date_of_birth'] != $profile->date_of_birth) {
            return true;
        } if ($request['gender'] != $profile->gender) {
            return true;
        } if ($request['nik'] != $profile->nik) {
            return true;
        } if ($request['identity_image_change']) {
            return true;
        } if ($request['address_identity'] != $profile->address_identity) {
            return true;
        } if ($request['zip_code_identity'] != $profile->zip_code_identity) {
            return true;
        } if ($request['province_identity'] != $profile->province_identity) {
            return true;
        } if ($request['city_identity'] != $profile->city_identity) {
            return true;
        } if ($request['marital_status'] != $profile->marital_status) {
            return true;
        } if ($request['religion'] != $profile->religion) {
            return true;
        } if ($request['recent_education'] != $profile->recent_education) {
            return true;
        } if ($request['phone_number'] != $profile->phone_number) {
            return true;
        }  if ($request['email'] != $profile->email) {
            return true;
        } if ($request['secondary_phone'] != $profile->secondary_phone) {
            return true;
        } if ($request['mother_name'] != $profile->mother_name) {
            return true;
        } if ($request['npwp_number'] != $profile->npwp_number) {
            return true;
        } if ($request['npwp_periode'] != $profile->npwp_periode) {
            return true;
        } if ($request['npwp_image_change']) {
            return true;
        } if ($request['domicile_address'] != $profile->domicile_address) {
            return true;
        } if ($request['domicile_province'] != $profile->domicile_province) {
            return true;
        } if ($request['domicile_city'] != $profile->domicile_city) {
            return true;
        } if ($request['domicile_zip_code'] != $profile->domicile_zip_code) {
            return true;
        }

        return false;
    }

    public function updateEmployeeBpr($simid, $npo = false)
    {
        DB::beginTransaction();
        try {
            if (auth()->user()->is_bpr == 0) {
                $simid = '01' . sprintf("%06s", auth()->user()->sim_id);
            }

            $apiEmp = new DataKaryawan;
            // $apiAss = new DataPenempatan;
            // $apiCon = new GetHistoryContract;
            $result           = $apiEmp->getEmployeeData($simid);
            // $resultAssignment = $apiAss->getDataPenempatan($simid);

            $npwpPeriode = explode('-', $result->tahun_terbit_npwp);
            if ((int)$npwpPeriode[0] === 0) {
                $npwpPeriode = null;
            }
            else {
                $npwpPeriode = (int )$npwpPeriode[0];
            }

            if($result) {
                if ($result->npwp) {
                    if(strlen($result->npwp) == '15' or strlen($result->npwp) == '16'){
                        $result->npwp = substr_replace($result->npwp,".",12,0);
                        $result->npwp = substr_replace($result->npwp,"-",9,0);
                        $result->npwp = substr_replace($result->npwp,".",8,0);
                        $result->npwp = substr_replace($result->npwp,".",5,0);
                        $result->npwp = substr_replace($result->npwp,".",2,0);
                    }
                }
                if ($npo) {
                    // if  (auth()->user()->assignment) {
                    //     auth()->user()->assignment->update([
                    //         // 'job_id'           => $result[0]->job,
                    //         'job_name'         => $resultAssignment->nama_job,
                    //         'client_branch_id' => $resultAssignment->id_cabang
                    //     ]);
                    // }
                }
                else {
                    $apiFam     = new DataKeluarga;
                    $apiBank    = new DataBank;
                    $apiKota    = new MasterCity;
                    $resultFam  = $apiFam->getDataKeluarga($simid);
                    $resultBank = $apiBank->getDataBank($simid);
                    auth()->user()->profile->update([
                        'name'              => $result->nama_pegawai,
                        'place_of_birth'    => $apiKota->getMasterCityParams(['id' => $result->tempat_lahir])->namaData,
                        'date_of_birth'     => $result->{"tanggal lahir"},
                        'gender'            => ($result->jenis_kelamin == 'M') ? 'LAKI-LAKI' : "PEREMPUAN",
                        'email'               => $result->email,
                        'nik'               => $result->no_ktp,
                        'address_identity'  => $result->alamat_ktp,
                        'city_identity'     => ($result->kota) ? $result->kota : null,
                        'province_identity' => ($result->provinsi) ? $result->provinsi : NULL,
                        'marital_status'    => $resultFam->{"matrial status"},
                        'religion'          => $result->agama,
                        // 'recent_education'  => $result->education,
                        'phone_number'    => ($result->{"telephone 1"} && $result->{"telephone 1"} != '-') ? str_replace('-', '', $result->{"telephone 1"}) : null,
                        'secondary_phone' => ($result->{"telephone 2"} && $result->{"telephone 2"} != '-') ? str_replace('-', '', $result->{"telephone 2"}) : null,
                        // 'mother_name'       => $result->nama_ibu,
                        'domicile_address'  => $result->alamat_domisili,
                        'domicile_province' => $result->provinsi_domisili,
                        'domicile_city'     => $result->kota_domisili,
                        // DATA on BPR
                        'npwp_number'        => $result->npwp,
                        'npwp_periode'       => $npwpPeriode,
                        'family_card_number' => $resultFam->{"no kk"}
                    ]);

                    if (isset($resultFam->detail)) {
                        foreach ($resultFam->detail as $famDetail) {
                            $exp = explode('-', $famDetail->{"tanggal lahir"});echo '--';
// print_r($famDetail->{"tanggal lahir"});die;
                            Families::create([
                                'uuid'           => Uuid::uuid4(),
                                'verified_by'    => 7,
                                'account_id'     => auth()->user()->id,
                                'name'           => $famDetail->nama,
                                'place_of_birth' => $famDetail->tempat_lahir,
                                'date_of_birth'  => ($exp[0] && $exp[1] && $exp[2]) ? $famDetail->{"tanggal lahir"} : null,
                                'nik'            => $famDetail->ktp_no,
                                'gender'         => ($famDetail->gender == 'M') ? 'LAKI-LAKI' : "PEREMPUAN",
                                'relation'       => $famDetail->hubungan,
                                'status'         => 1,
                                'action'         => 'create'
                            ]);
                        }
                    }

                    if(auth()->user()->bank_account) {
                        auth()->user()->bank_account->update([
                            'name_of_bank'   => $resultBank->nama_bank,
                            'account_number' => $resultBank->no_rek,
                            'account_name'   => $resultBank->nama_pemilik_rekening
                        ]);
                    }
                    else {
                        BankAccounts::create([
                            'account_id'     => auth()->user()->id,
                            'name_of_bank'   => $resultBank->nama_bank,
                            'account_number' => $resultBank->no_rek,
                            'account_name'   => $resultBank->nama_pemilik_rekening
                        ]);
                    }

                    auth()->user()->actived_at = Carbon::now();
                    auth()->user()->bpr_sync   = Carbon::now();
                    auth()->user()->save();
                }

                if (auth()->user()->is_bpr == 0) {
                    $apiFam     = new DataKeluarga;
                    $apiBank    = new DataBank;
                    $apiKota    = new MasterCity;
                    $resultFam  = $apiFam->getDataKeluarga($simid);
                    $resultBank = $apiBank->getDataBank($simid);

                    auth()->user()->profile->update([
                        'name' => $result->nama_pegawai,
                        'place_of_birth'    => $apiKota->getMasterCityParams(['id' => $result->tempat_lahir])->namaData,
                        'date_of_birth'     => $result->{"tanggal lahir"},
                        'gender'            => ($result->jenis_kelamin == 'M') ? 'LAKI-LAKI' : "PEREMPUAN",
                        'nik'               => $result->no_ktp,
                        'address_identity'  => $result->alamat_ktp,
                        'city_identity'     => ($result->kota) ? $result->kota : null,
                        'province_identity' => ($result->provinsi) ? $result->provinsi : NULL,
                        'marital_status'    => $resultFam->{"matrial status"},
                        'religion'          => $result->agama,
                        // 'recent_education'  => $result->education,
                        'phone_number'    => ($result->{"telephone 1"} && $result->{"telephone 1"} != '-') ? str_replace('-', '', $result->{"telephone 1"}) : null,
                        'secondary_phone' => ($result->{"telephone 2"} && $result->{"telephone 2"} != '-') ? str_replace('-', '', $result->{"telephone 2"}) : null,
                        // 'mother_name'       => $result->nama_ibu,
                        'domicile_address'  => $result->alamat_domisili,
                        'domicile_province' => $result->provinsi_domisili,
                        'domicile_city'     => $result->kota_domisili,
                        // DATA on BPR
                        'npwp_number'        => $result->npwp,
                        'npwp_periode'       => $npwpPeriode,
                        'family_card_number' => $resultFam->{"no kk"}
                    ]);

                    if(auth()->user()->bank_account) {
                        auth()->user()->bank_account->update([
                            'name_of_bank'   => $resultBank->nama_bank,
                            'account_number' => $resultBank->no_rek,
                            'account_name'   => $resultBank->nama_pemilik_rekening
                        ]);
                    }
                    else {
                        BankAccounts::create([
                            'account_id'     => auth()->user()->id,
                            'name_of_bank'   => $resultBank->nama_bank,
                            'account_number' => $resultBank->no_rek,
                            'account_name'   => $resultBank->nama_pemilik_rekening
                        ]);
                    }

                    auth()->user()->sim_id = $simid;
                    auth()->user()->save();
                }
            }
            else {
                return false;
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return $result;
    }

    public function updateEmployee($simid, $npo = false)
    {
        DB::beginTransaction();
        try {
            $apiEmp = new GetEmployeeData;
            $apiCon = new GetHistoryContract;
            $result = $apiEmp->getEmployeeData(['id' => $simid]);

            if($result) {
                if ($npo) {
                    auth()->user()->assignment->update([
                        'job_id'           => $result[0]->job,
                        'client_branch_id' => $result[0]->branch
                    ]);
                }
                else {
                    auth()->user()->profile->update([
                        'name'              => $result[0]->emp_name,
                        'place_of_birth'    => $result[0]->place,
                        'date_of_birth'     => $result[0]->date_birth,
                        'gender'            => $result[0]->gender,
                        'nik'               => $result[0]->id_card,
                        'address_identity'  => $result[0]->alamat,
                        'city_identity'     => $result[0]->city,
                        'province_identity' => $result[0]->provinsi,
                        'marital_status'    => $result[0]->marital,
                        'religion'          => $result[0]->agama,
                        'recent_education'  => $result[0]->education,
                        'phone_number'      => ($result[0]->phone1 && $result[0]->phone1 != '-') ? str_replace('-', '', $result[0]->phone1) : null,
                        'secondary_phone'   => ($result[0]->phone2 && $result[0]->phone2 != '-') ? str_replace('-', '', $result[0]->phone2) : null,
                        'mother_name'       => $result[0]->nama_ibu,
                        'domicile_address'  => $result[0]->cur_alamat,
                        'domicile_province' => $result[0]->cur_city,
                        'domicile_city'     => $result[0]->cur_provinsi,
                    ]);

                    if(auth()->user()->bank_account) {
                        auth()->user()->bank_account->update([
                            'name_of_bank'   => $result[0]->nama_bank,
                            'account_number' => $result[0]->no_rek,
                            'account_name'   => $result[0]->nama_rek
                        ]);
                    }
                    else {
                        BankAccounts::create([
                            'account_id'     => auth()->user()->id,
                            'name_of_bank'   => $result[0]->nama_bank,
                            'account_number' => $result[0]->no_rek,
                            'account_name'   => $result[0]->nama_rek
                        ]);
                    }

                    auth()->user()->actived_at = Carbon::now();
                    auth()->user()->save();
                }
            }
            else {
                return false;
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return $result[0];
    }

    public function updateSignature()
    {
        DB::beginTransaction();
        try {
            if (!auth()->user()->signature) {
                $expName = explode(' ', auth()->user()->profile->name);

                if (count($expName) > 1) {
                    $nickname = $expName[0] . ' ' . $expName[1];
                }
                else {
                    $nickname = $expName[0];
                }

                $font = new Font(ucwords(strtolower($nickname)));
                $font->valign('top');
                $font->color('#000');
                $font->file(public_path('assets/fonts/Amarula.ttf'));
                $font->size(50);
                $size = $font->getBoxSize();

                $img = Image::canvas($size['width'] + 10, $size['height'] + 10);

                $font->applyToImage($img);
                $img->save(public_path('storage/signature' . '/' . auth()->user()->sim_id . '.png'));

                auth()->user()->signature = asset('storage/signature' . '/' . auth()->user()->sim_id . '.png');
                auth()->user()->save();
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    }
}
