<?php

namespace App\Repositories;

use Ramsey\Uuid\Uuid;
use App\Models\Accounts\Families;
use Illuminate\Support\Facades\DB;
use App\Libraries\Facades\ImageHandler;
use Illuminate\Support\Facades\Storage;
use App\Models\Temporary\Families as TemporaryFamilies;

class FamilyRepository
{
    public function updateData($req)
    {
        DB::beginTransaction();
        try {
            $tmpFamily = $this->getTmpFamily();

            if($tmpFamily) {
                return 'restricted';
            }

            $request = $req->all();
            $request['family_card_image']        = auth()->user()->profile->family_card_image;
            $request['family_card_image_change'] = false;

            if($req->hasFile('family_card_image')) {
                $image    = $req->file('family_card_image');
                $filename = auth()->user()->sim_id.'.'.$image->extension();
                $path     = 'images/temporary/kartu_keluarga/';
                $image    = ImageHandler::resizing($image);

                Storage::put('images/temporary/kartu_keluarga/' . $filename, $image);

                $request['family_card_image'] = $path . $filename;
                $request['family_card_image_change'] = true;
            }

            $change = "no-updated";
            $profile = auth()->user()->profile;
            if ($request['tax_marital_status'] != $profile->tax_marital_status) {
                $change = "updated";
            } if ($request['family_card_number'] != $profile->family_card_number) {
                $change = "updated";
            } if ($request['family_card_image_change']) {
                $change = "updated";
            }

            if ($change == 'no-updated') {
                return $change;
            }

            if($tmpFamily) {
                $tmpFamily->update([
                    'content' => json_encode($request)
                ]);
            }
            else {
                TemporaryFamilies::create([
                    'uuid'       => Uuid::uuid4(),
                    'account_id' => auth()->user()->id,
                    'status'     => 0,
                    'content'    => json_encode($request)
                ]);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function updateFamily($req, $uuid = null, $action = null)
    {
        DB::beginTransaction();
        try {
            if (!auth()->user()->profile->family_card_number || !auth()->user()->profile->family_card_image) {
                return 'restricted';
            }

            if (!$uuid) {
                Families::create([
                    'uuid'           => Uuid::uuid4(),
                    'account_id'     => auth()->user()->id,
                    'name'           => $req->name,
                    'place_of_birth' => $req->place_of_birth,
                    'date_of_birth'  => $req->date_of_birth,
                    'nik'            => $req->nik,
                    'gender'         => $req->jenis_kelamin,
                    'relation'       => $req->relation,
                    'phone_number'   => $req->phone_number,
                    'status'         => 0,
                    'action'         => 'create'
                ]);
            }
            elseif ($uuid) {
                if ($action == 'delete') {
                    $result = $this->getFamily('uuid', $uuid);
                    $result->status         = 0;
                    $result->action         = 'delete';

                    $result->save();
                }
                else {
                    $result = $this->getFamily('uuid', $uuid);

                    $result->name           = $req->name;
                    $result->place_of_birth = $req->place_of_birth;
                    $result->date_of_birth  = $req->date_of_birth;
                    $result->nik            = $req->nik;
                    $result->gender         = $req->jenis_kelamin;
                    $result->relation       = $req->relation;
                    $result->phone_number   = $req->phone_number;
                    $result->status         = 0;
                    $result->action         = 'update';

                    $result->save();
                }
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function getTmpFamily($uuid = null)
    {
        if ($uuid) {
            return TemporaryFamilies::where('uuid', $uuid)->first();
        }

        return TemporaryFamilies::where('account_id', auth()->user()->id)->where('status', 0)->first();
    }

    public function getFamilies()
    {
        return Families::where('account_id', auth()->user()->id)->get();
    }

    public function getFamily($columnSearch, $value)
    {
        return Families::with('account.profile')->where($columnSearch, $value)->first();
    }
}
