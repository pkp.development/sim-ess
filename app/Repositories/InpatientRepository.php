<?php

namespace App\Repositories;

use App\Models\Claim\Inpatient;
use App\Models\Claim\HistoryInpatient;
use Yajra\DataTables\Facades\DataTables;

class InpatientRepository
{
    public function datatable()
    {
        $model = Inpatient::select('inpatient.*')->where('account_id', auth()->user()->id);

        return DataTables::of($model)
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('d F Y');
            })
            ->editColumn('nominal', function ($data) {
                return number_format($data->nominal);
            })
            ->editColumn('status', function ($data) {
                return ucwords(str_replace('_', ' ', $data->status));
            })
            ->editColumn('disbursment', function ($data) {
                return ($data->disbursment) ? number_format($data->disbursment) : "-";
            })
            ->addColumn('action', function ($data) {
                $html = '<a href="'. route('pe.inpatient.detail', $data->uuid) .'" class="action-icon" title="Detail" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-details"></i></a>';

                return $html;
            })
            ->make(true);
    }

    public function get($uuid)
    {
        $inpatient = Inpatient::with('createdby.admin', 'account.profile')->where('uuid', $uuid)->first();

        if (!$inpatient) {
            return false;
        }

        return $inpatient;
    }

    public function historyDatatable($uuid)
    {
        $inpatient = Inpatient::select('id')->where('uuid', $uuid)->first();
        $histories = HistoryInpatient::with('updatedby.admin')->where('inpatient_id', $inpatient->id)->select('history_inpatient.*');

        return DataTables::of($histories)
            ->addColumn('tanggal', function($data) {
                return $data->created_at->format('d F Y H:i');
            })
            ->editColumn('status', function ($data) {
                return ucwords(str_replace('_', ' ', $data->status));
            })
            ->make(true);
    }
}
