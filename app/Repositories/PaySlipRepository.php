<?php

namespace App\Repositories;

use App\Models\SlipGaji\PaySlip;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Organization\SimCompanies;
use App\Repositories\SipasApi\GetSimOrganization;

class PaySlipRepository
{
    public function datatables($req)
    {
        $model = PaySlip::where('account_id', auth()->user()->id)->select('payslip.*');

        return DataTables::of($model)
            ->editColumn('month_periode', function ($data) {
                return $this->getNameMonth($data->month_periode);
            })
            ->addColumn('action', function ($data) {
                $html = action_btn(route('pe.download.payslip', $data->uuid), [], 'fas fa-file-download');

                return $html;
            })
            ->make(true);
    }

    public function downloadPaySlipData($uuid)
    {
        $repoEmployee   = new EmployeeRepository;

        $employeeData = $repoEmployee->getProfile();
        // if (!$employeeData->npwp_number || !$employeeData->npwp_periode) {
        //     return 'restrict';
        // }
        // if (!$employeeData->selfie_image) {
        //     return 'restrict_selfie';
        // }

        $paySlip             = PaySlip::with('content.account', 'company')->where('uuid', $uuid)->first();
        $paySlip->month_name = $this->getNameMonth($paySlip->month_periode);

        if (!$paySlip->company) {
            return false;
        }
        if (!$paySlip->company || (!$paySlip->company->logo || !$paySlip->company->name)) {
            return false;
        }

        $data = [
            'employee' => $employeeData,
            'payslip'  => $paySlip,
        ];

        return $data;
    }

    protected function getNameMonth($month)
    {
        switch ($month) {
            case '1':
                return 'Januari';
                break;
            case '2':
                return 'Februari';
                break;
            case '3':
                return 'Maret';
                break;
            case '4':
                return 'April';
                break;
            case '5':
                return 'Mei';
                break;
            case '6':
                return 'Juni';
                break;
            case '7':
                return 'Juli';
                break;
            case '8':
                return 'Agustus';
                break;
            case '9':
                return 'September';
                break;
            case '10':
                return 'Oktober';
                break;
            case '11':
                return 'November';
                break;
            case '12':
                return 'Desember';
                break;
            default:
                return '-';
                break;
        }
    }
}
