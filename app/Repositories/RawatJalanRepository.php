<?php

namespace App\Repositories;

use Ramsey\Uuid\Uuid;
use App\Models\Claim\Outpatient;
use Illuminate\Support\Facades\DB;
use App\Libraries\Facades\ImageHandler;
use App\Repositories\BprApi\DataPlafond;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\SipasApi\RawatJalan;

class RawatJalanRepository
{
    public function datatables()
    {
        $model = Outpatient::select('outpatient.*')->where('account_id', auth()->user()->id);

        return DataTables::of($model)
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('d F Y');
            })
            ->editColumn('nominal', function ($data) {
                return number_format($data->nominal);
            })
            ->editColumn('status', function ($data) {
                if ($data->status == 0) {
                    return 'Menunggu Verifikasi Admin';
                }
                else if ($data->status == 1) {
                    return 'Terverifikasi';
                }
                else {
                    return 'Ditolak';
                }
            })
            ->addColumn('action', function ($data) {
                $html = action_btn('javascript:void(0);', ['btn-detail'], 'mdi mdi-details', null, [['key' => 'url', 'value' => route('pe.rawat-jalan.detail', $data->uuid)]]);

                return $html;
            })
            ->make(true);
    }

    public function getData($uuid)
    {
        return Outpatient::with('account.profile', 'verified_by')->where('account_id', auth()->user()->id)->where('uuid', $uuid)->first();
    }

    public function checkBalance($nominal)
    {
        $api          = new DataPlafond;
        $result       = $api->getPlafondData(auth()->user()->sim_id);
        $sisa_plafond = $result->benefit;

        if ($sisa_plafond < $nominal) {
            return false;
        }

        return true;
    }

    public function save($req)
    {
        DB::beginTransaction();
        try {
            $uuid = Uuid::uuid4()->toString();

            $receipt_file = null;
            if ($req->hasFile('receipt_file')) {
                $image    = $req->file('receipt_file');
                $filename = 'receipt.'.$image->extension();
                $path     = 'rawat_jalan/' . $uuid;
                $image    = ImageHandler::resizing($image);

                Storage::disk('public')->put($path . '/' . $filename, $image);

                $receipt_file = asset('storage/' . $path . '/' . $filename);
            }

            $doctorPrescription = null;
            if ($req->hasFile('doctor_prescription_file')) {
                $image    = $req->file('doctor_prescription_file');
                $filename = 'doctor-prescription.'.$image->extension();
                $path     = 'rawat_jalan/' . $uuid;
                $image    = ImageHandler::resizing($image);

                Storage::disk('public')->put($path . '/' . $filename, $image);

                $doctorPrescription = asset('storage/' . $path . '/' . $filename);
            }

            Outpatient::create([
                'account_id'               => auth()->user()->id,
                'uuid'                     => $uuid,
                'doctor_name'              => $req->doctor_name,
                'patient_name'             => $req->patient_name,
                'nominal'                  => str_replace(',', '', $req->nominal),
                'description'              => $req->description,
                'receipt_file'             => $receipt_file,
                'doctor_prescription_file' => $doctorPrescription,
                'receipt_date'             => $req->receipt_date,
                'status'                   => 0
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return true;
    }
}
