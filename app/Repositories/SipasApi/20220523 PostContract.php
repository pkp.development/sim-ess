<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class PostContract
{
    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    public function postContract($simid, $signDate)
    {
        $multipart = [[
                'name'     => 'simid',
                'contents' => $simid
            ],[
                'name'     => 'file_contract',
                'contents' => fopen(storage_path('app/contract/' . $simid . '.pdf'), 'r')
            ],[
                'name'     => 'pe_sign_date',
                'contents' => $signDate
            ]
        ];

        try {
            $response = GuzzleClient::request('post', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/postContract', [
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'multipart' => $multipart
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API POST Contract return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }
}
