<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class FamilyHandle
{
    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    public function postFamily($req, $type)
    {
        try {
            $url = null;
            $body = null;
            if ($type == 'create') {
                $url = '/api/v1/ess/createFamilies';
                $body = [
                    'id'             => $req->uuid,
                    'simid'          => $req->account->sim_id,
                    'name'           => sipas_validation($req->name),
                    'place_of_birth' => sipas_validation($req->place_of_birth),
                    'date_of_birth'  => $req->date_of_birth->format('Y-m-d'),
                    'nik'            => $req->nik,
                    'gender'         => sipas_validation($req->gender),
                    'relation'       => sipas_validation($req->relation),
                    'phone_number'   => $req->phone_number,
                ];
            }
            elseif ($type == 'update') {
                $url = '/api/v1/ess/editFamilies';
                $body = [
                    'id'             => $req->uuid,
                    'simid'          => $req->account->sim_id,
                    'name'           => sipas_validation($req->name),
                    'place_of_birth' => sipas_validation($req->place_of_birth),
                    'date_of_birth'  => $req->date_of_birth->format('Y-m-d'),
                    'nik'            => $req->nik,
                    'gender'         => sipas_validation($req->gender),
                    'relation'       => sipas_validation($req->relation),
                    'phone_number'   => $req->phone_number,
                ];
            }
            elseif ($type == 'delete') {
                $url = '/api/v1/ess/delFamilies';
                $body = [
                    'id' => $req->uuid
                ];
            }

            $response = GuzzleClient::request('POST', env('SIPAS_ENDPOINT', 'localhost').$url, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Post Family return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }
}
