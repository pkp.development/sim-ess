<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Cache;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GetClientOrganization
{
    protected $tokenAuth;
    protected $uri;
    protected $token;

    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    protected function callApi($page = 1, $filters = [])
    {
        try {
            $body = [
                'page'   => $page,
                'filter' => $filters
            ];

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Client Organization return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e->getResponse();
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getHolding($page = 1, $filters = [])
    {
        $this->uri = '/api/v1/ess/getHolding';

        if(!$filters) {
            if(!Cache::has('sipas-holding-' . $page)) {
                $result = $this->callApi($page);
                if($result == 401) {
                    $result          = $this->token->refreshToken();
                    $this->tokenAuth = $result['results']->access_token;

                    $result = $this->callApi($page);
                }

                $result = json_decode($result);

                Cache::put('sipas-holding-' . $page, $result->result, now()->addDay());

                return ['cache' => false, 'page' => $result->current_page, 'results' => Cache::get('sipas-holding-' . $page)];
            }

            return ['cache' => true, 'page' => $page, 'results' => Cache::get('sipas-holding-' . $page)];
        }

        $result = $this->callApi($page, $filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($page, $filters);
        }

        $result = json_decode($result);

        return ['cache' => false, 'page' => $result->current_page, 'results' => $result->result];
    }

    public function getCompany($page = 1, $filters = [])
    {
        $this->uri = '/api/v1/ess/getCompany';

        $result = $this->callApi($page, $filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($page, $filters);
        }

        $result = json_decode($result);

        return ['cache' => false, 'page' => $result->current_page, 'results' => $result];
    }

    public function getBranch($page = 1, $filters = [])
    {
        $this->uri = '/api/v1/ess/getBranch';

        $result = $this->callApi($page, $filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($page, $filters);
        }

        $result = json_decode($result);

        return ['cache' => false, 'page' => $result->current_page, 'results' => $result];
    }

    public function getCompanyFromBranch($branchId, $getVariable = null)
    {
        $branch = $this->getBranch(1, ['id' => $branchId]);

        if($branch['results']) {
            $company = $this->getCompany(1, ['id' => $branch['results']->result[0]->company_id]);

            if($company['results']->result) {
                if($getVariable) {
                    return $company['results']->result[0]->{$getVariable};
                }

                return $company['results']->result[0];
            }

            return null;
        }

        return null;
    }

    public function getCompanyWithoutCache($filters, $page = 1)
    {
        $this->uri = '/api/v1/ess/getCompany';

        $result = $this->callApi($page, $filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($filters);
        }

        $result = json_decode($result);

        return $result;
    }

    public function getBranchWithoutCache($filters, $page = 1)
    {
        $this->uri = '/api/v1/ess/getBranch';

        $result = $this->callApi($page, $filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($filters);
        }

        $result = json_decode($result);

        return $result;
    }
}
