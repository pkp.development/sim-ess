<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GetEmployeeCredentials
{
    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    protected function callApi($filters = [])
    {
        try {
            $body = [
                'filter' => $filters
            ];

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Employee Credentials return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
            // throw $e;
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getEmployeeCredentials($filters = [])
    {
        (env('APP_ENV', 'local') == 'production') ? $this->uri = '/api/v1/ess/getCredential' : $this->uri ='/api/v1/ess/getCredentialTest';

        $result = $this->callApi($filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($filters);
        }

        $result = json_decode($result);

        return $result;
    }

    public function getEmployeeCredentialsTest($filters = [])
    {
        $this->uri ='/api/v1/ess/getCredentialTest';

        $result = $this->callApi($filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($filters);
        }

        $result = json_decode($result);

        return $result;
    }
}
