<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Log;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GetEmployeeData
{
    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    protected function callApi($filters = [])
    {
        try {
            $body = [
                'filter' => $filters
            ];

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Employee return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getEmployeeData($filters = [])
    {
        $this->uri = '/api/v1/ess/getEmployee';

        $result = $this->callApi($filters);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($filters);
        }

        $result = json_decode($result);

        return $result->result;
    }

    public function postEmployeeData($req, $simid, $type = 'employee')
    {
        $multipart = $this->buildMultipartData($req, $type);
        $multipart[] = [
            'name'     => 'simid',
            'contents' => $simid
        ];

        try {
            $response = GuzzleClient::request('post', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/editEmployee', [
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'multipart' => $multipart
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API POST Employee return code : ' . $e->getCode(),
            ]);

            Log::warning($e);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    private function buildMultiparasdtData($req, $type = 'employee')
    {
        $result = [];
        if ($type == 'employee') {
            ($req->email) ? $result[] = ['name' => 'email', 'contents' => sipas_validation($req->email)] : null;
            ($req->profile_picture_change) ? $result[] = ['name' => 'profile_picture', 'contents' => fopen(storage_path('app/' . $req->profilePicture), 'r')] : null;
            ($req->name) ? $result[] = ['name' => 'name', 'contents' => sipas_validation($req->name)] : null;
            ($req->place_of_birth) ? $result[] = ['name' => 'place_of_birth', 'contents' => sipas_validation($req->place_of_birth)] : null;
            ($req->date_of_birth) ? $result[] = ['name' => 'date_of_birth', 'contents' => $req->date_of_birth] : null;
            ($req->gender) ? $result[] = ['name' => 'gender', 'contents' => $req->gender] : null;
            ($req->nik) ? $result[] = ['name' => 'nik', 'contents' => $req->nik] : null;
            ($req->identity_image_change) ? $result[] = ['name' => 'identity_image', 'contents' => fopen(storage_path('app/' . $req->identityImage), 'r')] : null;
            ($req->address_identity) ? $result[] = ['name' => 'address_identity', 'contents' => sipas_validation($req->address_identity)] : null;
            ($req->zip_code_identity) ? $result[] = ['name' => 'zip_code_identity', 'contents' => $req->zip_code_identity] : null;
            ($req->province_identity) ? $result[] = ['name' => 'province_identity', 'contents' => $req->province_identity] : null;
            ($req->city_identity) ? $result[] = ['name' => 'city_identity', 'contents' => $req->city_identity] : null;
            ($req->marital_status) ? $result[] = ['name' => 'marital_status', 'contents' => $req->marital_status] : null;
            ($req->religion) ? $result[] = ['name' => 'religion', 'contents' => $req->religion] : null;
            ($req->recent_education) ? $result[] = ['name' => 'recent_education', 'contents' => $req->recent_education] : null;
            ($req->phone_number) ? $result[] = ['name' => 'phone_number', 'contents' => $req->phone_number] : null;
            ($req->secondary_phone) ? $result[] = ['name' => 'secondary_phone', 'contents' => $req->secondary_phone] : null;
            ($req->mother_name) ? $result[] = ['name' => 'mother_name', 'contents' => sipas_validation($req->mother_name)] : null;
            ($req->npwp_number) ? $result[] = ['name' => 'npwp_number', 'contents' => $req->npwp_number] : null;
            ($req->npwp_image_change) ? $result[] = ['name' => 'npwp_image', 'contents' => fopen(storage_path('app/' . $req->npwpImage), 'r')] : null;
            ($req->domicile_address) ? $result[] = ['name' => 'domicile_address', 'contents' => sipas_validation($req->domicile_address)] : null;
            ($req->domicile_province) ? $result[] = ['name' => 'domicile_province', 'contents' => $req->domicile_province] : null;
            ($req->domicile_city) ? $result[] = ['name' => 'domicile_city', 'contents' => $req->domicile_city] : null;
            ($req->domicile_zip_code) ? $result[] = ['name' => 'domicile_zip_code', 'contents' => $req->domicile_zip_code] : null;
        }
        if ($type == 'family') {
            ($req->tax_marital_status) ? $result[] = ['name' => 'tax_marital_status', 'contents' => $req->tax_marital_status] : null;
            ($req->family_card_number) ? $result[] = ['name' => 'family_card_number', 'contents' => $req->family_card_number] : null;
            ($req->family_card_image_change) ? $result[] = ['name' => 'family_card_image', 'contents' => fopen(storage_path('app/' . $req->familyCardImage), 'r')] : null;
        }
        if ($type == 'bank') {
            ($req->name_of_bank) ? $result[] = ['name' => 'name_of_bank', 'contents' => $req->name_of_bank] : null;
            ($req->account_number) ? $result[] = ['name' => 'account_number', 'contents' => $req->account_number] : null;
            ($req->account_name) ? $result[] = ['name' => 'account_name', 'contents' => sipas_validation($req->account_name)] : null;
            ($req->bank_account_image) ? $result[] = ['name' => 'bank_account_image', 'contents' => fopen(storage_path('app/' . $req->bankImage), 'r')] : null;
        }

        return $result;
    }
}
