<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GetErrorLog
{
    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    protected function callApi($page)
    {
        try {
            $body = [
                'page'     => $page,
                'per_page' => 10,
                'order'    => 'DESC'
            ];

            $response = GuzzleClient::request('POST', env('SIPAS_ENDPOINT', 'localhost').'/api/v1/ess/getErrorLog', [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Error Log return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getLog($page = 1)
    {
        $this->uri = '/api/v1/ess/getErrorLog';

        $result = $this->callApi($page);
        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($page);
        }

        $result = json_decode($result);

        return $result;
    }
}
