<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;

class GetOther
{
    protected $tokenAuth;
    protected $uri;
    protected $token;

    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    protected function callApi($page = 1, $filters = [], $service = null)
    {
        try {
            $body = [
                'page'   => $page,
                'filter' => $filters
            ];

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API '. $service .' return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getTaxMaritalStatus($page = 1, $filter = [])
    {
        $this->uri = '/api/v1/ess/getMarital';

        if(!Cache::has('sipas-marital')) {
            $result = $this->callApi($page, $filter, 'Marital');

            if($result == 401) {
                $result          = $this->token->refreshToken();
                $this->tokenAuth = $result['results']->access_token;

                $result = $this->callApi($page, $filter, 'Marital');
            }

            $result = json_decode($result);

            Cache::put('sipas-marital', $result->result, now()->addDays(3));

            return Cache::get('sipas-marital');
        }

        return Cache::get('sipas-marital');
    }

    public function getReligion($page = 1, $filter = [])
    {
        $this->uri = '/api/v1/ess/getAgama';

        if(!Cache::has('sipas-religion')) {
            $result = $this->callApi($page, $filter, 'Religion');

            if($result == 401) {
                $result          = $this->token->refreshToken();
                $this->tokenAuth = $result['results']->access_token;

                $result = $this->callApi($page, $filter, 'Religion');
            }

            $result = json_decode($result);

            Cache::put('sipas-religion', $result->result, now()->addDays(3));

            return Cache::get('sipas-religion');
        }

        return Cache::get('sipas-religion');
    }

    public function getEducation($page = 1, $filter = [])
    {
        $this->uri = '/api/v1/ess/getPendidikan';

        if(!Cache::has('sipas-education')) {
            $result = $this->callApi($page, $filter, 'Education');

            if($result == 401) {
                $result          = $this->token->refreshToken();
                $this->tokenAuth = $result['results']->access_token;

                $result = $this->callApi($page, $filter, 'Education');
            }

            $result = json_decode($result);

            Cache::put('sipas-education', $result->result, now()->addDays(3));

            return Cache::get('sipas-education');
        }

        return Cache::get('sipas-education');
    }
}
