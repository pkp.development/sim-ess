<?php
namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Cache;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

Class GetRegion
{
    protected $tokenAuth;
    protected $uri;
    protected $token;

    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    protected function callApi($page = 1, $filters = [])
    {
        try {
            $body = [
                'page'   => $page,
                'filter' => $filters
            ];

            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').$this->uri, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => $body
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Region return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e->getResponse();
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function getProvince($page = 1, $filters = [])
    {
        $this->uri = '/api/v1/ess/getProvince';

        if(!$filters) {
            if(!Cache::has('sipas-province-' . $page)) {
                $result = $this->callApi($page);

                if($result == 401) {
                    $result          = $this->token->refreshToken();
                    $this->tokenAuth = $result['results']->access_token;

                    $result = $this->callApi($page);
                }

                $result = json_decode($result);

                Cache::put('sipas-province-' . $page, $result->result, now()->addDay());

                return ['cache' => false, 'page' => $result->current_page, 'results' => Cache::get('sipas-province-' . $page)];
            }

            return ['cache' => true, 'page' => $page, 'results' => Cache::get('sipas-province-' . $page)];
        }

        $result = $this->callApi($page, $filters);

        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($page, $filters);
        }

        $result = json_decode($result);

        return ['cache' => false, 'page' => $result->current_page, 'results' => $result];
    }

    public function getCity($page = 1, $filters = [])
    {
        $this->uri = '/api/v1/ess/getCity';

        if(!$filters) {
            if(!Cache::has('sipas-city-' . $page)) {
                $result = $this->callApi($page);

                if($result == 401) {
                    $result          = $this->token->refreshToken();
                    $this->tokenAuth = $result['results']->access_token;

                    $result = $this->callApi($page);
                }

                $result = json_decode($result);

                Cache::put('sipas-city-' . $page, $result->result, now()->addDay());

                return ['cache' => false, 'page' => $result->current_page, 'results' => Cache::get('sipas-city-' . $page)];
            }

            return ['cache' => true, 'page' => $page, 'results' => Cache::get('sipas-city-' . $page)];
        }

        $result = $this->callApi($page, $filters);

        if($result == 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApi($page, $filters);
        }

        $result = json_decode($result);

        return ['cache' => false, 'page' => $result->current_page, 'results' => $result];
    }

    public function getProvinceData($provinceCode)
    {
        $result = $this->getProvince(1, ['province_code' => $provinceCode]);

        if ($result['results']->result) {
            return ['province_code' => $result['results']->result[0]->province_code, 'province_name' => $result['results']->result[0]->province_name];
        }

        return null;
    }

    public function getCityData($cityCode)
    {
        $result = $this->getCity(1, ['city_code' => $cityCode]);

        if ($result['results']->result) {
            return ['city_code' => $result['results']->result[0]->city_code, 'city_name' => $result['results']->result[0]->city_name];
        }

        return null;
    }
}
