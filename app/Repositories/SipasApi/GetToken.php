<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\Cache;
use App\Libraries\Facades\GuzzleClient;
use \stdClass;

class GetToken
{
    protected function callApi()
    {
        try
        {
            $response = GuzzleClient::request('POST', env('SIPAS_ENDPOINT', 'localhost').'/api/v1/oauth/token', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json'
                ],
                'json' => [
                    "grant_type"    => "client_credentials",
                    "scope"         => "*",
                    "client_id"     => 2,
                    "client_secret" => "6CzGwvheECjO9YwyAvbnSrzLpBTNtTqGXZol9d19"
                ]
            ]);
        }
        catch(\GuzzleHttp\Exception\ClientException $e)
        {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API Get Token return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch(\GuzzleHttp\Exception\RequestException $e)
        {
            throw $e;
            // return "Request";
        }
        catch(\Exception $e)
        {
            throw $e;
            // return "Exception";
        }

        return $response->getBody()->getContents();
    }

    public function getToken()
    {
        // if(!Cache::has('sipas-token')) {
        //     $result = json_decode($this->callApi());
        //     Cache::put('sipas-token', $result, $result->expires_in);

        //     return ['cache' => false, 'results' => Cache::get('sipas-token')];
        // }

        // return ['cache' => true, 'results' => Cache::get('sipas-token')];
        $object = new stdClass();
        $object->access_token = 'token-sipas';
        return ['cache' => true, 'results' => $object];
    }

    public function refreshToken()
    {
        $result = json_decode($this->callApi());
        Cache::put('sipas-token', $result, $result->expires_in);

        return ['cache' => false, 'results' => Cache::get('sipas-token')];
    }
}
