<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Libraries\Facades\GuzzleClient;
use App\Repositories\SipasApi\GetToken;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class RawatJalan
{
    protected $tokenAuth, $token;

    public function __construct()
    {
        $this->token     = new GetToken;
        $result          = $this->token->getToken();
        $this->tokenAuth = $result['results']->access_token;
    }

    public function callApiPlafond($simid)
    {
        try {
            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').'/api/v1/ess/getRawatJalan', [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => [
                    'filter' => [
                        'simid' => $simid
                    ]
                ]
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API GET Plafond return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return json_decode($response->getBody()->getContents());
    }

    public function callApiRawatJalan($filter)
    {
        try {
            $response = GuzzleClient::request('GET', env('SIPAS_ENDPOINT', 'localhost').'/api/v1/ess/getRawatJalanHistory', [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'json' => [
                    'filter' => $filter
                ]
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API GET Rawat Jalan return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return json_decode($response->getBody()->getContents());
    }

    public function getPlafond($simid)
    {
        $result = $this->callApiPlafond($simid);

        if($result === 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApiPlafond($simid);
        }

        return $result;
    }

    public function getRawatJalan($simid)
    {
        $filter = [
            'simid' => $simid,
            'create_by' => 'SIPAS'
        ];
        $result = $this->callApiRawatJalan($filter);

        if($result === 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callApiRawatJalan($filter);
        }

        return $result;
    }

    public function callPostOutpatient($multipart)
    {
        try {
            $response = GuzzleClient::request('post', env('SIPAS_ENDPOINT', 'localhost') . '/api/v1/ess/postRawatJalan', [
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $this->tokenAuth
                ],
                'multipart' => $multipart
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SIPAS API Exception',
                'description' => 'SIPAS API POST Outpatient return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function postOutpatient($data)
    {
        $exp = explode("storage/", parse_url($data->receipt_file)['path']);
        $multipart = [[
            'name'     => 'simid',
            'contents' => $data->account->sim_id
        ], [
            'name'     => 'id',
            'contents' => substr(str_replace('-', '', $data->uuid), 0, 10)
        ], [
            'name'     => 'receipt_date',
            'contents' => $data->receipt_date->format('m-d-Y')
        ], [
            'name'     => 'doctor_name',
            'contents' => $data->doctor_name
        ], [
            'name'     => 'patient_name',
            'contents' => $data->patient_name
        ], [
            'name'     => 'nominal',
            'contents' => (int) $data->nominal
        ], [
            'name'     => 'description',
            'contents' => $data->description
        ], [
            'name'     => 'receipt_file',
            'contents' => fopen(storage_path('app/public/' . $exp[1]), 'r')
        ], [
            'name'     => 'approved_date',
            'contents' => $data->approved_date->format('m-d-Y')
        ]];

        $result = $this->callPostOutpatient($multipart);

        if($result === 401) {
            $result          = $this->token->refreshToken();
            $this->tokenAuth = $result['results']->access_token;

            $result = $this->callPostOutpatient($multipart);
        }

        return $result;
    }
}
