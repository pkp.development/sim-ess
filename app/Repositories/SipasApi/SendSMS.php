<?php

namespace App\Repositories\SipasApi;

use Ramsey\Uuid\Uuid;
use App\Models\Config\AuditTrail;
use App\Models\Config\SmsCallback;
use Illuminate\Support\Facades\DB;
use App\Libraries\Facades\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class SendSMS
{
    public function sendSMS($simid, $phoneNumber, $content)
    {
        try {

            $trxId   = $simid. '-' . time() . uniqid(true);
            $trxDate = now()->format('YmdHis');

            $response = GuzzleClient::request('POST', env('SMS_ENDPOINT', 'localhost'), [
                'headers' => [
                    'Content-Type'  => 'application/json'
                ],
                'json' => [
                    "user"     => env('SMS_USER', 'sim'),
                    "apikey"   => env('SMS_API_KEY', '83d9cb090cfb11ea85360242ac0f0103'),
                    "phone"    => (env('SMS_PRODUCTION', false)) ? $phoneNumber : "085156734291",
                    "message"  => $content,
                    "trxid"    => $trxId,
                    "trxdate"  => $trxDate,
                    "senderid" => env('SMS_SENDERID', "SIMGROUP"),
                    "sendtype" => "blast",
                    "typesms"  => "mask"
                ]
            ]);
        }
        catch (ClientException $e) {
            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'system',
                'title'       => 'SMS API Exception',
                'description' => 'SMS API return code : ' . $e->getCode(),
            ]);

            return $e->getCode();
        }
        catch (RequestException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }

        $result = json_decode($response->getBody()->getContents());

        DB::beginTransaction();
        try {
            SmsCallback::create([
                'trxid'         => $trxId,
                'phone'         => $phoneNumber,
                'desc'          => $result->desc,
                'response_code' => $result->code
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        return $result;
    }
}
