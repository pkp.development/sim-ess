<?php

namespace App\Repositories;

use App\Models\SlipGaji\History;
use App\Models\SlipGaji\HistoryDetail;
use Illuminate\Support\Facades\DB;
use App\Models\SlipGaji\PaySlipAccount;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class SlipRepository
{
    public function datatables()
    {
        $model = PaySlipAccount::where('type', '!=', 'fixed')->select('payslip_account.*');

        return DataTables::of($model)
            ->editColumn('type', function ($data) {
                return ucwords($data->type);
            })
            ->addColumn('action', function ($data) {
                if($data->type != 'fixed') {
                    $html = action_btn('javascript:void(0)', ['btn-edit'], 'mdi mdi-square-edit-outline', "Edit", [['key' => 'code', 'value' => $data->code]]);

                    return $html;
                }

                return null;
            })
            ->make(true);
    }

    public function get()
    {
        $slips = PaySlipAccount::get();

        return $slips;
    }

    public function first($where, $content)
    {
        $slip = PaySlipAccount::where($where, $content)->first();

        return $slip;
    }

    public function save($req)
    {
        DB::beginTransaction();
        try {
            $type = null;

            if ($req->type == 'pendapatan') {
                $type = $req->type;
            }
            elseif ($req->type == 'pengeluaran') {
                $type = $req->type;
            }

            $slips = PaySlipAccount::create([
                'name' => $req->name,
                'type' => $type,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Session::flash('toast-success', 'Kategori slip gaji berhasil ditambahkan');

        return true;
    }

    public function update($req, $code)
    {
        DB::beginTransaction();
        try {
            $account = $this->first('code', $code);
            $account->update([
                'name' => $req->name,
                'type' => $req->type
            ]);
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        Session::flash('toast-success', 'Kategori slip gaji berhasil diupdate');

        return true;
    }

    public function datatablesHistory()
    {
        $model = History::with('uploadedby.admin')->select('history_payslip.*');

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Finished</span>';
                }
                else if ($data->status == 2) {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
                else {
                    return '<span class="badge bg-soft-danger text-danger">On Progress</span>';
                }
            })
            ->addColumn('total_failed', function($data){
                $count = HistoryDetail::where('history_payslip_id', $data->id)->where('status', 'Failed')->count();
                return '<span class="badge bg-soft-danger text-danger">'. $count .'</span>';
                // return '<span class="badge bg-soft-danger text-danger">'. count($data->details->where('status', 'Failed')) .'</span>';
            })
            ->addColumn('tanggal', function($data){
                return $data->created_at->format('d F Y H:i:s');
            })
            ->addColumn('action', function($data){
                $html = action_btn(route('admin.slip_gaji.detail', $data->uuid), [], 'mdi mdi-details');
                return $html;
            })
            ->rawColumns(['status', 'total_failed', 'action'])
            ->make(true);
    }

    public function detailHistory($uuid)
    {
        $history = History::select('id')->where('uuid', $uuid)->first();
        $model   = HistoryDetail::select('history_payslip_detail.*')->where('history_payslip_id', $history->id);

        return DataTables::of($model)
            ->editColumn('status', function($data){
                if($data->status == 'Passed') {
                    return '<span class="badge bg-soft-success text-success">Passed</span>';
                }

                else {
                    return '<span class="badge bg-soft-danger text-danger">Failed</span>';
                }
            })
            ->rawColumns(['status'])
            ->make(true);
    }
}
