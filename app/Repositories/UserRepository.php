<?php

namespace App\Repositories;

use App\Models\Accounts\Accounts;
use App\Models\Accounts\Admin;
use App\Models\Config\AuditTrail;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Yajra\DataTables\Facades\DataTables;

class UserRepository
{
    public function datatable()
    {
        $model = Admin::with('account')->select('admin.*');

        return DataTables::of($model)
            ->editColumn('account.status', function ($data) {
                if ($data->account->status == 1) {
                    return '<span class="badge bg-soft-success text-success">Aktif</span>';
                } else {
                    return '<span class="badge bg-soft-danger text-danger">Tidak Aktif</span>';
                }
            })
            ->addColumn('action', function ($data) {
                $html = null;

                if (auth()->user()->role == 'super-admin' || auth()->user()->id == $data->account->id) {
                    $html .= '<a href="'. route('admin.user.edit', $data->account->uuid) .'" class="action-icon" title="Edit" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-square-edit-outline"></i></a>';
                }

                if ($data->account->role == 'admin') {
                    $html .= '<a href="'. route('admin.user.permission', $data->account->uuid) .'" class="action-icon" title="Permission" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-shield-account"></i></a>';
                }

                if (auth()->user()->id != $data->account->id) {
                    if ($data->account->status == 1) {
                        $html .= '<form method="POST" action="'. route('admin.user.destroy', $data->account->uuid) .'" style="display: inline-block" accept-charset="UTF-8">';
                        $html .= method_field('DELETE');
                        $html .= csrf_field();
                        $html .= '<a href="#" class="action-icon btn-confirm" title="Disable" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-account-off"></i></a>';
                        $html .= '</form>';
                    }
                    else {
                        $html .= '<form method="POST" action="'. route('admin.user.destroy', $data->account->uuid) .'" style="display: inline-block" accept-charset="UTF-8">';
                        $html .= method_field('DELETE');
                        $html .= csrf_field();
                        $html .= '<a href="#" class="action-icon btn-confirm" title="Enable" data-plugin="tippy" data-tippy-placement="top"><i class="mdi mdi-account-check"></i></a>';
                        $html .= '</form>';
                    }
                }

                return $html;
            })
            ->rawColumns(['account.status', 'action'])
            ->make(true);
    }

    public function get($uuid)
    {
        $data = Accounts::with('admin.permissions')->where('uuid', $uuid)->first();
        if (!$data) {
            return false;
        }

        return $data;
    }

    public function save($req)
    {
        DB::beginTransaction();
        try {
            $role = 'admin';

            if ($req->has('for_super')) {
                $role = 'super-admin';
            }

            $account = Accounts::create([
                'uuid'     => Uuid::uuid4()->toString(),
                'username' => $req->username,
                'email'    => $req->email,
                'status'   => 1,
                'password' => bcrypt($req->password),
                'role'     => $role
            ]);

            $admin = Admin::create([
                'account_id'   => $account->id,
                'name'         => $req->name,
                'phone_number' => $req->phone_number
            ]);

            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'admin',
                'title'       => 'Create new admin',
                'account_id'  => auth()->user()->id,
                'description' => 'Created new admin with username : ' . $req->username
            ]);
        }
        catch (\Exception $e) {
            throw $e;
        }
        DB::commit();

        return $account;
    }

    public function update($req, $data)
    {
        DB::beginTransaction();
        try {
            $data->update([
                'username' => $req->username,
                'email'    => $req->email,
                'password' => ($req->password) ? bcrypt($req->password) : $data->password,
                'role'     => ($req->has('for_super')) ? 'super-admin' : 'admin'
            ]);

            $data->admin->update([
                'name'         => $req->name,
                'phone_number' => $req->phone_number
            ]);

            $desc = ($req->password) ? 'with password' : 'without password';

            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'admin',
                'title'       => 'Updated admin ' . $req->username,
                'account_id'  => auth()->user()->id,
                'description' => 'Updated admin ' . $desc
            ]);
        }
        catch (\Exception $e) {
            throw $e;
        }
        DB::commit();

        return true;
    }

    public function destroy($data)
    {
        DB::beginTransaction();
        try {
            if ($data->status == 1) {
                $data->status = 0;
                $data->save();

                $message = "Admin has disabled";
            }
            else {
                $data->status = 1;
                $data->save();

                $message = "Admin has enabled";
            }

            AuditTrail::create([
                'uuid'        => Uuid::uuid4()->toString(),
                'type'        => 'admin',
                'title'       => 'Updated admin ' . $data->username,
                'account_id'  => auth()->user()->id,
                'description' => $message
            ]);
        }
        catch (\Exception $e) {
            throw $e;
        }
        DB::commit();

        return $message;
    }
}
