<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertPermissionsCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try {
            Schema::disableForeignKeyConstraints();

            DB::table('permission_category')->insert([
                'id'   => 1,
                'name' => 'Employee'
            ]);
            DB::table('permission_category')->insert([
                'id'   => 2,
                'name' => 'Informasi Penggajian'
            ]);
            DB::table('permission_category')->insert([
                'id'   => 3,
                'name' => 'Notification'
            ]);

            Schema::enableForeignKeyConstraints();
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
