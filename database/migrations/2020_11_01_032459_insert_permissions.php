<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try {
            Schema::disableForeignKeyConstraints();

            // Employee
            DB::table('permissions')->insert([
                'permission_category_id' => 1,
                'key'                    => 'list-employee',
                'name'                   => 'List Employee'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 1,
                'key'                    => 'update-password',
                'name'                   => 'Update Password'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 1,
                'key'                    => 'upload-idcard',
                'name'                   => 'Upload ID Card'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 1,
                'key'                    => 'bpjs-kesehatan',
                'name'                   => 'BPJS Kesehatan'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 1,
                'key'                    => 'bpjs-ketenagakerjaan',
                'name'                   => 'BPJS Ketenagakerjaan'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 1,
                'key'                    => 'vendor-asuransi',
                'name'                   => 'Vendor Asuransi'
            ]);

            // Informasi Penggajian
            DB::table('permissions')->insert([
                'permission_category_id' => 2,
                'key'                    => 'upload-spt',
                'name'                   => 'Upload SPT'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 2,
                'key'                    => 'upload-payslip',
                'name'                   => 'Upload Payslip'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 2,
                'key'                    => 'setting-payslip',
                'name'                   => 'Setting Payslip'
            ]);

            // Notification
            DB::table('permissions')->insert([
                'permission_category_id' => 3,
                'key'                    => 'notif-profile',
                'name'                   => 'Profile'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 3,
                'key'                    => 'notif-bank',
                'name'                   => 'Bank'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 3,
                'key'                    => 'notif-family',
                'name'                   => 'Family'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 3,
                'key'                    => 'notif-families',
                'name'                   => 'Anggota Keluarga'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 3,
                'key'                    => 'notif-contract',
                'name'                   => 'Contract'
            ]);
            DB::table('permissions')->insert([
                'permission_category_id' => 3,
                'key'                    => 'notif-outpatient',
                'name'                   => 'Rawat Jalan'
            ]);

            // Uncategorize
            DB::table('permissions')->insert([
                'key'  => 'claim-inpatient',
                'name' => 'Klaim Rawat Inap'
            ]);
            DB::table('permissions')->insert([
                'key'  => 'history-contract',
                'name' => 'History Contract'
            ]);
            DB::table('permissions')->insert([
                'key'  => 'report',
                'name' => 'Report'
            ]);

            Schema::enableForeignKeyConstraints();
        }
        catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
