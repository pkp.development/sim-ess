function drawDatatable(selector) {
    $(selector).on('draw.dt', function () {
        if($('[data-plugin="tippy"]').length > 0) {
            tippy('[data-plugin="tippy"]');
        }

        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function(item) {
                    return item.el.attr('title');
                }
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function(element) {
                    return element.find('img');
                }
            }
        });

        $("body").delegate('.btn-confirm', 'click', function (e) {

            e.preventDefault();

            form = $(this).parents('form');

            Swal.fire({
                title: "Are you sure?",
                text: "Will you continue this process?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, process it!"
            }).then(function (result) {
                if (result.value) {
                    form.submit();
                }
            });
        });
    });
}

$(document).on('click', '.confirm-del', function (e) {

    e.preventDefault();

    form = $(this).parents('form');

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then(function (result) {
        if (result.value) {
            form.submit();
        }
    });

});

$(document).on('click', '.btn-confirm', function (e) {

    e.preventDefault();

    form = $(this).parents('form');

    Swal.fire({
        title: "Apakah anda yakin?",
        text: "apakah anda ingin melanjutkan proses ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Tidak",
        confirmButtonText: "Ya, lanjutkan!"
    }).then(function (result) {
        if (result.value) {
            form.submit();
        }
    });
});

function fasterPreview( uploader, selector ) {
    if ( uploader.files && uploader.files[0] ){
          $(selector).attr('src',
             window.URL.createObjectURL(uploader.files[0]) );
    }
}

$(document).ready(function(){
    if($(".image-fancy").length) {
        $(".image-fancy").fancybox({
            buttons: [
                "zoom",
                "fullScreen",
                "close"
            ],
            fullScreen: {
                autoStart: false,
            },
            afterShow: function() {
                /* $('.fancybox-button--zoom').click(); */
            }
        });
    }

    if($(".zbox").length) {
        $(".zbox").zbox( { margin:40 } );
    }

    if($('[data-toggle="select2"]').length) {
        if($(".form-modal").length) {
            parent = $(".form-modal").find('form');
            $('.form-modal [data-toggle="select2"]').select2({ width: '100%', dropdownParent: parent});
        }
        else {
            $('[data-toggle="select2"]').select2({ width: '100%' });
        }
    }

    $("[data-mask='currency-rupiah']").maskMoney({precision:0});
    $("[data-mask='currency-rupiah']").maskMoney('mask');

    // $('ul.ul-infinite-scroll').hide();
    // $('.noti-scroll').jscroll({
    //     autoTrigger: true,
    //     loadingHtml: '<div class="spinner-border text-primary m-2" role="status"></div>',
    //     padding: 0,
    //     nextSelector: '.ul-infinite-scroll li.active + li a',
    //     contentSelector: 'div.noti-scroll',
    //     callback: function() {
    //         $('ul.ul-infinite-scroll').remove();
    //     }
    // });

    $('.slimscroll-notif').slimScroll({
        height: '300px',
        disableFadeOut: false,
        width:'300px',
        railVisible: true,
        alwaysVisible: true
    }).bind('slimscroll', function(event, pos){
        console.log(pos);
        if (pos == 'bottom') {
            alert('bawah');
        }
    });

    $(".filestyle").filestyle({
        btnClass: "btn-primary",
        htmlIcon: '<i class="fas fa-upload"></i>',
        text: " Pilih File"
    });

    $('.image-popup').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }
    });
});
