var _args = {}; // private

var ADMIN_INDEX = ADMIN_INDEX || (function(){
    return {
        init : function(Args) {
            _args = Args;
            // some other initialising

            adminIndex();
        }
    };
}());

function adminIndex() {
    drawDatatable('#admin-datatable');
    var table = $("#admin-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: _args[0],
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'account.username', name: 'account.username'},
            {data: 'account.email', name: 'account.email'},
            {data: 'phone_number', name: 'phone_number'},
            {data: 'account.role', name: 'account.role'},
            {data: 'account.status', name: 'account.status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[7, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });
}
