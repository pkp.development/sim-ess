<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="SIM - Employee Self Service" name="description" />
    <meta name="author" content="Aether ID">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.ico') }}">
    <title>SIM ESS | Tanda tangan kontrak online</title>
    <!-- Ion icons -->
    <link href="{{ asset('aether/signer/assets/fonts/ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('aether/signer/assets/libs/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/css/simcify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Signer CSS -->
    <link href="{{ asset('aether/signer/assets/css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('aether/signer/assets/js/jscolor.js') }}"></script>

    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="authentication-bg-pattern">

    <div class="content">
        <div class="row">
            <div class="col-md-10">
                <div class="document">
                    <div class="signer-document">
                        <!-- open PDF docements -->
                        <div class="document-pagination">

                        </div>
                        <div class="document-load">
                            <div class="loader-box"><div class="circle-loader"></div></div>
                        </div>
                        <div class="document-error">
                            <i class="ion-android-warning text-danger"></i>
                            <p class="text-muted"><strong>Oops! </strong> <span class="error-message"> Something went wrong.</span></p>
                        </div>
                        <div class="text-center">
                            <div class="document-map"></div>
                            <canvas id="document-viewer" style="width: 100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="signer-overlay">
        <div class="signer-overlay-header">
            <div class="signer-overlay-logo">
                <a href="{{ route('pe.dashboard') }}"><img src="{{ asset('aether/signer/assets/logo2.png') }}" class="img-responsive"></a>
            </div>
            <div class="signer-overlay-action">
                <button class="btn btn-responsive btn-primary signer-save"><i class="mdi mdi-content-save"></i> <span>Simpan</span> </button>
            </div>
            <div class="signer-header-tools">
                <div class="signer-header-tool-holder text-center">
                    <div class="signer-tool" tool="signature" action="true" style="color: #0abb87;">
                        <div class="tool-icon"><i class="fas fa-signature" style="font-size: 20px;"></i></div>
                        <p style="color: #0abb87;">Tanda tangan</p>
                    </div>
                    <div class="signer-tool" tool="delete" action="false" style="color: #0abb87;">
                        <div class="tool-icon"><i class="fas fa-trash-alt" style="font-size: 20px;"></i></div>
                        <p style="color: #0abb87;">Hapus</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="signer-more-tools">
            <button id="prev" class="btn btn-success"><i class="fas fa-chevron-circle-left"></i> Sebelumnya</button>
            <span class="text-muted ml-15" style="margin-right: 15px">Halaman <span id="page_num">0</span> dari <span id="page_count">0</span></span>
            <button id="next" class="btn btn-success">Selanjutnya <i class="fas fa-chevron-circle-right"></i></button>
        </div>
        <div class="signer-overlay-previewer col-md-10 light-card"></div>
        <div class="signer-overlay-footer">
            <p class="text-center text-white"> Powered by Aether.id | <?=date("Y")?> &copy; SIM Employee Self Service | All Rights Reserved. </p>
        </div>
        <div class="signer-assembler"></div>
        <div class="signer-builder"></div>
        <div class="request-helper">Select input fields and signature position for <strong></strong></div>
    </div>

    <form action="{{ route('pe.contract.signature-post') }}" method="POST" id="form-signature" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="actions" id="form-action">
        <input type="hidden" name="docWidth" id="form-docWidth">
        <input type="hidden" name="totalPage" id="form-totalPage">
    </form>

    <!-- scripts -->
    <script src="{{ asset('aether/signer/assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/simcify.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js//jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jcanvas/jcanvas.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/touch-punch.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jcanvas/editor.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/pdf.js') }}"></script>
    <script type="text/javascript">
        var url = "{{ asset('storage/tmp-contract') . '/' . auth()->user()->sim_id . '.pdf' }}",
            isTemplate = 'No',
            // signDocumentUrl = "{{ route('pe.contract.signature-post') }}",
            baseUrl = '',
            csrf_token = "{{ csrf_token() }}",
            auth = true;
            document_key = '';
        PDFJS.workerSrc = "{{ asset('aether/signer/assets/js/pdf.worker.min.js') }}";

        var signature = "{{ asset('storage/signature/' . auth()->user()->sim_id . '.png') }}";

        $(document).ready(function(){
            inviting = false;
            enableTools();
            launchEditor();
        });
    </script>

    <!-- custom scripts -->
    <script src="{{ asset('aether/signer/assets/js/app.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/signer.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/render.js') }}"></script>
</body>

</html>
