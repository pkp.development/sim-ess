<!DOCTYPE html>
<html lang="en">

@include('partials.head')

<body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        @include('partials.topbar')
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        @if(auth()->user()->role == 'super-admin' || auth()->user()->role == 'admin')
        @include('partials.sidebar-admin')
        @else
        @include('partials.sidebar-pe')
        @endif
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                @yield('content')
                <!-- container -->
            </div>

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer" style="color: #000;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        Powered by <a href="http://aether.id">Aether.id</a> | 2020 &copy; SIM Employee Self Service | All Rights Reserved.
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    @include('partials.script')

</body>

</html>
