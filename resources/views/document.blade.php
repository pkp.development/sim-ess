<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="SIM - Employee Self Service" name="description" />
    <meta name="author" content="Aether ID">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.ico') }}">
    <title>SIM ESS | Tanda tangan kontrak online</title>
    <!-- Ion icons -->
    <link href="{{ asset('aether/signer/assets/fonts/ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('aether/signer/assets/libs/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/css/simcify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Signer CSS -->
    <link href="{{ asset('aether/signer/assets/css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('aether/signer/assets/js/jscolor.js') }}"></script>

    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="authentication-bg-pattern">

@php
    $contract_id = isset($contractId)? $contractId : '';
@endphp
    <div class="content">
        <div class="row">
            <div class="col-md-10">
                <div class="document">
                    <div class="signer-document">
                        <!-- open PDF docements -->
                        <div class="document-pagination">

                        </div>
                        <div class="document-load">
                            <div class="loader-box"><div class="circle-loader"></div></div>
                        </div>
                        <div class="document-error">
                            <i class="ion-android-warning text-danger"></i>
                            <p class="text-muted"><strong>Oops! </strong> <span class="error-message"> Something went wrong.
                            @if(!empty($error))
                                <div class="alert alert-success"> {{ $error }}</div>
                            @endif</span></p>
                        </div>
                        <div class="text-center">
                            <div class="document-map"></div>
                            <canvas id="document-viewer" style="width: 100%"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="signer-overlay">
        <div class="signer-overlay-header">
            <div class="signer-overlay-logo">
                <a href="{{ route('pe.dashboard') }}"><img src="{{ asset('aether/signer/assets/logo2.png') }}" class="img-responsive"></a>
            </div>
            <div class="signer-overlay-action">
                <button class="btn btn-responsive btn-primary signer-save"><i class="mdi mdi-content-save"></i> <span>Simpan</span> </button>
            </div>
            <div class="signer-header-tools">
                <div class="signer-header-tool-holder text-center">
                    <div id="init-signature" class="signer-tool" tool="editsignature" action="true" style="color: #0abb87;">
                        <div class="tool-icon"><i class="fas fa-signature" style="font-size: 20px;"></i></div>
                        <p style="color: #0abb87;">Tanda tangan</p>
                    </div>
                    <div class="signer-tool" tool="delete" action="false" style="color: #0abb87;">
                        <div class="tool-icon"><i class="fas fa-trash-alt" style="font-size: 20px;"></i></div>
                        <p style="color: #0abb87;">Clear Page from TTD</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="signature-canvas" class="col-md-12" style="text-align:center;margin-top:130px;display:none">
            <div class="light-card" style="margin:auto;width:50%;padding-bottom:50px">
            <div style="overflow-y:scroll;position:relative;height:205px">
                <canvas id="sig-canvas" width="320" height="200" style="overflow-x:hidden;">
                    Get a better browser, bro.
                </canvas>
</div>
                <div class="col-md-12">
                    <button class="btn btn-primary" id="sig-submitBtn">Submit Signature</button>
                    <button class="btn btn-default" id="sig-clearBtn">Clear Signature</button>
                    <div id="implement-signature" class="signer-tool" tool="signature" action="true" style="color: #0abb87;">
                        <div style="display:none" class="tool-icon"><i class="fas fa-signature" style="font-size: 20px;"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #sig-canvas {
                border: 2px dotted #CCCCCC;
                border-radius: 15px;
                cursor: crosshair;
            }
        </style>
        <div class="signer-more-tools">
            <button id="prev" class="btn btn-success"><i class="fas fa-chevron-circle-left"></i> Sebelumnya</button>
            <span class="text-muted ml-15" style="margin-right: 15px">Halaman <span id="page_num">0</span> dari <span id="page_count">0</span></span>
            <button id="next" class="btn btn-success">Selanjutnya <i class="fas fa-chevron-circle-right"></i></button>
        </div>


        <div class="signer-overlay-previewer col-md-10 light-card" style="margin-top:0"></div>
        <div class="signer-overlay-footer">
            <p class="text-center text-white"> Powered by Aether.id | <?=date("Y")?> &copy; SIM Employee Self Service | All Rights Reserved. </p>
        </div>
        <div class="signer-assembler"></div>
        <div class="signer-builder"></div>
        <div class="request-helper">Select input fields and signature position for <strong></strong></div>
    </div>

    <form action="{{ route('pe.contract.signature-post') }}" method="POST" id="form-signature" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="actions" id="form-action">
        <input type="hidden" name="docWidth" id="form-docWidth">
        <input type="hidden" name="totalPage" id="form-totalPage">
        <input type="hidden" name="contract_id" value="{{ $contract_id }}" id="form-totalPage">
    </form>

    <!-- scripts -->
    <script src="{{ asset('aether/signer/assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/simcify.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js//jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jcanvas/jcanvas.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/touch-punch.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jcanvas/editor.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/pdf.js') }}"></script>
    <script type="text/javascript">
        var isMobile = false; //initiate as false
        var url = "{{ asset('storage/tmp-contract') . '/kontrak-' . $contract_id . '.pdf' }}",
            isTemplate = 'No',
            // signDocumentUrl = "{{ route('pe.contract.signature-post') }}",
            baseUrl = '',
            csrf_token = "{{ csrf_token() }}",
            auth = true;
            document_key = '';
        PDFJS.workerSrc = "{{ asset('aether/signer/assets/js/pdf.worker.min.js') }}";

        var signature = "{{ asset('storage/signature/' . auth()->user()->sim_id . '.png') }}";

        $(document).ready(function(){
            inviting = false;
            enableTools();
            launchEditor();
        });
    </script>
    <script>
        $("#sig-submitBtn").on('click', function($data){
            var dataUrl = canvas_ttd.toDataURL();

            $.ajax({
                url : "{{ route('contract.store.ttd.custom') }}",
                type : 'POST',
                data : {
                    'data' : dataUrl,
                    'simid' : "{{  auth()->user()->sim_id }}",
                    "_token": "{{ csrf_token() }}"
                },
                dataType:'json',
                success : function(data) {
                    setTimeout($("#implement-signature").click(), 13000)
                },
                error : function(request,error)
                {
                }
            });

            });
    </script>
    <!-- custom scripts -->
    <script src="{{ asset('aether/signer/assets/js/app.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/signer.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/render.js') }}"></script>
    <script>
        
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
    isMobile = true;
    $('.signer-overlay-header').css('position', 'absolute');
    $('.signer-more-tools').css('position', 'sticky');
    $('.signer-header-tools').css('position', 'absolute');
    $('.signer-header-tools').css('overflow', 'unset');
    $('.slimScrollDiv').css('overflow', 'unset');
    $('.slimScrollDiv').css('position', 'absolute');
    $('.signer-more-tools').parent().css('top', '120px');
    $('.signer-overlay-header').css('height', '120px');
    $('#signature-canvas').css('margin-top', '200px');
    $('.signer-overlay-previewer').css('margin-top', '200px');
}

    </script>
</body>

</html>
