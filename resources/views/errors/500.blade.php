<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SIM - ESS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Aether.ID" name="author" />
    <meta content="SIM - Employee Self Service" name="description" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

</head>

<body>

    <!-- Begin page -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-6 p-5">
                        <img src="{{ asset('assets/images/500.svg') }}">
                    </div>
                    <div class="col-6 p-4">
                        <h1 style="margin-top: 0.5em; font-size: 163px; font-weight: bold; color: #1E3B5C;">500</h1>
                        <h2 style="color: #333333;">Oops...</h2>
                        <p style="font-size: 16px;">Tenang ini bukan salah kamu.<br/>Kami mengalami error pada internal server, mohon coba lagi nanti.</p>
                        <a href="{{ route('admin.dashboard') }}" class="btn btn-primary waves-effect waves-light pl-5 pr-5">Kembali Ke dashboard</a>
                    </div>
                </div> <!-- row  -->
            </div> <!-- containert fluid  -->


        </div> <!-- content page  -->
        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

</body>

</html>
