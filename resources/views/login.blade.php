<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>SIM - ESS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="SIM - Employee Self Service" name="description" />
        <meta name="author" content="Aether ID">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <!-- JQuery Toast -->
        <link href="{{ asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />

    </head>
    
    <style type="text/css">
       .justify { text-align: justify;}
   </style>
    <body class="authentication-bg authentication-bg-pattern">

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">

                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{ asset('assets/images/logo.png') }}" alt="" height="64"></span>
                                    </a>
                                    <h3 class="text-muted mb-4 mt-1">Employee Self Service</h3>
                                </div>

                                <form action="{{ route('authentication') }}" method="POST">
                                    {!! csrf_field() !!}

                                    <div class="form-group mb-3">
                                        <label for="sim_id">SIM ID</label>
                                        <input class="form-control" type="text" id="sim_id" name="sim_id" placeholder="Masukan SIM ID Anda">
                                    </div>
				
					<div>
                                   	<p  class="justify">
						Jika digit SIM ID anda berjumlah 5 digit maka tambahkan angka 010 didepan SIM ID 
						<br>(cth : 24678 menjadi 01024678)
						<br>Jika digit SIM ID anda berjumlah 6 digit maka tambahkan angka 01 didepan SIM ID 
						<br>(cth : 124678 menjadi 01124678)
						<br>Jika digit SIM ID anda dengan format PEGXXXXX maka penulisan disamakan 
						<br>(cth : PEG123456 menjadi PEG123456)
					</p>
                              		</div>


                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <div class="input-group">
                                            <input class="form-control" type="password" name="password" id="password" placeholder="Masukan Password Anda" aria-label="Masukan Password Anda">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-password waves-effect waves-light" type="button">Perlihatkan</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit"> Masuk </button>
                                    </div>

                                </form>

                                <div class="text-center mt-3">
                                   <p> <a href="{{ route('forgot-password') }}" class="text-muted-50 ml-1">Lupa Password ?</a></p>
                                </div>
                                <div>
                                   <p  class="justify">SIMGROUP tidak pernah melakukan pungutan dalam bentuk apapun (baik uang maupun barang) kepada Karyawan.<br><br>
                                                       Jika anda menemukan indikasi "PUNGLI" yang mengatas namakan SIMGROUP segera laporkan ke no whatapps dan alamat email berikut :
                                       <br> WA : 082111550541
                                       <br> Email : Lapor@sim.co.id 
				       <br>
                                    </p>
                               </div>
				<div>
                                   <p  class="justify">Khusus untuk pelayanan TAD (Tenaga Alih Daya) dapat menghubungi Customer Service SISKA Berikut.<br>
                                       <br>Email      : Customer.service@sim.co.id
                                       <br>Telegram   : @CustomerserviceSIM_BOT
				       <br>Live Chat  : www.sim.co.id
                                    </p>
                               </div>

				
				<div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{ asset('assets/images/QR Code SISKA.jpeg') }}" alt="" height="120"></span>
                                    </a>
                                    <h5 class="text-muted mb-4 mt-1">QR CODE SISKA</h5>
                                </div>
				


                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>

        <!-- JQuery Toast -->
        <script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

        <!-- Laravel Javascript Validation -->
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! @$validator !!}

        @include('partials.alert')

        <script type="text/javascript">
            $(document).ready(function(){
                $('.btn-password').click(function(){
                    if($('#password').attr('type') == 'text'){
                        $('#password').attr('type','password');
                        $('.btn-password').text('Perlihatkan');
                    }else{
                        $('#password').attr('type','text');
                        $('.btn-password').text('Sembunyikan');
                    }
                });
            });
        </script>
    </body>
</html>
