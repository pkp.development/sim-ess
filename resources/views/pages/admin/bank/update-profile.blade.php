@extends('app')

@section('page-title')
{!! page_title('Verifikasi Biodata PE') !!}
@endsection

@section('style')

@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-lg-12 mt-3">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pt-2 pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Attribute</th>
                                            <th>Data Lama</th>
                                            <th>Data Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr @if($dataOld->name_of_bank != $content->name_of_bank) class="table-warning" @endif>
                                            <td><strong>Nama Bank</strong></td>
                                            <td class="text-center">@if($dataOld->name_of_bank){{ isset($bpr_ref_old['bank'])? $bpr_ref_old['bank']['nama_data'] : '' /*$dataOld->name_of_bank*/ }}@else{{ "-" }}@endif</td>
                                            <td class="text-center">{{ isset($bpr_ref_new['bank'])? $bpr_ref_new['bank']['nama_data'] : '' /*$content->name_of_bank*/ }}</td>
                                        </tr>

                                        <tr @if($dataOld->account_number != $content->account_number) class="table-warning" @endif>
                                            <td><strong>No. Rekening</strong></td>
                                            <td class="text-center">{{ @$dataOld->account_number }}</td>
                                            <td class="text-center">{{ @$content->account_number }}</td>
                                        </tr>

                                        <tr @if($dataOld->account_name != $content->account_name) class="table-warning" @endif>
                                            <td><strong>Nama Pemilik Rekening</strong></td>
                                            <td class="text-center">{{ @$dataOld->account_name }}</td>
                                            <td class="text-center">{{ @$content->account_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->bank_account_image != $content->bank_account_image) class="table-warning" @endif>
                                            <td></td>
                                            <td class="text-center">
                                                @if(!$dataOld->bank_account_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="zbox" title="Foto Buku Tabungan">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: 150px;"  alt="rekening-image" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['buku-tabungan', $dataOld->account_id, 'false']) }}" class="zbox" title="Foto Buku Tabungan">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['buku-tabungan', $dataOld->account_id, 'false']) }}" style="height: 150px;"  alt="rekening-image" class="rounded" >
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['buku-tabungan', $profile->uuid]) }}" class="zbox" title="Foto Buku Tabungan">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['buku-tabungan', $profile->uuid]) }}" style="height: 150px;"  alt="rekening-image" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end .pt2 -->
                    </div> <!-- end col-->
                </div>

            </div> <!-- end .padding -->
            <div class="text-right">
                <button type="button" class="btn btn-refused btn-danger btn-lg waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-window-close"></i> Tolak</button>
                <form action="{{ route('admin.employee.update.rekening.post', $profile->uuid) }}" method="POST" style="display: inline-block">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" value="verified">
                    <button type="submit" class="btn btn-verified btn-confirm btn-success btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Verified</button>
                </form>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end row -->
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.employee.update.rekening.post', $profile->uuid) }}" method="POST" style="display: inline-block">
                {!! csrf_field() !!}
                <input type="hidden" name="type" value="refused">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="refused" class="control-label">Alasan Penolakan</label>
                                <textarea name="description" class="form-control" id="refused" rows="4" placeholder="Masukan Alasan Penolakan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<script>
$(document).ready(function(){
    $(".btn-refused").on('click', function(){
        $('#con-close-modal').modal('show');
    });
});
</script>
@endsection
