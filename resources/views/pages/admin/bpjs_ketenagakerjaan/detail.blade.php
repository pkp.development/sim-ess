@extends('app')

@section('page-title')
{!! page_title('Detail Task BPJS Ketenagakerjaan') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="task-benefit-datatable" class="table table-striped w-100">
                            <thead>
                                <tr>
                                    <th>SIMID</th>
                                    <th>Status</th>
                                    <th>Company Abbr</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <a href="{{ route('admin.bpjs-tkj.index') }}" class="btn btn-lg btn-light float-right mt-3">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#task-benefit-datatable');
        var table = $("#task-benefit-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.bpjs-tkj.detail', $uuid) }}",
            },
            columns: [
                {data: 'simid', name: 'simid'},
                {data: 'status', name: 'status', searchable: false},
                {data: 'abbreviation', name: 'abbreviation'},
                {data: 'description', name: 'description'},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[4, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    });
</script>
@endsection
