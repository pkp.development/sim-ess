@extends('app')

@section('page-title')
{!! page_title('Companies') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">List Perusahaan</h4>
                            <a href="{{ route('admin.company.sync') }}" class="btn btn-primary waves-effect waves-light float-right" >
                                <span class="btn-label"><i class="fas fa-sync"></i></span>Synchronize From BPR &nbsp;&nbsp;
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="company-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Logo</th>
                                <th>Nama</th>
                                <th>Abbreviation</th>
                                <th>Direktur</th>
                                <th>Last Sync at</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="modal-edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="javascript:void(0)" method="POST" enctype="multipart/form-data" id="form-edit-company">
                @csrf
                @method('PATCH')
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Penempatan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="name" class="control-label">Nama Perusahaan</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Perusahaan">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="npwp" class="control-label">NPWP Perusahaan</label><span class="text-danger">*</span>
                                <input type="text" class="form-control npwp" name="npwp" id="npwp" placeholder="Masukkan Nama Perusahaan">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="abbreviation" class="control-label">Abbreviation</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" name="abbreviation" id="abbreviation" placeholder="Abbreviation">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="coordinator" class="control-label">Nama Direktur</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" name="coordinator" id="coordinator" placeholder="Masukkan Nama Direktur Perusahaan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="coordinator_npwp" class="control-label">NPWP Direktur</label><span class="text-danger">*</span>
                                <input type="text" class="form-control npwp" name="coordinator_npwp" id="coordinator_npwp" placeholder="Masukkan NPWP Direktur Perusahaan">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="logo">Logo Perusahaan</label><span class="text-danger">*</span>
                                <div class=" w-75 mb-3">
                                    <img src="{{ asset('assets/images/logo-placeholder.png') }}" style="height: 100px; width: auto;" alt="user-image" class="rounded img-logo" onerror="this.src = '{{ asset('assets/images/logo-placeholder.png') }}'">
                                </div>
                                <input type="file" class="filestyle inp-logo" id="logo" name="logo" accept="image/*">
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="signature">Tanda tangan Direktur</label><span class="text-danger">*</span>
                                <div class=" w-75 mb-3">
                                    <img src="{{ asset('assets/images/logo-placeholder.png') }}" style="height: 100px; width: auto;" alt="user-image" class="rounded img-sign" onerror="this.src = '{{ asset('assets/images/logo-placeholder.png') }}'">
                                </div>
                                <input type="file" class="filestyle inp-signature" id="signature" name="coordinator_sign" accept="image/*">
                            </div>
                        </div> <!-- end col -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<div id="modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Penempatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="name" class="control-label">Nama Perusahaan</label>
                            <input type="text" class="form-control" id="detail-name" placeholder="Masukkan Nama Perusahaan" readonly="">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="name" class="control-label">NPWP Perusahaan</label>
                            <input type="text" class="form-control" id="detail-npwp" placeholder="Masukkan NPWP Perusahaan" readonly="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="abbreviation" class="control-label">Abbreviation</label>
                            <input type="text" class="form-control" id="detail-abbreviation" placeholder="Abbreviation" readonly="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="coordinator" class="control-label">Nama Direktur</label>
                            <input type="text" class="form-control" id="detail-coordinator" placeholder="Masukkan Nama Direktur Perusahaan" readonly="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="coordinator" class="control-label">NPWP Direktur</label>
                            <input type="text" class="form-control" id="detail-coordinator-npwp" placeholder="NPWP Direktur Perusahaan" readonly="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="logo">Logo Perusahaan</label>
                            <div class=" w-75 mb-3">
                                <img src="{{ asset('assets/images/logo-placeholder.png') }}" style="height: 100px; width: auto;" alt="user-image" class="rounded img-logo-detail" onerror="this.src = '{{ asset('assets/images/logo-placeholder.png') }}'">
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="signature">Tanda tangan Direktur</label>
                            <div class=" w-75 mb-3">
                                <img src="{{ asset('assets/images/logo-placeholder.png') }}" style="height: 100px; width: auto;" alt="user-image" class="rounded img-sign-detail" onerror="this.src = '{{ asset('assets/images/logo-placeholder.png') }}'">
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#company-datatable');
        var table = $("#company-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.company.index') }}",
            },
            columns: [
                {data: 'logo', name: 'logo'},
                {data: 'name', name: 'name'},
                {data: 'abbreviation', name: 'abbreviation'},
                {data: 'coordinator', name: 'coordinator'},
                {data: 'sync_at', name: 'sync_at', orderable: false, searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[6, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $(".inp-signature").change(function(){
            fasterPreview( this, '.img-sign');
        });
        $(".inp-logo").change(function(){
            fasterPreview( this, '.img-logo');
        });

        $(".npwp").mask('00.000.000.0-000.000');

        $("table").delegate('.btn-edit', 'click', function(){
            console.log($(this).data('url'));
            $.get($(this).data('url'), {}, function(data){
                $('#name').val(data.name);
                $('#npwp').val(data.npwp);
                $('#abbreviation').val(data.abbreviation);
                $('#coordinator').val(data.coordinator);
                $('#coordinator_npwp').val(data.coordinator_npwp);
                $('.img-logo').attr('src', data.logo);
                $('.img-sign').attr('src', data.coordinator_sign);

                $('#form-edit-company').attr('action', data.edit_url)

                $('#modal-edit').modal('show');

            }, 'json').fail(function(){
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });

        $("table").delegate('.btn-detail', 'click', function(){
            console.log($(this).data('url'));
            $.get($(this).data('url'), {}, function(data){
                $('#detail-name').val(data.name);
                $('#detail-npwp').val(data.npwp);
                $('#detail-abbreviation').val(data.abbreviation);
                $('#detail-coordinator').val(data.coordinator);
                $('#detail-coordinator-npwp').val(data.coordinator_npwp);
                $('.img-logo-detail').attr('src', data.logo);
                $('.img-sign-detail').attr('src', data.coordinator_sign);

                $('#modal-detail').modal('show');

            }, 'json').fail(function(){
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });
    });
</script>
@endsection
