@extends('app')

@section('page-title')
{!! page_title('Configurations') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">Configurations</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="audit-trail-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($settings as $setting)
                            <tr>
                                <td>{{ $setting->name }}</td>
                                <td>
                                    @if ($setting->code == "live_date")
                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', $setting->value)->format('d M Y') }}
                                    @elseif ($setting->code == "cut_off_periode")
                                    {{ $setting->value }} Months
                                    @elseif ($setting->code == "limit_credentials")
                                    {{ number_format($setting->value) }} User
                                    @else
                                    {{ $setting->value }}
                                    @endif
                                </td>
                                <td>
                                    @if ($setting->code == "live_date")
                                    <button class="btn btn-xs btn-primary btn-live" data-value="{{ $setting->value }}">Set Value</button>
                                    @elseif ($setting->code == "cut_off_periode")
                                    <button class="btn btn-xs btn-primary btn-cut-off">Set Value</button>
                                    @elseif ($setting->code == "limit_credentials")
                                    <button class="btn btn-xs btn-primary btn-limit">Set Value</button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-live-date" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.configuration.store') }}" method="POST" enctype="multipart/form-data" id="form-edit-company">
                @csrf
                <input type="hidden" name="code" value="live_date">
                <div class="modal-header">
                    <h4 class="modal-title">Setting Live Date</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="live_date" class="control-label">Live Date</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" name="live_date" id="live_date" placeholder="Masukkan Live Date" data-provide="datepicker" data-date-format="dd M yyyy" data-date-autoclose="true">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<div id="modal-cut-off" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.configuration.store') }}" method="POST" enctype="multipart/form-data" id="form-edit-company">
                @csrf
                <input type="hidden" name="code" value="cut_off">
                <div class="modal-header">
                    <h4 class="modal-title">Setting Batas waktu terminated</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="cut_off" class="control-label">Batas waktu terminated</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" name="cut_off" id="cut_off" placeholder="Masukkan Batas waktu terminated">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<div id="modal-user-limit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.configuration.store') }}" method="POST" enctype="multipart/form-data" id="form-edit-company">
                @csrf
                <input type="hidden" name="code" value="user_limit">
                <div class="modal-header">
                    <h4 class="modal-title">Setting Credentials Limit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="user_limit" class="control-label">Credentials Limit</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" name="user_limit" id="user_limit" placeholder="Masukkan Credentials Limit">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $(".btn-live").on('click', function($data){
            $('#modal-live-date').modal('show');
        });

        $(".btn-cut-off").on('click', function($data){
            $("#cut_off").TouchSpin({
                min: 0,
                max: 100,
                step: 1,
                boostat: 5,
                maxboostedstep: 10,
                postfix: 'Months'
            });

            $('#modal-cut-off').modal('show');
        });

        $(".btn-limit").on('click', function($data){
            $("#user_limit").TouchSpin({
                min: 0,
                max: 1000000000,
                step: 1,
                boostat: 5,
                maxboostedstep: 10,
                postfix: 'Users'
            });

            $('#modal-user-limit').modal('show');
        });
    });
</script>
@endsection
