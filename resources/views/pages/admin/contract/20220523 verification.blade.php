@extends('app')

@section('page-title')
{!! page_title('Verifikasi Kontrak') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <div class="text-right">
                                <button type="button" class="btn btn-refused btn-danger btn-lg waves-effect waves-light mt-2 mr-1" data-toggle="modal" data-target="#con-close-modal">
                                    <i class="mdi mdi-window-close"></i> Tolak
                                </button>
                                <form action="{{ route('admin.contract.verification.post', $account->uuid) }}" method="POST" style="display: inline-block">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="type" value="verified">
                                    <button type="submit" class="btn btn-verified btn-confirm btn-success btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Verified</button>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="reportdoc" class="embed-responsive-item" src="{{ route('admin.contract.view', $account->uuid) }}"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.contract.verification.post', $account->uuid) }}" id="form-refused" method="POST" style="display: inline-block">
                {!! csrf_field() !!}
                <input type="hidden" name="type" value="refused">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="refused" class="control-label">Alasan Penolakan</label>
                                <textarea name="description" class="form-control" id="refused" rows="4" placeholder="Masukan Alasan Penolakan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>
@endsection
