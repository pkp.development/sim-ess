@extends('app')

@section('page-title')
{!! page_title('History Kontrak') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="table-responsive">
                    <table id="contract-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>SIMID</th>
                                <th>Nama PE</th>
                                <th>Handled By</th>
                                <th>No. Kontrak</th>
                                <th>Status</th>
                                <th>Deskripsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>

<script>
$(document).ready(function(){
    drawDatatable('#contract-datatable');
    var table = $("#contract-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('admin.contract.index') }}",
        },
        columns: [
            {data: 'account.sim_id', name: 'account.sim_id'},
            {data: 'pe_name', name: 'account.profile.name'},
            {data: 'admin_name', name: 'verifiedBy.admin.name'},
            {data: 'contract_number', name: 'contract_number'},
            {data: 'status', name: 'status'},
            {data: 'description', name: 'description'},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[6, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });

    $(".status-select2").select2({
        width: '100%',
    });
});
</script>
@endsection
