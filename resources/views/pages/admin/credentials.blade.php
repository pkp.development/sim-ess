@extends('app')

@section('page-title')
{!! page_title('Setting Credentials') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-9">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">Setting Credentials</h4>
                            <button class="btn btn-primary waves-effect waves-light btn-branch float-right" >
                                <span class="btn-label"><i class="fas fa-plus"></i></span>Tambah Data
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="setting-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($settings as $setting)
                            <tr>
                                <td>{{ ucwords($setting->type) }}</td>
                                <td>{{ $setting->company_name }} @if($setting->branch_name) - <strong>({{ $setting->branch_name }})</strong>@endif</td>
                                <td>
                                    <form action="{{ route('admin.setting-credentials.delete', $setting->id) }}" method="POST" style="display: inline-block">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-xs btn-danger btn-confirm"><i class="mdi mdi-trash-can"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card-box mt-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <h3>Total Data</h3>
                            <h4>{{ number_format($totalCreds) }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">Data Employee Credentials</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="setting2-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>SIMID</th>
                                <th>Name</th>
                                <th>Phone 1</th>
                                <th>Phone 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($credentials as $credential)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $credential->id }}</td>
                                <td>{{ $credential->emp_name }}</td>
                                <td>{{ $credential->phone1 }}</td>
                                <td>{{ $credential->phone2 }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="modal-branch" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.setting-credentials.store') }}" method="POST" enctype="multipart/form-data" id="form-edit-company">
                @csrf
                <input type="hidden" name="code" value="branch">
                <div class="modal-header">
                    <h4 class="modal-title">Setting Credentials Branch</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="company" class="control-label">Type</label><span class="text-danger">*</span>
                                <select class="form-control custom-select" id="type" name="type">
                                    <option value="include">Include</option>
                                    <option value="exclude">Exclude</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 include">
                            <div class="form-group">
                                <label for="company" class="control-label">Company</label><span class="text-danger">*</span>
                                <select class="form-control sel-company" id="company" data-placeholder="Pilih Company" name="sim_company"></select>
                            </div>
                        </div>

                        <div class="col-md-12 include">
                            <div class="form-group">
                                <label for="branch" class="control-label">Branch</label><span class="text-danger">*</span>
                                <select class="form-control sel-branch" id="branch" data-placeholder="Pilih Branch" name="sim_branch"></select>
                            </div>
                        </div>

                        <div class="col-md-12 exclude">
                            <div class="form-group">
                                <label for="company" class="control-label">Client Company</label><span class="text-danger">*</span>
                                <select class="form-control sel-client-company" id="client-company" data-placeholder="Pilih Company" name="client_company"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('.exclude').hide();
        $("#setting-datatable").DataTable({
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $("#setting2-datatable").DataTable({
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $(".btn-branch").on('click', function($data){
            $(".sel-company").select2({
                width: '100%',
                ajax: {
                    url: '{{ route("service.sim-company") }}',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            nama: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: false,
                    delay: 300
                }
            });

            $(".sel-client-company").select2({
                width: '100%',
                ajax: {
                    url: '{{ route("service.client-company") }}',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            nama: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: false,
                    delay: 300
                }
            });

            $(".sel-branch").select2({width: '100%'});

            $(".sel-company").on('change', function(){
                $('.sel-branch').select2('destroy');

                company_data = $(".sel-company").select2('data');

                $('.sel-branch').val("");

                $('.sel-branch').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route("service.sim-branch") }}',
                        dataType: 'json',
                        data: function(params) {
                            return {
                                nama: params.term || '',
                                page: params.page || 1,
                                company_id: company_data[0].id
                            }
                        },
                        cache: false,
                        delay: 300
                    }
                });
            });

            $('#modal-branch').modal('show');
        });

        $("#modal-branch").delegate('#type', 'change', function(e){
            e.preventDefault();

            if ($(this).val() == 'include') {
                $('.include').show();
                $('.exclude').hide();
            }
            else {
                $('.include').hide();
                $('.exclude').show();
            }
        });
    });
</script>
@endsection
