@extends('app')

@section('style')
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-title')
{!! page_title('Dashboard') !!}
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-4 pb-0 opening-card">
                <div class="media">
                    <div class="media-body">
                        <h2 class="mt-2 mb-1 text-color-opening">Halo, Selamat Datang Kembali {{ auth()->user()->admin->name }}!</h2>
                        <p class="text-p mt-2">ini adalah layanan Administrator Employee Self Service, selamat menggunakan aplikasi kami.</p>
                    </div>
                    <div class="image-offside">
                        <img class="d-flex" src="{{ asset('assets/images/group_103.png') }}" alt="" height="145">
                    </div>
                </div>
            </div> <!-- end card-box -->
        </div>
    </div>
    <!-- end page title -->

    <!-- <div class="row">
        <div class="col-md-4">
            <div class="card-box">
                <h4 class="mt-0 font-16">Employee Login</h4>
                <h2 class="text-primary my-3 text-center">$<span data-plugin="counterup">200</span></h2>
                <p class="text-muted mb-0">Daily</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card-box">
                <h4 class="mt-0 font-16">Employee Login</h4>
                <h2 class="text-primary my-3 text-center">$<span data-plugin="counterup">200</span></h2>
                <p class="text-muted mb-0">Weekly</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card-box">
                <h4 class="mt-0 font-16">Employee Login</h4>
                <h2 class="text-primary my-3 text-center">$<span data-plugin="counterup">200</span></h2>
                <p class="text-muted mb-0">Monthly</p>
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Download History</h4>

                    <div class="form-group">
                        <input type="text" id="periode" class="form-control" data-provide="datepicker" data-date-min-view-mode="1" data-date-format="MM yyyy" data-date-autoclose="true" name="periode" placeholder="Periode Download">
                    </div>

                    <div class="col-lg-12 text-center div-loader" style="display: none">
                        <div class="spinner-border avatar-lg text-primary m-2" role="status"></div>
                    </div><!-- end col -->

                    <div class="mt-4 chartjs-chart">
                        <canvas id="bar-chart-example" height="350"></canvas>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->

        <div class="col-lg-4">
            <div class="card-box" dir="ltr">
                <h4 class="header-title mb-3">Active PE</h4>

                <div class="col-lg-12 text-center div-loader-second" style="display: none">
                    <div class="spinner-border avatar-lg text-primary m-2" role="status"></div>
                </div><!-- end col -->

                <div id="morris-donut-example" style="height: 350px;" class="morris-chart"></div>
                <div class="text-center">
                    <p class="text-muted font-15 font-family-secondary mb-0">
                        <span class="mx-2"><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Active User</span>
                        <span class="mx-2"><i class="mdi mdi-checkbox-blank-circle text-info"></i> In-Active User</span>
                    </p>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->
</div>
@endsection

@section('script')
<!-- Chart JS -->
<script src="{{ asset('assets/libs/chart-js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/morris-js/morris.min.js') }}"></script>
<script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
<script>
var chart;
function createDownload(periode = "", updateData = "") {
    var ctx = $("#bar-chart-example").get(0).getContext("2d");
    var barOpts = {
        maintainAspectRatio: false,
        legend: {
            display: true
        },
        scales: {
            yAxes: [{
                gridLines: {
                    display: false
                },
                stacked: false,
                ticks: {
                    stepSize: 20,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                barPercentage: 0.7,
                categoryPercentage: 0.5,
                stacked: false,
                gridLines: {
                    color: "rgba(0,0,0,0.01)"
                }
            }]
        }
    };

    $(".div-loader").show();

    $.get($(this).data('url'), {
        periode: periode
    }, function(dataResponse){

        var barChart = {
            labels: [dataResponse.label],
            datasets: [
                {
                    label: "Contract",
                    backgroundColor: "#6658dd",
                    borderColor: "#6658dd",
                    hoverBackgroundColor: "#6658dd",
                    hoverBorderColor: "#6658dd",
                    data: [dataResponse.contract]
                },
                {
                    label: "Surat Keterangan Kerja",
                    backgroundColor: "#1abc9c",
                    borderColor: "#1abc9c",
                    hoverBackgroundColor: "#1abc9c",
                    hoverBorderColor: "#1abc9c",
                    data: [dataResponse.skk]
                },
                {
                    label: "ID Card",
                    backgroundColor: "#4fc6e1",
                    borderColor: "#4fc6e1",
                    hoverBackgroundColor: "#4fc6e1",
                    hoverBorderColor: "#4fc6e1",
                    data: [dataResponse.id_card]
                },
                {
                    label: "Surat Riwayat Kerja",
                    backgroundColor: "#f7b84b",
                    borderColor: "#f7b84b",
                    hoverBackgroundColor: "#f7b84b",
                    hoverBorderColor: "#f7b84b",
                    data: [dataResponse.srk]
                },
                {
                    label: "BPJS Ketenagakerjaan",
                    backgroundColor: "#f1556c",
                    borderColor: "#f1556c",
                    hoverBackgroundColor: "#f1556c",
                    hoverBorderColor: "#f1556c",
                    data: [dataResponse.bpjstkj]
                },
                {
                    label: "BPJS Kesehatan",
                    backgroundColor: "#4a81d4",
                    borderColor: "#4a81d4",
                    hoverBackgroundColor: "#4a81d4",
                    hoverBorderColor: "#4a81d4",
                    data: [dataResponse.bpjskes]
                }
            ]
        };
        if (updateData == 'update') {
            chart.destroy();
            chart = new Chart(ctx, { type: 'bar', data: barChart, options: barOpts });
        }
        else {
            chart = new Chart(ctx, { type: 'bar', data: barChart, options: barOpts });

            Morris.Donut({
                element: 'morris-donut-example',
                data: [
                    {label: "Active User", value: dataResponse.activePe},
                    {label: "In-Active User", value: dataResponse.notActivePe}
                ],
                colors: ["#6658dd", "#4fc6e1"]
            });
        }

        $(".div-loader").hide();
        $(".div-loader-second").hide();

    }).fail(function(){
        $.toast().reset('all');
        $.toast({
            heading: "Oops!",
            text: "Data tidak ditemukan",
            position: 'top-right',
            loaderBg: '#bf441d',
            icon: 'error',
            hideAfter: 5000,
            stack: 1
        });
    });
}
$(document).ready(function(){
    $(".div-loader-second").show();

    createDownload();

    $("#periode").on('change', function(){
        createDownload($(this).val(), "update");
    });
});
</script>
@endsection
