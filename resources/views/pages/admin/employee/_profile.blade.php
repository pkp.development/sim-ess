<div class="row">
    <div class="col-sm-12">
        <h3 class="mb-4">Data Karyawan</h3>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="table-responsive">
            <table class="table table-striped mb-0">
                <tbody>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>SIMID</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->account->sim_id }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>NPO</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">@if($data->account->assignment){{ $data->account->assignment->npo }}@else{{ "-" }}@endif</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Email</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->account->email }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Foto Profile</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">
                            <div class="img-profile">
                                <a href="@if($data->profile_picture){{ route('admin.employee.get.tmp-image', ['profile', $data->uuid, 'false']) }}@else{{ Avatar::create($data->name)->toBase64() }}@endif" class="image-popup" title="Foto Profile">
                                    <img src="@if($data->profile_picture){{ route('admin.employee.get.tmp-image', ['profile', $data->uuid, 'false']) }}@else{{ Avatar::create($data->name)->toBase64() }}@endif" height="88"  style="object-fit: cover"alt="user-image" class="s-fluid rounded avatar-xxl" id="profile-picture" onerror="this.src = '{{ Avatar::create($data->name)->toBase64() }}'">
                                </a>
                                <input type="file" name="profile_picture" class="inp-profile-picture form-control" style="display: none;" accept="image/*">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Lengkap</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tempat, tanggal lahir</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->place_of_birth }}, {{ \Carbon\Carbon::createFromFormat('Y-m-d', $data->date_of_birth)->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Jenis Kelamin</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->gender }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Status Pernikahan</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->marital_status }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Agama</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->religion }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Pendidikan Terakhir</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->recent_education }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Ibu</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->mother_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Alamat Domisili</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->domicile_address }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Provinsi</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->domicile_province_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kota</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->domicile_city_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kode Pos</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->domicile_zip_code }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="table-responsive">
            <table class="table table-striped mb-0">
                <tbody>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>No. Telepon 1</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->phone_number }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>No. Telepon 2</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->secondary_phone }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>NIK</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->nik }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">
                            <a href="@if($data->identity_image){{ route('admin.employee.get.tmp-image', ['identity', $data->uuid, 'false']) }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-popup" title="Foto Identitas">
                                <img src="@if($data->identity_image){{ route('admin.employee.get.tmp-image', ['identity', $data->uuid, 'false']) }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;"  alt="identity-image" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>NPWP</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->npwp_number }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">
                            <a href="@if($data->npwp_image){{ route('admin.employee.get.tmp-image', ['npwp', $data->uuid, 'false']) }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-popup" title="Foto NPWP">
                                <img src="@if($data->npwp_image){{ route('admin.employee.get.tmp-image', ['npwp', $data->uuid, 'false']) }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;"  alt="npwp-image" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Alamat Identitas</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->address_identity }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Provinsi</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->province_identity_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kota</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->city_identity_name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kode Pos</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                        <td style="padding-bottom: 5px; padding-top: 5px;">{{ $data->zip_code_identity }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-md-12">
        <h3 class="mb-4">History</h3>
        <div class="table-responsive">
            <table id="employee-datatable" class="table table-striped w-100">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Handled By</th>
                        <th>Status</th>
                        <th>Rejected Reason</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content form-modal">
            <div class="modal-header">
                <h4 class="modal-title">Detail History</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="mb-4">Data Karyawan</h3>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">
                                <tbody>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Email</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-email">{{ $data->account->email }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Foto Profile</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-profile_picture_change"></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Lengkap</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-name">{{ $data->name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tempat, tanggal lahir</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-ttl"></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Jenis Kelamin</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-gender">{{ $data->gender }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Status Pernikahan</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-marital_status">{{ $data->marital_status }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Agama</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-religion">{{ $data->religion }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Pendidikan Terakhir</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-recent_education">{{ $data->recent_education }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Ibu</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-mother_name">{{ $data->mother_name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>No. Telepon 1</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-phone_number">{{ $data->phone_number }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>No. Telepon 2</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-secondary_phone">{{ $data->secondary_phone }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">
                                <tbody>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>NIK</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-nik">{{ $data->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Foto Identitas</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-identity_image_change"></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>NPWP</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-npwp_number">{{ $data->npwp_number }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Foto NPWP</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-npwp_image"></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Alamat Domisili</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-domicile_address">{{ $data->domicile_address }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Provinsi</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-domicile_province">{{ $data->domicile_province_name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kota</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-domicile_city">{{ $data->domicile_city }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kode Pos</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-domicile_zip_code">{{ $data->domicile_zip_code }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Alamat Identitas</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-address_identity">{{ $data->address_identity }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Provinsi</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-province_identity">{{ $data->provinces_identity_name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kota</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-city_identity">{{ $data->city_identity_name }}</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Kode Pos</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                        <td style="padding-bottom: 5px; padding-top: 5px;" class="td-zip_code_identity">{{ $data->zip_code_identity }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<div id="modal-reason" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alasan Penolakan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body p-4">
                <p id="txt-reason"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

@section('information-pe')
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>
<script>
    $(document).ready(function(){
        drawDatatable('#employee-datatable');
        var table = $("#employee-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.employee.detail', $data->uuid) }}",
            },
            columns: [
                {data: 'tanggal', name: 'tanggal'},
                {data: 'admin', name: 'admin', orderable: false, searchable: false},
                {data: 'status', name: 'status'},
                {data: 'reason', name: 'reason'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[4, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $("table").delegate('.btn-reason', 'click', function(e){
            $('#txt-reason').html($(this).data('reason'));
            $('#modal-reason').modal('show');
        });

        $("table").delegate('.btn-detail', 'click', function(e){
            e.preventDefault();
            url = $(this).attr('href');

            $.get(url, {}, function(data){
                console.log(data);

                $.each(data, function(i,v){
                    $('.td-' + i).html(v);

                    if(i == 'place_of_birth') {
                        $('.td-ttl').html(data.place_of_birth + ', ' + data.date_of_birth_format);
                    }
                    if(i == 'domicile_province_name') {
                        $('.td-domicile_province').html(data.domicile_province_name);
                    }
                    if(i == 'domicile_city_name') {
                        $('.td-domicile_city').html(data.domicile_city_name);
                    }
                    if(i == 'city_identity_name') {
                        $('.td-city_identity').html(data.city_identity_name);
                    }
                    if(i == 'province_identity_name') {
                        $('.td-province_identity').html(data.province_identity_name);
                    }
                });

                $('#con-close-modal').modal('show');
            }, 'json').fail(function(){
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });
    });
</script>
@endsection
