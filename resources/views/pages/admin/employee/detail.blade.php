@extends('app')

@section('style')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">

<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-title')
{!! page_title('Detail Informasi Karyawan') !!}
@endsection

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12 ">
            <div class="card-box mt-3">
                <ul class="nav nav-tabs nav-justified nav-bordered navbar-information">
                    <li class="nav-item ">
                        <a href="{{ route('pe.info.karyawan') }}" @if($tabs == 'profile')aria-expanded="true"@endif class="nav-link @if($tabs == 'profile') active @endif">
                            Biodata Karyawan
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane @if($tabs == 'profile') show active @endif" id="profile">
                        @if($tabs == 'profile')
                        @include('pages.admin.employee._profile')
                        @endif
                    </div>
                </div> <!-- end Tab Content-->
            </div> <!-- end card-box-->


        </div> <!-- col 12  -->
    </div> <!-- row  -->
</div> <!-- containert fluid  -->
<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->
@endsection

@section('script')
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>

<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

@yield('information-pe')
@endsection
