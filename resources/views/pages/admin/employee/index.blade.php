@extends('app')

@section('page-title')
{!! page_title('Employee') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">List Employee</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="employee-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>SIM ID</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No. Telepon</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form-modal">
            <div class="modal-header">
                <h4 class="modal-title">Form Reset Password Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="javascript:void(0)" method="POST" enctype="multipart/form-data" id="form-change-password">
            <div class="modal-body p-4">
                @csrf
                <input type="hidden" name="_method" value="POST" id="_method">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="password">Password</label><span class="text-danger"> *</span>
                            <div class="input-group">
                                <input class="form-control" type="password" name="password" id="password" placeholder="Masukan Password" aria-label="Masukan Password">
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-password waves-effect waves-light" type="button">Perlihatkan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="password">Konfirmasi Password</label>
                            <div class="input-group">
                                <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Masukan Konfirmasi Password" aria-label="Masukan Konfirmasi Password">
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-password waves-effect waves-light" type="button">Perlihatkan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/core/admin.init.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#employee-datatable');
        var table = $("#employee-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.employee.index') }}",
            },
            columns: [
                {data: 'account.sim_id', name: 'account.sim_id'},
                {data: 'name', name: 'name'},
                {data: 'account.email', name: 'account.email'},
                {data: 'phone_number', name: 'phone_number'},
                {data: 'account.status', name: 'account.status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[6, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $('table').delegate('.btn-edit', 'click', function(e){
            url = $(this).data('route');
            $('#form-change-password').attr('action', url);

            $("#con-close-modal").modal('show');
        });

        $('.btn-password').click(function(){
            parent    = $(this).parents('.input-group');
            formInput = parent.find('.form-control');

            if(formInput.attr('type') == 'text'){
                formInput.attr('type','password');
                $(this).text('Perlihatkan');
            }else{
                formInput.attr('type','text');
                $(this).text('Sembunyikan');
            }
        });
    });
</script>
@endsection
