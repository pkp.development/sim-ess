@extends('app')

@section('page-title')
{!! page_title('Verifikasi Biodata PE') !!}
@endsection

@section('style')

@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-lg-12 mt-3">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pt-2 pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Attribute</th>
                                            <th>Data Lama</th>
                                            <th>Data Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if($dataOld->profile_picture != $content->profile_picture) class="table-warning" @endif>
                                            <td><strong>Profile Picture</strong></td>
                                            <td class="text-center">
                                                @if(!$dataOld->profile_picture)
                                                <a href="{{ Avatar::create($dataOld->name)->toBase64() }}" class="zbox" title="Foto Profile">
                                                    <img src="{{ Avatar::create($dataOld->name)->toBase64() }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['profile', $dataOld->uuid, 'false']) }}" class="zbox" title="Foto Profile">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['profile', $dataOld->uuid, 'false']) }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['profile', $profile->uuid]) }}" class="zbox" title="Foto Profile">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['profile', $profile->uuid]) }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image" onerror="this.src = '{{ Avatar::create($dataOld->name)->toBase64() }}'">
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->name != $content->name) class="table-warning" @endif>
                                            <td><strong>Nama Lengkap</strong></td>
                                            <td class="text-center">{{ $dataOld->name }}</td>
                                            <td class="text-center">{{ $content->name }}</td>
                                        </tr>

                                        <tr @if($dataOld->place_of_birth != $content->place_of_birth || $dataOld->date_of_birth != $content->date_of_birth) class="table-warning" @endif>
                                            <td><strong>Tempat dan Tanggal Lahir</strong></td>
                                            <td class="text-center">{{ isset($old_profile['birth_place'])? $old_profile['birth_place']['nama_data'] : '' /*$dataOld->place_of_birth*/ }}, {{ $dataOld->date_of_birth }}</td>
                                            <td class="text-center">{{ isset($new_profile['birth_place'])? $new_profile['birth_place']['nama_data'] : '' /*$content->place_of_birth*/ }}, {{ $content->date_of_birth }}</td>
                                        </tr>

                                        <tr @if($dataOld->religion != $content->religion) class="table-warning" @endif>
                                            <td><strong>Agama</strong></td>
                                            <td class="text-center">{{ isset($old_profile['religion'])? $old_profile['religion']['nama_data'] : '' /*$dataOld->religion */}}</td>
                                            <td class="text-center">{{ isset($new_profile['religion'])? $new_profile['religion']['nama_data'] : '' /*$content->religion */ }}</td>
                                        </tr>

                                        <tr @if($dataOld->gender != $content->gender) class="table-warning" @endif>
                                            <td><strong>Jenis Kelamin</strong></td>
                                            <td class="text-center">{{ $dataOld->gender }}</td>
                                            <td class="text-center">{{ $content->gender }}</td>
                                        </tr>

                                        <tr @if($dataOld->nik != $content->nik) class="table-warning" @endif>
                                            <td><strong>NIK</strong></td>
                                            <td class="text-center">{{ $dataOld->nik }}</td>
                                            <td class="text-center">{{ $content->nik }}</td>
                                        </tr>

                                        <tr @if($dataOld->identity_image != $content->identity_image) class="table-warning" @endif>
                                            <td></td>
                                            <td class="text-center">
                                                @if(!$dataOld->identity_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="zbox" title="Foto Identitas">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: 150px; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['identity', $dataOld->uuid, 'false']) }}" class="zbox" title="Foto Identitas">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['identity', $dataOld->uuid, 'false']) }}" style="height: auto; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['identity', $profile->uuid]) }}" class="zbox" title="Foto Identitas">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['identity', $profile->uuid]) }}" style="height: auto; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if(isset($dataOld->selfie_image) && isset($content->selfie_image)) @if($dataOld->selfie_image != $content->selfie_image) class="table-warning" @endif @endif>
                                            <td><strong>Foto Selfie KTP</strong></td>
                                            <td class="text-center">
                                                @if(isset($dataOld->selfie_image))
                                                @if(!$dataOld->selfie_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="zbox" title="Foto Selfie KTP">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: 150px; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['selfie', $dataOld->uuid, 'false']) }}" class="zbox" title="Foto Selfie KTP">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['selfie', $dataOld->uuid, 'false']) }}" style="height: auto; width: 250px;"  alt="selfie-image" class="rounded" >
                                                </a>
                                                @endif
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['selfie', $profile->uuid]) }}" class="zbox" title="Foto Selfie KTP">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['selfie', $profile->uuid]) }}" style="height: auto; width: 250px;"  alt="selfie-image" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->address_identity != $content->address_identity) class="table-warning" @endif>
                                            <td><strong>Alamat Identitas</strong></td>
                                            <td class="text-center">{{ $dataOld->address_identity }}</td>
                                            <td class="text-center">{{ $content->address_identity }}</td>
                                        </tr>

                                        <tr @if($dataOld->city_identity != $content->city_identity) class="table-warning" @endif>
                                            <td><strong>Kota</strong></td>
                                            <td class="text-center">{{ @$dataOld->city_identity_name }}</td>
                                            <td class="text-center">{{ @$content->city_identity_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->province_identity != $content->province_identity) class="table-warning" @endif>
                                            <td><strong>Provinsi</strong></td>
                                            <td class="text-center">{{ @$dataOld->province_identity_name }}</td>
                                            <td class="text-center">{{ @$content->province_identity_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->zip_code_identity != $content->zip_code_identity) class="table-warning" @endif>
                                            <td><strong>Kode Pos</strong></td>
                                            <td class="text-center">{{ $dataOld->zip_code_identity }}</td>
                                            <td class="text-center">{{ $content->zip_code_identity }}</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Status Pernikahan</strong></td>
                                            <td class="text-center">{{ $dataOld->marital_status }}</td>
                                            <td class="text-center">{{ $content->marital_status }}</td>
                                        </tr>

                                        <tr @if($dataOld->recent_education != $content->recent_education) class="table-warning" @endif>
                                            <td><strong>Pendidikan Terakhir</strong></td>
                                            <td class="text-center">{{ isset($old_profile['education'])? $old_profile['education']['nama_data'] : '' /*$dataOld->recent_education*/ }}</td>
                                            <td class="text-center">{{ isset($new_profile['education'])? $new_profile['education']['nama_data'] : '' /*$content->recent_education*/ }}</td>
                                        </tr>

                                        <tr @if($dataOld->phone_number != $content->phone_number) class="table-warning" @endif>
                                            <td><strong>No. Telepon</strong></td>
                                            <td class="text-center">{{ $dataOld->phone_number }}</td>
                                            <td class="text-center">{{ $content->phone_number }}</td>
                                        </tr>

                                        <tr @if($dataOld->secondary_phone != $content->secondary_phone) class="table-warning" @endif>
                                            <td><strong>No. Telepon 2</strong></td>
                                            <td class="text-center">{{ $dataOld->secondary_phone }}</td>
                                            <td class="text-center">{{ $content->secondary_phone }}</td>
                                        </tr>

                                        <tr @if($dataOld->mother_name != $content->mother_name) class="table-warning" @endif>
                                            <td><strong>Nama Ibu</strong></td>
                                            <td class="text-center">{{ $dataOld->mother_name }}</td>
                                            <td class="text-center">{{ $content->mother_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->account->email != $content->email) class="table-warning" @endif>
                                            <td><strong>Email</strong></td>
                                            <td class="text-center">{{ $dataOld->account->email }}</td>
                                            <td class="text-center">{{ $content->email }}</td>
                                        </tr>

                                        <tr @if($dataOld->npwp_number != $content->npwp_number) class="table-warning" @endif>
                                            <td><strong>No. NPWP</strong></td>
                                            <td class="text-center">{{ $dataOld->npwp_number }}</td>
                                            <td class="text-center">{{ $content->npwp_number }}</td>
                                        </tr>

                                        <tr @if($dataOld->npwp_periode != @$content->npwp_periode) class="table-warning" @endif>
                                            <td><strong>Tahun Terbit NPWP</strong></td>
                                            <td class="text-center">{{ $dataOld->npwp_periode }}</td>
                                            <td class="text-center">{{ @$content->npwp_periode }}</td>
                                        </tr>

                                        <tr @if($dataOld->npwp_image != $content->npwp_image) class="table-warning" @endif>
                                            <td></td>
                                            <td class="text-center">
                                                @if(!$dataOld->npwp_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="zbox" title="Foto NPWP">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: auto; width: 250px;"  alt="npwp-image" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['npwp', $dataOld->uuid, 'false']) }}" class="zbox" title="Foto NPWP">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['npwp', $dataOld->uuid, 'false']) }}" style="height: auto; width: 250px;"  alt="npwp-image" class="rounded" >
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['npwp', $profile->uuid]) }}" class="zbox" title="Foto NPWP">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['npwp', $profile->uuid]) }}" style="height: auto; width: 250px;"  alt="npwp-image" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->domicile_address != $content->domicile_address) class="table-warning" @endif>
                                            <td><strong>Alamat Domisili</strong></td>
                                            <td class="text-center">{{ $dataOld->domicile_address }}</td>
                                            <td class="text-center">{{ $content->domicile_address }}</td>
                                        </tr>

                                        <tr @if($dataOld->domicile_city != $content->domicile_city) class="table-warning" @endif>
                                            <td><strong>Kota</strong></td>
                                            <td class="text-center">{{ @$dataOld->domicile_city_name }}</td>
                                            <td class="text-center">{{ @$content->domicile_city_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->domicile_province != $content->domicile_province) class="table-warning" @endif>
                                            <td><strong>Provinsi</strong></td>
                                            <td class="text-center">{{ @$dataOld->domicile_province_name }}</td>
                                            <td class="text-center">{{ @$content->domicile_province_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->domicile_zip_code != $content->domicile_zip_code) class="table-warning" @endif>
                                            <td><strong>Kode Pos</strong></td>
                                            <td class="text-center">{{ @$dataOld->domicile_zip_code }}</td>
                                            <td class="text-center">{{ @$content->domicile_zip_code }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end .pt2 -->
                    </div> <!-- end col-->
                </div>

            </div> <!-- end .padding -->
            <div class="text-right">
                <button type="button" class="btn btn-refused btn-danger btn-lg waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-window-close"></i> Tolak</button>
                <form action="{{ route('admin.employee.update.post', $profile->uuid) }}" method="POST" style="display: inline-block">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" value="verified">
                    <button type="submit" class="btn btn-verified btn-confirm btn-success btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Verified</button>
                </form>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end row -->
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.employee.update.post', $profile->uuid) }}" method="POST" style="display: inline-block">
                {!! csrf_field() !!}
                <input type="hidden" name="type" value="refused">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="refused" class="control-label">Alasan Penolakan</label>
                                <textarea name="description" class="form-control" id="refused" rows="4" placeholder="Masukan Alasan Penolakan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<script>
$(document).ready(function(){
    $(".btn-refused").on('click', function(){
        $('#con-close-modal').modal('show');
    });
});
</script>
@endsection
