@extends('app')

@section('page-title')
{!! page_title('Audit Trail') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">Audit Trail</h4>
                            <div class="float-right">
                                <div class="input-group">
                                    <input type="text" class="form-control datetime-datepicker" id="filter_date" name="filter_date" placeholder="Filter by Date" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="audit-trail-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Type</th>
                                <th>Handled By</th>
                                <th>Description</th>
                                <th>Created at</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#audit-trail-datatable');
        var table = $("#audit-trail-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.audit-trail.index') }}",
                data: function(d) {
                    d.filter_date = $('#filter_date').val();
                }
            },
            columns: [
                {data: 'title', name: 'title'},
                {data: 'type', name: 'type'},
                {data: 'admin', name: 'account.admin.name'},
                {data: 'description', name: 'description'},
                {data: 'date', name: 'created_at'},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[5, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $('#filter_date').on('change', function() {
            table.draw();
        });
    });

    var flatPicker = $('.datetime-datepicker').flatpickr({
        altInput: true,
        altFormat: "d F Y",
        dateFormat: "Y-m-d",
        // appendTo: window.document.querySelector(".clockpicker")
        static: true
    });
</script>
@endsection
