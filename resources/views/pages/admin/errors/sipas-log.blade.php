@extends('app')

@section('page-title')
{!! page_title('Sipas Logs') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="table-responsive">
                    <table id="sms-callback-datatable" class="table table-striped table-hover w-100">
                        <thead>
                            <tr>
                                <th>Module</th>
                                <th>Data</th>
                                <th>Message</th>
                                <th>Created at</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($result->data as $data)
                            <tr>
                                <td>{{ $data->module }}</td>
                                <td>
                                    <button data-content="{{ preg_replace("/\r\n|\r|\n/",'<br>',preg_replace("/^\n*/", '', json_encode(json_decode($data->data), JSON_PRETTY_PRINT))) }}" class="btn btn-sm btn-primary btn-content">Tampilkan</button>
                                </td>
                                <td>
                                    <button data-content="{{ $data->message }}" class="btn btn-sm btn-primary btn-content">Tampilkan</button>
                                </td>
                                <td>{{ $data->created_at }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" class="text-center">Data tidak ditemukan</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <div class="btn-group mb-2 float-right">
                        @if ($result->current_page != 1)
                        <a href="{{ route('admin.sipas-log.index', ['page' => $result->current_page - 1]) }}" class="btn btn-primary"><i class="mdi mdi-chevron-double-left"></i> Sebelumnya</a>
                        @else
                        <button type="button" class="btn btn-primary disabled"><i class="mdi mdi-chevron-double-left" disabled=""></i> Sebelumnya</button>
                        @endif

                        @if ($result->current_page < $result->last_page)
                        <a href="{{ route('admin.sipas-log.index', ['page' => $result->current_page+1]) }}" class="btn btn-primary">Berikutnya <i class="mdi mdi-chevron-double-right"></i></a>
                        @else
                        <button type="button" class="btn btn-primary disabled" disabled="">Berikutnya <i class="mdi mdi-chevron-double-right"></i></button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12 div-content">

                    </div> <!-- end col -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $("table").delegate('.btn-content', 'click', function(){
            $(".div-content").html($(this).data('content'));
            $('#modal-detail').modal('show');
        });
    });
</script>
@endsection
