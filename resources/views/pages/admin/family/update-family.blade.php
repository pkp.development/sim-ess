@extends('app')

@section('page-title')
{!! page_title('Verifikasi Daftar Keluarga') !!}
@endsection

@section('style')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 pt-2 pb-2">
            <div class="card-box text-center">
                <a href="{{ route('admin.employee.get.tmp-image', ['family-card', $family->account->profile->uuid, 'false']) }}" class="image-popup" title="Foto Kartu Keluarga">
                    <img src="{{ route('admin.employee.get.tmp-image', ['family-card', $family->account->profile->uuid, 'false']) }}" style="height: 150px; width: auto;"  alt="foto kartu keluarga" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                </a>

                <h4 class="mb-0">{{ $family->account->profile->family_card_number }}</h4>
            </div> <!-- end card-box -->
        </div> <!-- end col-->

        <div class="col-lg-8 pt-2 pb-2">
            <div class="card-box">
                @if($family->action == 'create')
                <h4>Aksi tambah daftar keluarga</h4>
                @elseif($family->action == 'update')
                <h4>Aksi ubah daftar keluarga</h4>
                @elseif($family->action == 'delete')
                <h4>Aksi hapus daftar keluarga</h4>
                @endif


                <div class="table-responsive">
                    <table class="table table-borderless mb-0">
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Nama</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $family->name }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>NIK</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $family->nik }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Tempat, tanggal lahir</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ isset($bpr_ref_new['place_of_birth'])? $bpr_ref_new['place_of_birth']['nama_data'] : '' /*$family->place_of_birth*/ }}, {{ $family->date_of_birth->format('d F Y') }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>No. Telepon</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">@if($family->phone_number){{ $family->phone_number }}@else{{ "-" }}@endif</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Jenis Kelamin</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ ucwords(strtolower($family->gender)) }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Hubungan Keluarga</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ isset($bpr_ref_new['relation'])? $bpr_ref_new['relation']['nama_data'] : ''/*strtoupper($family->relation)*/ }}</td>
                        </tr>
                    </table>
                </div>
            </div> <!-- end .padding -->
            <div class="text-right">
                <button type="button" class="btn btn-refused btn-danger btn-lg waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-window-close"></i> Tolak</button>
                <form action="{{ route('admin.employee.daftar.family.post', $family->uuid) }}" method="POST" style="display: inline-block">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" value="verified">
                    <button type="submit" class="btn btn-verified btn-confirm btn-success btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Verified</button>
                </form>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end row -->
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.employee.daftar.family.post', $family->uuid) }}" method="POST" style="display: inline-block">
                {!! csrf_field() !!}
                <input type="hidden" name="type" value="refused">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="refused" class="control-label">Alasan Penolakan</label>
                                <textarea name="description" class="form-control" id="refused" rows="4" placeholder="Masukan Alasan Penolakan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<script>
$(document).ready(function(){
    $(".btn-refused").on('click', function(){
        $('#con-close-modal').modal('show');
    });
});
</script>
@endsection
