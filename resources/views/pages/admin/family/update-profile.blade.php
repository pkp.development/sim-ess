@extends('app')

@section('page-title')
{!! page_title('Verifikasi Biodata Keluarga') !!}
@endsection

@section('style')

@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-lg-12 mt-3">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pt-2 pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Attribute</th>
                                            <th>Data Lama</th>
                                            <th>Data Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if($dataOld->family_card_number != $content->family_card_number) class="table-warning" @endif>
                                            <td><strong>Nomor Kartu Keluarga</strong></td>
                                            <td class="text-center">{{ $dataOld->family_card_number }}</td>
                                            <td class="text-center">{{ $content->family_card_number }}</td>
                                        </tr>

                                        <tr @if($dataOld->family_card_image != $content->family_card_image) class="table-warning" @endif>
                                            <td></td>
                                            <td class="text-center">
                                                @if(!$dataOld->family_card_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="zbox" title="Foto Kartu Keluarga">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: 150px; width: auto;"  alt="foto kartu keluarga" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['family-card', $dataOld->uuid, 'false']) }}" class="zbox" title="Foto Kartu Keluarga">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['family-card', $dataOld->uuid, 'false']) }}" style="height: 150px; width: auto;"  alt="foto kartu keluarga" class="rounded" >
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['family-card', $family->uuid]) }}" class="zbox" title="Foto Kartu Keluarga">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['family-card', $family->uuid]) }}" style="height: 150px; width: auto;"  alt="foto kartu keluarga" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->tax_marital_status != $content->tax_marital_status) class="table-warning" @endif>
                                            <td><strong>Tax Marital Status</strong></td>
                                            <td class="text-center">{{ isset($bpr_ref_old['marital'])? $bpr_ref_old['marital']['nama_data'] : ''  /*$dataOld->tax_marital_status*/ }}</td>
                                            <td class="text-center">{{ isset($bpr_ref_new['marital'])? $bpr_ref_new['marital']['nama_data'] : '' /*$content->tax_marital_status*/ }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end .pt2 -->
                    </div> <!-- end col-->
                </div>

            </div> <!-- end .padding -->
            <div class="text-right">
                <button type="button" class="btn btn-refused btn-danger btn-lg waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-window-close"></i> Tolak</button>
                <form action="{{ route('admin.employee.update.family.post', $family->uuid) }}" method="POST" style="display: inline-block">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" value="verified">
                    <button type="submit" class="btn btn-verified btn-confirm btn-success btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Verified</button>
                </form>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end row -->
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.employee.update.family.post', $family->uuid) }}" method="POST" style="display: inline-block">
                {!! csrf_field() !!}
                <input type="hidden" name="type" value="refused">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="refused" class="control-label">Alasan Penolakan</label>
                                <textarea name="description" class="form-control" id="refused" rows="4" placeholder="Masukan Alasan Penolakan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<script>
$(document).ready(function(){
    $(".btn-refused").on('click', function(){
        $('#con-close-modal').modal('show');
    });
});
</script>
@endsection
