@extends('app')

@section('page-title')
{!! page_title('Klaim Rawat Inap') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="card-box mt-3">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="mb-4">Detail Rawat Inap</h3>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>SIMID</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->account->sim_id }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Karyawan</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->account->profile->name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Dibuat Oleh</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->createdby->admin->name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Pasien</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->patient_name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Rumah Sakit</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->hospital_name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Diagnosis</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->diagnosis }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nominal</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ number_format($result->nominal) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>

                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Status</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ ucwords(str_replace('_', ' ', $result->status)) }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tanggal Kwitansi</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->receipt_date->format('d F Y') }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tanggal Berkas Diterima</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->received_date->format('d F Y') }}</td>
                            </tr>
                            @if ($result->status == 'dana_cair_ke_karyawan')
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nominal Pencairan</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ number_format($result->disbursment) }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tanggal Transfer</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">@if($result->transfer_date){{ $result->transfer_date->format('d F Y') }}@else{{ '-' }}@endif</td>
                            </tr>
                            @endif
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Keterangan</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">@if($result->transfer_date){{ $result->description }}@else{{ "-" }}@endif</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @if ($result->status == 'berkas_tidak_lengkap' || $result->status == 'kirim_berkas_ke_asuransi' || $result->status == 'berkas_disetujui_asuransi')
        <div class="row">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-lg btn-primary waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">
                    <i class="mdi mdi-send "></i> Edit
                </button>
            </div>
        </div>
        @endif
    </div>

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <h3 class="mb-3">History Perubahan</h3>
                <div class="table-responsive">
                    <table id="inpatient-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Diupdate Oleh</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

@if ($result->status == 'berkas_tidak_lengkap' || $result->status == 'kirim_berkas_ke_asuransi' || $result->status == 'berkas_disetujui_asuransi')
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form-modal">
            <div class="modal-header">
                <h4 class="modal-title">Edit Klaim Rawat Inap</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form action="{{ route('admin.inpatient.update', $result->uuid) }}" method="POST" id="form-inpatient" enctype="multipart/form-data">
            <div class="modal-body p-4">
                <div class="row">
                    @csrf
                    @method('PATCH')

                    <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                            <label for="status">Status</label><span class="text-danger">*</span>
                            <select class="form-control status-select2" id="status" data-placeholder="Pilih Status Klaim" name="status">
                                <option></option>
                                <option value="berkas_tidak_lengkap">Berkas Tidak Lengkap</option>
                                <option value="kirim_berkas_ke_asuransi">Kirim Berkas ke Asuransi</option>
                                <option value="berkas_disetujui_asuransi">Berkas Disetujui Asuransi</option>
                                <option value="dana_cair_ke_karyawan">Dana Cair ke Karyawan</option>
                                <option value="berkas_ditolak_asuransi">Berkas Ditolak Asuransi</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12" id="col-transfer-date" style="display: none;">
                        <div class="form-group">
                            <label for="transfer_date" class="control-label">Tanggal Transfer ke Karyawan</label><span class="text-danger">*</span>
                            <input type="text" class="form-control datetime-datepicker" id="transfer_date" name="transfer_date" placeholder="Masukkan Tanggal Transfer ke Karyawan" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12" id="col-disbursment" style="display: none;">
                        <div class="form-group">
                            <label for="disbursment">Nominal Cair</label><span class="text-danger">*</span>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend">Rp</span>
                                </div>
                                <input type="text" class="form-control" data-mask='currency-rupiah' id="disbursment" name="disbursment" placeholder="Nominal Cair" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description" class="control-label">Keterangan</label>
                            <textarea name="description" id="description" class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">
                    <i class="mdi mdi-send "></i> Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endif
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
$(document).ready(function(){
    drawDatatable('#inpatient-datatable');
    var table = $("#inpatient-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('admin.inpatient.detail') }}/" + "{{ $result->uuid }}",
        },
        columns: [
            {data: 'updatedby.admin.name', name: 'updatedby.admin.name'},
            {data: 'tanggal', name: 'tanggal'},
            {data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[3, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });

    var flatPicker = $('.datetime-datepicker').flatpickr({
        altInput: true,
        altFormat: "d F Y",
        dateFormat: "Y-m-d",
        // appendTo: window.document.querySelector(".clockpicker")
        static: true
    });

    $(".status-select2").select2({
        width: '100%',
    });

    $("#status").on('change', function(e){
        if ($(this).val() == 'dana_cair_ke_karyawan') {
            $("#col-transfer-date").show();
            $("#col-disbursment").show();
        }
        else {
            $("#col-transfer-date").hide();
            $("#col-disbursment").hide();
        }
    });
});
</script>
@endsection
