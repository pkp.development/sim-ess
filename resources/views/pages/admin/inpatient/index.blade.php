@extends('app')

@section('page-title')
{!! page_title('Klaim Rawat Inap') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="table-responsive">
                    <table id="inpatient-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>SIMID</th>
                                <th>Nama Pasien</th>
                                <th>Nama RS</th>
                                <th>Diagnosis</th>
                                <th>Nominal</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Input Klaim Rawat Inap</h4>

                <form action="{{ route('admin.inpatient.store') }}" method="POST" id="form-inpatient" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="simid">SIMID</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="simid" name="simid" placeholder="SIMID" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="patient_name">Nama Pasien</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="patient_name" name="patient_name" placeholder="Nama Pasien" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="diagnosis">Diagnosis</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis" autocomplete="off">
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="hospital_name">Nama Rumah Sakit</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="hospital_name" name="hospital_name" placeholder="Nama RUmah Sakit" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="nominal">Nominal Klaim</label><span class="text-danger">*</span>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" data-mask='currency-rupiah' id="nominal" name="nominal" placeholder="Nominal Klaim" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="status">Status</label><span class="text-danger">*</span>
                                <select class="form-control status-select2" id="status" data-placeholder="Pilih Status Klaim" name="status">
                                    <option></option>
                                    <option value="berkas_tidak_lengkap">Berkas Tidak Lengkap</option>
                                    <option value="kirim_berkas_ke_asuransi">Kirim Berkas ke Asuransi</option>
                                    <option value="berkas_disetujui_asuransi">Berkas Disetujui Asuransi</option>
                                    <option value="dana_cair_ke_karyawan">Dana Cair ke Karyawan</option>
                                    <option value="berkas_ditolak_asuransi">Berkas Ditolak Asuransi</option>
                                </select>
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="receipt_date" class="control-label">Tanggal Kwitansi</label><span class="text-danger">*</span>

                                <div class="input-group">
                                    <input type="text" class="form-control datetime-datepicker" id="receipt_date" name="receipt_date" placeholder="Masukkan Tanggal Kwitansi" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="received_date" class="control-label">Tanggal Berkas Diterima</label><span class="text-danger">*</span>

                                <div class="input-group">
                                    <input type="text" class="form-control datetime-datepicker" id="received_date" name="received_date" placeholder="Masukkan Tanggal Berkas Diterima" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" id="col-transfer-date" style="display: none;">
                            <div class="form-group">
                                <label for="transfer_date" class="control-label">Tanggal Transfer ke Karyawan</label><span class="text-danger">*</span>

                                <div class="input-group">
                                    <input type="text" class="form-control datetime-datepicker" id="transfer_date" name="transfer_date" placeholder="Masukkan Tanggal Transfer ke Karyawan" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-6" id="col-disbursment" style="display: none;">
                            <div class="form-group">
                                <label for="disbursment">Nominal Cair</label><span class="text-danger">*</span>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" data-mask='currency-rupiah' id="disbursment" name="disbursment" placeholder="Nominal Cair" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="description" class="control-label">Keterangan</label>
                                <textarea name="description" id="description" class="form-control" placeholder="Keterangan"></textarea>
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-lg btn-primary waves-effect waves-light">
                                <i class="mdi mdi-send "></i> Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- end row-->
    </div> <!-- end card-box -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>

<script>
$(document).ready(function(){
    drawDatatable('#inpatient-datatable');
    var table = $("#inpatient-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('admin.inpatient.index') }}",
        },
        columns: [
            {data: 'account.sim_id', name: 'account.sim_id'},
            {data: 'patient_name', name: 'patient_name'},
            {data: 'hospital_name', name: 'hospital_name'},
            {data: 'diagnosis', name: 'diagnosis'},
            {data: 'nominal', name: 'nominal'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[7, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });

    var flatPicker = $('.datetime-datepicker').flatpickr({
        altInput: true,
        altFormat: "d F Y",
        dateFormat: "Y-m-d",
        // appendTo: window.document.querySelector(".clockpicker")
        // static: true
    });

    $(".status-select2").select2({
        width: '100%',
    });

    $("#status").on('change', function(e){
        if ($(this).val() == 'dana_cair_ke_karyawan') {
            $("#col-transfer-date").show();
            $("#col-disbursment").show();
        }
        else {
            $("#col-transfer-date").hide();
            $("#col-disbursment").hide();
        }
    });
});
</script>
@endsection
