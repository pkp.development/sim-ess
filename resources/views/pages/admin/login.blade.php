<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>SIM - ESS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <!-- JQuery Toast -->
        <link href="{{ asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg authentication-bg-pattern">

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">

                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{ asset('assets/images/logo.png') }}" alt="" height="64"></span>
                                    </a>
                                    <h3 class="text-muted mb-4 mt-1">Employee Self Service</h3>
                                </div>

                                <form action="{{ route('admin.auth') }}" method="POST">
                                    {!! csrf_field() !!}

                                    <div class="form-group mb-3">
                                        <label for="username">Username</label>
                                        <input class="form-control" type="text" id="username" name="username" placeholder="Masukan Username Anda">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <div class="input-group">
                                            <input class="form-control" type="password" name="password" id="password" placeholder="Masukan Password Anda" aria-label="Masukan Password Anda">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-password waves-effect waves-light" type="button">Perlihatkan</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="remember-me" id="checkbox-signin" checked>
                                            <label class="custom-control-label" for="checkbox-signin">Ingat Saya</label>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit"> Masuk </button>
                                    </div>

                                </form>

                                <div class="text-center mt-3">
                                    <p> <a href="pages-recoverpw.html" class="text-muted-50 ml-1">Lupa Password ?</a></p>
                                </div>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>

        <!-- JQuery Toast -->
        <script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

        <!-- Laravel Javascript Validation -->
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! @$validator !!}

        @include('partials.alert')

        <script type="text/javascript">
            $(document).ready(function(){
                $('.btn-password').click(function(){
                    if($('#password').attr('type') == 'text'){
                        $('#password').attr('type','password');
                        $('.btn-password').text('Perlihatkan');
                    }else{
                        $('#password').attr('type','text');
                        $('.btn-password').text('Sembunyikan');
                    }
                });
            });
        </script>
    </body>
</html>
