@extends('app')

@section('page-title')
{!! page_title('Verifikasi Daftar Keluarga') !!}
@endsection

@section('style')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 pt-2 pb-2">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget-rounded-circle card-box">
                        <div class="row">
                            <div class="col-6">
                                <div class="avatar-lg rounded bg-soft-primary">
                                    <i class="dripicons-wallet font-24 avatar-title text-primary"></i>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="text-right">
                                    <h3 class="text-dark mt-1">Rp <span data-plugin="counterup">{{ number_format($sisa_plafond) }}</span></h3>
                                    <p class="text-muted mb-1 text-truncate">Sisa Plafond</p>
                                </div>
                            </div>
                        </div> <!-- end row-->
                    </div> <!-- end widget-rounded-circle-->
                </div>

                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-12">
                                <a href="{{ $data->receipt_file }}" class="image-popup" title="Foto Kartu Keluarga">
                                    <img src="{{ $data->receipt_file }}" style="height: 100px; width: auto;"  alt="foto kartu keluarga" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                                </a>

                                <h4 class="mb-0">File Kwitansi</h4>
                            </div>
                            <div class="col-12 pt-3">
                                <a href="{{ $data->doctor_prescription_file }}" class="image-popup" title="Foto Kartu Keluarga">
                                    <img src="{{ $data->doctor_prescription_file }}" style="height: 100px; width: auto;"  alt="foto kartu keluarga" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                                </a>

                                <h4 class="mb-0">File Resep Dokter</h4>
                            </div>
                        </div> <!-- end row-->
                    </div>
                </div>
            </div>
        </div> <!-- end col-->

        <div class="col-lg-8 pt-2 pb-2">
            <div class="card-box">
                <h4>Verifikasi Rawat Jalan</h4>

                <div class="table-responsive">
                    <table class="table table-borderless mb-0">
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>SIMID</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $data->account->sim_id }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Nama Karyawan</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $data->account->profile->name }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Nama Pasien</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $data->patient_name }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Nama Dokter</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $data->doctor_name }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Deskripsi</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $data->description }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Tanggal Kwitansi</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">{{ $data->receipt_date->format('d F Y') }}</td>
                        </tr>
                        <tr style="">
                            <td style="width: 25%; padding: .5rem"><strong>Nominal</strong></td>
                            <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                            <td style="padding: .5rem">Rp {{ number_format($data->nominal) }}</td>
                        </tr>
                    </table>
                </div>
            </div> <!-- end .padding -->
            <div class="text-right">
                <button type="button" class="btn btn-refused btn-danger btn-lg waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-window-close"></i> Tolak</button>
                <form action="{{ route('admin.outpatient.verification.post', $data->uuid) }}" method="POST" style="display: inline-block">
                    {!! csrf_field() !!}
                    <input type="hidden" name="type" value="verified">
                    <button type="submit" class="btn btn-verified btn-confirm btn-success btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Verified</button>
                </form>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end row -->
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('admin.outpatient.verification.post', $data->uuid) }}" method="POST" style="display: inline-block">
                {!! csrf_field() !!}
                <input type="hidden" name="type" value="refused">
                <div class="modal-header">
                    <h4 class="modal-title">Form Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="refused" class="control-label">Alasan Penolakan</label>
                                <textarea name="description" class="form-control" id="refused" rows="4" placeholder="Masukan Alasan Penolakan"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<script>
$(document).ready(function(){
    $(".btn-refused").on('click', function(){
        $('#con-close-modal').modal('show');
    });
});
</script>
@endsection
