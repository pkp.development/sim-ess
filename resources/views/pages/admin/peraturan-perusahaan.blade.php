@extends('app')

@section('page-title')
{!! page_title('Peraturan Perusahaan') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Peraturan Perusahaan</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- start page title -->
    <div class="row">

        <div class="col-lg-12 col-sm-12">
            <div class="card-box">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">List Peraturan Perusahaan</h4>
                            <button class="btn btn-primary btn-add waves-effect waves-light float-right">
                                <i class="mdi mdi-plus"></i> Tambah
                            </button>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table id="peraturan-perusahaan-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Effective Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- end page title -->
</div>

<div id="modal-perusahaan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('admin.peraturan-perusahaan.store') }}" method="POST" enctype="multipart/form-data" id="form-peraturan-perusahaan">
                @csrf
                <input type="hidden" id="method" name="_method" value="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Peraturan Perusahaan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="title">Judul</label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="Judul" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="radio radio-primary form-check-inline">
                                    <input type="radio" id="type-file" class="type_peraturan" value="file" name="type" checked>
                                    <label for="type-file"> File </label>
                                </div>
                                <div class="radio radio-primary form-check-inline">
                                    <input type="radio" id="type-uri" class="type_peraturan" value="link" name="type">
                                    <label for="type-uri"> Link </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="effective_date">Effective Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control datetime-datepicker" id="effective_date" name="effective_date" placeholder="Masukkan Tanggal Berlaku" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="type-file">
                                    <label for="file_peraturan">File Peraturan</label>
                                    <input type="file" class="filestyle" id="file_peraturan" name="file_peraturan" accept="application/pdf">
                                    <span class="help-block"><small>Tipe file yang diizinkan adalah PDF</small></span>
                                </div>
                                <div class="type-link" style="display: none">
                                    <label for="file_peraturan">Link Peraturan</label>
                                    <input type="text" class="form-control" id="link_peraturan" name="link_peraturan" placeholder="Link Peraturan" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/core/admin.init.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#peraturan-perusahaan-datatable');
        var table = $("#peraturan-perusahaan-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.peraturan-perusahaan.index') }}",
            },
            columns: [
                {data: 'title', name: 'title'},
                {data: 'effective_date', name: 'effective_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[3, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $(".modal").delegate('.type_peraturan', 'change', function(e){
            let type = $("input[name='type']:checked").val();
            if (type == 'file') {
                $(".type-file").show();
                $(".type-link").hide();
            }

            if (type == 'link') {
                $(".type-file").hide();
                $(".type-link").show();
            }
        });

        $(".btn-add").on('click', function(e){
            $("#form-peraturan-perusahaan").attr('action', "{{ route('admin.peraturan-perusahaan.store') }}");
            $("#method").val('post');
            $("#title").val("");
            flatPicker.clear();
            $('#modal-perusahaan').modal('show');
        });

        $("table").delegate('.btn-edit', 'click', function (e){
            e.preventDefault();

            const url = $(this).attr('href');

            $.get(url, function (data) {
                $("#form-peraturan-perusahaan").attr('action', url);
                $("#method").val('patch');
                $("#title").val(data.title);
                flatPicker.destroy();
                flatPicker = $('.datetime-datepicker').flatpickr({
                    altInput: true,
                    altFormat: "d F Y",
                    dateFormat: "Y-m-d",
                    defaultDate: data.effective_date,
                    static: true
                });

                if (data.type == 'file') {
                    $('#type-file').prop('checked', true);
                    $('#type-uri').prop('checked', false);

                    $(".type-file").show();
                    $(".type-link").hide();
                }
                if (data.type == 'link') {
                    $('#type-file').prop('checked', false);
                    $('#type-uri').prop('checked', true);

                    $("#link_peraturan").val(data.file);

                    $(".type-file").hide();
                    $(".type-link").show();
                }

                $('#modal-perusahaan').modal('show');
            }).fail(function() {
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });

        let flatPicker = $('.datetime-datepicker').flatpickr({
            altInput: true,
            altFormat: "d F Y",
            dateFormat: "Y-m-d",
            // appendTo: window.document.querySelector(".clockpicker")
            static: true
        });
    });
</script>
@endsection
