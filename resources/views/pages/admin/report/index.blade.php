@extends('app')

@section('page-title')
{!! page_title('Reporting') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card mt-3">
                <div class="card-body">

                    <form action="{{ route('admin.report.store') }}" method="POST" id="form-report">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tipe Laporan</label>
                                    <select class="form-control type-select2" id="type" data-placeholder="Pilih Tipe Laporan" name="type">
                                        <option></option>
                                        <option value="employee_login">Employee Login</option>
                                        <option value="tanda_tangan_kontrak">Tanda Tangan Kontrak</option>
                                        <option value="perubahan_biodata">Perubahan Biodata Karyawan</option>
                                        <option value="input_klaim_rawat_jalan">Input Klaim Rawat Jalan</option>
                                        <option value="download_history_kontrak">History Download Kontrak</option>
                                        <option value="download_payslip">History Download Payslip</option>
                                        <option value="download_spt">History Download SPT</option>
                                        <option value="downlad_skk">History Download SKK</option>
                                        <option value="download_srk">History Download SRK</option>
                                        <option value="download_idcard">History Download ID Card</option>
                                        <option value="download_bpjstk">History Download BPJSTK</option>
                                        <option value="download_bpjs_kesehatan">History Download BPJS Kesehatan</option>
                                        <option value="upload_payslip">History Upload Payslip</option>
                                        <option value="upload_spt">History Upload SPT</option>
                                        <option value="upload_idcard">History Upload ID Card</option>
                                        <option value="upload_bpjstk">History Upload BPJSTK</option>
                                        <option value="upload_bpjs_kesehatan">History Upload BPJS Kesehatan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Periode Laporan</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="periode-laporan" name="periode_laporan" placeholder="Periode Laporan" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary waves-effect waves-light mt-2"><i class="mdi mdi-cloud-download-outline"></i> Download Report</button>
                        </div>
                    </form>

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".type-select2").select2({
            width: '100%',
        });

        var flatPicker = $('#periode-laporan').flatpickr({
            altInput: true,
            altFormat: "d F Y",
            dateFormat: "Y-m-d",
            // appendTo: window.document.querySelector(".clockpicker")
            // static: true,
            mode: 'range'
        });
    });
</script>
@endsection
