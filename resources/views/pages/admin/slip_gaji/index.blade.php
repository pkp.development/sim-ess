@extends('app')

@section('page-title')
{!! page_title('Slip Gaji') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">Settings</h4>
                            <a href="javascript:void(0)" class="btn btn-primary btn-create waves-effect waves-light float-right">
                                <span class="btn-label"><i class="mdi mdi-plus"></i></span>Input Data &nbsp;&nbsp;
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="payslip-account-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Nama Kategori</th>
                                <th>Tipe</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Masukan Data Slip Gaji</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form action="{{ route('admin.slip_gaji.store') }} " method='POST' id="form_slip">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" id="method" value="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Nama Kategori</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Kategori">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Tipe Kategori</label>
                                <br>
                                <div class="radio form-check-inline">
                                    <input type="radio" id="pendapatan" value="pendapatan" name="type" checked="">
                                    <label for="pendapatan"> Pendapatan </label>
                                </div>
                                <div class="radio form-check-inline">
                                    <input type="radio" id="pengeluaran" value="pengeluaran" name="type">
                                    <label for="pengeluaran"> Pengurang </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#payslip-account-datatable');
        var table = $("#payslip-account-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.slip_gaji.index') }}",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'type', name: 'type'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[3, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $(".btn-create").on('click', function(e){
            e.preventDefault();

            $("#form_slip").attr('action', "{{ route('admin.slip_gaji.store') }}")

            $('input#method').val('POST');
            $('input#name').val("");
            $('input#pendapatan').prop('checked', false);
            $('input#pengeluaran').prop('checked', false);

            $("#con-close-modal").modal().show();
        });

        $("#payslip-account-datatable").delegate(".btn-edit", 'click', function(e){
            e.preventDefault();

            code = $(this).data('code');

            $.get("{{ route('admin.slip_gaji.get.account-first') }}" + "/" + code, {}, function(data){

                $("#form_slip").attr('action', "{{ route('admin.slip_gaji.update') }}" + "/" + code)

                $('input#method').val('PATCH');
                $('input#name').val(data.name);
                if(data.type == 'pendapatan') {
                    $('input#pendapatan').prop('checked', true);
                }
                else if (data.type == 'pengeluaran') {
                    $('input#pengeluaran').prop('checked', true);
                }

                $("#con-close-modal").modal().show();

            }, 'json').fail(function(){
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });
    });
</script>

@endsection
