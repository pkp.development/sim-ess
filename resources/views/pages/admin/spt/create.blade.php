@extends('app')

@section('page-title')
{!! page_title('Form Upload SPT') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card mt-3">
                <div class="card-body">

                    <form action="{{ route('admin.spt.store') }}" method="POST" enctype="multipart/form-data" id="form-spt">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tahun SPT</label>
                                    <input type="text" class="form-control" data-provide="datepicker" data-date-min-view-mode="2" data-date-format="yyyy" name="year">
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <label for="upload">Upload Data SPT</label><span class="text-danger">*</span>
                                    <input type="file" class="filestyle" name="spt_upload" id="spt_upload">
                                    <span class="help-block"><small>Tipe file yang diizinkan adalah csv.</small></span>
                                </div>
                            </div> <!-- end col -->
                        </div>
                        <div class="text-right">
                            <a href="{{ route('admin.spt.download-template') }}" class="btn btn-secondary waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-cloud-download-outline"></i> Download Template</a>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Simpan</button>
                        </div>
                    </form>

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="task-benefit-datatable" class="table table-striped w-100">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Diupload Oleh</th>
                                    <th>Total Failed</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
    $(document).ready(function(){
        drawDatatable('#task-benefit-datatable');
        var table = $("#task-benefit-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.spt.index') }}",
            },
            columns: [
                {data: 'tanggal', name: 'tanggal'},
                {data: 'uploadedby.admin.name', name: 'uploadedby.admin.name'},
                {data: 'total_failed', name: 'total_failed'},
                {data: 'status', name: 'status', searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[5, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    });
</script>
@endsection
