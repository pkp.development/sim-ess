
@extends('app')

@section('page-title')
{!! page_title('User Admin') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">Form Tambah User Admin</h4>
                            <a href="{{ route('admin.user.index') }}" class="btn btn-light waves-effect waves-light float-right" >
                                <span class="btn-label"><i class="mdi mdi-chevron-left"></i></span>Kembali
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12">
                        <form method="POST" action="{{ route('admin.user.update', $data->uuid) }}">
                             @csrf
                             @method('PATCH')
                            <div class="row">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Nama Lengkap</label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control" name="name" id="name" value="{{ $data->admin->name }}" placeholder="Masukkan Nama Lengkap" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control" name="email" id="email" value="{{ $data->email }}" placeholder="Masukkan Email" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_number">No. Telepon</label>
                                        <input type="text" class="form-control" name="phone_number" id="phone_number" value="{{ $data->admin->phone_number }}" placeholder="Masukkan No. Telepon" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="username">Username</label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control" name="username" id="username" value="{{ $data->username }}" placeholder="Masukkan Username" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <div class="input-group">
                                            <input class="form-control" type="password" name="password" id="password" placeholder="Masukan Password" aria-label="Masukan Password">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-password waves-effect waves-light" type="button">Perlihatkan</button>
                                            </div>
                                        </div>
                                        <small id="passwordHelp" class="form-text text-muted">Abaikan field password bila tidak ingin mengubah password.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Konfirmasi Password</label>
                                        <div class="input-group">
                                            <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Masukan Konfirmasi Password" aria-label="Masukan Konfirmasi Password">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-password waves-effect waves-light" type="button">Perlihatkan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-sm-12">
                                    <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="for_super" id="for_super" @if($data->role == 'super-admin') checked="" @endif>
                                            <label class="custom-control-label" for="for_super">Berikan Akses Sebagai Super Admin?</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/core/admin.init.js') }}"></script>

<script>
    $(document).ready(function(){
        $('.btn-password').click(function(){
            parent    = $(this).parents('.input-group');
            formInput = parent.find('.form-control');

            if(formInput.attr('type') == 'text'){
                formInput.attr('type','password');
                $(this).text('Perlihatkan');
            }else{
                formInput.attr('type','text');
                $(this).text('Sembunyikan');
            }
        });
    });
</script>
@endsection
