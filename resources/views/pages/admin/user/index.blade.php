@extends('app')

@section('page-title')
{!! page_title('User Admin') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <div class="mb-3 row">
                    <div class="col-lg-12">
                        <div>
                            <h4 class="header-title float-left">List User Admin</h4>
                            <a href="{{ route('admin.user.create') }}" class="btn btn-primary waves-effect waves-light float-right" >
                                <span class="btn-label"><i class="mdi mdi-plus"></i></span>Input Data Baru &nbsp;&nbsp;
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="admin-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>No. Telepon</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<script src="{{ asset('assets/js/pages/core/admin.init.js') }}"></script>

<script>
    ADMIN_INDEX.init(["{{ route('admin.user.datatable') }}"]);
</script>
@endsection
