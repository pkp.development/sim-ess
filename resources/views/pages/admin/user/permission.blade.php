@extends('app')

@section('page-title')
{!! page_title('User Admin') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom box css -->
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Form Permission User Admin</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- start page title -->
    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <div class="card-box">
                <div class="mb-4">
                    <h4 class="header-title float-left">Client Company</h4>
                    <button class="btn btn-xs btn-primary btn-client-company waves-effect waves-light float-right">
                        <i class="mdi mdi-plus"></i>
                    </button>
                </div>

                <div class="table-responsive">
                    <table id="client-company-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Company</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div class="card-box">
                <div class="mb-4">
                    <h4 class="header-title float-left">SIM Branch</h4>
                    <button class="btn btn-xs btn-primary btn-sim-branch waves-effect waves-light float-right">
                        <i class="mdi mdi-plus"></i>
                    </button>
                </div>

                <div class="table-responsive">
                    <table id="sim-branch-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Branch</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <form method="POST" action="{{ route('admin.user.permission.store', $uuid) }}">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Permission</h4>

                        <div class="row">
                            <div class="col-lg-12">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <select multiple="multiple" class="multi-select" id="permissions" name="permissions[]" data-plugin="multiselect" data-selectable-optgroup="true">
                                                @foreach ($permissionCategories as $category)
                                                    <optgroup label="{{ $category->name }}">
                                                        @foreach ($category->permissions as $permission)
                                                            <option value="{{ $permission->id }}" @if($user->admin->permissions->where('id', $permission->id)->first()) selected @endif>{{ $permission->name }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach

                                                <optgroup label="Uncategorize">
                                                @foreach ($permissionUncategories as $permission)
                                                    <option value="{{ $permission->id }}" @if($user->admin->permissions->where('id', $permission->id)->first()) selected @endif>{{ $permission->name }}</option>
                                                @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12">
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="btn-label"><i class="mdi mdi-content-save"></i></span>Simpan</button>
                    <a href="{{ route('admin.user.index') }}" class="btn btn-dark waves-effect waves-light" >
                        <span class="btn-label"><i class="mdi mdi-chevron-left"></i></span>Kembali
                    </a>
                </div>
            </form>
        </div>
    </div>
    <!-- end page title -->
</div>

<div id="modal-client-company" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('admin.user.permission-client.store', $uuid) }}" method="POST" enctype="multipart/form-data" id="form-client-company">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Client Company</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="client_company">Client Company</label>
                                <select class="form-control client-company" id="client_company" data-placeholder="Pilih Client Company" name="client_company[]" multiple="multiple"></select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="client_company_for_all" id="client_company_for_all">
                                    <label class="custom-control-label" for="client_company_for_all">Berikan Akses Semua Client Company!</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

<div id="modal-sim-branch" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('admin.user.permission-sim-branch.store', $uuid) }}" method="POST" enctype="multipart/form-data" id="form-sim-branch">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Tambah SIM Branch</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="sim_branch">SIM Branch</label>
                                <select class="form-control sim-branch" id="sim_branch" data-placeholder="Pilih SIM Branch" name="sim_branch[]" multiple="multiple"></select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="sim_branch_for_all" id="sim_branch_for_all">
                                    <label class="custom-control-label" for="sim_branch_for_all">Berikan Akses Semua SIM Branch!</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/core/admin.init.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#client-company-datatable');
        var table = $("#client-company-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.user.permission-client.datatable', $uuid) }}",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            order: [[0, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        drawDatatable('#sim-branch-datatable');
        var table = $("#sim-branch-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('admin.user.permission-sim-branch.datatable', $uuid) }}",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            order: [[0, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });

        $('.sim-branch').select2({
            width: '100%',
            ajax: {
                url: '{{ route("service.sim-branch") }}',
                dataType: 'json',
                data: function(params) {
                    return {
                        nama: params.term || '',
                        page: params.page || 1
                    }
                },
                cache: false,
                delay: 300
            }
        });

        $(".client-company").select2({
            width: '100%',
            ajax: {
                url: '{{ route("service.client-company") }}',
                dataType: 'json',
                data: function(params) {
                    return {
                        nama: params.term || '',
                        page: params.page || 1
                    }
                },
                cache: false,
                delay: 300
            }
        });

        $('#permissions').multiSelect();

        $(".btn-client-company").on('click', function($data){
            $('#modal-client-company').modal('show');
        });

        $(".btn-sim-branch").on('click', function($data){
            $('#modal-sim-branch').modal('show');
        });
    });
</script>
@endsection
