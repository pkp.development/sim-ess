@extends('app')

@section('style')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-title')
{!! page_title('Ubah Kata Sandi') !!}
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Ubah Kata Sandi</h4>

                <form action="{{ route('change-password.post') }}" method="POST" id="form-change-password" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="password">Password Lama</label><span class="text-danger">*</span>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password Lama" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="new_password">Password Baru</label><span class="text-danger">*</span>
                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Password Baru" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="new_password_confirmation">Konfirmasi Password Baru</label><span class="text-danger">*</span>
                                <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" placeholder="Konfirmasi Password Baru" autocomplete="off">
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-lg btn-primary waves-effect waves-light">
                                <i class="mdi mdi-send "></i> Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- end row-->
    </div> <!-- end card-box -->
</div>

@endsection

@section('script')

<script>
$(document).ready(function(){

});
</script>
@endsection
