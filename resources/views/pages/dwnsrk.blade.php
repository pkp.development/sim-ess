@extends('app')

@section('page-title')
{!! page_title('Download File') !!}
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <br>
    <?php
        // echo "<pre>";
        // print_r($response);
        
        $total     = $response->jumlah;
        $total-- ;
        for ($x = 0; $x <= $total; $x++) {
            ?>  
                
                 <a href="<?php echo $response->detail[0]->srk ;?>" class="btn btn-danger" >Download</a>
            <?php
        }
    ?>
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-4 pb-0 opening-card">
                <div class="media">
                    <div class="media-body">
                        <h2 class="mt-2 mb-1 text-color-opening">Halo000, Selamat Datang Kembali {{ auth()->user()->profile->name }}!</h2>
                        <p class="text-p mt-2">ini adalah layanan Employee Self Service, selamat menggunakan aplikasi kami.</p>
                        
                    </div>
            
                    <div class="image-offside">
                        <img class="d-flex" src="{{ asset('assets/images/group_103.png') }}" alt="" height="145">
                    </div>
                </div>
            </div> <!-- end card-box -->
        </div>
    </div>
    <div class ="row">
    <div class="col-12">
            <div class="card-box mt-4 pb-0 opening-card">
                <div class="media">
                    <div class="media-body">
                        <p  class="text-p mt-2">SIMGROUP tidak pernah melakukan pungutan dalam bentuk apapun (baik uang maupun barang) kepada Karyawan.<br>
                                                       Jika anda menemukan indikasi "PUNGLI" yang mengatas namakan SIMGROUP segera laporkan ke no whatapps dan alamat email berikut :
                                       <br> WA : 082111550541
                                       <br> Email : Lapor@sim.co.id 
                        </p>
                    </div> 
                </div>
            </div>
        </div>    
    </div>    
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-4">
            <div class="text-center card-box">
                <div class="pt-2 pb-2">
                    <a href="@if(auth()->user()->profile->profile_picture){{ route('pe.get-image', 'profile') }}@else{{ Avatar::create(auth()->user()->profile->name)->toBase64() }}@endif" class="image-popup" title="Foto Profile">
                        <img src="@if(auth()->user()->profile->profile_picture){{ route('pe.get-image', 'profile') }}@else{{ Avatar::create(auth()->user()->profile->name)->toBase64() }}@endif" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                    </a>

                    <h4 class="mt-3"><a href="admin_index.html" class="text-dark">{{ auth()->user()->profile->name }}</a></h4>
                    <p class="text-muted">{{ $activeContract->job_name }} <span> | </span> <span> {{ auth()->user()->sim_id }} </span></p>
                    <div class="row mt-2">
                        <div class="col-12">
                            @if(!$activeContract->terminated_date)
                            <div class="mt-3 btn-font">
                                <span class="btn-custom-width btn-sm waves-effect waves-light" style="background-color: #E5F6FF; color: #1ABC9C;">Aktif</span>
                            </div>
                            @else
                            <div class="mt-3 btn-font">
                                <span class="btn-custom-width btn-sm waves-effect waves-light" style="background-color: #f7e5e9; color: #ff808b;">Tidak Aktif</span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div> <!-- end .padding -->
            </div> <!-- end card-box-->
        </div> <!-- end col -->

        <div class="col-lg-8">
            <div class="text-center card-box">
                <div class="pb-2">
                    <div>
                        <h4 class="text-left mt-0">Kontrak Anda</h4>
                    </div>

                </div>
                <hr>
                @if ($activeContract->ess_status == 1)
                    <div class="row">
                        <div class="col-6">
                            <div class="media mb-3 mt-4">
                                <i class="far fa-clock" style="font-size: 40px;"></i>
                                <div class="media-body text-left ml-3">
                                    <h5 class="mt-0 font-primary">Tanggal Mulai Kontrak</h5>
                                    <p class="mb-1" style="font-size: 1.250em;"><b>{{ $activeContract->contract_start->format('d F Y') }}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="media mb-3 mt-4">
                                <i class="far fa-clock" style="font-size: 40px;"></i>
                                <div class="media-body text-left ml-3">
                                    <h5 class="mt-0">Tanggal Selesai Kontrak</h5>
                                    <p class="mb-1" style="font-size: 1.250em;"><b>{{ $activeContract->contract_end->format('d F Y') }}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <a href="{{ route('pe.info.penempatan') }}" class="btn btn-block btn-lg btn-font waves-effect waves-light p-4" style="background-color: #E5F6FF; color: #1ABC9C;">Lihat Surat Kontrak</a>
                @elseif ($activeContract->ess_status == 0 && $activeContract->signed_at)
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mt-2 mb-1 text-color-opening">Kontrak yang anda telah tanda tangani sedang dalam pengecekan oleh admin!</h4>
                        </div>
                    </div>
                @elseif ($activeContract->ess_status == 2)
                    <div class="row text-left">
                        <div class="col-md-12 mb-3">
                            <h4 class="mt-2 mb-1 text-color-opening">Kontrak yang anda telah tanda tangani ditolak dengan alasan</h4>
                            <p class="text-muted mt-2">{{ $activeContract->rejected_reason }}</p>
                        </div>
                    </div>
                    <hr>
                    <a href="{{ route('pe.contract.signature-get') }}" class="btn btn-block btn-lg btn-font waves-effect waves-light p-4" style="background-color: #E5F6FF; color: #1ABC9C;">Tanda tangani Kontrak</a>
                @else
                    <div class="row text-left">
                        <div class="col-md-12 mb-3">
                            <h4 class="mt-2 mb-1 text-color-opening">Anda belum menandatangani kontrak secara digital</h4>
                            <p class="mt-2">Silahkan klik tautan dibawah untuk menandatangani kontrak.<br>Dengan belumnya ditanda tangani kontrak anda, mengakibatkan anda tidak dapat mengakses seluruh sistem ini!</p>
                        </div>
                    </div>
                    <hr>
                    <a href="{{ route('pe.contract.signature-get') }}" class="btn btn-block btn-lg btn-font waves-effect waves-light p-4" style="background-color: #E5F6FF; color: #1ABC9C;">Tanda tangani Kontrak</a>
                @endif
            </div> <!-- end card-box-->
        </div> <!-- end col -->
    </div>

    @if ($activeContract->ess_status == 1)

    <h4>Navigasi</h4>
    <div class="row mt-3">

        <div class="col-md-6 col-xl-4">
            <a href="{{ route('pe.info.karyawan') }}">
                <div class="card-box" style="background-color: #FF808B;">
                    <div class="row">
                        <div class="col-2">
                            <div class="avatar-sm rounded mt-2 p-3" style="background-color: #FFA1A9;">
                                <i class="fas fa-user-alt avatar-title font-22 text-white"></i>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="text-left ml-2 mt-1">
                                <h4 class="text-light my-1">Informasi</h4>
                                <p class="text-light mb-1">Karyawan</p>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <i class="fas fa-arrow-right text-white mb-0"></i>
                    </div>
                </div> <!-- end card-box-->
            </a>
        </div> <!-- end col -->

        <div class="col-md-6 col-xl-4">
            <a href="{{ route('pe.info.pay-slip') }}">
                <div class="card-box" style="background-color: #4D4CAC;">
                    <div class="row">
                        <div class="col-2">
                            <div class="avatar-sm rounded mt-2 p-3" style="background-color: #5C5BCF;">
                                <i class="fas fa-money-check-alt avatar-title font-22 text-white"></i>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="text-left ml-2 mt-1">
                                <h4 class="text-light my-1">Informasi Penggajian</h4>
                                <p class="text-light mb-1">Slip Gaji</p>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <i class="fas fa-arrow-right text-white mb-0"></i>
                    </div>
                </div> <!-- end card-box-->
            </a>
        </div> <!-- end col -->

        <div class="col-md-6 col-xl-4">
            <a href="{{ route('pe.info.spt') }}">
                <div class="card-box" style="background-color: #4D4CAC;">
                    <div class="row">
                        <div class="col-2">
                            <div class="avatar-sm rounded mt-2 p-3" style="background-color: #5C5BCF;">
                                <i class="fas fa-money-check-alt avatar-title font-22 text-white"></i>
                            </div>

                        </div>
                        <div class="col-10">
                            <div class="text-left ml-2 mt-1">
                                <h4 class="text-light my-1">Informasi Penggajian</h4>
                                <p class="text-light mb-1">Laporan SPT</p>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <i class="fas fa-arrow-right text-white mb-0"></i>
                    </div>
                </div> <!-- end card-box-->
            </a>
        </div> <!-- end col -->

        <div class="col-md-6 col-xl-4">
            <a href="{{ route('pe.inpatient.index') }}">
                <div class="card-box" style="background-color: #FFC257;">
                    <div class="row">
                        <div class="col-2">
                            <div class="avatar-sm rounded mt-2 p-3" style="background-color: #FFCC73;">
                                <i class="fas fa-list-ul avatar-title font-22 text-white"></i>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="text-left ml-2 mt-1">
                                <h4 class="text-light my-1">Klaim Benefit</h4>
                                <p class="text-light mb-1">Klaim Rawat Inap</p>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <i class="fas fa-arrow-right text-white mb-0"></i>
                    </div>
                </div> <!-- end card-box-->
            </a>
        </div> <!-- end col -->

        <div class="col-md-6 col-xl-4">
            <a href="{{ route('pe.rawat-jalan.index') }}">
                <div class="card-box" style="background-color: #FFC257;">
                    <div class="row">
                        <div class="col-2">
                            <div class="avatar-sm rounded mt-2 p-3" style="background-color: #FFCC73;">
                                <i class="fas fa-list-ul avatar-title font-22 text-white"></i>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="text-left ml-2 mt-1">
                                <h4 class="text-light my-1">Klaim Benefit</h4>
                                <p class="text-light mb-1">Klaim Rawat Jalan</p>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <i class="fas fa-arrow-right text-white mb-0"></i>
                    </div>
                </div> <!-- end card-box-->
            </a>
        </div> <!-- end col -->
    </div>

    @endif

</div>

<div id="simid-change" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Informasi Perubahan Akun</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <h4>Hi SIMers,</h4>
                <br>
                <p>Terima kasih sudah menggunakan ESS (Employee Self Service)</p>
                <p>Untuk login selanjutnya, silahkan menggunakan <b>{{ auth()->user()->sim_id }}</b> sebagai username untuk login ke ESS. <b>{{ auth()->user()->sim_id }}</b> merupakan SIMID kamu yang baru.</p>
                <br>
                <p>Terima Kasih</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
@if(!$isBpr)
<script>
$('#simid-change').modal('show');
</script>
@endif
@endsection

