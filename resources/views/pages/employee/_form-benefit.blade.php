<form>
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="form-group">
                <label for="vendor_asuransi">Vendor Asuransi</label>
                <input type="text" class="form-control" id="vendor_asuransi" placeholder="Masukan NamaVendor Asuransi" value="{{ @$benefit->insurance_vendor }}" readonly="">
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="form-group">
                <label for="no_asuransi">Nomor Asuransi</label>
                <input type="text" class="form-control" id="no_asuransi" placeholder="Masukan Nomor Asuransi" value="{{ @$benefit->insurance_number }}" readonly="">
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-4">
        <div class="col-md-12">
            <div class="form-group">
                <label for="no_bpjs">Nomor BPJS Kesehatan</label>
                <input type="text" class="form-control" id="no_bpjs" placeholder="Masukan Nomor BPJS Kesehatan" value="{{ @$benefit->no_bpjs_kesehatan }}" readonly="">

                @if(@$benefit->image_bpjs_kesehatan)
                <div class="mt-2">
                    <a href="{{ route('pe.info.benefit.bpjs-kes.download') }}" target="_blank" class="btn btn-secondary waves-effect waves-light"><i class="mdi mdi-download "></i> Download Kartu</a>
                </div>
                @endif
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-4">
        <div class="col-md-12">
            <label for="no_bpjs">BPJS Ketenagakerjaan</label>
            <div class="table-responsive">
                <table class="table mb-0 text-center">
                    <thead>
                        <tr>
                            <th><strong>No.</strong></th>
                            <th><strong>No. BPJS Ketenagakerjaan </strong></th>
                            <th><strong>Perusahaan</strong></th>
                            <th><strong>Aksi</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($bpjstk_ as $bpjstk)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $bpjstk->card_number }}</td>
                            <td>{{ $bpjstk->company_abbreviation }}</td>
                            <td>
                                @if($bpjstk->image)
                                <a href="{{ route('pe.info.benefit.bpjs-tkj.download', $bpjstk->card_number) }}" target="_blank" class="btn btn-secondary waves-effect waves-light"><i class="mdi mdi-download "></i> Download PDF</a>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <th scope="row" colspan="7" class="text-center">Data tidak ditemukan</th>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
</form>
