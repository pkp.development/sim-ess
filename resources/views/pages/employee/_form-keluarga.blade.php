<form method="POST" action="" id="form-data" enctype="multipart/form-data">
    @csrf
    <h3 class="mt-4">Informasi Keluarga</h3>
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="form-group">
                <label for="tax">Tax Marital Status</label>
                <select class="select-marital my-1 mr-sm-3" id="tax" name="tax_marital_status" data-placeholder="Pilih Tax Marital Status">
                    <option></option>
                    @foreach($marital as $mar)
                    <option value="{{ $mar->kode_data }}" @if($profile->tax_marital_status == $mar->kode_data) selected @endif>{{ $mar->nama_data . ' - ' . $mar->keterangan_data }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="nkk">Nomor Kartu Keluarga</label>
                <input type="text" class="form-control" id="nkk" placeholder="Masukan Nomor Kartu Keluarga" name="family_card_number" value="{{ $profile->family_card_number }}" autocomplete="off">
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-12">
            <div class="form-group">
                <label for="upload">Upload Kartu Keluarga</label>
                <div class=" w-75 mb-3">
                    <a href="@if($profile->family_card_image){{ route('pe.get-image', 'kartu_keluarga') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="Foto Kartu Keluarga">
                        <img src="@if($profile->family_card_image){{ route('pe.get-image', 'kartu_keluarga') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded img-kk" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                <input type="file" class="filestyle inp-kartu-keluarga" id="upload" name="family_card_image" accept="image/*">
                <small>Jenis gambar yang diizinkan yaitu : png, jpg, jpeg dan Maksimal ukuran gambar 1MB</small>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
    <div class="clearfix"></div>
    <div class="text-right">
        <button type="submit" class="btn btn-primary btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Simpan</button>
    </div>
</form>

<div class="row mt-5">
    <div class="col-6">
        <div class="form-group">
            <h2 for="upload">Data Keluarga</h2>
        </div>
    </div> <!-- end col -->
    <div class="col-6" style="text-align: right;">
        <div class="form-group">
            <button id="demo-btn-addrow" class="btn btn-primary btn-xs btn-open-modal"><i class="mdi mdi-plus mr-2"></i> Tambah Anggota
                Keluarga</button>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table mb-0 text-center">
                    <thead>
                        <tr>
                            <th><strong>Nama</strong></th>
                            <th><strong>Tempat Lahir </strong></th>
                            <th><strong>Tanggal Lahir</strong></th>
                            <th><strong>NIK</strong></th>
                            <th><strong>No. Telepon</strong></th>
                            <th><strong>Jenis Kelamin</strong></th>
                            <th><strong>Hubungan</strong></th>
                            <th><strong>Actions</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($families as $family)
                        <tr @if($family->status == 0)class="table-warning"@endif>
                            <th scope="row">{{ $family->name }}</th>
                            <td>{{ $family->place_of_birth }}</td>
                            <td>{{ $family->date_of_birth->format('d F Y') }}</td>
                            <td>{{ $family->nik }}</td>
                            <td>{{ $family->phone_number }}</td>
                            <td>{{ $family->gender }}</td>
                            <td>{{ ucwords($family->relation) }}</td>
                            <td>
                                {!! action_btn('javascript:void(0)', ['btn-edit'], 'mdi mdi-square-edit-outline', null, [['key' => 'url', 'value' => route('pe.info.keluarga.get-family', $family->uuid)]]) !!}
                                {!! action_btn('javascript:void(0)', ['btn-delete'], 'mdi mdi-delete', null, [['key' => 'url', 'value' => route('pe.info.keluarga.get-family', $family->uuid)]]) !!}
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <th scope="row" colspan="7" class="text-center">Data tidak ditemukan</th>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
<!-- end row -->

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form-modal">
            <div class="modal-header">
                <h4 class="modal-title">Form Keluarga</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{ route('pe.info.keluarga.post-family') }}" method="POST" enctype="multipart/form-data" id="form-family">
            <div class="modal-body p-4">
                @csrf
                <input type="hidden" name="_method" value="POST" id="_method">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="control-label">Nama Keluarga</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan nama keluarga">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nik" class="control-label">Nomor Identitas</label>
                            <input type="text" name="nik" class="form-control" id="nik" placeholder="Masukkan Nomor Identitas Keluarga">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="place_of_birth" class="control-label">Tempat Lahir</label>
                            <select class="form-control tmp_lahir" id="tmp_lahir" data-placeholder="Pilih Kota Lahir Sesuai Identitas" name="place_of_birth">
                                <option></option>
                                @foreach($region as $reg)
                                    <option value="{{ $reg->kode_data }}">{{ $reg->nama_data }}</option>
                                @endforeach
                            </select>
                            <!--<input type="text" name="place_of_birth" class="form-control" id="place_of_birth" placeholder="Masukkan Tempat Lahir Keluarga">-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_of_birth" class="control-label">Tanggal Lahir Keluarga</label>

                            <div class="input-group">
                                <input type="text" name="date_of_birth" class="form-control datetime-datepicker" id="date_of_birth" placeholder="Masukkan Tanggal Lahir Keluarga">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone_number" class="control-label">No. Telepon</label>
                            <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Masukkan No. Telepon Keluarga">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group mb-1">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                            </div>
                            <div class="radio form-check-inline ml-1">
                                <input type="radio" id="laki" value="LAKI-LAKI" name="jenis_kelamin">
                                <label for="laki"> Laki - Laki </label>
                            </div>
                            <div class="radio form-check-inline ml-1">
                                <input type="radio" id="perampuan" value="PEREMPUAN" name="jenis_kelamin">
                                <label for="perampuan"> Perempuan </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group mb-1">
                                <label for="relation">Hubungan Keluarga</label>
                                <select class="form-control relation" data-toggle="select2" id="relation" data-placeholder="Pilih Hubungan Keluarga" name="relation">
                                    <option></option>
                                    <!--<option value="197801">Kepala Keluarga</option>-->
                                    <option value="197803">Suami</option>
                                    <option value="197800">Istri</option>
                                    <!--<option value="197797">Anak</option>-->
                                    <!--<option value="197802">Menantu</option>-->
                                    <!--<option value="197798">Cucu</option>-->
                                    <!--<option value="197804">Orang Tua</option>-->
                                    <!--<option value="mertua">Mertua</option>-->
                                   <!-- <option value="197799">Famili Lain</option>-->
                                    <!--<option value="pembantu">Pembantu</option>-->
				    <!--<option value="197805">Lainnya</option>-->
                                    <option value="286972">IBU KANDUNG</option>
                                    <option value="286973">ANAK PERTAMA</option>
                                    <option value="286974">ANAK KEDUA</option>
                                    <option value="286975">ANAK KETIGA</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->

@section('information-pe')
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".inp-kartu-keluarga").change(function(){
            fasterPreview( this, '.img-kk');
        });

        $(".select-marital").select2({width: '100%'});

        $(".btn-open-modal").on('click', function(){
            $('#form-family').attr('action', "{{ route('pe.info.keluarga.post-family') }}");
            $('#_method').val('POST');
            $('#name').val("");
            $('#nik').val("");
            $('#place_of_birth').val("");
            $('#date_of_birth').val("");
            $('#phone_number').val("");

            $('#perampuan').prop("checked", false);
            $('#laki').prop("checked", false);

            $('#relation').val("").trigger('change');
            flatPicker.setDate("");

            $('#con-close-modal').modal('show');
        });

        $(".btn-edit").on('click', function(){
            console.log($(this).data('url'));
            var url =  $(this).data('url');
            $.get(url, {}, function(data){
                console.log(data);

                if (data == 'restricted') {
                    $.toast().reset('all');
                    $.toast({
                        heading: "Oops!",
                        text: "Status daftar keluarga masih menunggu verifikasi admin",
                        position: 'top-right',
                        loaderBg: '#bf441d',
                        icon: 'error',
                        hideAfter: 5000,
                        stack: 1
                    });
                }
                else {

                    $('#_method').val('PATCH');
                    $('#form-family').attr('action', url);
                    $('#name').val(data.name);
                    $('#nik').val(data.nik);
                    $('#place_of_birth').val(data.place_of_birth);
                    $('#tmp_lahir').val(data.place_of_birth);
                    $('#tmp_lahir').trigger('change');
                    // $("#place_of_birth").select2().val(data.place_of_birth).trigger("change");
                    $('#phone_number').val(data.phone_number);

                    if (data.gender == 'PEREMPUAN') {
                        $('#perampuan').prop("checked", true);
                    }
                    else if (data.gender == 'LAKI-LAKI') {
                        $('#laki').prop("checked", true);
                    }

                    $('#relation').val(data.relation).trigger('change');
                    flatPicker.setDate(data.date_of_birth, false, 'Y-m-d H:i:s');

                    $('#con-close-modal').modal('show');
                }

            }, 'json').fail(function(){
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });

        $(".btn-delete").on('click', function(){
            console.log($(this).data('url'));

            var url         = $(this).data('url');
            var this_delete = $(this);

            $.get(url, {}, function(data){
                console.log(data);

                if (data == 'restricted') {
                    $.toast().reset('all');
                    $.toast({
                        heading: "Oops!",
                        text: "Status daftar keluarga masih menunggu verifikasi admin",
                        position: 'top-right',
                        loaderBg: '#bf441d',
                        icon: 'error',
                        hideAfter: 5000,
                        stack: 1
                    });
                }
                else {
                    Swal.fire({
                        title: "Apakah anda yakin?",
                        text: "anda akan menghapus daftar keluarga.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Yes, process it!"
                    }).then(function (result) {
                        if (result.value) {
                            $.post(url, {_method: 'delete', '_token': '{{ csrf_token() }}'}, function(data){
                                this_delete.closest('tr').addClass('table-warning');
                                $.toast().reset('all');
                                $.toast({
                                    text: data,
                                    position: 'top-right',
                                    loaderBg: '#1ABC9C',
                                    icon: 'success',
                                    hideAfter: 5000,
                                    stack: 1
                                });

                            }, 'json').fail(function(){
                                $.toast().reset('all');
                                $.toast({
                                    heading: "Oops!",
                                    text: data,
                                    position: 'top-right',
                                    loaderBg: '#bf441d',
                                    icon: 'error',
                                    hideAfter: 5000,
                                    stack: 1
                                });
                            });
                        }
                    });
                }

            }, 'json').fail(function(){
                $.toast().reset('all');
                $.toast({
                    heading: "Oops!",
                    text: "Data tidak ditemukan",
                    position: 'top-right',
                    loaderBg: '#bf441d',
                    icon: 'error',
                    hideAfter: 5000,
                    stack: 1
                });
            });
        });

        var flatPicker = $('.datetime-datepicker').flatpickr({
            altInput: true,
            altFormat: "d F Y",
            dateFormat: "Y-m-d",
            // appendTo: window.document.querySelector(".clockpicker")
            static: true
        });

        $(".city_identity").select2({width: '100%'});
        $(".tmp_lahir").select2({width: '100%'});
    });
</script>

{!! $validator2 !!}
@endsection
