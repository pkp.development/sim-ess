<form>
    <div class="row">
        <div class="col-6">
            <div class="form-group text-center">
                <label for="id_card">ID Card</label>
                <div class="w-100">
                    <a href="@if($assignment->id_card_image){{ route('pe.get-image', 'id_card') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="ID Card">
                        <img src="@if($assignment->id_card_image){{ route('pe.get-image', 'id_card') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                <div class="mt-3">
                    <a href="{{ route('pe.info.download-idcard') }}" class="btn btn-secondary waves-effect waves-light"><i class="mdi mdi-download "></i> Download PDF</a>
                </div>
            </div>
        </div> <!-- end col -->
        <div class="col-6">
            <div class="form-group text-center">
                <label for="id_card">Upload ID Card</label>
                <div class="w-100">
                    <a href="@if($assignment->account->history_idcard){{ route('pe.get-image', 'idcard_selfie') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="ID Card">
                        <img src="@if($assignment->account->history_idcard){{ route('pe.get-image', 'idcard_selfie') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                <div class="mt-3">
                    <button type="button" class="btn btn-upload-idcard btn-secondary waves-effect waves-light"><i class="mdi mdi-upload "></i> Upload Foto</button>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="penempatan_klien">Klien Penempatan</label>
                <input type="text" class="form-control" id="penempatan_klien" placeholder="Masukan  Penempatan Klien" value="{{ $assignment->client_company }}" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="cabang">Cabang</label>
                <input type="text" class="form-control" id="cabang" placeholder="Masukan  Nama Cabang" value="{{ $assignment->client_branch }}" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="jabatan">Jabatan</label>
                <input type="text" class="form-control" id="jabatan" placeholder="Masukan  Jabatan" value="{{ $assignment->job_name }}" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="status">Status</label>
                <input type="text" class="form-control" id="status" placeholder="Masukan  Status" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="tgl_bergabung">Tanggal Bergabung</label>
                <input type="text" class="form-control" id="tgl_bergabung" placeholder="Masukan  Tanggal Gabung" value="@if($assignment->join_date){{ $assignment->join_date->format('d F Y') }}@endif" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-6">
            <div class="form-group">
                <label for="start_kontrak">Tanggal Kontrak</label>
                <input type="text" class="form-control" id="start_kontrak" placeholder="Masukan Tanggal Kontrak" value="@if($assignment->contract_start){{ $assignment->contract_start->format('d F Y') }}@endif" disabled>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="end_kontrak">Tanggal Selesai Kontrak</label>
                <input type="text" class="form-control" id="end_kontrak" placeholder="Masukan Tanggal Selesai Kontrak" value="@if($assignment->contract_end){{ $assignment->contract_end->format('d F Y') }}@endif" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="riwayat_kontrak">Riwayat Kontrak</label>
                <!-- <input type="text" class="form-control" id="riwayat_kontrak" placeholder="Masukan  ID KLIEN"  disabled> -->
                <table class="table mb-0 mt-2 text-center">
                    <thead>
                        <tr>
                            <th><strong>No</strong></th>
                            <th><strong>Tanggal Mulai </strong></th>
                            <th><strong>Tanggal Akhir</strong></th>
                            <th><strong>Flow Kontrak</strong></th>
                            <th><strong>Download Link</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($histories as $history)
                        @php
                            $parts = parse_url($history->url);
                            parse_str($parts['query'], $query);
                        @endphp
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $history->contractstart)->format('d F Y') }}</td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $history->contractfinish)->format('d F Y') }}</td>
                            <td>{{ $history->contract_type->ctype_name }}</td>
                            <td><a href="{{ route('pe.info.download-contract', $query['id']) }}" target="_blank"><img src="{{ asset('assets/images/pdf.png') }}" alt=""></a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">Data tidak ditemukan</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2 text-center">
        <div class="col-md-3" style="text-align:right;">
            <div class="form-group">
                <label>Surat Keterangan Kerja : </label>
            </div> <!-- end col -->
        </div>
        <div class="col-md-7" style="text-align:left;">
            <div class="form-group">
            <?php 
                 if($response->skk <> "kosong" ){
            ?>
                <a href="<?php echo $response->skk ;?>" class="btn btn-primary waves-effect waves-light mdi mdi-download" target="_blank">Download</a>
            <?php
                 }
            ?>
            
                <!-- <a href="{{ route('pe.info.download-srk') }}" target="_blank" class="btn btn-primary waves-effect waves-light @if(@$status->is_active != 'N') disabled @endif"><i class="mdi mdi-download "></i> Download PDF</a> -->
            </div> <!-- end col -->
        </div>
    </div> <!-- end row -->

    <div class="row mt-2 text-center">
        <div class="col-md-3" style="text-align:right;">
            <div class="form-group">
                <label>SRK Paklaring : </label>
            </div> <!-- end col -->
        </div>
        <div class="col-md-7" style="text-align:left;">
            <div class="form-group">
            <?php
                $total     = $response->jumlah;
                $total-- ;
                for ($x = 0; $x <= $total; $x++) {
            ?>  
                
                <a href="<?php echo $response->detail[$x]->srk ;?>" class="btn btn-primary waves-effect waves-light mdi mdi-download" target="_blank">Download</a>
            <?php
                }
            ?>
                <!-- <a href="{{ route('pe.info.download-skk') }}" target="_blank" class="btn btn-primary waves-effect waves-light @if(@$status->is_active != 'Y') disabled @endif"><i class="mdi mdi-download "></i> Download PDF</a> -->
            </div> <!-- end col -->
        </div>
    </div> <!-- end row -->
</form>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Foto ID Card</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <form action="{{ route('pe.info.upload-idcard') }} " method='POST' id="form-upload-idcard" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" id="method" value="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="idcard-image">Upload Foto ID Card</label>
                                <input type="file" class="filestyle" id="idcard-image" name="idcard_image" accept="image/*">
                                <small>Jenis gambar yang diizinkan yaitu : png, jpg, jpeg dan Maksimal ukuran gambar 1MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light btn-add-assign">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div><!-- /.modal -->

@section('information-pe')
<script>
    $(".btn-upload-idcard").on('click', function(e){
        e.preventDefault();

        $("#con-close-modal").modal().show();
    });
</script>
@endsection
