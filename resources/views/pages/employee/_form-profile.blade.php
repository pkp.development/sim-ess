<form action="{{ route('pe.update.profile') }}" method="POST" enctype="multipart/form-data" id="form-profile">
    {!! csrf_field() !!}
    {!! method_field('PATCH') !!}

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="sim_id">SIM ID</label>
                <input type="text" class="form-control" id="sim_id" placeholder="Masukan SIM ID" value="{{ $profile->account->sim_id }}" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="id_klien">ID Klien (NPO)</label>
                <input type="text" class="form-control" id="id_klien" placeholder="Masukan Id Klien" value="{{ $profile->account->assignment->npo }}" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="perusahaan">Perusahaan</label>
                <input type="text" class="form-control" id="perusahaan" placeholder="Masukan Nama Perusahaan" value="{{ auth()->user()->assignment->sim_company }}" disabled>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label for="userbio">Foto Profil</label>
                <div class="img-profile">
                    <a href="" class="btn-upload-img btn-profile-picture">
                        <i class="mdi mdi-pencil"></i>
                    </a>
                    <a href="@if($profile->profile_picture){{ route('pe.get-image', 'profile') }}@else{{ Avatar::create(auth()->user()->profile->name)->toBase64() }}@endif" class="image-fancy" title="Foto Profile">
                        <img src="@if($profile->profile_picture){{ route('pe.get-image', 'profile') }}@else{{ Avatar::create(auth()->user()->profile->name)->toBase64() }}@endif" height="88"  style="object-fit: cover"alt="user-image" class="s-fluid rounded avatar-xxl" id="profile-picture" onerror="this.src = '{{ Avatar::create(auth()->user()->profile->name)->toBase64() }}'">
                    </a>
                    <input type="file" name="profile_picture" class="inp-profile-picture form-control" style="display: none;" accept="image/*">
                </div>
                <small>Jenis gambar yang diizinkan yaitu : png, jpg, jpeg dan Maksimal ukuran gambar 1MB</small>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label for="upload2">Upload Selfie dengan KTP</label><span class="text-danger">*</span>
                <div class=" w-100 mb-3">
                    <a href="@if($profile->selfie_image){{ route('pe.get-image', 'selfie_image') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="Foto Selfie KTP">
                        <img src="@if($profile->selfie_image){{ route('pe.get-image', 'selfie_image') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded img-selfie-ktp" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                @if (!$profile->selfie_image)
                <input type="file" class="filestyle inp-selfie-ktp" id="inp_selfie_ktp" name="selfie_identity_image" accept="image/*">
                <input type="hidden" name="selfie_image" class="selfie_image" id="selfie_image" value="{{ $profile->selfie_image }}">
                <small>Jenis gambar yang diizinkan yaitu : png, jpg, jpeg dan Maksimal ukuran gambar 1MB</small>
                @endif
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">Nama Lengkap</label><span class="text-danger">*</span>
                <input type="text" class="form-control" id="name" placeholder="Masukan Nama Lengkap" name="name" value="{{ $profile->name }}" autocomplete="off">
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-6">
            <div class="form-group">
                <label for="tmp_lahir">Tempat Lahir</label><span class="text-danger">*</span>
                <select class="form-control tmp_lahir" id="tmp_lahir" data-placeholder="Pilih Kota Lahir Sesuai Identitas" name="place_of_birth">
                <option value="" ></option>
                    @foreach($region as $reg)
                        <option value="{{ $reg->kode_data }}" @if($reg->kode_data == $profile->place_of_birth) selected @endif>{{ $reg->nama_data }}</option>
                    @endforeach
                </select>
                <?php /*<input type="text" class="form-control" id="tmp_lahir" placeholder="Masukan Tempat Lahir" name="place_of_birth" value="{{ $profile->place_of_birth }}" autocomplete="off"> */ ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir</label><span
                    class="text-danger">*</span>
                <div class="input-group clockpicker" data-placement="top"
                    data-align="top" data-autoclose="true">
                    <input type="date" class="form-control datetime-datepicker" name="date_of_birth" value="{{ $profile->date_of_birth }}">
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-3">
            <div class="form-group mb-1">
                <label for="jenis_kelamin">Jenis Kelamin</label>
            </div>

            <div class="radio form-check-inline ml-1">
                <input type="radio" id="laki" value="LAKI-LAKI" name="gender" @if(strtolower($profile->gender) == 'laki-laki') checked @endif>
                <label for="laki"> Laki-Laki </label>
            </div>
            <div class="radio form-check-inline ml-1">
                <input type="radio" id="perempuan" value="PEREMPUAN" name="gender" @if(strtolower($profile->gender) == 'perempuan') checked @endif>
                <label for="perempuan"> Perempuan </label>
            </div>
        </div>
    </div> <!-- end row -->


    <div class="row mt-3">
        <div class="col-md-6">
            <div class="form-group">
                <label for="nik">NIK</label><span class="text-danger">*</span>
                <input type="text" class="form-control" id="nik" placeholder="Masukan  NIK" name="nik" value="{{ $profile->nik }}" autocomplete="off">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="npwp">NPWP </label><span class="text-danger">*</span>
                <input type="text" class="form-control" id="npwp" placeholder="Masukan NPWP" name="npwp_number" value="{{ $profile->npwp_number }}" autocomplete="off">
                <input type="hidden" id="npwp_change" name="npwp_image_change" value="">
            </div>
        </div>

        <div class="col-md-6 offset-md-6">
            <div class="form-group">
                <label for="npwp">Tahun Terbit NPWP </label><span class="text-danger">*</span>
                <input type="text" class="form-control" data-provide="datepicker" data-date-min-view-mode="2" data-date-format="yyyy" name="npwp_periode" value="{{ $profile->npwp_periode }}" placeholder="Tahun Terbit NPWP" readonly>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-3">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label for="upload">Upload KTP</label><span class="text-danger">*</span>
                <div class=" w-75 mb-3">
                    <a href="@if($profile->identity_image){{ route('pe.get-image', 'identity') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="Foto KTP">
                        <img src="@if($profile->identity_image){{ route('pe.get-image', 'identity') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded img-identity-image" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                <input type="file" class="filestyle inp-identity-image" name="inp_identity_image" id="inp_identity_image" accept="image/*">
                <input type="hidden" name="identity_image" class="identity_image" id="identity_image" value="{{ $profile->identity_image }}">
                <small>Jenis gambar yang diizinkan yaitu : png, jpg, jpeg dan Maksimal ukuran gambar 1MB</small>
            </div>
        </div> <!-- end col -->

        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <label for="upload2">Upload NPWP</label><span class="text-danger">*</span>
                <div class=" w-75 mb-3">
                    <a href="@if($profile->npwp_image){{ route('pe.get-image', 'npwp') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="Foto KTP">
                        <img src="@if($profile->npwp_image){{ route('pe.get-image', 'npwp') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded img-npwp-image" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                <input type="file" class="filestyle inp-npwp-image" id="upload2" name="npwp_image" accept="image/*">
                <small>Jenis gambar yang diizinkan yaitu : png, jpg, jpeg dan Maksimal ukuran gambar 1MB</small>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-12">
            <div class="form-group">
                <label for="alamat">Alamat KTP</label><span class="text-danger">*</span>
                <textarea class="form-control" id="alamat" rows="4" placeholder="Masukan Alamat sesuai identitas" name="address_identity">{{ $profile->address_identity }}</textarea>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


    <div class="row mt-2">
        <div class="col-md-4">
            <div class="form-group">
                <label for="province_identity">Provinsii</label><span
                    class="text-danger">*</span>
                <select class="form-control province_identity" id="province_identity" data-placeholder="Pilih Provinsi Sesuai Identitas" name="province_identity"></select>
            </div>
        </div> <!-- end col -->
        <div class="col-md-4">
            <div class="form-group">
                <label for="city_identity">Kota</label><span class="text-danger">*</span>
                <select class="form-control city_identity" id="city_identity" data-placeholder="Pilih Kota Sesuai Identitas" name="city_identity"></select>
            </div>
        </div> <!-- end col -->
        <div class="col-md-4">
            <div class="form-group">
                <label for="zipcode">Kode Pos</label><span
                    class="text-danger">*</span>
                <input type="text" class="form-control" id="zipcode" placeholder="Masukan Kode Pos Sesuai Identitas" name="zip_code_identity" value="{{ $profile->zip_code_identity }}" autocomplete="off">
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-4">
            <div class="form-group">
                <div class="form-group mb-1">
                    <label for="status_pernikahan">Status Pernikahan</label>
                </div>
                <div class="radio form-check-inline ml-1">
                    <input type="radio" id="single" value="SINGLE" name="marital_status" @if(strtolower($profile->marital_status) == 'single')checked=""@endif>
                    <label for="single"> Lajang </label>
                </div>
                <div class="radio form-check-inline ml-1">
                    <input type="radio" id="married" value="MARRIED" name="marital_status" @if(strtolower($profile->marital_status) == 'married')checked=""@endif>
                    <label for="married"> Nikah </label>
                </div>
                <div class="radio form-check-inline ml-1">
                    <input type="radio" id="divorced" value="DIVORCED" name="marital_status" @if(strtolower($profile->marital_status) == 'divorced')checked=""@endif>
                    <label for="divorced"> Cerai </label>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="religion">Agama</label><span
                    class="text-danger">*</span>
                <select class="form-control religion" id="religion" data-placeholder="Pilih Agama" name="religion">
                    <option></option>
                    <option value="197900" @if($profile->religion == '197900') selected @endif>BUDHA</option>
                    <option value="197901" @if($profile->religion == '197901') selected @endif>HINDU</option>
                    <option value="197904" @if($profile->religion == '197904') selected @endif>KONGHUCU</option>
                    <option value="197903" @if($profile->religion == '197903') selected @endif>KATOLIK</option>
                    <option value="197902" @if($profile->religion == '197902') selected @endif>ISLAM</option>
                    <option value="197905" @if($profile->religion == '197905') selected @endif>KRISTEN</option>
                    <option value="197906" @if($profile->religion == '197906') selected @endif>TIDAK TERDAFTAR</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="recent_education">Pendidikan Terakhir</label>
                <select class="form-control recent_education" id="recent_education" data-placeholder="Pilih Pendidikan Terakhir" name="recent_education">
                    <option></option>
                    <option value="197785" @if($profile->recent_education == '197785') selected @endif>SMP</option>
                    <option value="197786" @if($profile->recent_education == '197786') selected @endif>SMA</option>
                    <option value="197787" @if($profile->recent_education == '197787') selected @endif>SMK</option>
                    <option value="197788" @if($profile->recent_education == '197788') selected @endif>SMU</option>
                    <option value="197789" @if($profile->recent_education == '197789') selected @endif>D1</option>
                    <option value="197790" @if($profile->recent_education == '197790') selected @endif>D2</option>
                    <option value="197791" @if($profile->recent_education == '197791') selected @endif>D3</option>
                    <option value="197792" @if($profile->recent_education == '197792') selected @endif>D4</option>
                    <option value="197793" @if($profile->recent_education == '197793') selected @endif>S1</option>
                    <option value="197794" @if($profile->recent_education == '197794') selected @endif>S2</option>
                    <option value="197795" @if($profile->recent_education == '197795') selected @endif>S3</option>
                    <option value="197796" @if($profile->recent_education == '197796') selected @endif>SMA/SMK</option>
                </select>
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-6">
            <div class="form-group">
                <label for="no_telp1">Nomor Telpon 1</label><span
                    class="text-danger">*</span>
                <input type="tel" class="form-control" id="no_telp1" placeholder="Masukan Nomor Telpon 1" name="phone_number" value="{{ $profile->phone_number }}" autocomplete="off">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="no_telp2">Nomor Telpon 2 </label>
                <input type="tel" class="form-control" id="no_telp2" placeholder="Masukan  Nomor Telpon 2" name="secondary_phone" value="{{ $profile->secondary_phone }}" autocomplete="off">
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="nama_ibu">Nama Ibu</label>
                <input type="text" class="form-control" id="nama_ibu" placeholder="Masukan Nama Ibu" name="mother_name" value="{{ $profile->mother_name }}" autocomplete="off">
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="email">Email</label><span class="text-danger">*</span>
                <input type="email" class="form-control" id="email" placeholder="Masukan Email" name="email" value="{{ $profile->account->email }}" autocomplete="off">
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-12">
            <div class="form-group">
                <label for="alamat_domisili">Alamat Domisili</label><span class="text-danger">*</span>
                <textarea class="form-control" id="alamat_domisili" rows="4" placeholder="Masukan  Alamat Domisili" name="domicile_address">{{ $profile->domicile_address }}</textarea>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-md-4">
            <div class="form-group">
                <label for="domicile_province">Provinsi</label><span class="text-danger">*</span>
                <select class="form-control domicile_province" id="domicile_province" data-placeholder="Pilih Provinsi Sesuai Domisili" name="domicile_province"></select>
            </div>
        </div> <!-- end col -->

        <div class="col-md-4">
            <div class="form-group">
                <label for="domicile_city">Kota</label><span class="text-danger">*</span>
                <select class="form-control domicile_city" id="domicile_city" data-placeholder="Pilih Kota Sesuai Domisili" name="domicile_city"></select>
            </div>
        </div> <!-- end col -->

        <div class="col-md-4">
            <div class="form-group">
                <label for="domicile_zip_code">Kode Pos</label><span class="text-danger">*</span>
                <input type="text" class="form-control" id="domicile_zip_code" placeholder="Masukan Kode Pos Sesuai Domisili" name="domicile_zip_code" value="{{ $profile->domicile_zip_code }}" autocomplete="off">
            </div>
        </div>

    </div> <!-- end row -->
    <div class="text-right">
        <button type="submit" class="btn btn-primary btn-lg waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Simpan</button>
    </div>
</form>

@section('information-pe')
<script>
    $(document).ready(function(){
        $(".btn-profile-picture").on('click', function(e){
            e.preventDefault();

            $("#form-profile").find(".inp-profile-picture").click();
        });

        $(".inp-profile-picture").change(function(){
            fasterPreview( this, '#profile-picture');
        });

        $(".inp-identity-image").change(function(){
            fasterPreview( this, '.img-identity-image');

            if (document.getElementById("inp_identity_image").files.length != 0) {
                $(".identity_image").val("true");
            }
            else {
                $(".identity_image").val("");
            }
        });

        $("[name='npwp_number']").mask('00.000.000.0-000.000');

        $(".inp-npwp-image").change(function(){
            fasterPreview( this, '.img-npwp-image');
        });

        $("#npwp").change(function(e){
            if ("{{ $profile->npwp_number }}" != $(this).val()) {
                $("#npwp_change").val("change");
            }
            else {
                $("#npwp_change").val("");
            }
        });

        $(".inp-selfie-ktp").change(function(){
            fasterPreview( this, '.img-selfie-ktp');

            if (document.getElementById("inp_selfie_ktp").files.length != 0) {
                $(".selfie_image").val("true");
            }
            else {
                $(".selfie_image").val("");
            }
        });

        $('.datetime-datepicker').flatpickr({
            altInput: true,
            altFormat: "d F Y",
            dateFormat: "Y-m-d"
        });
        $(".tmp_lahir").select2();
        $(".province_identity, .domicile_province").select2({
            width: '100%',
            ajax: {
                url: '{{ route("service.province") }}',
                dataType: 'json',
                data: function(params) {
                    return {
                        nama: params.term || '',
                        page: params.page || 1
                    }
                },
                cache: false,
                delay: 300
            }
        });

        if("{{ $profile->province_identity }}") {
            var option = new Option("{{ @$profile->province_identity_name }}", "{{ @$profile->province_identity }}", true, true);
            $(".province_identity").append(option).trigger('change');
            $(".province_identity").trigger({
                type: 'select2:select',
                params: {
                    data: {
                        id: "{{ @$profile->province_identity }}",
                        text: "{{ @$profile->province_identity_name }}"
                    }
                }
            });

            $(".city_identity").select2({
                width: '100%',
                ajax: {
                    url: '{{ route("service.city") }}',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            nama: params.term || '',
                            page: params.page || 1,
                            province_code: "{{ @$profile->province_identity }}"
                        }
                    },
                    cache: false,
                    delay: 300
                }
            });
        }
        else {
            $(".city_identity").select2({width: '100%'});
        }

        if("{{ $profile->domicile_province }}") {
            var option = new Option("{{ @$profile->domicile_province_name }}", "{{ @$profile->domicile_province }}", true, true);
            $(".domicile_province").append(option).trigger('change');
            $(".domicile_province").trigger({
                type: 'select2:select',
                params: {
                    data: {
                        id: "{{ @$profile->domicile_province }}",
                        text: "{{ @$profile->domicile_province_name }}"
                    }
                }
            });

            $(".domicile_city").select2({
                width: '100%',
                ajax: {
                    url: '{{ route("service.city") }}',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            nama: params.term || '',
                            page: params.page || 1,
                            province_code: "{{ @$profile->domicile_province }}"
                        }
                    },
                    cache: false,
                    delay: 300
                }
            });
        }
        else {
            $(".domicile_city").select2({width: '100%'});
        }

        $(".province_identity").on('change', function(){
            $('.city_identity').select2('destroy');

            province_data = $(".province_identity").select2('data');

            $('.city_identity').val("");

            $('.city_identity').select2({
                width: '100%',
                ajax: {
                    url: '{{ route("service.city") }}',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            nama: params.term || '',
                            page: params.page || 1,
                            province_code: province_data[0].id
                        }
                    },
                    cache: false,
                    delay: 300
                }
            });
        });

        var city_code = "{{ $profile->city_identity }}";
        if(city_code) {
            var optionCity = new Option("{{ @$profile->city_identity_name }}", "{{ @$profile->city_identity }}", true, true);
            $(".city_identity").append(optionCity).trigger('change');
            $(".city_identity").trigger({
                type: 'select2:select',
                params: {
                    data: {
                        id: "{{ @$profile->city_identity }}",
                        text: "{{ @$profile->city_identity_name }}"
                    }
                }
            });
        }

        var domicile_city_code = "{{ $profile->domicile_city }}";
        if(domicile_city_code) {
            var optionCity = new Option("{{ @$profile->domicile_city_name }}", "{{ @$profile->domicile_city }}", true, true);
            $(".domicile_city").append(optionCity).trigger('change');
            $(".domicile_city").trigger({
                type: 'select2:select',
                params: {
                    data: {
                        id: "{{ @$profile->domicile_city }}",
                        text: "{{ @$profile->domicile_city_name }}"
                    }
                }
            });
        }

        $(".religion").select2({width: '100%'});
        $(".recent_education").select2({width: '100%'});

        $(".domicile_province").on('change', function(){
            $('.domicile_city').select2('destroy');

            domicile_province_data = $(".domicile_province").select2('data');

            $('.domicile_city').val("");

            $('.domicile_city').select2({
                width: '100%',
                ajax: {
                    url: '{{ route("service.city") }}',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            nama: params.term || '',
                            page: params.page || 1,
                            province_code: domicile_province_data[0].id
                        }
                    },
                    cache: false,
                    delay: 300
                }
            });
        });
    });
</script>
@endsection
