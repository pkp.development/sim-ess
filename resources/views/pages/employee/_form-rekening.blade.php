<form action="{{ route('pe.info.rekening.store') }}" method="POST" enctype="multipart/form-data" id="form-rekening">
    @csrf
    <h3 class="mt-4">Informasi Rekening</h3>
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name_of_bank">Bank</label><span class="text-danger">*</span>

                <select class="form-control name_of_bank" id="name_of_bank" data-placeholder="Pilih Kota Lahir Sesuai Identitas" name="name_of_bank">
                    @foreach($banks as $bank_bpr)
                        <option value="{{ $bank_bpr->kode_data }}" @if($bank_bpr->kode_data == $bank->name_of_bank) selected @endif>{{ $bank_bpr->nama_data }}</option>
                    @endforeach
                </select>
                <?php /*<input type="text" class="form-control name_of_bank" id="name_of_bank" placeholder="Masukan  Bank Anda" name="name_of_bank" value="{{ $bank->name_of_bank }}">*/ ?>

            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="no_rek">Nomor Rekening</label><span class="text-danger">*</span>
                <input type="text" class="form-control" id="no_rek" placeholder="Masukan  Nomor Rekening" name="account_number" value="{{ $bank->account_number }}">
            </div>
        </div>
    </div> <!-- end row -->
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="bank_acc_name">Nama Pemilik Rekening</label><span class="text-danger">*</span>
                <input type="text" class="form-control" id="bank_acc_name" placeholder="Masukan  Nama Pemilik Rekening" name="account_name" value="{{ $bank->account_name }}">
            </div>
        </div>
    </div> <!-- end row -->

    <div class="row mt-2">
        <div class="col-12">
            <div class="form-group">
                <label for="upload_req">Upload Buku Rekening</label><span class="text-danger">*</span>
                <div class=" w-75 mb-3">
                    <a href="@if($bank->bank_account_image){{ route('pe.get-image', 'rekening') }}@else{{ asset('assets/images/group_50.png') }}@endif" class="image-fancy" title="Foto Buku Rekening">
                        <img src="@if($bank->bank_account_image){{ route('pe.get-image', 'rekening') }}@else{{ asset('assets/images/group_50.png') }}@endif" style="height: 150px; width: auto;" alt="user-image" class="rounded img-bank" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                    </a>
                </div>
                <input type="file" class="filestyle inp-buku-tabungan" id="upload" name="bank_account_image" accept="image/*">
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <div class="text-right">
        <button type="submit"
            class="btn btn-primary btn-lg waves-effect waves-light mt-2"><i
                class="mdi mdi-content-save"></i> Simpan</button>
    </div>
</form>

@section('information-pe')
<script>
    $(document).ready(function(){
        $('#name_of_bank').select2();
        $(".inp-buku-tabungan").change(function(){
            fasterPreview( this, '.img-bank');
        });

        $(".name_of_bank").on('change', function(e){
            e.preventDefault();

            @if($bank->name_of_bank)
            valBank = "{{ $bank->name_of_bank }}";
            @else
            valBank = null;
            @endif

            valNumb = "{{ $bank->account_number }}";
            valName = "{{ $bank->account_name }}";

            if(valBank != $(this).val()) {
                $("#no_rek").val("");
                $("#bank_acc_name").val("");
                $(".inp-buku-tabungan").filestyle('clear');
                $(".img-bank").attr('src', "{{ asset('assets/images/group_50.png') }}");
            }
            if(valBank == $(this).val()) {
                $("#no_rek").val(valNumb);
                $("#bank_acc_name").val(valName);
                $(".img-bank").attr('src', "{{ route('pe.get-image', 'rekening') }}");
            }
        });
    });
</script>
@endsection
