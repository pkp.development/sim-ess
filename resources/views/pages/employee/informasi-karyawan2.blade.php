@extends('app')

@section('style')
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('page-title')
{!! page_title('Informasi Karyawann') !!}
@endsection

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12 ">
            <div class="card-box mt-3">
                <ul class="nav nav-tabs nav-justified nav-bordered navbar-information">
                    <li class="nav-item ">
                        <a href="{{ route('pe.info.karyawan') }}" @if($tabs == 'profile')aria-expanded="true"@endif class="nav-link @if($tabs == 'profile') active @endif">
                            Biodata Karyawan
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="{{ route('pe.info.penempatan') }}" @if($tabs == 'penempatan')aria-expanded="true"@endif class="nav-link @if($tabs == 'penempatan') active @endif">
                            Informasi Penempatan
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pe.info.keluarga') }}" @if($tabs == 'keluarga')aria-expanded="true"@endif class="nav-link @if($tabs == 'keluarga') active @endif">
                            Informasi Keluarga
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pe.info.rekening') }}" @if($tabs == 'rekening')aria-expanded="true"@endif class="nav-link @if($tabs == 'rekening') active @endif">
                            Informasi Rekening
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('pe.info.benefit') }}" @if($tabs == 'benefit')aria-expanded="true"@endif class="nav-link @if($tabs == 'benefit') active @endif">
                            Informasi Benefit
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane @if($tabs == 'profile') show active @endif" id="karyawan">
                        @if($tabs == 'profile')
                        @include('pages.employee._form-profile')
                        @endif
                    </div>

                    <div class="tab-pane @if($tabs == 'penempatan') show active @endif" id="penempatan">
                        @if($tabs == 'penempatan')
                        @include('pages.employee._form-penempatan2')
                        @endif
                    </div>

                    <div class="tab-pane @if($tabs == 'keluarga') show active @endif" id="keluarga">
                        @if($tabs == 'keluarga')
                        @include('pages.employee._form-keluarga')
                        @endif
                    </div>

                    <div class="tab-pane @if($tabs == 'rekening') show active @endif" id="rekening">
                        @if($tabs == 'rekening')
                        @include('pages.employee._form-rekening')
                        @endif
                    </div>

                    <div class="tab-pane @if($tabs == 'benefit') show active @endif" id="benefit">
                        @if($tabs == 'benefit')
                        @include('pages.employee._form-benefit')
                        @endif
                    </div>
                </div> <!-- end Tab Content-->
            </div> <!-- end card-box-->


        </div> <!-- col 12  -->
    </div> <!-- row  -->
</div> <!-- containert fluid  -->
<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->
@endsection

@section('script')
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

@yield('information-pe')
@endsection
