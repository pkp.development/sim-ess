@extends('app')

@section('page-title')
{!! page_title('Verifikasi Biodata PE') !!}
@endsection

@section('style')

@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-lg-12 mt-3">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pt-2 pb-2">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Attribute</th>
                                            <th>Data Lama</th>
                                            <th>Data Baru</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if($dataOld->profile_picture != $content->profile_picture) class="table-warning" @endif>
                                            <td><strong>Profile Picture</strong></td>
                                            <td class="text-center">
                                                @if(!$dataOld->profile_picture)
                                                <a href="{{ Avatar::create($dataOld->name)->toBase64() }}" class="image-popup" title="Foto Profile">
                                                    <img src="{{ Avatar::create($dataOld->name)->toBase64() }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['profile', $dataOld->id, false]) }}" class="image-popup" title="Foto Profile">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['profile', $dataOld->id, false]) }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['profile', $profile->id]) }}" class="image-popup" title="Foto Profile">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['profile', $profile->id]) }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->name != $content->name) class="table-warning" @endif>
                                            <td><strong>Nama Lengkap</strong></td>
                                            <td class="text-center">{{ $dataOld->name }}</td>
                                            <td class="text-center">{{ $content->name }}</td>
                                        </tr>

                                        <tr @if($dataOld->place_of_birth != $content->place_of_birth || $dataOld->date_of_birth != $content->date_of_birth) class="table-warning" @endif>
                                            <td><strong>Tempat dan Tanggal Lahir</strong></td>
                                            <td class="text-center">{{ $dataOld->place_of_birth }}, {{ $dataOld->date_of_birth }}</td>
                                            <td class="text-center">{{ $content->place_of_birth }}, {{ $content->date_of_birth }}</td>
                                        </tr>

                                        <tr @if($dataOld->religion != $content->religion) class="table-warning" @endif>
                                            <td><strong>Agama</strong></td>
                                            <td class="text-center">{{ $dataOld->religion }}</td>
                                            <td class="text-center">{{ $content->religion }}</td>
                                        </tr>

                                        <tr @if($dataOld->gender != $content->gender) class="table-warning" @endif>
                                            <td><strong>Jenis Kelamin</strong></td>
                                            <td class="text-center">{{ $dataOld->gender }}</td>
                                            <td class="text-center">{{ $content->gender }}</td>
                                        </tr>

                                        <tr @if($dataOld->nik != $content->nik) class="table-warning" @endif>
                                            <td><strong>NIK</strong></td>
                                            <td class="text-center">{{ $dataOld->nik }}</td>
                                            <td class="text-center">{{ $content->nik }}</td>
                                        </tr>

                                        <tr @if($dataOld->identity_image != $content->identity_image) class="table-warning" @endif>
                                            <td></td>
                                            <td class="text-center">
                                                @if(!$dataOld->identity_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="image-popup" title="Foto Identitas">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: 150px; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['identity', $dataOld->id, false]) }}" class="image-popup" title="Foto Identitas">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['identity', $dataOld->id, false]) }}" style="height: 150px; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['identity', $profile->id]) }}" class="image-popup" title="Foto Identitas">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['identity', $profile->id]) }}" style="height: 150px; width: 250px;"  alt="identity-image" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->address_identity != $content->address_identity) class="table-warning" @endif>
                                            <td><strong>Alamat Identitas</strong></td>
                                            <td class="text-center">{{ $dataOld->address_identity }}</td>
                                            <td class="text-center">{{ $content->address_identity }}</td>
                                        </tr>

                                        <tr @if($dataOld->city_identity != $content->city_identity) class="table-warning" @endif>
                                            <td><strong>Kota</strong></td>
                                            <td class="text-center">{{ @$dataOld->cities_identity['city_name'] }}</td>
                                            <td class="text-center">{{ @$content->cities_identity['city_name'] }}</td>
                                        </tr>

                                        <tr @if($dataOld->province_identity != $content->province_identity) class="table-warning" @endif>
                                            <td><strong>Provinsi</strong></td>
                                            <td class="text-center">{{ @$dataOld->provinces_identity['province_name'] }}</td>
                                            <td class="text-center">{{ @$content->provinces_identity['province_name'] }}</td>
                                        </tr>

                                        <tr @if($dataOld->zip_code_identity != $content->zip_code_identity) class="table-warning" @endif>
                                            <td><strong>Kode Pos</strong></td>
                                            <td class="text-center">{{ $dataOld->zip_code_identity }}</td>
                                            <td class="text-center">{{ $content->zip_code_identity }}</td>
                                        </tr>

                                        <tr @if($dataOld->marital_status != $content->marital_status) class="table-warning" @endif>
                                            <td><strong>Status Pernikahan</strong></td>
                                            <td class="text-center">{{ $dataOld->marital_status }}</td>
                                            <td class="text-center">{{ $content->marital_status }}</td>
                                        </tr>

                                        <tr @if($dataOld->recent_education != $content->recent_education) class="table-warning" @endif>
                                            <td><strong>Pendidikan Terakhir</strong></td>
                                            <td class="text-center">{{ $dataOld->recent_education }}</td>
                                            <td class="text-center">{{ $content->recent_education }}</td>
                                        </tr>

                                        <tr @if($dataOld->phone_number != $content->phone_number) class="table-warning" @endif>
                                            <td><strong>No. Telepon</strong></td>
                                            <td class="text-center">{{ $dataOld->phone_number }}</td>
                                            <td class="text-center">{{ $content->phone_number }}</td>
                                        </tr>

                                        <tr @if($dataOld->secondary_phone != $content->secondary_phone) class="table-warning" @endif>
                                            <td><strong>No. Telepon 2</strong></td>
                                            <td class="text-center">{{ $dataOld->secondary_phone }}</td>
                                            <td class="text-center">{{ $content->secondary_phone }}</td>
                                        </tr>

                                        <tr @if($dataOld->mother_name != $content->mother_name) class="table-warning" @endif>
                                            <td><strong>Nama Ibu</strong></td>
                                            <td class="text-center">{{ $dataOld->mother_name }}</td>
                                            <td class="text-center">{{ $content->mother_name }}</td>
                                        </tr>

                                        <tr @if($dataOld->account->email != $content->email) class="table-warning" @endif>
                                            <td><strong>Email</strong></td>
                                            <td class="text-center">{{ $dataOld->account->email }}</td>
                                            <td class="text-center">{{ $content->email }}</td>
                                        </tr>

                                        <tr @if($dataOld->npwp_number != $content->npwp_number) class="table-warning" @endif>
                                            <td><strong>No. NPWP</strong></td>
                                            <td class="text-center">{{ $dataOld->npwp_number }}</td>
                                            <td class="text-center">{{ $content->npwp_number }}</td>
                                        </tr>

                                        <tr @if($dataOld->npwp_image != $content->npwp_image) class="table-warning" @endif>
                                            <td></td>
                                            <td class="text-center">
                                                @if(!$dataOld->npwp_image)
                                                <a href="{{ asset('assets/images/group_50.png') }}" class="image-popup" title="Foto NPWP">
                                                    <img src="{{ asset('assets/images/group_50.png') }}" style="height: 150px; width: 250px;"  alt="npwp-image" class="rounded" >
                                                </a>
                                                @else
                                                <a href="{{ route('admin.employee.get.tmp-image', ['npwp', $dataOld->id, false]) }}" class="image-popup" title="Foto NPWP">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['npwp', $dataOld->id, false]) }}" style="height: 150px; width: 250px;"  alt="npwp-image" class="rounded" >
                                                </a>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.employee.get.tmp-image', ['npwp', $profile->id, false]) }}" class="image-popup" title="Foto NPWP">
                                                    <img src="{{ route('admin.employee.get.tmp-image', ['npwp', $profile->id]) }}" style="height: 150px; width: 250px;"  alt="npwp-image" class="rounded" >
                                                </a>
                                            </td>
                                        </tr>

                                        <tr @if($dataOld->domicile_address != $content->domicile_address) class="table-warning" @endif>
                                            <td><strong>Alamat Domisili</strong></td>
                                            <td class="text-center">{{ $dataOld->domicile_address }}</td>
                                            <td class="text-center">{{ $content->domicile_address }}</td>
                                        </tr>

                                        <tr @if($dataOld->domicile_city != $content->domicile_city) class="table-warning" @endif>
                                            <td><strong>Kota</strong></td>
                                            <td class="text-center">{{ @$dataOld->domicile_cities['city_name'] }}</td>
                                            <td class="text-center">{{ @$content->domicile_cities['city_name'] }}</td>
                                        </tr>

                                        <tr @if($dataOld->domicile_province != $content->domicile_province) class="table-warning" @endif>
                                            <td><strong>Provinsi</strong></td>
                                            <td class="text-center">{{ @$dataOld->domicile_provinces['province_name'] }}</td>
                                            <td class="text-center">{{ @$content->domicile_provinces['province_name'] }}</td>
                                        </tr>

                                        <tr @if($dataOld->domicile_zip_code != $content->domicile_zip_code) class="table-warning" @endif>
                                            <td><strong>Kode Pos</strong></td>
                                            <td class="text-center">{{ $dataOld->domicile_zip_code }}</td>
                                            <td class="text-center">{{ $content->domicile_zip_code }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end .pt2 -->
                    </div> <!-- end col-->
                </div>

            </div> <!-- end .padding -->
            <div class="text-right">
                <button type="button" class="btn btn-primary btn-lg waves-effect waves-light mt-2 mr-1"><i class="mdi mdi-chevron-left"></i> Kembali</button>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end row -->
    <!-- end page title -->
</div>
@endsection

@section('script')

@endsection
