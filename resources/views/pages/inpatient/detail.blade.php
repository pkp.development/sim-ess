@extends('app')

@section('page-title')
{!! page_title('Klaim Rawat Inap') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="card-box mt-3">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="mb-4">Detail Rawat Inap</h3>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>SIMID</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->account->sim_id }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Karyawan</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->account->profile->name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Dibuat Oleh</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->createdby->admin->name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nama Pasien</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->patient_name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Rumah Sakit</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->hospital_name }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Diagnosis</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->diagnosis }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nominal</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ number_format($result->nominal) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>

                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Status</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ ucwords(str_replace('_', ' ', $result->status)) }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tanggal Kwitansi</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->receipt_date->format('d F Y') }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tanggal Berkas Diterima</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ $result->received_date->format('d F Y') }}</td>
                            </tr>
                            @if ($result->status == 'dana_cair_ke_karyawan')
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Nominal Pencairan</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">{{ number_format($result->disbursment) }}</td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Tanggal Transfer</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">@if($result->transfer_date){{ $result->transfer_date->format('d F Y') }}@else{{ '-' }}@endif</td>
                            </tr>
                            @endif
                            <tr>
                                <td style="padding-bottom: 5px; padding-top: 5px;"><strong>Keterangan</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;" width="10px"><strong>:</strong></td>
                                <td style="padding-bottom: 5px; padding-top: 5px;">@if($result->description){{ $result->description }}@else{{ "-" }}@endif</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-3">
                <h3 class="mb-3">History Perubahan</h3>
                <div class="table-responsive">
                    <table id="inpatient-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Diupdate Oleh</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>

<script>
$(document).ready(function(){
    drawDatatable('#inpatient-datatable');
    var table = $("#inpatient-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('pe.inpatient.detail') }}/" + "{{ $result->uuid }}",
        },
        columns: [
            {data: 'updatedby.admin.name', name: 'updatedby.admin.name'},
            {data: 'tanggal', name: 'tanggal'},
            {data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[3, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });
});
</script>
@endsection
