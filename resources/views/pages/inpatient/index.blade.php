@extends('app')

@section('style')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-title')
{!! page_title('Rawat Inap') !!}
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Klaim Rawat Inap<span class="text-muted ml-2">History</span></h4>

                <div class="table-responsive">
                    <table id="inpatient-datatable" class="table table-striped w-100 mb-0 mt-3">
                        <thead>
                            <tr>
                                <th><strong>Tanggal</strong></th>
                                <th><strong>Nama Pasien</strong></th>
                                <th><strong>Rumah Sakit</strong></th>
                                <th><strong>Diagnosis</strong></th>
                                <th><strong>Nominal</strong></th>
                                <th><strong>Status</strong></th>
                                <th><strong>Nominal Cair</strong></th>
                                <th><strong>Aksi</strong></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div><!-- end row-->
    </div> <!-- end card-box -->
</div>
@endsection

@section('script')
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
$(document).ready(function(){
    drawDatatable('#inpatient-datatable');
    var table = $("#inpatient-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('pe.inpatient.index') }}",
        },
        columns: [
            {data: 'created_at', name: 'created_at', searchable: false},
            {data: 'patient_name', name: 'patient_name'},
            {data: 'hospital_name', name: 'hospital_name'},
            {data: 'diagnosis', name: 'diagnosis'},
            {data: 'nominal', name: 'nominal'},
            {data: 'status', name: 'status'},
            {data: 'disbursment', name: 'disbursment'},
            {data: 'action', name: 'action'},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[7, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });
});
</script>
@endsection
