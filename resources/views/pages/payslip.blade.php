<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {
            background: #f0f0f0;
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            padding: 20px;
            height: 100%;
        }

        @import url('https://fonts.googleapis.com/css?family=Roboto:200,300,400,600,700');

        * {
            font-family: 'Roboto', sans-serif;
            font-size: 12px;
            color: #444;
        }

        #payslip {
            width: calc( 8.5in - 80px );
            height: calc( 11in - 60px );
            background: #fff;
            padding: 30px 40px;
        }

        #title {
            margin-bottom: 20px;
            font-size: 38px;
            font-weight: 600;
        }

        #scope {
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 7px 0 4px 0;
            display: flex;
            justify-content: space-around;
        }

        #scope > .scope-entry {
            text-align: center;
        }

        #scope > .scope-entry > .value {
            font-size: 14px;
            font-weight: 700;
        }

        .content {
            display: flex;
            border-bottom: 1px solid #ccc;
            height: 900px;
        }

        /* .content .left-panel {
            border-right: 1px solid #ccc;
            min-width: 200px;
            padding: 20px 16px 0 0;
        } */

        .content .right-panel {
            width: 100%;
            padding: 5px;
        }

        #employee {
            text-align: center;
            margin-bottom: 20px;
        }
        #employee #name {
            font-size: 15px;
            font-weight: 700;
        }

        #employee #email {
            font-size: 11px;
            font-weight: 300;
        }

        .details, .contributions, .ytd, .gross {
            margin-bottom: 20px;
        }

        .details .entry, .contributions .entry, .ytd .entry {
            display: flex;
            justify-content: space-between;
            margin-bottom: 6px;
        }

        .details .entry .value, .contributions .entry .value, .ytd .entry .value {
            font-weight: 700;
            max-width: 1520px;
            text-align: right;
        }

        .gross .entry .value {
            font-weight: 700;
            text-align: right;
            font-size: 16px;
        }

        .contributions .title, .ytd .title, .gross .title {
            font-size: 15px;
            font-weight: 700;
            border-bottom: 1px solid #ccc;
            padding-bottom: 4px;
            margin-bottom: 6px;
        }

        .content .right-panel .details {
            width: 100%;
        }

        .content .right-panel .details .entry {
            display: flex;
            padding: 0 10px;
            margin: 6px 0;
        }

        .content .right-panel .details .label {
            font-weight: 700;
            width: 120px;
        }

        .content .right-panel .details .detail {
            font-weight: 600;
            width: 50px;
        }

        .content .right-panel .details .rate {
            font-weight: 400;
            width: 80px;
            font-style: italic;
            letter-spacing: 1px;
        }

        .content .right-panel .details .amount {
            text-align: right;
            font-weight: 700;
            width: 90px;
        }

        .content .right-panel .details .net_pay div, .content .right-panel .details .nti div {
            font-weight: 600;
            font-size: 12px;
        }

        .content .right-panel .details .net_pay, .content .right-panel .details .nti {
            padding: 3px 0 2px 0;
            margin-bottom: 10px;
            background: rgba(0, 0, 0, 0.04);
        }


        /* left panel custom */
        .content .left-panel {
            width: 100%;
            padding: 5px;
        }

        .content .left-panel .details {
            width: 100%;
        }

        .content .left-panel .details .entry {
            display: flex;
            padding: 0 10px;
            margin: 6px 0;
        }

        .content .left-panel .details .label {
            font-weight: 700;
            width: 120px;
        }

        .content .left-panel .details .detail {
            font-weight: 600;
            width: 20px;
            /* widthnya diubah */
        }

        .content .left-panel .details .rate {
            font-weight: 400;
            width: 80px;
            font-style: italic;
            letter-spacing: 1px;
        }

        .content .left-panel .details .amount {
            text-align: right;
            font-weight: 700;
            width: 90px;
        }

        .content .left-panel .details .net_pay div, .content .left-panel .details .nti div {
            font-weight: 600;
            font-size: 12px;
        }

        .content .left-panel .details .net_pay, .content .left-panel .details .nti {
            padding: 3px 0 2px 0;
            margin-bottom: 10px;
            background: rgba(0, 0, 0, 0.04);
        }

    </style>
</head>
<body>
    <div id="payslip">
        <div id="title" style="text-align: center;">
            <img src="http://sim-ess.site/assets/images/logo_kemenkeu.png" alt="" style="width: 100px; height: auto;">
            <p style="font-size: 12pt;">PT SIM</p>
        </div>
        <div id="scope">
            <div class="details">
                <div class="entry" style="width:300px;padding-top: 15px;">
                    <div class="label">SIM ID</div>
                    <div class="value">{{ $employee->account->sim_id }}</div>
                </div>
                <div class="entry" style="width:300px;padding-top: 10px;">
                    <div class="label">Nama Karyawan</div>
                    <div class="value">{{ $employee->name }}</div>
                </div>
                <div class="entry" style="width:300px;padding-top: 10px;">
                    <div class="label">Jabatan</div>
                    <div class="value">{{ $contract->job->job_name }}</div>
                </div>
            </div>
            <div class="details">
                <div class="entry" style="width:300px;padding-top: 15px;">
                    <div class="label">Lokasi Penempatan</div>
                    <div class="value">{{ $contract->company->organization_name }}</div>
                </div>
                <div class="entry" style="width:300px;padding-top: 10px;">
                    <div class="label">Tanggal Bergabung</div>
                    <div class="value">{{ $contract->join_date->format('d F Y') }}</div>
                </div>
                <div class="entry" style="width:300px;padding-top: 10px;">
                    <div class="label">Nomor Rekening</div>
                    <div class="value">{{ $bank->account_number }}</div>
                </div>
                <div class="entry" style="width:300px;padding-top: 10px;">
                    <div class="label">Nama Bank</div>
                    <div class="value">{{ $bank->api->bank_name }}</div>
                </div>
                <div class="entry" style="width:300px;padding-top: 10px;">
                    <div class="label">Tanggal Pembayaran</div>
                    <div class="value">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $payslip->payment_date)->format('d F Y') }}</div>
                </div>
            </div>
        </div>
        <div id="scope">
            @php
                $totalPendapatan = 0;
                $totalPengurangan = 0;
            @endphp
            <div class="scope-entry">
                <div class="value">Total Penerimaan Upah {{ $payslip->month_name }} {{ $payslip->year_periode }}</div>
                <div class="value" style="padding-top: 10px; font-size: 20pt;">Rp {{ number_format($totalPendapatan - $totalPengurangan) }},-</div>
            </div>
        </div>
        <div class="content">
            <div class="left-panel">
                <div class="details">
                    <div class="pendapatan">
                        <div class="nti">
                            <div class="entry">
                                <div class="label">PENDAPATAN</div>
                            </div>
                        </div>

                        @foreach($payslip->content as $content)
                            @if($content->account->type == 'pendapatan')
                                <div class="basic-pay">
                                    <div class="entry" style="padding-top: 10px;">
                                        <div class="label" style="width: 55%;">{{ $content->account->name }}</div>
                                        <div class="amount">Rp {{ number_format($content->nominal) }},-</div>
                                    </div>
                                </div>
                                @php
                                    $totalPendapatan = $totalPendapatan + $content->nominal;
                                @endphp
                            @endif
                        @endforeach

                        <div class="nti" style="margin-top: 10px;">
                            <div class="entry">
                                <div class="label">Total Pendapatan</div>
                                <div class="amount">Rp {{ number_format($totalPendapatan) }},-</div>
                            </div>
                        </div>
                        <br>

                    </div>
                </div>
            </div>
                <div class="right-panel">
                <div class="details">
                    <div class="pendapatan">
                        <div class="nti">
                            <div class="entry">
                                <div class="label">PENGELUARAN</div>
                            </div>
                        </div>

                        @foreach($payslip->content as $content)
                            @if($content->account->type == 'pengeluaran')
                                <div class="basic-pay">
                                    <div class="entry" style="padding-top: 10px;">
                                        <div class="label" style="width: 55%;">{{ $content->account->name }}</div>
                                        <div class="amount">Rp {{ number_format($content->nominal) }},-</div>
                                    </div>
                                </div>
                                @php
                                    $totalPengurangan = $totalPengurangan + $content->nominal;
                                @endphp
                            @endif
                        @endforeach

                        <div class="nti" style="margin-top: 10px;">
                            <div class="entry">
                                <div class="label">Total Pengeluaran</div>
                                <div class="amount">Rp {{ number_format($totalPengurangan) }},-</div>
                            </div>
                        </div>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
