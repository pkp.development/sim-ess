@extends('app')

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-title')
{!! page_title('Slip Gaji') !!}
@endsection

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Slip Gaji</h4>
                <hr>
                <div class="table-responsive mt-3">
                    <table id="payslip-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Periode Tahun</th>
                                <th>Periode Bulan</th>
                                <th>Download Link</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div> <!-- end card-box -->
        </div>
    </div> <!-- container -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#payslip-datatable');
        var table = $("#payslip-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('pe.info.pay-slip') }}",
            },
            columns: [
                {data: 'year_periode', name: 'year_periode'},
                {data: 'month_periode', name: 'month_periode', searchable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[3, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    });
</script>
@endsection
