@extends('app')

@section('page-title')
{!! page_title('Peraturan Perusahaan') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card mt-3">
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="peraturan-perusahaan-datatable" class="table table-striped w-100">
                            <thead>
                                <tr>
                                    <th>Judul</th>
                                    <th>Tanggal Berlaku</th>
                                    <th>Unduh</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
    $(document).ready(function (){
        drawDatatable('#peraturan-perusahaan-datatable');
        var table = $("#peraturan-perusahaan-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('pe.peraturan-perusahaan') }}",
            },
            columns: [
                {data: 'title', name: 'title'},
                {data: 'effective_date', name: 'effective_date'},
                {data: 'download', name: 'download', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[3, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    });
</script>
@endsection
