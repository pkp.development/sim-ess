@extends('app')

@section('style')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-title')
{!! page_title('Rawat Jalan') !!}
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Klaim Rawat Jalan<span class="text-muted ml-2">History</span></h4>
                <ul class="nav nav-tabs nav-justified nav-bordered navbar-information">
                    <li class="nav-item">
                        <a href="#ess" data-toggle="tab" class="nav-link active" aria-expanded="true">
                            Employee Self Service
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#sipas" data-toggle="tab" class="nav-link" aria-expanded="false">
                            Klaim Manual
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="ess">
                        <div class="table-responsive">
                            <table id="rawat-jalan-datatable" class="table table-striped w-100 mb-0 mt-3">
                                <thead>
                                    <tr>
                                        <th><strong>Tanggal</strong></th>
                                        <th><strong>Nama Pasien</strong></th>
                                        <th><strong>Nominal</strong></th>
                                        <th><strong>Status</strong></th>
                                        <th><strong>Aksi</strong></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="sipas">
                        <div class="table-responsive">
                            <table class="table table-striped w-100 mb-0 mt-3">
                                <thead>
                                    <tr>
                                        <th><strong>Tanggal Kwitansi</strong></th>
                                        <th><strong>Tanggal Terima</strong></th>
                                        <th><strong>Nominal</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($rawat_jalan as $data)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->note_date)->format('d F Y') }}</td>
                                        <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->receive_date)->format('d F Y') }}</td>
                                        <td>{{ number_format($data->amount) }}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="3" class="text-center">Data tidak ditemukan</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end row-->
    </div> <!-- end card-box -->

    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Klaim Rawat Jalan<span class="text-muted ml-2">Form</span></h4>

                <form action="{{ route('pe.rawat-jalan.store') }}" method="POST" id="form-rawat-jalan" enctype="multipart/form-data">
                    @csrf
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-12">
                            <div class="alert alert-info" role="alert">
                                <i class="mdi mdi-alert-circle-outline mr-2"></i>Khusus untuk jabatan yang mendapatkan plafon medikal.
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="widget-rounded-circle">
                                <div class="col-sm-12 bg-soft-primary rounded">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="avatar-lg">
                                                <i class="dripicons-wallet font-24 avatar-title text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-right">
                                                <h3 class="text-dark mt-1">Rp <span data-plugin="counterup">{{ number_format($sisa_plafond) }}</span></h3>
                                                <p class="text-primary mb-1 text-truncate">Sisa Plafond Anda</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end widget-rounded-circle-->
                        </div> <!-- end col-->
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="doctor_name">Nama Dokter</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="doctor_name" name="doctor_name" placeholder="Nama Dokter" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="patient_name">Nama Pasien</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="patient_name" name="patient_name" placeholder="Nama Pasien" autocomplete="off">
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="nominal">Nominal Klaim</label><span class="text-danger">*</span>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">Rp</span>
                                    </div>
                                    <input type="text" class="form-control currency-rupiah" id="nominal" name="nominal" placeholder="Nominal Klaim" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth" class="control-label">Tanggal Kwitansi</label><span class="text-danger">*</span>

                                <div class="input-group">
                                    <input type="text" class="form-control datetime-datepicker" id="date_of_birth" name="receipt_date" placeholder="Masukkan Tanggal Kwitansi" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Keterangan</label><span class="text-danger">*</span>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Keterangan" autocomplete="off">
                                <span class="help-block"><small>Isi dengan keterangan penyakit yand diderita dan nama rumah sakit atau klinik.</small></span>
                            </div>
                        </div>
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="upload">File Kwitansi</label><span class="text-danger">*</span>
                                <input type="file" class="filestyle" name="receipt_file" id="receipt_file">
                                <span class="help-block"><small>Tipe file yang diizinkan adalah jpg, png, jpeg.</small></span>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="upload">File Resep Dokter</label><span class="text-danger">*</span>
                                <input type="file" class="filestyle" name="doctor_prescription_file" id="doctor_prescription_file">
                                <span class="help-block"><small>Tipe file yang diizinkan adalah jpg, png, jpeg.</small></span>
                            </div>
                        </div> <!-- end col -->
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-lg btn-primary waves-effect waves-light">
                                <i class="mdi mdi-send "></i> Kirim
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- end row-->
    </div> <!-- end card-box -->
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content form-modal">
            <div class="modal-header">
                <h4 class="modal-title">Form Rawat Jalan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-12">
                                <a href="" class="image-popup a-receipt_file" title="File Kwitansi">
                                    <img src="" style="height: auto; width: 200px;"  alt="File Kwitansi" class="rounded img-receipt_file" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                                </a>

                                <h4 class="mb-0">File Kwitansi</h4>
                            </div>
                            <div class="col-12 pt-3">
                                <a href="" class="image-popup a-doctor_file" title="File Resep Dokter">
                                    <img src="" style="height: auto; width: 200px;"  alt="File Resep Dokter" class="rounded img-doctor_file" onerror="this.src = '{{ asset('assets/images/group_50.png') }}'">
                                </a>

                                <h4 class="mb-0">File Resep Dokter</h4>
                            </div>
                        </div> <!-- end row-->
                    </div>

                    <div class="col-sm-8">
                        <div class="table-responsive">
                            <table class="table table-borderless mb-0">
                                <tr style="">
                                    <td style="width: 35%; padding: .5rem"><strong>Nama Pasien</strong></td>
                                    <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                                    <td style="padding: .5rem" class="td-patient"></td>
                                </tr>
                                <tr style="">
                                    <td style="width: 35%; padding: .5rem"><strong>Nama Dokter</strong></td>
                                    <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                                    <td style="padding: .5rem" class="td-doctor"></td>
                                </tr>
                                <tr style="">
                                    <td style="width: 35%; padding: .5rem"><strong>Deskripsi</strong></td>
                                    <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                                    <td style="padding: .5rem" class="td-description"></td>
                                </tr>
                                <tr style="">
                                    <td style="width: 35%; padding: .5rem"><strong>Tanggal Kwitansi</strong></td>
                                    <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                                    <td style="padding: .5rem" class="td-receipt_date"></td>
                                </tr>
                                <tr style="">
                                    <td style="width: 35%; padding: .5rem"><strong>Nominal</strong></td>
                                    <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                                    <td style="padding: .5rem" class="td-nominal"></td>
                                </tr>
                                <tr style="">
                                    <td style="width: 35%; padding: .5rem"><strong>Alasan Ditolak</strong></td>
                                    <td style="width: 5px; padding: .5rem"><strong>:</strong></td>
                                    <td style="padding: .5rem" class="td-reason"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endsection

@section('script')
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mask-plugin/jquery.mask.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
$(document).ready(function(){
    drawDatatable('#rawat-jalan-datatable');
    var table = $("#rawat-jalan-datatable").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('pe.rawat-jalan.index') }}",
        },
        columns: [
            {data: 'created_at', name: 'created_at', searchable: false},
            {data: 'patient_name', name: 'patient_name'},
            {data: 'nominal', name: 'nominal'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
            {data: 'created_at', name: 'created_at', searchable: false, visible: false}
        ],
        order: [[5, 'desc']],
        'searchDelay': 2000,
        scrollX: !0,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        }
    });

    $("#rawat-jalan-datatable").delegate('.btn-detail', 'click', function(e){
        $.get($(this).data('url'), {}, function(data){

            console.log(data);

            $('.td-patient').html(data.patient_name);
            $('.td-doctor').html(data.doctor_name);
            $('.td-description').html(data.description);
            $('.td-receipt_date').html(data.date);
            $('.td-nominal').html(data.nominal);
            $('.td-reason').html(data.rejection_reason);

            $('.a-receipt_file').attr('href', data.receipt_file);
            $('.a-doctor_file').attr('href', data.doctor_prescription_file);

            $('.img-receipt_file').attr('src', data.receipt_file);
            $('.img-doctor_file').attr('src', data.doctor_prescription_file);

            $('#con-close-modal').modal('show');

        }).fail(function(){
            $.toast().reset('all');
            $.toast({
                heading: "Oops!",
                text: "Data tidak ditemukan",
                position: 'top-right',
                loaderBg: '#bf441d',
                icon: 'error',
                hideAfter: 5000,
                stack: 1
            });
        });;
    });

    var flatPicker = $('.datetime-datepicker').flatpickr({
        altInput: true,
        altFormat: "d F Y",
        dateFormat: "Y-m-d",
        // appendTo: window.document.querySelector(".clockpicker")
        static: true
    });

    $(".currency-rupiah").maskMoney({precision:0});
    $(".currency-rupiah").maskMoney('mask');
});
</script>
@endsection
