<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    </head>
<body style="font-family:arial; font-size:11pt; margin:0; max-height:33cm; max-width:860pt; padding:0">
    <div class="__container" style="height:33cm; width:860pt" height="33cm" width="860pt">
        <div class="__header-bounds">
            <div class="__staples-area">
                <p style="color:#A5A5A5; line-height:30pt; margin:0; padding-left:11pt">a r e a    s t a p l e s</p>
                <hr style="border-top:1px dotted #000; margin:0">
            </div>
            <div class="__header-content" style="padding-left:11pt; padding-right:11pt">
                <div class="header-attr">
                    <div class="left-header-attr" style="background-color:#000; float:left; height:14px; width:28px" bgcolor="#000000" height="14" width="28"></div>
                    <div class="right-header-attr" style="background-color:#000; float:right; height:14px; width:28px" bgcolor="#000000" height="14" width="28"></div>
                    <div class="clearfix" style="clear:both"></div>
                </div>
                <div class="hc-left" style="border-right:2px solid #000; box-sizing:border-box; float:left; width:30%" width="30%">
                    <div class="hc-left-content">
                        <img src="{{ $localIp . 'assets/images/logo_kemenkeu.png' }}" alt="logo" style="margin:0 auto; padding-left: 100px; padding-top:20px; width:100pt" width="100pt">
                        <p id="logo-name" style="font-size:10pt; font-weight:bold; line-height:12pt; margin:20px 0; text-align:center" align="center">KEMENTERIAN KEUANGAN RI <br> DIREKTORAT JENDERAL PAJAK</p>
                    </div>
                </div>
                <div class="hc-center" style="float:left; width:40%" width="40%">
                    <p id="hcc-title" style="font-size:10pt; font-weight:bold; line-height:12pt; margin:30px 50px 60px 30px; text-align:center" align="center">BUKTI PEMOTONGAN PAJAK PENGHASILAN
                        <br> PASAL 21 BAGI PEGAWAI TETAP ATAU
                        <br> PENERIMA PENSIUN ATAU TUNJANGAN HARI
                        TUA/JAMINAN HARI TUA BERKALA</p>
                    <div class="hcc-nomor">
                        <div class="hcc-nomor-bounds" style="border-right:2px solid #000; border-top:2px solid #000; box-sizing:border-box; padding:14px; width:580px" width="580">
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:20px">NOMOR :</p>
                            <p id="hccn-caption" style="color:#A5A5A5; float:left; font-size:7pt; padding-left:40px; padding-top:5px">H.01</p>
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:20px">1   .</p>
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:20px">1     -</p>
                            <p id="hccn-title" class="__border" style="border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; float:left; font-size:9pt; font-weight:bold; padding-left:20px" width="20">12</p>
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:10px">-</p>
                            <p id="hccn-title" class="__border" style="border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; float:left; font-size:9pt; font-weight:bold; padding-left:20px" width="20">{{ $spt->year }}</p>
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:10px">-</p>
                            <p id="hccn-title" class="__border __long" style="border-bottom:1px solid black; margin-left:10px; padding-right:90px; width:20px; float:left; font-size:9pt; font-weight:bold; padding-left:20px;" width="20">{{ $spt->content->no }}</p>
                            <div class="clearfix" style="clear:both"></div>
                        </div>

                    </div>
                </div>
                <div class="hc-right" style="height:154px; border-left:2px solid #000; box-sizing:border-box; float:left; padding-bottom:25px; padding-left:10px; width:30%" height="150" width="30%">
                    <div class="attr-dot" style="margin-top:10px;float: right;">
                        <div class="__ad ad-fill" style="height:14px; float: right; margin-left:10px; width:14px; border:1px solid #000; box-sizing:border-box" height="14" width="14"></div>
                        <div class="__ad ad-line" style="height:14px; float: right; margin-left:10px; width:14px; background-color:#000" height="14" width="14" bgcolor="#000000"></div>
                        <div class="__ad ad-fill" style="height:14px; float: right; margin-left:10px; width:14px; border:1px solid #000; box-sizing:border-box" height="14" width="14"></div>
                        <div class="__ad ad-line" style="height:14px; float: right; margin-left:10px; width:14px; background-color:#000" height="14" width="14" bgcolor="#000000"></div>
                    </div>
                    <div class="cleardot" style="clear:right"></div>
                    <p id="hcr-formulir" style="font-size:13pt; font-weight:bold; text-align:right" align="right">FORMULIR 1721 - A1</p>
                    <p id="hrc-lembar" style="font-size:8pt; text-align:left" align="left">Lembar ke-1 : untuk Penerima Penghasilan</p>
                    <p id="hrc-lembar" style="font-size:8pt; text-align:left" align="left">Lembar ke-2 : untuk Pemotong</p>
                    <div class="hcr-penghasilan" style="position:relative">
                        <p class="hrcp-title" style="font-size:8pt; font-weight:bold; position:absolute; right:0; text-align:center" align="center">MASA PEROLEHAN <br> PENGHASILAN [mm - mm]</p>
                        <p id="hrcp-caption" style="color:#A5A5A5; font-size:7pt; padding-top:5px; position:absolute; right:160px; top:40px">H.02</p>
                        <p id="hrcp-title" class="__border " style="margin-top: 66px;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; float:right; font-size:9pt; font-weight:bold; padding-left:20px" width="20">{{ $spt->content->perolehan_2 }}</p>
                        <p id="hrcp-title" style="float:right; font-size:9pt; font-weight:bold; margin-top:50px; padding-left:10px">-</p>
                        <p id="hrcp-title" class="__border" style="margin-top: 66px;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; float:right; font-size:9pt; font-weight:bold; padding-left:20px" width="20">{{ $spt->content->perolehan_1 }}</p>
                        <div class="clearfix" style="clear:both"></div>
                    </div>
                </div>
                <div class="clearfix" style="clear:both"></div>
                <div class="pemotong" style="border:2px solid #000; box-sizing:border-box; height:120px; padding:0 20px; width:100%" height="120" width="100%">
                    <div class="pm-npwp">
                        @php
                            $compNpwpExp = explode('-', $company->npwp);
                            $compNpwpBck = explode('.', $compNpwpExp[1]);
                        @endphp
                        <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">NPWP <br> PEMOTONG</p>
                        <span style="font-size:9pt">:</span>
                        <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">H.03</p>
                        <p id="pmn-title" class="__border __long" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:250px; width:20px; display:inline-block" width="20">
                            {{ $compNpwpExp[0] }}
                        </p>
                        <p id="pmn-title" style="padding-left: 10px;display: inline-block;">-</p>
                        <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:50px; width:20px; display:inline-block" width="20">
                            {{ $compNpwpBck[0] }}
                        </p>
                        <p id="pmn-title" style="padding-left: 10px;display: inline-block;">.</p>
                        <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:50px; width:20px; display:inline-block" width="20">
                            {{ $compNpwpBck[1] }}
                        </p>
                    </div>

                    <div class="pm-npwp">
                        <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">NAMA <br> PEMOTONG</p>
                        <span style="font-size:9pt">:</span>
                        <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">H.04</p>
                        <p id="pmn-title" class="__border __longl" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:250px; width:620px; display:inline-block" width="620">{{ $company->name }}</p>
                    </div>
                </div>

                <div class="identitas" style="height:auto; width:100%" height="auto" width="100%">
                    <p class="identitas-title" style="font-size:10pt; font-weight:bold">A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</p>
                    <div class="id-content" style="border:2px solid #000; box-sizing:border-box; height:260px; padding:10px 20px; width:100%" height="260" width="100%">
                        <div style="display: inline-block;">
                            <div class="pm-npwp">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">1. NPWP</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.01</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:200px; width:20px; display:inline-block" width="20">
                                    @if($npwpExp){{ $npwpExp[0] }}@endif
                                </p>
                                <p id="pmn-title" style="padding-left: 10px;display: inline-block;">-</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:50px; width:20px; display:inline-block" width="20">
                                    @if($npwpExp){{ $npwpBck[0] }}@endif
                                </p>
                                <p id="pmn-title" style="padding-left: 10px;display: inline-block;">.</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:50px; width:20px; display:inline-block" width="20">
                                    @if($npwpExp){{ $npwpBck[1] }}@endif
                                </p>
                            </div>
                            <div class="pm-npwp">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">2. NIK/NO. <br>  &nbsp;&nbsp;&nbsp;&nbsp;PASPOR</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.02</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;margin-left:10px; padding-right:50px; width:320px; display:inline-block" width="320">
                                    @if($profile->nik){{ $profile->nik }}@endif
                                </p>
                            </div>
                            <div class="pm-npwp">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">3. NAMA</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.03</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;margin-left:10px; padding-right:50px; width:320px; display:inline-block" width="320">
                                    {{ $profile->name }}
                                </p>
                            </div>
                            <div class="pm-npwp">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">4. ALAMAT</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.04</p>
                                <p id="pmn-title" class="__border" style="font-size: 8pt;margin-bottom: 0px;margin-left:10px; padding-right:0; width:420px; display:inline-block" width="420">
                                    @if($profile->address_identity){{ $profile->address_identity }}@endif
                                </p>
                            </div>
                            <div class="pm-npwp __rr">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">5. JENIS KELAMIN</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.05</p>
                                <div class="box-field" style="border:1px solid #000; box-sizing:border-box; display:inline-block; height:20px; margin-left:10px; text-align:center; width:40px" height="20" align="center" width="40">@if($profile->gender)@if($profile->gender == 'LAKI-LAKI'){{ 'X' }}@else{{ '-' }}@endif @endif</div>
                                <span style="font-size:9pt">LAKI-LAKI</span>

                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.06</p>
                                <div class="box-field" style="border:1px solid #000; box-sizing:border-box; display:inline-block; height:20px; margin-left:10px; text-align:center; width:40px" height="20" align="center" width="40">@if($profile->gender)@if($profile->gender == 'PEREMPUAN'){{ 'X' }}@else{{ '-' }}@endif @endif</div>
                                <span style="font-size:9pt">PEREMPUAN</span>
                            </div>
                        </div>

                        <div class="idc-right" style="display:inline-block; margin-left:20px; top:-100px; width:420px" width="420">
                            <div class="pm-npwp">
                                <p class="title __title-long" style="display:inline-block; font-size:9pt; width:100%" width="100%">6. STATUS /JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP</p>
                                <span style="font-size:9pt; padding-left:30px">K /</span>
                                <p id="pmn-title" class="__border" style="margin-bottom: -10px;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; display:inline-block" width="20">
                                    @if(is_numeric($spt->content->tax_marital_k)){{ $spt->content->tax_marital_k }}@else{{ "-" }}@endif
                                </p>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:5px; padding-top:5px; padding-right:15px">A.07</p>

                                <span style="font-size:9pt">TK /</span>
                                <p id="pmn-title" class="__border" style="margin-bottom: -10px;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; display:inline-block" width="20">
                                    @if(is_numeric($spt->content->tax_marital_tk)){{ $spt->content->tax_marital_tk }}@else{{ "-" }}@endif
                                </p>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:5px; padding-top:5px; padding-right:15px">A.08</p>

                                <span style="font-size:9pt">HB /</span>
                                <p id="pmn-title" class="__border" style="margin-bottom: -10px;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; display:inline-block" width="20">
                                    -
                                </p>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:5px; padding-top:5px; padding-right:15px">A.09</p>
                            </div>

                            <div class="pm-npwp">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">7. NAMA JABATAN :</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.10</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; font-size:7pt; margin-left:10px; padding-right:0; width:220px; display:inline-block" width="220">
                                    {{ $spt->content->job_name }}
                                </p>
                            </div>

                            <div class="pm-npwp __rr">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">8. KARYAWAN <br> &nbsp;&nbsp;&nbsp;&nbsp;ASING</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.05</p>
                                <div class="box-field" style="border:1px solid #000; box-sizing:border-box; display:inline-block; height:20px; margin-left:10px; text-align:center; width:40px" height="20" align="center" width="40">-</div>
                                <span style="font-size:9pt">YA</span>
                            </div>

                            <div class="pm-npwp">
                                <p class="title" style="display:inline-block; font-size:9pt; width:130px" width="130">9. KODE NEGARA <br> &nbsp;&nbsp;&nbsp;&nbsp;DOMISILI</p>
                                <span style="font-size:9pt">:</span>
                                <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">A.12</p>
                                <p id="pmn-title" class="__border" style="margin-bottom: 0px;border-bottom:1px solid black; margin-left:10px; padding-right:0; width:220px; display:inline-block; text-align:center" width="220">-</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="rincian">
                    <p class="identitas-title" style="font-size:10pt; font-weight:bold">B. RINCIAN  PENGHASILAN DAN PENGHITUNGAN PPh PASAL 21</p>
                    <table style="border:1px solid black; border-collapse:collapse; width:100%" width="100%">
                        <tr>
                            <th colspan="2" width="72%" style="border:1px solid black; font-size:9pt; font-weight:bold; padding:10px 0">URAIAN</th>
                            <th width="25%" style="border:1px solid black; font-size:9pt; font-weight:bold; padding:10px 0">JUMLAH (Rp)</th>
                        </tr>
                        <tr>
                            <td colspan="2" style="border:1px solid black">
                                <div class="pm-npwp">
                                    <p class="title" style="display:inline-block; font-size:9pt; width:150px; font-weight:bold; padding-left:20px" width="150">KODE OBJEK PAJAK :</p>
                                    <div class="box-field" style="border:1px solid #000; box-sizing:border-box; display:inline-block; height:20px; margin-left:10px; text-align:center; width:40px" height="20" align="center" width="40">X</div>
                                    <span style="font-size:9pt">21-100-01</span>

                                    <div class="box-field" style="border:1px solid #000; box-sizing:border-box; display:inline-block; height:20px; margin-left:10px; text-align:center; width:40px" height="20" align="center" width="40">-</div>
                                    <span style="font-size:9pt">21-100-02</span>
                                </div>
                            </td>
                            <td style="border:1px solid black; background-color:#D9D9D9" bgcolor="#D9D9D9"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border:1px solid black">
                                <div class="pm-npwp">
                                    <p class="title" style="display:inline-block; font-size:9pt; width:250px; font-weight:bold; padding-left:20px" width="250">  PENGHASILAN BRUTO :</p>
                                </div>
                            </td>
                            <td style="border:1px solid black; background-color:#D9D9D9" bgcolor="#D9D9D9"></td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">1.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">GAJI/PENSIUN ATAU THT/JHT</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"1_gaji_pokok"})) ? number_format($spt->content->{"1_gaji_pokok"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">2.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">TUNJANGAN PPh</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"2_tunjangan_pajak"})) ? number_format($spt->content->{"2_tunjangan_pajak"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">3.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">TUNJANGAN LAINNYA, UANG LEMBUR DAN SEBAGAINYA</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"3_tunjangan_lainnya"})) ? number_format($spt->content->{"3_tunjangan_lainnya"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">4.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">HONORARIUM DAN IMBALAN LAIN SEJENISNYA</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"4_honorarium"})) ? number_format($spt->content->{"4_honorarium"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">5.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PREMI ASURANSI YANG DIBAYAR PEMBERI KERJA</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"5_asuransi"})) ? number_format($spt->content->{"5_asuransi"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">6.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PENERIMAAN DALAM BENTUK NATURA DAN KENIKMATAN LAINNYA YANG DIKENAKAN PEMOTONGAN PPh PASAL 21</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"6_natura"})) ? number_format($spt->content->{"6_natura"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">7.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">TANTIEM, BONUS, GRATIFIKASI, JASA PRODUKSI DAN THR</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"7_tantiem"})) ? number_format($spt->content->{"7_tantiem"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">8.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">JUMLAH PENGHASILAN BRUTO (1 S.D.7)</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"8_jmlh_penghasilan_bruto"})) ? number_format($spt->content->{"8_jmlh_penghasilan_bruto"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border:1px solid black">
                                <div class="pm-npwp">
                                    <p class="title" style="display:inline-block; font-size:9pt; width:150px; font-weight:bold; padding-left:20px" width="150"> PENGURANGAN :</p>
                                </div>
                            </td>
                            <td style="border:1px solid black; background-color:#D9D9D9" bgcolor="#D9D9D9"></td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">9.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">BIAYA JABATAN/ BIAYA PENSIUN</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"9_biaya_jabatan"})) ? number_format($spt->content->{"9_biaya_jabatan"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">10.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">IURAN PENSIUN ATAU IURAN THT/JHT</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"10_iuran_pensiun"})) ? number_format($spt->content->{"10_iuran_pensiun"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">11.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">JUMLAH PENGURANGAN (9 S.D 10)</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"11_jmlh_pengurangan"})) ? number_format($spt->content->{"11_jmlh_pengurangan"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border:1px solid black">
                                <div class="pm-npwp">
                                    <p class="title" style="display:inline-block; font-size:9pt; width:250px; font-weight:bold; padding-left:20px" width="250"> PENGHITUNGAN PPh PASAL 21 :</p>
                                </div>
                            </td>
                            <td style="border:1px solid black; background-color:#D9D9D9" bgcolor="#D9D9D9"></td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">12.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">JUMLAH PENGHASILAN NETO (8-11)</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"12_jmlh_netto"})) ? number_format($spt->content->{"12_jmlh_netto"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">13.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PENGHASILAN NETO MASA SEBELUMNYA</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"13_jmlh_netto_sblmnya"})) ? number_format($spt->content->{"13_jmlh_netto_sblmnya"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">14.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">JUMLAH PENGHASILAN NETO UNTUK PENGHITUNGAN PPh PASAL 21 (SETAHUN/DISETAHUNKAN)</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"14_jmlh_netto_pph"})) ? number_format($spt->content->{"14_jmlh_netto_pph"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="clear:both;border:1px solid black; font-size:8pt; padding:10px">15.</td>
                            <td width="72%" class="__colcontent" style="clear:both;border:1px solid black; font-size:8pt; padding:10px">PENGHASILAN TIDAK KENA PAJAK (PTKP)</td>
                            <td width="25%" class="__colcontent" style="clear:both;border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"15_ptkp"})) ? number_format($spt->content->{"15_ptkp"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">16.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN (14 - 15)</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"16_pkp"})) ? number_format($spt->content->{"16_pkp"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">17.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PPh PASAL 21 ATAS PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"17_pkp_pph"})) ? number_format($spt->content->{"17_pkp_pph"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">18.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PPh PASAL 21 YANG TELAH DIPOTONG MASA SEBELUMNYA</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"18_pph_sblmnya"})) ? number_format($spt->content->{"18_pph_sblmnya"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">19.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PPh PASAL 21 TERUTANG</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"19_pph_terutang"})) ? number_format($spt->content->{"19_pph_terutang"}) : "-" }}
                            </td>
                        </tr>
                        <tr>
                            <td width="3%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">20.</td>
                            <td width="72%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px">PPh PASAL 21 DAN PPh PASAL 26 YANG TELAH DIPOTONG DAN DILUNASI</td>
                            <td width="25%" class="__colcontent" style="border:1px solid black; font-size:8pt; padding:10px;text-align:right">
                                {{ (is_numeric($spt->content->{"20_pph_dilunasi"})) ? number_format($spt->content->{"20_pph_dilunasi"}) : "-" }}
                            </td>
                        </tr>
                    </table>
                </div>
                <p class="identitas-title" style="font-size:10pt; font-weight:bold">C. IDENTITAS PEMOTONG</p>
                <div class="pemotong" style="border:2px solid #000; box-sizing:border-box; height:120px; padding:0 20px; width:100%;position: relative;" height="120" width="100%">
                    <div class="__ssleft" style="display:inline-grid">
                        <div class="pm-npwp">
                            @php
                                $dirNpwpExp = explode('-', $company->coordinator_npwp);
                                $dirNpwpBck = explode('.', $dirNpwpExp[1]);
                            @endphp
                            <p class="title" style="display:inline-block; font-size:9pt; width:70px" width="70">1. NPWP</p>
                            <span style="font-size:9pt">:</span>
                            <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">C.01</p>
                            <p id="pmn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:200px; width:20px; display:inline-block" width="20">{{ $dirNpwpExp[0] }}</p>
                            <p id="pmn-title" style="padding-left: 10px;display: inline-block;">-</p>
                            <p id="pmn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:50px; width:20px; display:inline-block" width="20">{{ $dirNpwpBck [0]}}</p>
                            <p id="pmn-title" style="padding-left: 10px;display: inline-block;">.</p>
                            <p id="pmn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:50px; width:20px; display:inline-block" width="20">{{ $dirNpwpBck[1] }}</p>
                        </div>

                        <div class="pm-npwp">
                            <p class="title" style="display:inline-block; font-size:9pt; width:70px" width="70">2. NAMA</p>
                            <span style="font-size:9pt">:</span>
                            <p id="pmn-caption" style="color:#A5A5A5; display:inline-block; font-size:7pt; padding-left:20px; padding-top:5px">C.02</p>
                            <p id="pmn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:0; width:300px; display:inline-block" width="300">{{ $company->coordinator }}</p>
                        </div>
                    </div>
                    <div class="__ssright" style="top: 10px;margin-right: 190px;position: absolute;right: 0;">
                        <div class="pm-npwp">
                            <p class="title __title-long" style="display:inline-block; font-size:9pt; width:200px" width="200">3. TANGGAL &amp; TANDA TANGAN</p>
                        </div>

                        <div>
                            <p id="hccn-caption" style="color:#A5A5A5; float:left; font-size:7pt; padding-left:0; padding-top:5px">C.03</p>
                            <p id="hccn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; float:left; font-size:9pt; font-weight:bold; padding-left:20px" width="20">31</p>
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:10px">-</p>
                            <p id="hccn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:20px; float:left; font-size:9pt; font-weight:bold; padding-left:20px" width="20">12</p>
                            <p id="hccn-title" style="float:left; font-size:9pt; font-weight:bold; padding-left:10px">-</p>
                            <p id="hccn-title" class="__border" style="margin-bottom: 0;border-bottom:1px solid black; margin-left:10px; padding-right:20px; width:50px; float:left; font-size:9pt; font-weight:bold; padding-left:20px" width="50">{{ $spt->year }}</p>
                            <div class="clearfix" style="clear:both"></div>
                            <p class="__date" style="display:table; font-size:8pt; font-weight:bold; margin:0; padding-left:55px">[dd - mm - yyyy]</p>
                        </div>
                    </div>
                    <div class="__ssclear" style="border:1px solid #000;height:90px;margin-right: 12px;margin-top:12px;width:160px;position: absolute;right: 0;top: 0; text-align: center" height="90" width="160">
                        <img src="{{ $localIp . 'storage/' . $company->coordinator_sign }}" alt="logo" style="margin:0 auto; max-width:150px; max-height:85px; padding: 5px;" height="85px">
					</div>
                    <div class="_ssclearfix" style="clear:right"></div>
                </div>
                <div class="header-attr" style="margin-top: 30px;">
                    <div class="left-header-attr" style="background-color:#000; float:left; height:14px; width:28px" bgcolor="#000000" height="14" width="28"></div>
                    <div class="right-header-attr" style="background-color:#000; float:right; height:14px; width:28px" bgcolor="#000000" height="14" width="28"></div>
                    <div class="clearfix" style="clear:both"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
