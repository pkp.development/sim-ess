@extends('app')

@section('page-title')
{!! page_title('Laporan SPT') !!}
@endsection

@section('style')
<!-- third party css -->
<link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="card-box mt-2">
                <h4>Laporan SPT</h4>
                <hr>
                <div class="button-list">
                    <span class="p-2">Download Form 1721 : </span>
                    <a href="{{ route('pe.download.spt-template') }}" target="_blank" class="btn btn-primary waves-effect waves-light mt-2"><i class="fas fa-file-download mr-2"></i> Download</a>
                </div>
                <hr>
                <h4>Bukti Potongan SPT Tahunan</h4>
                <div class="table-responsive mt-3">
                    <table id="spt-datatable" class="table table-striped w-100">
                        <thead>
                            <tr>
                                <th>Tahun</th>
                                <th>File</th>
                                <th>Download Link</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <hr>

            </div> <!-- end card-box -->
        </div>
    </div> <!-- container -->
</div>
@endsection

@section('script')
<!-- third party js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
    $(document).ready(function(){
        drawDatatable('#spt-datatable');
        var table = $("#spt-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('pe.info.spt') }}",
            },
            columns: [
                {data: 'year', name: 'year'},
                {data: 'file', name: 'file'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'created_at', name: 'created_at', searchable: false, visible: false}
            ],
            order: [[3, 'desc']],
            'searchDelay': 2000,
            scrollX: !0,
            language: {
                paginate: {
                    previous: "<i class='mdi mdi-chevron-left'>",
                    next: "<i class='mdi mdi-chevron-right'>"
                }
            },
            drawCallback: function() {
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
        });
    });
</script>
@endsection
