<script>
    @if(Session::has('toast-error'))
        $.toast().reset('all');
        $.toast({
            heading: "Oops!",
            text: "{{ Session::get('toast-error') }}",
            position: 'top-right',
            loaderBg: '#bf441d',
            icon: 'error',
            hideAfter: 5000,
            stack: 1
        });
    @elseif(Session::has('toast-success'))
        $.toast().reset('all');
        $.toast({
            text: "{{ Session::get('toast-success') }}",
            position: 'top-right',
            loaderBg: '#1ABC9C',
            icon: 'success',
            hideAfter: 5000,
            stack: 1
        });
    @endif
</script>
