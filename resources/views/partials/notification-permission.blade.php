<li class="dropdown notification-list">
    <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
        <i class="fe-bell noti-icon"></i>
        <span class="badge badge-danger rounded-circle noti-icon-badge">{{ $counts_notif }}</span>
    </a>

    <div class="dropdown-menu dropdown-menu-right dropdown-lg">
        <!-- item-->
        <div class="dropdown-item noti-title">
            <h5 class="m-0">
                Notifikasi
            </h5>
        </div>

        <div class="slimscroll noti-scroll">
            @foreach($notifications as $notif)
            <!-- item-->
            @if($notif->type == 'profile' && auth()->user()->can('notif-profile'))
            <a href="{{ route('admin.employee.update.view', $notif->uuid) }}" class="dropdown-item notify-item">
                <div class="notify-icon bg-primary">
                    <i class="fe-users"></i>
                </div>
                <p class="notify-details">{{ ucwords(strtolower($notif->account->profile->name)) }} | {{ $notif->account->assignment->npo }}</p>
                <p class="text-muted mb-0 user-msg">
                    <small>Telah melakukan perubahan biodata</small>
                </p>
            </a>
            @elseif($notif->type == 'family' && auth()->user()->can('notif-family'))
            <a href="{{ route('admin.employee.update.family.view', $notif->uuid) }}" class="dropdown-item notify-item">
                <div class="notify-icon bg-danger">
                    <i class="fe-users"></i>
                </div>
                <p class="notify-details">{{ ucwords(strtolower($notif->account->profile->name)) }} | {{ $notif->account->assignment->npo }}</p>
                <p class="text-muted mb-0 user-msg">
                    <small>Telah melakukan perubahan data keluarga</small>
                </p>
            </a>
            @elseif($notif->type == 'bank' && auth()->user()->can('notif-bank'))
            <a href="{{ route('admin.employee.update.rekening.view', $notif->uuid) }}" class="dropdown-item notify-item">
                <div class="notify-icon bg-success">
                    <i class="mdi mdi-bank"></i>
                </div>
                <p class="notify-details">{{ ucwords(strtolower($notif->account->profile->name)) }} | {{ $notif->account->assignment->npo }}</p>
                <p class="text-muted mb-0 user-msg">
                    <small>Telah melakukan perubahan data rekening</small>
                </p>
            </a>
            @elseif($notif->type == 'families' && auth()->user()->can('notif-families'))
            <a href="{{ route('admin.employee.daftar.family', $notif->uuid) }}" class="dropdown-item notify-item">
                <div class="notify-icon bg-danger">
                    <i class="fe-users"></i>
                </div>
                <p class="notify-details">{{ ucwords(strtolower($notif->account->profile->name)) }} | {{ $notif->account->assignment->npo }}</p>
                <p class="text-muted mb-0 user-msg">
                    @if($notif->action == 'create')
                    <small>Telah menambahkan daftar keluarga</small>
                    @elseif($notif->action == 'update')
                    <small>Telah mengubah daftar keluarga</small>
                    @elseif($notif->action == 'delete')
                    <small>Telah menghapus daftar keluarga</small>
                    @endif
                </p>
            </a>
            @elseif($notif->type == 'outpatient' && auth()->user()->can('notif-outpatient'))
            <a href="{{ route('admin.outpatient.verification', $notif->uuid) }}" class="dropdown-item notify-item">
                <div class="notify-icon bg-success">
                    <i class="mdi mdi-hospital"></i>
                </div>
                <p class="notify-details">{{ ucwords(strtolower($notif->account->profile->name)) }} | {{ $notif->account->assignment->npo }}</p>
                <p class="text-muted mb-0 user-msg">
                    <small>Telah melakukan klaim rawat jalan</small>
                </p>
            </a>
            @elseif($notif->type == 'contract' && auth()->user()->can('notif-contract'))
            <a href="{{ route('admin.contract.verification', $notif->account->uuid) }}" class="dropdown-item notify-item">
                <div class="notify-icon bg-success">
                    <i class="mdi mdi-file-check"></i>
                </div>
                <p class="notify-details">{{ ucwords(strtolower($notif->account->profile->name)) }} | {{ $notif->account->assignment->npo }}</p>
                <p class="text-muted mb-0 user-msg">
                    <small>Telah menandatangani kontrak</small>
                </p>
            </a>
            @endif
            @endforeach
        </div>

        <!-- All-->
        <a href="{{ route('admin.notification') }}" class="dropdown-item text-center text-primary notify-item notify-all">
            Lihat Semua Notifikasi
            <i class="fi-arrow-right"></i>
        </a>
    </div>
</li>
