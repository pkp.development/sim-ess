<!-- Vendor js -->
<script src="{{ asset('assets/js/vendor.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>

<!-- JQuery Toast -->
<script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

<!-- Tippy js-->
<script src="{{ asset('assets/libs/tippy-js/tippy.all.min.js') }}"></script>

<!-- Sweet Alerts js -->
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- Magnific Popup-->
<script src="{{ asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

<!-- Bootstrap Filestyle -->
<script src="{{ asset('assets/libs/bootstrap-filestyle/bootstrap-filestyle.min.js') }}"></script>

<!-- JQuery SlimScroll -->
<script src="{{ asset('assets/libs/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- JQuery Mask Money -->
<script src="{{ asset('assets/libs/mask-money/jquery.maskMoney.js') }}"></script>

<!-- JQuery ZBox -->
<script src="{{ asset('assets/libs/zbox/jquery.zbox.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

@yield('script')

<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! @$validator !!}

@include('partials.alert')
@include('partials.validate-script')
