<div class="left-side-menu bg-left-menu with-image">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                {!! menu('dashboard', 'Dashboard', route('admin.dashboard'), 'fas fa-home') !!}

                {!!
                    menu_with_sub('Employee', 'fas fa-user-alt', [
                        ["uri" => route('admin.employee.index'), "title" => "List Employee", "can" => 'list-employee'],
                        ["uri" => route('admin.employee.idcard.index'), "title" => "Upload ID Card", 'can' => 'upload-idcard'],
                        ["uri" => route('admin.bpjs-kes.index'), "title" => "BPJS Kesehatan", 'can' => 'bpjs-kesehatan'],
                        ["uri" => route('admin.bpjs-tkj.index'), "title" => "BPJS Ketenagakerjaan", 'can' => 'bpjs-ketenagakerjaan'],
                        ["uri" => route('admin.vendor-asuransi.index'), "title" => "Vendor Asuransi", 'can' => 'vendor-asuransi'],
                    ])
                !!}

                {!!
                    menu_with_sub('Informasi Penggajian', 'fas fa-money-check-alt', [
                        ["uri" => route('admin.spt.index'), "title" => "Upload SPT", 'can' => 'upload-spt'],
                        ["uri" => route('admin.slip_gaji.index-upload'), "title" => "Upload Payslip", 'can' => 'upload-payslip'],
                        ["uri" => route('admin.slip_gaji.index'), "title" => "Setting Payslip", 'can' => 'setting-payslip'],
                    ])
                !!}

                {!! menu('inpatient', 'Klaim Rawat Inap', route('admin.inpatient.index'), 'mdi mdi-hospital-building', 'claim-inpatient') !!}

                {!! menu('contracts', 'History Kontrak', route('admin.contract.index'), 'mdi mdi-file-check', 'history-contract') !!}

                @if (auth()->user()->role == 'super-admin')
                {!! menu('companies', 'Companies', route('admin.company.index'), 'fas fa-flag') !!}

                {!! menu('users', 'Users', route('admin.user.index'), 'fas fa-user-tie') !!}
                @endif

                {!! menu('report', 'Report', route('admin.report.index'), 'fas fa-file-alt ', 'report') !!}

                @if (auth()->user()->role == 'super-admin')
                {!!
                    menu_with_sub('Configurations', 'fas fa-cogs', [
                        ["uri" => route('admin.peraturan-perusahaan.index'), "title" => "Peraturan Perusahaan"],
                        ["uri" => route('admin.configuration.index'), "title" => "Configurations"],
                        ["uri" => route('admin.setting-credentials.index'), "title" => "Setting Credentials"]
                    ])
                !!}

                {!!
                    menu_with_sub('Logs', 'fas fa-clipboard-list', [
                        ["uri" => route('admin.credentials.index'), "title" => "Credentials"],
                        ["uri" => route('admin.audit-trail.index'), "title" => "Audit Trail"],
                        ["uri" => route('admin.sms-callback.index'), "title" => "SMS Callback"],
                        ["uri" => route('admin.sipas-log.index'), "title" => "Sipas Logs"],
                    ])
                !!}
                @endif

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
