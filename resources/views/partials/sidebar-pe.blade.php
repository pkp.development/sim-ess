<div class="left-side-menu bg-left-menu with-image">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                {!! menu('dashboard', 'Dashboard', route('pe.dashboard'), 'fas fa-home') !!}

                {!! menu('informasi-karyawan', 'Informasi Karyawan', route('pe.info.karyawan'), 'fas fa-user-alt') !!}

                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-money-check-alt"></i>
                        <span> Informasi Penggajian </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{ route('pe.info.pay-slip') }}">Slip Gaji</a>
                        </li>
                        <li>
                            <a href="{{ route('pe.info.spt') }}">Laporan SPT</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-list-ul"></i>
                        <span> Klaim Benefit </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{ route('pe.rawat-jalan.index') }}">Klaim Rawat Jalan</a>
                        </li>
                        <li>
                            <a href="{{ route('pe.inpatient.index') }}">Klaim Rawat Inap</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('pe.peraturan-perusahaan') }}">
                        <i class="fas fa-cogs"></i>
                        <span> Peraturan Perusahaan </span>
                    </a>
                </li>

                <li>
                    <a href="https://gajianduluan.id/" target="_blank">
                        <i class="fas fa-book"></i>
                        <span> Gajian Duluan </span>
                    </a>
                </li>

                <!--<li>
                    <a href="../hubungi_kami.html">
                        <i class="fas fa-info-circle"></i>
                        <span> Hubungi Kami </span>
                    </a>
                </li>-->
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
