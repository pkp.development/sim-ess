<div class="navbar-custom navbar-custom-light">
    <ul class="list-unstyled topnav-menu float-right mb-0">
        @if(auth()->user()->role == 'super-admin' || auth()->user()->role == 'admin')
            @if (auth()->user()->role == 'admin')
                @include('partials.notification-permission')
            @else
                @include('partials.notification-admin')
            @endif
        @else
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <i class="fe-bell noti-icon"></i>
                <span class="badge badge-danger rounded-circle noti-icon-badge">{{ $counts_notif }}</span>
            </a>

            <div class="dropdown-menu dropdown-menu-right dropdown-lg">
                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0">
                        Notifikasi
                    </h5>
                </div>

                <div class="slimscroll noti-scroll">
                    @foreach($notifications as $notif)
                    <!-- item-->
                    <a href="{{ route('pe.profile.read', [$notif->type, $notif->uuid]) }}" class="dropdown-item notify-item">
                        @if($notif->type == 'profile')
                            @if($notif->status == 1)
                                <div class="notify-icon bg-success">
                                    <i class="far fa-user"></i>
                                </div>
                                <p class="notify-details">Notifikasi Biodata</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data anda telah diverifikasi</small>
                                </p>
                            @elseif($notif->status == 2)
                                <div class="notify-icon bg-danger">
                                    <i class="far fa-user"></i>
                                </div>
                                <p class="notify-details">Notifikasi Biodata</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data ditolak dengan alasan, {{ $notif->description }}</small>
                                </p>
                            @endif
                        @elseif($notif->type == 'family')
                            @if($notif->status == 1)
                                <div class="notify-icon bg-success">
                                    <i class="far fa-address-card"></i>
                                </div>
                                <p class="notify-details">Notifikasi Biodata Keluarga</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data anda telah diverifikasi</small>
                                </p>
                            @elseif($notif->status == 2)
                                <div class="notify-icon bg-danger">
                                    <i class="far fa-address-card"></i>
                                </div>
                                <p class="notify-details">Notifikasi Biodata Keluarga</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data ditolak dengan alasan, {{ $notif->description }}</small>
                                </p>
                            @endif
                        @elseif($notif->type == 'bank')
                            @if($notif->status == 1)
                                <div class="notify-icon bg-success">
                                    <i class="mdi mdi-bank"></i>
                                </div>
                                <p class="notify-details">Notifikasi Data Rekening</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data anda telah diverifikasi</small>
                                </p>
                            @elseif($notif->status == 2)
                                <div class="notify-icon bg-danger">
                                    <i class="mdi mdi-bank"></i>
                                </div>
                                <p class="notify-details">Notifikasi Data Rekening</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data ditolak dengan alasan, {{ $notif->description }}</small>
                                </p>
                            @endif
                        @elseif($notif->type == 'families')
                            @if($notif->status == 1)
                                <div class="notify-icon bg-success">
                                    <i class="fas fa-users"></i>
                                </div>
                                <p class="notify-details">Notifikasi Daftar Keluarga</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data anda telah diverifikasi</small>
                                </p>
                            @elseif($notif->status == 2)
                                <div class="notify-icon bg-danger">
                                    <i class="fas fa-users"></i>
                                </div>
                                <p class="notify-details">Notifikasi Daftar Keluarga</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data ditolak dengan alasan, {{ $notif->description }}</small>
                                </p>
                            @endif
                        @elseif($notif->type == 'outpatient')
                            @if($notif->status == 1)
                                <div class="notify-icon bg-success">
                                    <i class="mdi mdi-hospital"></i>
                                </div>
                                <p class="notify-details">Notifikasi Klaim Rawat Jalan</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data anda telah diverifikasi</small>
                                </p>
                            @elseif($notif->status == 2)
                                <div class="notify-icon bg-danger">
                                    <i class="mdi mdi-hospital"></i>
                                </div>
                                <p class="notify-details">Notifikasi Klaim Rawat Jalan</p>
                                <p class="text-muted mb-0 user-msg">
                                    <small>Perubahan data ditolak dengan alasan, {{ $notif->description }}</small>
                                </p>
                            @endif
                        @endif
                    </a>
                    @endforeach
                </div>

                <!-- All-->
                <a href="{{ route('pe.read.all-notification') }}" class="dropdown-item text-center text-primary notify-item notify-all">
                    Hapus Semua Notifikasi
                    <i class="fi-arrow-right"></i>
                </a>

            </div>
        </li>
        @endif

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="
                    @if(auth()->user()->role == 'super-admin' || auth()->user()->role == 'admin')
                        {{ Avatar::create(auth()->user()->admin->name)->toBase64() }}
                    @else
                        {{ Avatar::create(auth()->user()->profile->name)->toBase64() }}
                    @endif" alt="user-image" class="avatar-sm rounded">
                <span class="pro-user-name mr-1"> Hi,
                    @if(auth()->user()->role == 'super-admin' || auth()->user()->role == 'admin')
                        {{ auth()->user()->admin->name }}
                    @else
                        {{ ucwords(strtolower(auth()->user()->profile->name)) }}
                    @endif
                ! </span>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <a href="{{ route('change-password') }}" class="dropdown-item notify-item">
                        <i class="fe-lock"></i>
                        <span>Ubah Password</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <!-- item-->
                    <a href="{{ route('logout') }}" class="dropdown-item notify-item">
                        <i class="fe-log-out"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </a>
        </li>
    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="index.html" class="logo text-left ml-3">
            <span class="logo-lg">
                <img class="mb-1" src="{{ asset('assets/images/group_6.png') }}" alt="" height="36">
                <span class="logo-lg-text-ess">ESS</span>
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">U</span> -->
                <img src="{{ asset('assets/images/group_6.png') }}" alt="" height="36">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect waves-light">
                <img src="{{ asset('assets/images/group_125.png') }}" alt="">
            </button>
        </li>

        <li class="d-none d-lg-block">
            @yield('page-title')
        </li>
    </ul>
</div>
