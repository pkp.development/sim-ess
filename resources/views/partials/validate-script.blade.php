<script>
    @if ($errors->count() > 0)
        @foreach ($errors->getMessages() as $key => $message)
            $.toast().reset('all');
            $.toast({
                heading: "Oops!",
                text: "{{ $message[0] }}",
                position: 'top-right',
                loaderBg: '#bf441d',
                icon: 'error',
                hideAfter: 5000,
                stack: 1
            });
        @endforeach
    @endif
</script>
