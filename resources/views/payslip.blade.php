<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-e7aw{background-color: #e8edff;color: #666699;font-weight:bold;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-4688{font-weight:bold;font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:left;vertical-align:top; width: 30%}
.tg .tg-fzq1{font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-n0m6{background-color: #b9c9fe;color: #003399;font-weight:bold;font-size:15px;text-align:right;vertical-align:top;border-color:inherit;}
.tg .tg-y09a{background-color: #e8edff;color: #666699;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:right;vertical-align:top}
.tg .tg-288q{font-weight:bold;font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:center;vertical-align:top;border:none;width: 2px !important}
.tg .tg-222q{background-color: #b9c9fe;color: #003399;font-weight:bold;font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:center;vertical-align:top;}
.tg .tg-pe1m{background-color: #b9c9fe;color: #003399;font-weight:bold;font-size:15px;text-align:left;vertical-align:top;border-color:inherit;}
.tg .tg-total{background-color: #b9c9fe;color: #003399;font-weight:bold;font-size:18px;text-align:left;vertical-align:top;border-color:inherit;}
.tg .tg-totalnumber{background-color: #b9c9fe;color: #003399;font-weight:bold;font-size:18px;text-align:right;vertical-align:top;border-color:inherit;}
.mt-1{margin-top: 20px;}
</style>
<div class="tg-wrap"><table class="tg" style="table-layout: fixed; width: 100%;">
  <tr>
    <th class="tg-y09a" colspan="6" style="border-bottom: none;padding-right: 20px;background-color: white;"><img src="{{ asset('assets/images/logo-placeholder.png') }}" alt="LOGO" width="160" height="auto"></th>
  </tr>
  <tr>
    <td class="tg-4688" style="border-right: none;border-top: none; border-bottom: none;">Sim ID</td>
    <td class="tg-288q" >:</td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">{{ $employee->account->sim_id }}</td>

    <td class="tg-4688" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">Lokasi Penempatan</td>
    <td class="tg-288q" >:</td>
    <td class="tg-fzq1" style="border-top: none; border-left: none; border-bottom: none;">{{ $contract->company->organization_name }}</td>
  </tr>
  <tr>
    <td class="tg-4688" style="border-right: none;border-top: none; border-bottom: none;">Nama Karyawan</td>
    <td class="tg-288q" >:</td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">{{ $employee->name }}</td>

    <td class="tg-4688" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">Tanggal Bergabung</td>
    <td class="tg-288q">:</td>
    <td class="tg-fzq1" style="border-top: none; border-left: none; border-bottom: none;">{{ $contract->join_date->format('d F Y') }}</td>
  </tr>
  <tr>
    <td class="tg-4688" style="border-right: none;border-top: none; border-bottom: none;">Jabatan</td>
    <td class="tg-288q">:</td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">{{ $contract->job->job_name }}</td>

    <td class="tg-4688" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">Nomor Rekening</td>
    <td class="tg-288q" >:</td>
    <td class="tg-fzq1" style="none;border-top: none; border-left: none; border-bottom: none;">{{ $bank->account_number }}</td>
  </tr>
  <tr>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-bottom: none;"></td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;"></td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;"></td>

    <td class="tg-4688" style="border-right: none;border-top: none; border-left: none; border-bottom: none;">Nama Bank</td>
    <td class="tg-288q">:</td>
    <td class="tg-fzq1" style="border-top: none; border-left: none; border-bottom: none;">{{ $bank->api->bank_name }}</td>
  </tr>
  <tr>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-bottom: none;"></td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;"></td>
    <td class="tg-fzq1" style="border-right: none;border-top: none; border-left: none; border-bottom: none;"></td>

    <td class="tg-4688" style="border-right: none;border-top: none; border-left: none; border-bottom: none; padding-bottom: 50px;">Tanggal Pembayaran</td>
    <td class="tg-288q">:</td>
    <td class="tg-fzq1" style="border-top: none; border-left: none; border-bottom: none;">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $payslip->payment_date)->format('d F Y') }}</td>
    </tr>
</table>
</div>

<div class="tg-wrap"><table class="tg" style="table-layout: fixed; width: 50%;float: left;">
    <tr>
        <td class="tg-222q" colspan="6">Pendapatan</td>
    </tr>

    @php
        $totalPendapatan = 0
    @endphp

    @foreach($payslip->content as $content)
        @if($content->account->type == 'pendapatan')
        <tr>
            <td class="tg-e7aw" colspan="3">{{ $content->account->name }}</td>
            <td class="tg-y09a" colspan="3">Rp {{ number_format($content->nominal) }},-</td>
        </tr>
        @php
            $totalPendapatan = $totalPendapatan + $content->nominal;
        @endphp
        @endif
    @endforeach
    <tr>
        <td class="tg-pe1m" colspan="3">TOTAL PENDAPATAN</td>
        <td class="tg-n0m6" colspan="3">Rp {{ number_format($totalPendapatan) }},-</td>
    </tr>
</table>
</div>

<div class="tg-wrap"><table class="tg" style="table-layout: fixed; width: 50%;float: right;">
    <tr>
        <td class="tg-222q" colspan="6">Pengeluaran</td>
    </tr>
    @php
        $totalPengurangan = 0
    @endphp

    @foreach($payslip->content as $content)
        @if($content->account->type == 'pengeluaran')
        <tr>
            <td class="tg-e7aw" colspan="3">{{ $content->account->name }}</td>
            <td class="tg-y09a" colspan="3">Rp {{ number_format($content->nominal) }},-</td>
        </tr>
        @php
            $totalPengurangan = $totalPengurangan + $content->nominal;
        @endphp
        @endif
    @endforeach
    <tr>
        <td class="tg-pe1m" colspan="3">TOTAL PENGELUARAN</td>
        <td class="tg-n0m6" colspan="3">Rp {{ number_format($totalPengurangan) }},-</td>
    </tr>
</table>
</div>

<div class="tg-wrap .mt-1" style="position:relative;"><table class="tg" style="width: 100%;">
    <tr>
        <td class="tg-total" colspan="3" style="border-right: none;">Total Penerimaan Upah {{ $payslip->month_name }} {{ $payslip->year_periode }}</td>
        <td class="tg-totalnumber" colspan="3" style="border-left: none;">Rp {{ number_format($totalPendapatan - $totalPengurangan) }},-</td>
      </tr>
</table>
</div>
</body>
</html>
