<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>SIM - ESS</title>

        <!-- App css -->
        <link href="{{ $localIp . 'assets/css/bootstrap.min.css' }}" rel="stylesheet" type="text/css" />

        <style>
            .table td, .table th {
                padding: .30rem;
                vertical-align: top;
                border-top: 1px solid #dee2e6;
            }

            .table-striped tbody tr:nth-of-type(2n+1) {
                background-color: #f1f5f7;
            }

            .table-striped tbody tr.sum {
            }

            .table-borderless tbody + tbody, .table-borderless td, .table-borderless th, .table-borderless thead th {
                border: 0;
            }
        </style>

    </head>

    <body style="max-width: 1140px;">

        <div class="">
            <div class="container">
                <div class="row">
                    <div class="col-12" style="padding-left: 0; padding-right: 0">
                        <div class="card">
                            <div style="border-bottom: 1px solid #000; position: relative; max-height: 150px;">
                                <div class="p-2" style="vertical-align: middle; position: absolute; bottom: 0">
                                    <h3 style="display: inline-block; vertical-align: middle; line-height: normal;">@if($payslip->company){{ $payslip->company->name }}@else{{ '-' }}@endif</h3>
                                </div>
                                <div class="float-right p-2" style="right: 0;">
                                    <img src="@if($payslip->company){{ $localIp . 'storage/' . $payslip->company->logo }}@endif" alt="" style="height: 100px; width: auto;">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-2 mt-2">
                                    <h4 class="text-right"><strong style="border-bottom: 1px solid #000; padding-bottom: 2px">{{ strtoupper('Slip Gaji Karyawan') }}</strong></h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="max-width: 50%">
                                    <table class="table table-borderless mb-0" style="color: #000">
                                        <tbody>
                                            <tr>
                                                <td><b>SIM ID</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ $employee->account->sim_id }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Nama Karyawan</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ $employee->name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Jabatan</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ $payslip->job_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>No. NPWP</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ ($employee->npwp_number) ? $employee->npwp_number : "-" }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6" style="max-width: 50%">
                                    <table class="table table-borderless mb-0" style="color: #000">
                                        <tbody>
                                            <tr>
                                                <td><b>Lokasi Penempatan</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ $payslip->client_company_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Tanggal Bergabung</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>@if($payslip->join_date){{ \Carbon\Carbon::createFromFormat('Y-m-d', $payslip->join_date)->format('d F Y') }}@else{{ "-" }}@endif</td>
                                            </tr>
                                            <tr>
                                                <td><b>Nomor Rekening</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ $payslip->account_number }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Nama Bank</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ $payslip->bank_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Tanggal Pembayaran</b></td>
                                                <td style="width: 2%">:</td>
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $payslip->payment_date)->format('d F Y') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <table class="table table-borderless mb-0" style="color: #000">
                                        <tbody>
                                            @foreach($payslip->content as $content)
                                                @if($content->account->type == 'fixed')
                                                @if($content->account->code == 'jumlah_kehadiran')
                                                <tr>
                                                    <td style="font-size: 20px; border-top: 1px solid #000; border-bottom: 1px solid #000"><b>{{ $content->account->name }}</b></td>
                                                    <td class="text-right" style="font-size: 20px; border-top: 1px solid #000; border-bottom: 1px solid #000"><b>{{ $content->nominal }} Hari</b></td>
                                                </tr>
                                                @else
                                                <tr>
                                                    <td style="font-size: 20px; border-top: 1px solid #000; border-bottom: 1px solid #000"><b>{{ $content->account->name }}</b></td>
                                                    <td class="text-right" style="font-size: 20px; border-top: 1px solid #000; border-bottom: 1px solid #000"><b>Rp {{ number_format($content->nominal) }}</b></td>
                                                </tr>
                                                @endif
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row mt-2">
                                @php
                                    $totalPendapatan = 0;
                                    $totalPengurangan = 0;
                                @endphp

                                <div class="col-md-6" style="max-width: 50%">
                                    <table class="table table-borderless table-striped mb-0" style="color: #000">
                                        <thead>
                                            <tr>
                                                <th colspan="3" style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000">PENDAPATAN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payslip->content as $content)
                                                @if($content->account->type == 'pendapatan')
                                                @if(is_numeric($content->nominal))
                                                <tr>
                                                    <td><b>{{ $content->account->name }}</b></td>
                                                    <td style="width: 2%">:</td>
                                                    <td class="text-right">Rp {{ number_format($content->nominal) }}</td>
                                                </tr>
                                                @endif
                                                @php
                                                    $totalPendapatan = $totalPendapatan + $content->nominal;
                                                @endphp
                                                @endif
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="sum" style=" font-size: 17px">
                                                <td style="width: 60%; border-top: 1px solid #000; border-bottom: 1px solid #000; border-bottom-style: double"><b>Total Pendapatan</b></td>
                                                <td colspan="2" style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000; border-bottom-style: double"><b>Rp {{ number_format($totalPendapatan) }}</b></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6" style="max-width: 50%">
                                    <table class="table table-borderless table-striped mb-0" style="color: #000">
                                        <thead>
                                            <tr>
                                                <th colspan="3" style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000">POTONGAN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payslip->content as $content)
                                                @if($content->account->type == 'pengeluaran')
                                                @if(is_numeric($content->nominal))
                                                <tr>
                                                    <td><b>{{ $content->account->name }}</b></td>
                                                    <td style="width: 2%">:</td>
                                                    <td class="text-right">Rp {{ number_format($content->nominal) }}</td>
                                                </tr>
                                                @endif
                                                @php
                                                    $totalPengurangan = $totalPengurangan + $content->nominal;
                                                @endphp
                                                @endif
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="sum" style=" font-size: 17px">
                                                <td style="width: 60%; border-top: 1px solid #000; border-bottom: 1px solid #000; border-bottom-style: double"><b>Total Potongan</b></td>
                                                <td colspan="2" style="text-align: right; border-top: 1px solid #000; border-bottom: 1px solid #000; border-bottom-style: double"><b>Rp {{ number_format($totalPengurangan) }}</b></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <table class="table table-borderless mb-0" style="color: #000">
                                        <tbody>
                                            <tr>
                                                <td style="font-size: 20px; border-top: 1px solid #000; border-bottom: 1px solid #000"><b>Total Penerimaan Upah {{ $payslip->month_name }} {{ $payslip->year_periode }}</b></td>
                                                <td class="text-right" style="font-size: 20px; border-top: 1px solid #000; border-bottom: 1px solid #000"><b>Rp {{ number_format($totalPendapatan - $totalPengurangan) }}</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
    </body>
</html>
