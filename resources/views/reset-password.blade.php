<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>SIM - ESS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="SIM - Employee Self Service" name="description" />
        <meta name="author" content="Aether ID">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <!-- JQuery Toast -->
        <link href="{{ asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg authentication-bg-pattern">

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">

                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{ asset('assets/images/logo.png') }}" alt="" height="64"></span>
                                    </a>
                                    <h3 class="text-muted mb-4 mt-1">Employee Self Service</h3>
                                </div>

                                <form action="{{ route('recovery-password-post') }}" method="POST">
                                    {!! csrf_field() !!}

                                    <div class="form-group mb-3">
                                        <label for="sim_id">Kata Sandi</label>
                                        <input class="form-control" type="password" id="password" name="password" placeholder="Kata sandi baru">
                                        <input type="hidden" name="token" value="{{ $token }}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="sim_id">Konfirmasi Kata Sandi</label>
                                        <input class="form-control" type="password" id="password_confirmation" name="password_confirmation" placeholder="Ulangi kata sandi">
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-primary btn-block" type="submit"> Perbarui </button>
                                    </div>

                                </form>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.min.js') }}"></script>

        <!-- JQuery Toast -->
        <script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>

        <!-- Laravel Javascript Validation -->
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! @$validator !!}

        @include('partials.alert')
    </body>
</html>
