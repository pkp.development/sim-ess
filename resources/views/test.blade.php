<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="SIM - Employee Self Service" name="description" />
    <meta name="author" content="Aether ID">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.ico') }}">
    <title>SIM ESS | Tanda tangan kontrak online</title>
    <!-- Ion icons -->
    <link href="{{ asset('aether/signer/assets/fonts/ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=B612+Mono:400,400i,700|Charm:400,700|EB+Garamond:400,400i,700|Noto+Sans+TC:400,700|Open+Sans:400,400i,700|Pacifico|Reem+Kufi|Scheherazade:400,700|Tajawal:400,700&amp;subset=arabic" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('aether/signer/assets/libs/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/css/simcify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('aether/signer/assets/libs/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/jquery-toast/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Signer CSS -->
    <link href="{{ asset('aether/signer/assets/css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('aether/signer/assets/js/jscolor.js') }}"></script>

    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        /* mengatur ukuran canvas tanda tangan  */
        canvas {
            border: 1px solid #ccc;
            border-radius: 0.5rem;
            width: 100%;
            height: 400px;
        }
    </style>
</head>

<body class="authentication-bg-pattern">

<div class="container">
    <h1 class="alert alert-success mt-5 text-dark">
        Latihan Membuat Tanda Tangan Digital - sahretech.com
    </h1>
    <div class="card">
        <div class="card-header">
            Form Tanda Tangan
        </div>
        <div class="card-body">
            <!-- canvas tanda tangan  -->
            <canvas id="signature-pad" class="signature-pad"></canvas>

            <!-- tombol submit  -->
            <div style="float: left;">
                <button id="btn-submit" class="btn btn-primary">
                    Submit
                </button>
            </div>

            <div style="float: right;">
                <!-- tombol undo  -->
                <button type="button" class="btn btn-dark" id="undo">
                    <span class="fas fa-undo"></span>
                    Undo
                </button>

                <!-- tombol hapus tanda tangan  -->
                <button type="button" class="btn btn-danger" id="clear">
                    <span class="fas fa-eraser"></span>
                    Clear
                </button>
            </div>
        </div>
    </div>
</div>
    <!-- scripts -->
    <script src="{{ asset('aether/signer/assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/simcify.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js//jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jcanvas/jcanvas.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/js/touch-punch.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-toast/jquery.toast.min.js') }}"></script>
    <script src="{{ asset('aether/signer/assets/libs/jcanvas/editor.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <script src="{{ asset('aether/signer/assets/js/pdf.js') }}"></script>
<script>
    // script di dalam ini akan dijalankan pertama kali saat dokumen dimuat
    document.addEventListener('DOMContentLoaded', function () {
        resizeCanvas();
    })

    //script ini berfungsi untuk menyesuaikan tanda tangan dengan ukuran canvas
    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
    }


    var canvas = document.getElementById('signature-pad');

    //warna dasar signaturepad
    var signaturePad = new SignaturePad(canvas, {
        backgroundColor: 'rgb(255, 255, 255)'
    });

    //saat tombol clear diklik maka akan menghilangkan seluruh tanda tangan
    document.getElementById('clear').addEventListener('click', function () {
        signaturePad.clear();
    });

    //saat tombol undo diklik maka akan mengembalikan tanda tangan sebelumnya
    document.getElementById('undo').addEventListener('click', function () {
        var data = signaturePad.toData();
        if (data) {
            data.pop(); // remove the last dot or line
            signaturePad.fromData(data);
        }
    });
    //fungsi untuk menyimpan tanda tangan dengan metode ajax
    $(document).on('click', '#btn-submit', function () {
        var signature = signaturePad.toDataURL();

        $.ajax({
            url: "proses.php",
            data: {
                foto: signature,
            },
            method: "POST",
            success: function () {
                location.reload();
                alert('Tanda Tangan Berhasil Disimpan');
            }

        })
    })
</script>
    <!-- custom scripts -->
</body>

</html>
