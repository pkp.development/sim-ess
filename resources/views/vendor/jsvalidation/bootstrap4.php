<script>
    jQuery(document).ready(function(){

        $("<?= $validator['selector']; ?>").each(function() {
            proengsoft_validate = $(this).validate({
                errorElement: 'span',
                errorClass: 'invalid-feedback',

                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length ||
                        element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                        error.insertAfter(element.parent());
                        // else just place the validation message immediately after the input
                    }
                    else if(element.hasClass('selectpicker')) {
                        $parent = element.parent();
                        $parent.addClass('container-error');
                        error.insertAfter($parent);
                    }
                    else if (element.parent('.custom-file').length) {
                        parent = element.parent();
                        parent.addClass('container-error');
                        error.insertAfter(element.parent().parent());
                        // else just place the validation message immediately after the input
                    }
                    else if(element.hasClass('select2-hidden-accessible')) {


                        parent = element.parent();
                        span_select2 = parent.find('span.select2-container');
                        span_select2.addClass('select2-container-error');
                        error.insertAfter(span_select2);
                    }
                    else if(element.hasClass('datetime-datepicker')) {
                        parent = element.parent();
                        error.insertAfter(parent.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                },
                highlight: function (element) {
                    $(element).closest('.form-control').removeClass('is-valid').addClass('is-invalid'); // add the Bootstrap error class to the control group
                    if($(element).hasClass('select2-hidden-accessible')) {
                        parent = $(element).parent();
                        parent.find("span.select2-container").removeClass('select2-container-success').addClass('select2-container-error');
                        // parent.find("invalid-feedback").remove();
                    }
                    if($(element).hasClass('selectpicker')) {
                        parent = $(element).parent();
                        parent.removeClass('container-success').addClass('container-error');
                    }
                },

                ignore: "",


                unhighlight: function(element) {
                    $(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
                    if($(element).hasClass('select2-hidden-accessible')) {
                        parent = $(element).parent();
                        parent.find("span.select2-container").removeClass('select2-container-error').addClass('select2-container-success');
                        // parent.find("invalid-feedback").remove();
                    }
                    if($(element).hasClass('selectpicker')) {
                        parent = $(element).parent();
                        parent.removeClass('container-error').addClass('container-success');
                    }
                },

                success: function (element) {
                    // Bootstrap Select
                    $(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid'); // remove the Boostrap error class from the control group
                    if($(element).hasClass('select2-hidden-accessible')) {
                        parent = $(element).parent();
                        parent.find("span.select2-container").removeClass('select2-container-error').addClass('select2-container-success');
                        // parent.find("invalid-feedback").remove();
                    }
                    if($(element).hasClass('selectpicker')) {
                        parent = $(element).parent();
                        parent.removeClass('container-error').addClass('container-success');
                    }
                },

                focusInvalid: false, // do not focus the last invalid input
                <?php if (Config::get('jsvalidation.focus_on_error')): ?>
                invalidHandler: function (form, validator) {

                    if (!validator.numberOfInvalids())
                        return;

                    $('html, body').animate({
                        scrollTop: $(validator.errorList[0].element).offset().top
                    }, <?= Config::get('jsvalidation.duration_animate') ?>);
                    $(validator.errorList[0].element).focus();

                },
                <?php endif; ?>

                rules: <?= json_encode($validator['rules']); ?>
            });
        });

        $("form").delegate(".select2-hidden-accessible", "change", function(e){
            if($(this).val() != "") {
                parent = $(this).parent();
                parent.find("span.select2-container").removeClass('select2-container-error').addClass('select2-container-success');
                parent.find("span.invalid-feedback").hide();
            }
            else {
                parent = $(this).parent();
                parent.find("span.select2-container").removeClass('select2-container-success').addClass('select2-container-error');
                parent.find("span.invalid-feedback").show();
            }
        });

        $("form").delegate(".selectpicker", "change", function(e){
            if($(this).val() != "") {
                parent = $(this).parent();
                parent.removeClass('container-error').addClass('container-success');
                parent.parent().find("span.invalid-feedback").hide();
            }
            else {
                parent = $(this).parent();
                parent.removeClass('container-success').addClass('container-error');
                parent.parent().find("span.invalid-feedback").show();
            }
        });
    });
</script>
