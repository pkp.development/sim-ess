<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('admin.login');
})->middleware('guest')->name('index');

Route::get('login', 'AuthController@login')
    ->middleware('guest')->name('login');
Route::post('authentication', 'AuthController@auth')
    ->middleware('guest')->name('auth');

Route::get('dashboard', 'HomeController@dashboard')
    ->middleware(['ess-auth', 'admin'])->name('dashboard');

// Users
Route::get('users/datatable', 'UserController@datatable')
    ->middleware(['ess-auth', 'admin:true'])->name('user.datatable');
Route::get('users', 'UserController@index')
    ->middleware(['ess-auth', 'admin:true'])->name('user.index');
Route::get('users/create', 'UserController@create')
    ->middleware(['ess-auth', 'admin:true'])->name('user.create');
Route::get('users/{uuid}/edit', 'UserController@edit')
    ->middleware(['ess-auth', 'admin:true'])->name('user.edit');
Route::get('users/{uuid}/permission', 'UserController@permission')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission');
Route::post('users', 'UserController@store')
    ->middleware(['ess-auth', 'admin:true'])->name('user.store');
Route::patch('users/{uuid}', 'UserController@update')
    ->middleware(['ess-auth', 'admin:true'])->name('user.update');
Route::delete('users/{uuid}', 'UserController@destroy')
    ->middleware(['ess-auth', 'admin:true'])->name('user.destroy');

// Permissions
Route::get('users/{uuid}/permission', 'UserController@permission')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission');
Route::post('users/{uuid}/permission', 'PermissionController@permissionStore')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission.store');
Route::get('user/{uuid}/permission-client/datatable', 'PermissionController@clientDatatable')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission-client.datatable');
Route::post('user/{uuid}/permission-client', 'PermissionController@clientStore')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission-client.store');
Route::delete('user/{id}/permission-client', 'PermissionController@clientDelete')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission-client.delete');
Route::get('user/{uuid}/permission-sim-branch/datatable', 'PermissionController@simBranchDatatable')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission-sim-branch.datatable');
Route::post('user/{uuid}/permission-sim-branch', 'PermissionController@simBranchStore')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission-sim-branch.store');
Route::delete('user/{id}/permission-sim-branch', 'PermissionController@simBranchDelete')
    ->middleware(['ess-auth', 'admin:true'])->name('user.permission-sim-branch.delete');

// Employee
Route::get('employee', 'EmployeeController@index')
    ->middleware(['ess-auth', 'admin', 'can:list-employee'])->name('employee.index');
Route::get('employee/detail/{uuid}', 'EmployeeController@detail')
    ->middleware(['ess-auth', 'admin', 'can:list-employee'])->name('employee.detail');
Route::post('employee/change-password/{uuid}', 'EmployeeController@changePassword')
    ->middleware(['ess-auth', 'admin', 'can:update-password'])->name('employee.change-password');

Route::get('employee/content/{uuid}', 'EmployeeController@getTmpData')
    ->middleware(['ess-auth', 'admin'])->name('employee.tmp-data');

Route::get('employee/update/{uuid}', 'EmployeeController@updateBiodataView')
    ->middleware(['ess-auth', 'admin'])->name('employee.update.view');
Route::post('employee/update/{uuid}', 'EmployeeController@updateBiodataPost')
    ->middleware(['ess-auth', 'admin'])->name('employee.update.post');
Route::get('employee/get-image/{type?}/{uuid?}/{temporary?}', 'EmployeeController@getImage')
    ->middleware(['ess-auth', 'admin'])->name('employee.get.tmp-image');

// Employee ID Card
Route::get('employee/id-card', 'IdCardController@index')
    ->middleware(['ess-auth', 'admin', 'can:upload-idcard'])->name('employee.idcard.index');
Route::get('employee/id-card/detail/{uuid?}', 'IdCardController@detail')
    ->middleware(['ess-auth', 'admin', 'can:upload-idcard'])->name('employee.idcard.detail');
Route::post('employee/id-card', 'IdCardController@upload')
    ->middleware(['ess-auth', 'admin', 'can:upload-idcard'])->name('employee.idcard.store-upload');
Route::get('employee/id-card/download/template', 'IdCardController@downloadTemplate')
    ->middleware(['ess-auth', 'admin', 'can:upload-idcard'])->name('employee.idcard.download-template');

// Family
Route::get('employee/update/family/{uuid}', 'FamilyController@updateFamilyView')
    ->middleware(['ess-auth', 'admin'])->name('employee.update.family.view');
Route::post('employee/update/family/{uuid}', 'FamilyController@updateFamilyPost')
    ->middleware(['ess-auth', 'admin'])->name('employee.update.family.post');
Route::get('employee/daftar/family/{uuid}', 'FamilyController@daftarFamily')
    ->middleware(['ess-auth', 'admin'])->name('employee.daftar.family');
Route::post('employee/daftar/family/{uuid}', 'FamilyController@daftarFamilyPost')
    ->middleware(['ess-auth', 'admin'])->name('employee.daftar.family.post');

// Rekening
Route::get('employee/update/rekening/{uuid}', 'BankController@updateRekeningView')
    ->middleware(['ess-auth', 'admin'])->name('employee.update.rekening.view');
Route::post('employee/update/rekening/{uuid}', 'BankController@updateRekeningPost')
    ->middleware(['ess-auth', 'admin'])->name('employee.update.rekening.post');

// SPT
Route::get('spt', 'SptController@index')
    ->middleware(['ess-auth', 'admin', 'can:upload-spt'])->name('spt.index');
Route::get('spt/detail/{uuid}', 'SptController@detail')
    ->middleware(['ess-auth', 'admin', 'can:upload-spt'])->name('spt.detail');
Route::post('spt', 'SptController@store')
    ->middleware(['ess-auth', 'admin', 'can:upload-spt'])->name('spt.store');
Route::get('spt/download-template', 'SptController@downloadTemplate')
    ->middleware(['ess-auth', 'admin', 'can:upload-spt'])->name('spt.download-template');

// Slip Gaji
Route::get('slip-gaji/upload', 'SlipController@indexUpload')
    ->middleware(['ess-auth', 'admin', 'can:upload-payslip'])->name('slip_gaji.index-upload');
Route::get('slip-gaji/{uuid}', 'SlipController@detail')
    ->middleware(['ess-auth', 'admin', 'can:upload-payslip'])->name('slip_gaji.detail');
Route::get('slip-gaji/download/template', 'SlipController@downloadTemplate')
    ->middleware(['ess-auth', 'admin', 'can:upload-payslip'])->name('slip_gaji.download-template');
Route::post('slip-gaji/upload', 'SlipController@storeUpload')
    ->middleware(['ess-auth', 'admin', 'can:upload-payslip'])->name('slip_gaji.store-upload');

// Setting Slip Gaji
Route::get('slip-gaji', 'SlipController@index')
    ->middleware(['ess-auth', 'admin', 'can:setting-payslip'])->name('slip_gaji.index');
Route::post('slip-gaji', 'SlipController@store')
    ->middleware(['ess-auth', 'admin', 'can:setting-payslip'])->name('slip_gaji.store');
Route::patch('slip-gaji/{code?}', 'SlipController@update')
    ->middleware(['ess-auth', 'admin', 'can:setting-payslip'])->name('slip_gaji.update');
Route::get('slip-gaji/get/account/{code?}', 'SlipController@getAccount')
    ->middleware(['ess-auth', 'admin', 'can:setting-payslip'])->name('slip_gaji.get.account-first');

// Klaim Benefit
Route::get('bpjs-kesehatan', 'BpjsKesehatanController@index')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-kesehatan'])->name('bpjs-kes.index');
Route::get('bpjs-kesehatan/detail/{uuid?}', 'BpjsKesehatanController@detail')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-kesehatan'])->name('bpjs-kes.detail');
Route::post('bpjs-kesehatan', 'BpjsKesehatanController@upload')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-kesehatan'])->name('bpjs-kes.store-upload');
Route::get('bpjs-kesehatan/download/template', 'BpjsKesehatanController@downloadTemplate')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-kesehatan'])->name('bpjs-kes.download-template');

Route::get('bpjs-ketenagakerjaan', 'BpjsKetenagakerjaanController@index')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-ketenagakerjaan'])->name('bpjs-tkj.index');
Route::get('bpjs-ketenagakerjaan/detail/{uuid?}', 'BpjsKetenagakerjaanController@detail')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-ketenagakerjaan'])->name('bpjs-tkj.detail');
Route::post('bpjs-ketenagakerjaan', 'BpjsKetenagakerjaanController@upload')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-ketenagakerjaan'])->name('bpjs-tkj.store-upload');
Route::get('bpjs-ketenagakerjaan/download/template', 'BpjsKetenagakerjaanController@downloadTemplate')
    ->middleware(['ess-auth', 'admin', 'can:bpjs-ketenagakerjaan'])->name('bpjs-tkj.download-template');

Route::get('vendor-asuransi', 'VendorAsuransiController@index')
    ->middleware(['ess-auth', 'admin', 'can:vendor-asuransi'])->name('vendor-asuransi.index');
Route::get('vendor-asuransi/detail/{uuid?}', 'VendorAsuransiController@detail')
    ->middleware(['ess-auth', 'admin', 'can:vendor-asuransi'])->name('vendor-asuransi.detail');
Route::post('vendor-asuransi', 'VendorAsuransiController@upload')
    ->middleware(['ess-auth', 'admin', 'can:vendor-asuransi'])->name('vendor-asuransi.store-upload');
Route::get('vendor-asuransi/download/template', 'VendorAsuransiController@downloadTemplate')
    ->middleware(['ess-auth', 'admin', 'can:vendor-asuransi'])->name('vendor-asuransi.download-template');

Route::get('rawat-jalan/verification/{uuid}', 'OutpatientController@verification')
    ->middleware(['ess-auth', 'admin'])->name('outpatient.verification');
Route::post('rawat-jalan/verification/{uuid}', 'OutpatientController@verificationPost')
    ->middleware(['ess-auth', 'admin'])->name('outpatient.verification.post');

Route::get('inpatient', 'InpatientController@index')
    ->middleware(['ess-auth', 'admin', 'can:claim-inpatient'])->name('inpatient.index');
Route::get('inpatient/{uuid?}', 'InpatientController@detail')
    ->middleware(['ess-auth', 'admin', 'can:claim-inpatient'])->name('inpatient.detail');
Route::patch('inpatient/{uuid}', 'InpatientController@update')
    ->middleware(['ess-auth', 'admin', 'can:claim-inpatient'])->name('inpatient.update');
Route::post('inpatient', 'InpatientController@store')
    ->middleware(['ess-auth', 'admin', 'can:claim-inpatient'])->name('inpatient.store');

// Companies
Route::get('companies', 'CompaniesController@index')
    ->middleware(['ess-auth', 'admin:true'])->name('company.index');
Route::get('companies/sync', 'CompaniesController@sync')
    ->middleware(['ess-auth', 'admin:true'])->name('company.sync');
Route::get('companies/get/{uuid}', 'CompaniesController@getData')
    ->middleware(['ess-auth', 'admin:true'])->name('company.getData');
Route::get('companies/get-image/{uuid}/{type}', 'CompaniesController@getImage')
    ->middleware(['ess-auth', 'admin:true'])->name('company.getImage');
Route::patch('companies/update/{uuid}', 'CompaniesController@update')
    ->middleware(['ess-auth', 'admin:true'])->name('company.update');

// Contract
Route::get('contracts', 'ContractController@index')
    ->middleware(['ess-auth', 'admin', 'can:history-contract'])->name('contract.index');
Route::get('contract/{uuid}/view', 'ContractController@view')
    ->middleware(['ess-auth', 'admin', 'can:notif-contract'])->name('contract.view');
Route::get('contract/{uuid}/verification', 'ContractController@verification')
    ->middleware(['ess-auth', 'admin', 'can:notif-contract'])->name('contract.verification');
Route::post('contract/{uuid}/verification', 'ContractController@verificationPost')
    ->middleware(['ess-auth', 'admin', 'can:notif-contract'])->name('contract.verification.post');

// Logs
Route::get('credentials', 'ErrorController@credentials')
    ->middleware(['ess-auth', 'admin:true'])->name('credentials.index');
Route::get('credentials/{uuid}', 'ErrorController@credentialsDetails')
    ->middleware(['ess-auth', 'admin:true'])->name('credentials.detail');
Route::get('audit-trail', 'ErrorController@auditTrail')
    ->middleware(['ess-auth', 'admin:true'])->name('audit-trail.index');
Route::get('sms-callback', 'ErrorController@smsCallback')
    ->middleware(['ess-auth', 'admin:true'])->name('sms-callback.index');
Route::get('sipas-log', 'ErrorController@sipasLog')
    ->middleware(['ess-auth', 'admin:true'])->name('sipas-log.index');

// Configuration
Route::get('configuration', 'ConfigurationController@index')
    ->middleware(['ess-auth', 'admin:true'])->name('configuration.index');
Route::post('configuration', 'ConfigurationController@store')
    ->middleware(['ess-auth', 'admin:true'])->name('configuration.store');
Route::get('setting-credentials', 'ConfigurationController@settingCredentials')
    ->middleware(['ess-auth', 'admin:true'])->name('setting-credentials.index');
Route::post('setting-credentials', 'ConfigurationController@settingCredentialsStore')
    ->middleware(['ess-auth', 'admin:true'])->name('setting-credentials.store');
Route::delete('setting-credentials/{id}', 'ConfigurationController@settingCredentialsDelete')
    ->middleware(['ess-auth', 'admin:true'])->name('setting-credentials.delete');

// Peraturan Perusahaam
Route::get('peraturan-perusahaan', 'PeraturanPerusahaanController@index')
    ->middleware(['ess-auth', 'admin:true'])->name('peraturan-perusahaan.index');
Route::post('peraturan-perusahaan', 'PeraturanPerusahaanController@store')
    ->middleware(['ess-auth', 'admin:true'])->name('peraturan-perusahaan.store');
Route::get('peraturan-perusahaan/detail/{uuid}', 'PeraturanPerusahaanController@getData')
    ->middleware(['ess-auth', 'admin:true'])->name('peraturan-perusahaan.getData');
Route::patch('peraturan-perusahaan/detail/{uuid}', 'PeraturanPerusahaanController@update')
    ->middleware(['ess-auth', 'admin:true'])->name('peraturan-perusahaan.update');
Route::delete('peraturan-perusahaan/{uuid}', 'PeraturanPerusahaanController@destroy')
    ->middleware(['ess-auth', 'admin:true'])->name('peraturan-perusahaan.delete');

// Notification
Route::get('notification', 'HomeController@notification')
    ->middleware(['ess-auth', 'admin'])->name('notification');

// Report
Route::get('report', 'ReportController@index')
    ->middleware(['ess-auth', 'admin', 'can:report'])->name('report.index');
Route::post('report', 'ReportController@store')
    ->middleware(['ess-auth', 'admin', 'can:report'])->name('report.store');
