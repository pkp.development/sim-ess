<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@login')->middleware('guest')->name('login');
Route::post('/', 'AuthController@auth')->middleware('guest')->name('authentication');
Route::get('/logout', 'AuthController@logout')->middleware(['ess-auth'])->name('logout');
Route::get('/ubah-password', 'HomeController@changePassword')->middleware(['ess-auth'])->name('change-password');
Route::post('/ubah-password', 'HomeController@changePasswordPost')->middleware(['ess-auth'])->name('change-password.post');

Route::get('/lupa-password', 'AuthController@forgotPassword')->middleware('guest')->name('forgot-password');
Route::post('/lupa-password', 'AuthController@forgotPasswordPost')->middleware('guest')->name('forgot-password.post');

Route::get('/pemulihan-password', 'AuthController@recoveryPassword')->middleware('guest')->name('recovery-password');
Route::post('/pemulihan-password', 'AuthController@recoveryPasswordPost')->middleware('guest')->name('recovery-password-post');

Route::get('dashboard', 'HomeController@dashboard')->middleware(['ess-auth', 'employee'])->name('pe.dashboard');

Route::get('surat', 'HomeController@surat')->middleware(['ess-auth', 'employee'])->name('pe.surat');


Route::get('employee/get-tmp-image/{type}', 'EmployeeController@getImage')->middleware(['ess-auth', 'employee'])->name('pe.get-image');

Route::group(['middleware' => ['ess-auth', 'employee', 'can:pe-terminated']], function () {

    Route::get('informasi-karyawan', 'EmployeeController@index')->name('pe.info.karyawan');
    Route::patch('informasi-karyawan/profile', 'EmployeeController@updateProfile')->name('pe.update.profile');
    Route::get('informasi-karyawan/{type}/read/{uuid?}', 'EmployeeController@compareProfile')->name('pe.profile.read');

    Route::get('informasi-penempatan', 'AssignmentController@index')->name('pe.info.penempatan');
    Route::get('download-idcard', 'AssignmentController@downloadIdCard')->name('pe.info.download-idcard');
    Route::post('upload-idcard', 'AssignmentController@uploadIdCard')->name('pe.info.upload-idcard');
    Route::get('download-contract/{url?}', 'AssignmentController@downloadContract')->name('pe.info.download-contract');
    Route::get('download-skk', 'AssignmentController@downloadSKK')->name('pe.info.download-skk');
    Route::get('download-srk', 'AssignmentController@downloadSRK')->name('pe.info.download-srk');

    Route::get('informasi-keluarga', 'FamilyController@index')->name('pe.info.keluarga');
    Route::post('informasi-keluarga', 'FamilyController@updateData')->name('pe.info.keluarga');
    Route::get('informasi-keluarga/family/{uuid}', 'FamilyController@getFamily')->name('pe.info.keluarga.get-family');
    Route::post('informasi-keluarga/family', 'FamilyController@updateFamily')->name('pe.info.keluarga.post-family');
    Route::patch('informasi-keluarga/family/{uuid}', 'FamilyController@patchFamily')->name('pe.info.keluarga.patch-family');
    Route::delete('informasi-keluarga/family/{uuid}', 'FamilyController@deleteFamily')->name('pe.info.keluarga.delete-family');

    Route::get('informasi-rekening', 'BankController@index')->name('pe.info.rekening');
    Route::post('informasi-rekening', 'BankController@store')->name('pe.info.rekening.store');

    Route::get('informasi-benefit', 'BenefitController@index')->name('pe.info.benefit');
    Route::get('informasi-benefit/bpjs-kesehatan/download', 'BenefitController@bpjsKesehatanDownload')->name('pe.info.benefit.bpjs-kes.download');
    Route::get('informasi-benefit/bpjs-ketenagakerjaan/{notkj}/download', 'BenefitController@bpjsKetenagakerjaanDownload')->name('pe.info.benefit.bpjs-tkj.download');

    Route::get('rawat-jalan', 'RawatJalanController@index')->name('pe.rawat-jalan.index');
    Route::get('rawat-jalan/{uuid}', 'RawatJalanController@detail')->name('pe.rawat-jalan.detail');
    Route::post('rawat-jalan', 'RawatJalanController@store')->name('pe.rawat-jalan.store');

    Route::get('rawat-inap', 'InpatientController@index')->name('pe.inpatient.index');
    Route::get('rawat-inap/{uuid?}', 'InpatientController@detail')->name('pe.inpatient.detail');

    Route::get('peraturan-perusahaan', 'HomeController@peraturanPerusahaan')->name('pe.peraturan-perusahaan');
});

Route::get('contract-signature', 'DocumentController@index')->middleware(['ess-auth', 'employee', 'can:pe-terminated'])->name('pe.contract.signature-get');
Route::post('contract-signature', 'DocumentController@signContract')->middleware(['ess-auth', 'employee', 'can:pe-terminated'])->name('pe.contract.signature-post');

Route::get('read-all', 'EmployeeController@readAll')->middleware(['ess-auth', 'employee'])->name('pe.read.all-notification');
Route::get('informasi-slip-gaji', 'PaySlipController@index')->middleware(['ess-auth', 'employee'])->name('pe.info.pay-slip');
Route::get('download-payslip/{id}', 'PaySlipController@downloadPayslip')->middleware(['ess-auth', 'employee'])->name('pe.download.payslip');

Route::get('informasi-spt', 'SptController@index')->middleware(['ess-auth', 'employee'])->name('pe.info.spt');
Route::get('download-spt/{id}', 'SptController@downloadSpt')->middleware(['ess-auth', 'employee'])->name('pe.download.spt');
Route::get('download-spt-template', 'SptController@downloadSptTemplate')->middleware(['ess-auth', 'employee'])->name('pe.download.spt-template');

if (env('APP_ENV', 'local') == 'local') {
    Route::get('test', function(){
        $api = new \App\Repositories\BprApi\MasterProvince;
        dd($api->getMasterProvinceParams(['id' => 1]));
    });
}

Route::post('sms-callback', 'CallbackController@sms')->name('callback.sms');;
