<?php

/*
|--------------------------------------------------------------------------
| Service Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Client Org
Route::get('client-company', 'ServiceClientOrgController@getCompanies')->name('client-company');
Route::get('client-branch', 'ServiceClientOrgController@getBranch')->name('client-branch');

// Region
Route::get('provinces', 'ServiceRegionController@getProvince')->name('province');
Route::get('cities', 'ServiceRegionController@getCity')->name('city');

// Bank
Route::get('banks', 'ServiceBankController@getBank')->name('bank');

// SIM Org
Route::get('sim-company', 'ServiceOrgController@getCompanies')->name('sim-company');
Route::get('sim-branch', 'ServiceOrgController@getBranch')->name('sim-branch');
